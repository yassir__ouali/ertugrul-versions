﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericVirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericVirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct GenericInterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Entitas.CollectorException
struct CollectorException_tC99C67E8041C8145EFA76944442A2E52DD916E84;
// Entitas.ContextDoesNotContainEntityException
struct ContextDoesNotContainEntityException_t7080D5711853883454B78F851D81DFE846DE5B95;
// Entitas.ContextEntityChanged
struct ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1;
// Entitas.ContextEntityIndexDoesAlreadyExistException
struct ContextEntityIndexDoesAlreadyExistException_tCB4C94A55764B9852F030A20334B50BE8299E6BC;
// Entitas.ContextEntityIndexDoesNotExistException
struct ContextEntityIndexDoesNotExistException_t2191FE494E5293B1BA0242B7D67CCA9C05602672;
// Entitas.ContextGroupChanged
struct ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728;
// Entitas.ContextInfo
struct ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED;
// Entitas.ContextInfoException
struct ContextInfoException_t5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381;
// Entitas.ContextStillHasRetainedEntitiesException
struct ContextStillHasRetainedEntitiesException_tFDD6AAA582B14187B08C2B3A07787308DE518C17;
// Entitas.ContextStillHasRetainedEntitiesException/<>c
struct U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4;
// Entitas.EntitasException
struct EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33;
// Entitas.Entity
struct Entity_tB86FED06A87B5FEA836FF73B89D5168789557783;
// Entitas.EntityAlreadyHasComponentException
struct EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB;
// Entitas.EntityComponentChanged
struct EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A;
// Entitas.EntityComponentReplaced
struct EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57;
// Entitas.EntityDoesNotHaveComponentException
struct EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C;
// Entitas.EntityEvent
struct EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6;
// Entitas.EntityIndexException
struct EntityIndexException_t6834F599460536EA81AFA69CA928AA2022334EF4;
// Entitas.EntityIsAlreadyRetainedByOwnerException
struct EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783;
// Entitas.EntityIsNotDestroyedException
struct EntityIsNotDestroyedException_tE5F9909534769331523B7F21128EF075CE107364;
// Entitas.EntityIsNotEnabledException
struct EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11;
// Entitas.EntityIsNotRetainedByOwnerException
struct EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225;
// Entitas.IAERC
struct IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0;
// Entitas.ICleanupSystem
struct ICleanupSystem_tFB9ECEEEA8A40D25C420257F34FB7363362A076E;
// Entitas.ICleanupSystem[]
struct ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3;
// Entitas.IComponent
struct IComponent_tE1E43FF3BDF6A6960F83258783FE4CEADD8B85DC;
// Entitas.IComponent[]
struct IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B;
// Entitas.IContext
struct IContext_t620F28E017D438765172A4D4551A2D82B0448C50;
// Entitas.IEntity
struct IEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379;
// Entitas.IEntity[]
struct IEntityU5BU5D_t67E0A22209E555D9A801B4B2D36DFC5D75A409F9;
// Entitas.IExecuteSystem
struct IExecuteSystem_t5B9A85420E9C4FA897B1F7D27EEC5137B9AD461D;
// Entitas.IExecuteSystem[]
struct IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2;
// Entitas.IGroup
struct IGroup_t94F870F72969813321E23B41ECE258F1EBE226F2;
// Entitas.IInitializeSystem
struct IInitializeSystem_tE88D6DB439FB771759B22891FF5C34DFE84F92E9;
// Entitas.IInitializeSystem[]
struct IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882;
// Entitas.ISystem
struct ISystem_t644E5680CF31970978459D912831FDC4FB9592E2;
// Entitas.ITearDownSystem
struct ITearDownSystem_t86A3B6BAA8C08944EBB0BD477551943AE70D4688;
// Entitas.ITearDownSystem[]
struct ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F;
// Entitas.MatcherException
struct MatcherException_t753A013446CEF9B87BE52DDE80A5536442489653;
// Entitas.SafeAERC
struct SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30;
// Entitas.Systems
struct Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.HashSet`1/Slot<System.Object>[]
struct SlotU5BU5D_t434AC0EDDFC7F65D36C63DBE35C2B09A5EFF6B84;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3;
// System.Collections.Generic.IEnumerable`1<Entitas.IEntity>
struct IEnumerable_1_tEFDF9D11D2D6E10C93717BCFDD2CA0EEAA3DE25F;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2F75FCBEC68AFE08982DA43985F9D04056E2BE73;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_tAE7A8756D8CF0882DD348DC328FB36FEE0FB7DD0;
// System.Collections.Generic.List`1<Entitas.ICleanupSystem>
struct List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC;
// System.Collections.Generic.List`1<Entitas.IComponent>
struct List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE;
// System.Collections.Generic.List`1<Entitas.IExecuteSystem>
struct List_1_tE232269578E5E48549D25DD0C9823B612D968293;
// System.Collections.Generic.List`1<Entitas.IInitializeSystem>
struct List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C;
// System.Collections.Generic.List`1<Entitas.ITearDownSystem>
struct List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.Stack`1<Entitas.IComponent>
struct Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5;
// System.Collections.Generic.Stack`1<Entitas.IComponent>[]
struct Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Func`2<Entitas.IEntity,System.String>
struct Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.Object,System.String>
struct Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

extern RuntimeClass* ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11_il2cpp_TypeInfo_var;
extern RuntimeClass* EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF_il2cpp_TypeInfo_var;
extern RuntimeClass* HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3_il2cpp_TypeInfo_var;
extern RuntimeClass* IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0_il2cpp_TypeInfo_var;
extern RuntimeClass* ICleanupSystem_tFB9ECEEEA8A40D25C420257F34FB7363362A076E_il2cpp_TypeInfo_var;
extern RuntimeClass* IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B_il2cpp_TypeInfo_var;
extern RuntimeClass* IComponent_tE1E43FF3BDF6A6960F83258783FE4CEADD8B85DC_il2cpp_TypeInfo_var;
extern RuntimeClass* IContext_t620F28E017D438765172A4D4551A2D82B0448C50_il2cpp_TypeInfo_var;
extern RuntimeClass* IEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379_il2cpp_TypeInfo_var;
extern RuntimeClass* IExecuteSystem_t5B9A85420E9C4FA897B1F7D27EEC5137B9AD461D_il2cpp_TypeInfo_var;
extern RuntimeClass* IInitializeSystem_tE88D6DB439FB771759B22891FF5C34DFE84F92E9_il2cpp_TypeInfo_var;
extern RuntimeClass* IReactiveSystem_tB39B6CD38C789CC6001B15F7F2589E635B81241C_il2cpp_TypeInfo_var;
extern RuntimeClass* ITearDownSystem_t86A3B6BAA8C08944EBB0BD477551943AE70D4688_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tE232269578E5E48549D25DD0C9823B612D968293_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
extern RuntimeClass* SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30_il2cpp_TypeInfo_var;
extern RuntimeClass* Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5_il2cpp_TypeInfo_var;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
extern RuntimeClass* Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral015AA75E8DFA1E2F0E82A7FB2CAFA09A121B183F;
extern String_t* _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9;
extern String_t* _stringLiteral05CA1CA9FDDFFA322568053773207EFDDBBCA308;
extern String_t* _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D;
extern String_t* _stringLiteral0F4B4026945A92B9D60527EEBE5F70F186EB6BD8;
extern String_t* _stringLiteral1059E91D018E70A11B850239FDD5D56E56B7EDFA;
extern String_t* _stringLiteral10DCA3B1CB1BBE244825A82F943245CCD0DBD298;
extern String_t* _stringLiteral171503CAE2A9F0079A6FDBFE8E5FBD88806C33CA;
extern String_t* _stringLiteral200C16EBADD4D3D62FBE907CE5A69F089AD3CC62;
extern String_t* _stringLiteral204D98988F3FEA6ACD89D4D7B7A64988BDC544F9;
extern String_t* _stringLiteral215541285235990E03ADE4EB1C1F83199DBE566D;
extern String_t* _stringLiteral24A5A60DC72D713F359E119B485D26F18CA208AA;
extern String_t* _stringLiteral2654F5CCEAF52997DFCFCED105D22EF40DEF62CC;
extern String_t* _stringLiteral28ED3A797DA3C48C309A4EF792147F3C56CFEC40;
extern String_t* _stringLiteral2C8B7D70BF0882F9D0D9E2D8489B0C3B616E0E9A;
extern String_t* _stringLiteral3D2A14F50A1337B537062A89C8E70F3391F3FA85;
extern String_t* _stringLiteral44E0A393E108EFD549601652D0713A316EE64399;
extern String_t* _stringLiteral53E651D1B2D0C1DD91A2A708D7D0D2574ACDC519;
extern String_t* _stringLiteral60534582FC064596609725F08086D4D4C0B1EE94;
extern String_t* _stringLiteral73BB7503BA2D3D447953641B975197DBAD0B2EA8;
extern String_t* _stringLiteral791D879F08C51DDE8430935280105E22F3179D13;
extern String_t* _stringLiteral80ACF505DD6D69F9053713807E97F7897D7C6EFD;
extern String_t* _stringLiteral876322E252252218BA31C7C5A2685D0A3638BA7F;
extern String_t* _stringLiteral89EF6D88E54CD9F1E1A7D7F7AF51C6CDCBDA6755;
extern String_t* _stringLiteral971386486EB94529C3F863133E53C85C69E5E91B;
extern String_t* _stringLiteral98F4C5700762C3501ED70A29E6ECA7B964F15C8F;
extern String_t* _stringLiteralA18BB4E7033B240C56F8A9283F7FD28202E0C7E5;
extern String_t* _stringLiteralAB66D26BBBCB986D88182E749B51F0AB7AFEAA37;
extern String_t* _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC;
extern String_t* _stringLiteralBAFB61407AE607E32387AE40DB637CDD10DDBC24;
extern String_t* _stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8;
extern String_t* _stringLiteralC24D66C37AB36CD7593C8EE4978736439B099D18;
extern String_t* _stringLiteralCA2FF84A6A92D9849061664B0317CC18E1D3E5C1;
extern String_t* _stringLiteralCE291E2FB2AF53CF866E83465E4230F8FDFD0D12;
extern String_t* _stringLiteralCF236536D0E19572D619A88C16CC2605B22B1348;
extern String_t* _stringLiteralD10AC531A571FA3F469A9795BF8BD1BC2440497E;
extern String_t* _stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46;
extern String_t* _stringLiteralE3C7E409DA731FE17ED5D291402B93A5030C5875;
extern String_t* _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A;
extern String_t* _stringLiteralEDD998E00DFD4094167A7ED68E52AAB09DA5CE23;
extern String_t* _stringLiteralF26F7059D0DDD056FED7BBE4138911C0690AE6C7;
extern String_t* _stringLiteralF472D17A616AE4E9F937C0C1CF4AFC000038C596;
extern String_t* _stringLiteralF5928667734E3310B1C6A9AF93EC9A6C14D9F052;
extern String_t* _stringLiteralF8D7FCEE6F5BC8DFA3075A2BC78D27F3E63D099B;
extern String_t* _stringLiteralFBFD64C7AE07B85E8C75803841C6E669258128A8;
extern String_t* _stringLiteralFC02E199EAA2A6900AEC454AD22BAA13B6E8F8E6;
extern String_t* _stringLiteralFFDE6F5EFE67D4B479474DBA3C4E14667A62A474;
extern const RuntimeMethod* Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7_RuntimeMethod_var;
extern const RuntimeMethod* Entity_Destroy_m4DDC656E3B45AA164F7A0220BB66C4E66D6B6F04_RuntimeMethod_var;
extern const RuntimeMethod* Entity_GetComponent_m65E4F66188239D9C7CFC1B264DDF49669881189B_RuntimeMethod_var;
extern const RuntimeMethod* Entity_RemoveComponent_m15668325C50C7F44C6FA106733C71EB0A678AB4B_RuntimeMethod_var;
extern const RuntimeMethod* Entity_ReplaceComponent_mA8B52E87C7EB944E6CF811218F94B1D9CCBE9D6C_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Select_TisIEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379_TisString_t_m99E1CC3914417788EAA75FCC8E287A367BF57A0D_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Select_TisRuntimeObject_TisString_t_mF297F0ADF2AF7D55B365D2122F5A887784112832_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m0E11CA9B34D5352759D42FDEA69CC14E7C949427_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_mE84225DB2E17CE18158B8FA6B70FACCC34D63E9B_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Add_m1A3167674F8646F908B2AF4977144E4D8175D695_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Remove_mDEE995DC0C479BC045A7490A0F7921B9ACBB4790_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1__ctor_m8A209D312FD08A21AFB5551881E7A6946A07C0D5_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_get_Count_m4594EB8357F5E4068DEE99E351ADE10202D2AF10_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m0BBC37E3FDCD60F153B75D9E56F2260FEEB41165_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3E18D0DFE1373E92D4D23B8CF9F1F2CC67E19AB7_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m88E063CFB9F1ED228CAB4ECCFC08E5BF80CF234D_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mA730CED3CC3D80FCC82584EBE806ED022613B069_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_mE9E33EB2ED285DBCE781434C34464D5B6A5DF800_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m786D360E04623387E0CB0FCC17A815D1ED1CCB74_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m7BEC5A680E5ED7915147F1C2D158922831DA59D2_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3851B7F861C2057D9C3BC286F44772F35A8575B0_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m54489866F749EC6026BA519D0B0EBEBC4BC2CAD1_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m9587932254DEE91014C9C59AA0F5AE5E017BA7D0_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mB36C11BB84D2C63932AB8C91C902593368F62AAE_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_mEF29B452B49300F7653F3A5FC6340E1F01FA889B_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m05018813FF4A80F1342F8692ACAFAE1A03DC5512_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1043386622543BFD25CA715705DA1D97D4F074D9_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m357AD56D8549B5DBDEEE32F070A942A8FFDF5E9C_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m7DF025BDC3A448B58F52D4FE6329F8CE2D6CA04C_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m5EEBC49C9FBF9326533FFB51F0BF3424BF333B70_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m75CEF9AEFEB69CD96795F522D1AB9B2FFC163BBE_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m92C03E07978B485072E96FC891797B8ADBBD15DD_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mB7AD700D64EDBCB425370286E3EAB603539BC41C_RuntimeMethod_var;
extern const RuntimeMethod* SafeAERC_Release_m556C922E1281A319B1C2748B147D4BB2793A30BE_RuntimeMethod_var;
extern const RuntimeMethod* SafeAERC_Retain_m24D271B54B5548342860BF177D10B05DFAA9C468_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m252F7AF64D8E273A7A921A037342F8BA121F8C93_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m90E9E0ABAB946873DC74924DAC696403DA669B84_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1__ctor_mD6EE87D90C123FC7ED2D0BE5B6CADE1704BE3A09_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m89BAF285F607EF62CC860B779D8F40495BE24D8A_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec_U3C_ctorU3Eb__0_0_m033F2367172299A2B12EB3C390AB85C948623E4D_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec_U3C_ctorU3Eb__0_1_mEFED69862F44B43EC8B1F68715A25CF1EE04DC6E_RuntimeMethod_var;
extern const uint32_t ContextDoesNotContainEntityException__ctor_m1180F70AA578AF59952BF63E5FF7140BC030A64C_MetadataUsageId;
extern const uint32_t ContextEntityIndexDoesAlreadyExistException__ctor_m9CC32949340F4D7F68A1398E98C959D20B89E2E6_MetadataUsageId;
extern const uint32_t ContextEntityIndexDoesNotExistException__ctor_mA058884DA2C4915AFEE6751A9C2883E2EB092451_MetadataUsageId;
extern const uint32_t ContextInfoException__ctor_m1B9C16ECF443D2A2FA4A1209EC24BEF4B5A01CCD_MetadataUsageId;
extern const uint32_t ContextStillHasRetainedEntitiesException__ctor_m60B798F5D9F0358BB815AF0235A5614BD0FD1CBF_MetadataUsageId;
extern const uint32_t EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26_MetadataUsageId;
extern const uint32_t EntityAlreadyHasComponentException__ctor_m19F540EA19E143D39F2366773AECB8E831D1182E_MetadataUsageId;
extern const uint32_t EntityComponentChanged_BeginInvoke_mD3E2C3818F65EC85DAA713E73DBB93E190FEBCCC_MetadataUsageId;
extern const uint32_t EntityComponentReplaced_BeginInvoke_m9399F0F64C4C28659BA1C766DEEAF62246E62962_MetadataUsageId;
extern const uint32_t EntityDoesNotHaveComponentException__ctor_m15C877635E6695C5DF57F1A45D28A91CE24EDB65_MetadataUsageId;
extern const uint32_t EntityIsAlreadyRetainedByOwnerException__ctor_mFBDE6BDC3650783F8C26EA2EB82F8579C7278784_MetadataUsageId;
extern const uint32_t EntityIsNotDestroyedException__ctor_mE4794016DBA07D0BDF895C822BA4FC763039451E_MetadataUsageId;
extern const uint32_t EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313_MetadataUsageId;
extern const uint32_t EntityIsNotRetainedByOwnerException__ctor_mB6FFC485F1A74C09C39BC2A4CBAD2E065E622E8B_MetadataUsageId;
extern const uint32_t Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7_MetadataUsageId;
extern const uint32_t Entity_CreateComponent_mBD244433F9F7931E685D923FEEB0904E07F90059_MetadataUsageId;
extern const uint32_t Entity_Destroy_m4DDC656E3B45AA164F7A0220BB66C4E66D6B6F04_MetadataUsageId;
extern const uint32_t Entity_GetComponentPool_m09EFD65E02F8FAC721807E1640C7B5AE10AA8957_MetadataUsageId;
extern const uint32_t Entity_GetComponent_m65E4F66188239D9C7CFC1B264DDF49669881189B_MetadataUsageId;
extern const uint32_t Entity_GetComponents_m4B0440F78D4D6148DF032841238C06BEA38E6532_MetadataUsageId;
extern const uint32_t Entity_Initialize_mE01A79AC651F7DA6314799654310BBF01680F75E_MetadataUsageId;
extern const uint32_t Entity_Release_m081A6C423132BCE072791579FE78FE9548AA9ACC_MetadataUsageId;
extern const uint32_t Entity_RemoveComponent_m15668325C50C7F44C6FA106733C71EB0A678AB4B_MetadataUsageId;
extern const uint32_t Entity_ReplaceComponent_mA8B52E87C7EB944E6CF811218F94B1D9CCBE9D6C_MetadataUsageId;
extern const uint32_t Entity_Retain_m0DA441090A622236AC87E40ABEE0EAEFF05EA84A_MetadataUsageId;
extern const uint32_t Entity_ToString_mA67A8FED351F17559E633EE71BEA2474E5B81077_MetadataUsageId;
extern const uint32_t Entity__ctor_mE4651D95541B7C3E38134B265724325762D842F7_MetadataUsageId;
extern const uint32_t Entity_add_OnComponentAdded_m1BDF039DEDB9F7DC262F6FA071FC98BCB77C5B15_MetadataUsageId;
extern const uint32_t Entity_add_OnComponentRemoved_mEB6B350C7BABEABAAD960B6DCC1B333355416968_MetadataUsageId;
extern const uint32_t Entity_add_OnComponentReplaced_m7643566509BC9F1B3B499C1B887C01BF16D703EC_MetadataUsageId;
extern const uint32_t Entity_add_OnDestroyEntity_m78113890D1F8393C4729151C57655BDCB5665E21_MetadataUsageId;
extern const uint32_t Entity_add_OnEntityReleased_m3B8C04DB39AFF7555FE2332869C2E1F129E66CA9_MetadataUsageId;
extern const uint32_t Entity_createDefaultContextInfo_m2048E85A3BDC948AAAA3452EEF56475215572790_MetadataUsageId;
extern const uint32_t Entity_get_retainCount_m6BCD89D9543984D57AE4B1159580AA529790D8F8_MetadataUsageId;
extern const uint32_t Entity_remove_OnComponentAdded_m50B58A37A8D0E4E525FC1DDC362F35A11CE2A1CE_MetadataUsageId;
extern const uint32_t Entity_remove_OnComponentRemoved_m338E77474CED2C1645AE6E62D08473B3A8F0CE66_MetadataUsageId;
extern const uint32_t Entity_remove_OnComponentReplaced_m64A8B6F732CF0D2BDCF2727EFDDEFACBC961F7C5_MetadataUsageId;
extern const uint32_t Entity_remove_OnDestroyEntity_mEFC765AEF6EA2FD6631028EFB307BB58DD0A72AC_MetadataUsageId;
extern const uint32_t Entity_remove_OnEntityReleased_m81698CA73D8419E6D4D35166599BBF36A0F88471_MetadataUsageId;
extern const uint32_t Entity_replaceComponent_mC4CA5CCC0D1EDD836ED494D8DEEE470DB4B1A1C7_MetadataUsageId;
extern const uint32_t MatcherException__ctor_m49E9717E224C80519E5AD189E7D945F55CFFD83B_MetadataUsageId;
extern const uint32_t SafeAERC_Release_m556C922E1281A319B1C2748B147D4BB2793A30BE_MetadataUsageId;
extern const uint32_t SafeAERC_Retain_m24D271B54B5548342860BF177D10B05DFAA9C468_MetadataUsageId;
extern const uint32_t SafeAERC__ctor_mC8216DC64624BDCEB24698DF7ADC85F2A26FDCAB_MetadataUsageId;
extern const uint32_t SafeAERC_get_retainCount_m7E100D325FA89EBB03BB0F877BEC6C8122CCC152_MetadataUsageId;
extern const uint32_t Systems_Add_m993517FBD7AB1189A89B5F7A69C9C8FC7339E82C_MetadataUsageId;
extern const uint32_t Systems_Cleanup_m18D647EE9ED3008C297FC58C068B278D45050B90_MetadataUsageId;
extern const uint32_t Systems_DeactivateReactiveSystems_m2D9A6B73CA9A9BE0DC3D5CC10ADA3F2EDDFDE060_MetadataUsageId;
extern const uint32_t Systems_Execute_m3388F82671E5DCF5980371AD7302596FE83A3FA6_MetadataUsageId;
extern const uint32_t Systems_Initialize_m7697AE097B5DBEE3A8F525C2E96612584C3B937B_MetadataUsageId;
extern const uint32_t Systems_TearDown_mFFB66A28D21CA8C6B0C721F06CC76806AB77EEB7_MetadataUsageId;
extern const uint32_t Systems__ctor_m1D75E40819D0A23654A05858BEF0C978128DF947_MetadataUsageId;
extern const uint32_t U3CU3Ec_U3C_ctorU3Eb__0_0_m033F2367172299A2B12EB3C390AB85C948623E4D_MetadataUsageId;
extern const uint32_t U3CU3Ec__cctor_mC9FA5A53082CB6983B0F4EB3FFE42C08122EF9E1_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B;
struct IEntityU5BU5D_t67E0A22209E555D9A801B4B2D36DFC5D75A409F9;
struct Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;


#ifndef U3CMODULEU3E_T742F26D51A4DF97E0C52DE993659E98281A71377_H
#define U3CMODULEU3E_T742F26D51A4DF97E0C52DE993659E98281A71377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t742F26D51A4DF97E0C52DE993659E98281A71377 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T742F26D51A4DF97E0C52DE993659E98281A71377_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COLLECTORCONTEXTEXTENSION_TAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4_H
#define COLLECTORCONTEXTEXTENSION_TAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CollectorContextExtension
struct  CollectorContextExtension_tAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTORCONTEXTEXTENSION_TAE2C92F0329285A4FA7360BB31C30E6FF8BB32F4_H
#ifndef CONTEXTINFO_TC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_H
#define CONTEXTINFO_TC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextInfo
struct  ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED  : public RuntimeObject
{
public:
	// System.String Entitas.ContextInfo::name
	String_t* ___name_0;
	// System.String[] Entitas.ContextInfo::componentNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___componentNames_1;
	// System.Type[] Entitas.ContextInfo::componentTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___componentTypes_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_componentNames_1() { return static_cast<int32_t>(offsetof(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED, ___componentNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_componentNames_1() const { return ___componentNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_componentNames_1() { return &___componentNames_1; }
	inline void set_componentNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___componentNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___componentNames_1), value);
	}

	inline static int32_t get_offset_of_componentTypes_2() { return static_cast<int32_t>(offsetof(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED, ___componentTypes_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_componentTypes_2() const { return ___componentTypes_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_componentTypes_2() { return &___componentTypes_2; }
	inline void set_componentTypes_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___componentTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___componentTypes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTINFO_TC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_H
#ifndef U3CU3EC_TBB75825E443987A79032BCE92971925042E081B4_H
#define U3CU3EC_TBB75825E443987A79032BCE92971925042E081B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextStillHasRetainedEntitiesException_<>c
struct  U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields
{
public:
	// Entitas.ContextStillHasRetainedEntitiesException_<>c Entitas.ContextStillHasRetainedEntitiesException_<>c::<>9
	U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.String> Entitas.ContextStillHasRetainedEntitiesException_<>c::<>9__0_1
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___U3CU3E9__0_1_1;
	// System.Func`2<Entitas.IEntity,System.String> Entitas.ContextStillHasRetainedEntitiesException_<>c::<>9__0_0
	Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * ___U3CU3E9__0_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields, ___U3CU3E9__0_1_1)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_U3CU3E9__0_1_1() const { return ___U3CU3E9__0_1_1; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_U3CU3E9__0_1_1() { return &___U3CU3E9__0_1_1; }
	inline void set_U3CU3E9__0_1_1(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___U3CU3E9__0_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields, ___U3CU3E9__0_0_2)); }
	inline Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * get_U3CU3E9__0_0_2() const { return ___U3CU3E9__0_0_2; }
	inline Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE ** get_address_of_U3CU3E9__0_0_2() { return &___U3CU3E9__0_0_2; }
	inline void set_U3CU3E9__0_0_2(Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * value)
	{
		___U3CU3E9__0_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TBB75825E443987A79032BCE92971925042E081B4_H
#ifndef ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#define ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Entity
struct  Entity_tB86FED06A87B5FEA836FF73B89D5168789557783  : public RuntimeObject
{
public:
	// Entitas.EntityComponentChanged Entitas.Entity::OnComponentAdded
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___OnComponentAdded_0;
	// Entitas.EntityComponentChanged Entitas.Entity::OnComponentRemoved
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___OnComponentRemoved_1;
	// Entitas.EntityComponentReplaced Entitas.Entity::OnComponentReplaced
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * ___OnComponentReplaced_2;
	// Entitas.EntityEvent Entitas.Entity::OnEntityReleased
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___OnEntityReleased_3;
	// Entitas.EntityEvent Entitas.Entity::OnDestroyEntity
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___OnDestroyEntity_4;
	// System.Collections.Generic.List`1<Entitas.IComponent> Entitas.Entity::_componentBuffer
	List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * ____componentBuffer_5;
	// System.Collections.Generic.List`1<System.Int32> Entitas.Entity::_indexBuffer
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____indexBuffer_6;
	// System.Int32 Entitas.Entity::_creationIndex
	int32_t ____creationIndex_7;
	// System.Boolean Entitas.Entity::_isEnabled
	bool ____isEnabled_8;
	// System.Int32 Entitas.Entity::_totalComponents
	int32_t ____totalComponents_9;
	// Entitas.IComponent[] Entitas.Entity::_components
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____components_10;
	// System.Collections.Generic.Stack`1<Entitas.IComponent>[] Entitas.Entity::_componentPools
	Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* ____componentPools_11;
	// Entitas.ContextInfo Entitas.Entity::_contextInfo
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * ____contextInfo_12;
	// Entitas.IAERC Entitas.Entity::_aerc
	RuntimeObject* ____aerc_13;
	// Entitas.IComponent[] Entitas.Entity::_componentsCache
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____componentsCache_14;
	// System.Int32[] Entitas.Entity::_componentIndicesCache
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____componentIndicesCache_15;
	// System.String Entitas.Entity::_toStringCache
	String_t* ____toStringCache_16;
	// System.Text.StringBuilder Entitas.Entity::_toStringBuilder
	StringBuilder_t * ____toStringBuilder_17;

public:
	inline static int32_t get_offset_of_OnComponentAdded_0() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentAdded_0)); }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * get_OnComponentAdded_0() const { return ___OnComponentAdded_0; }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** get_address_of_OnComponentAdded_0() { return &___OnComponentAdded_0; }
	inline void set_OnComponentAdded_0(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * value)
	{
		___OnComponentAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentAdded_0), value);
	}

	inline static int32_t get_offset_of_OnComponentRemoved_1() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentRemoved_1)); }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * get_OnComponentRemoved_1() const { return ___OnComponentRemoved_1; }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** get_address_of_OnComponentRemoved_1() { return &___OnComponentRemoved_1; }
	inline void set_OnComponentRemoved_1(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * value)
	{
		___OnComponentRemoved_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentRemoved_1), value);
	}

	inline static int32_t get_offset_of_OnComponentReplaced_2() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentReplaced_2)); }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * get_OnComponentReplaced_2() const { return ___OnComponentReplaced_2; }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 ** get_address_of_OnComponentReplaced_2() { return &___OnComponentReplaced_2; }
	inline void set_OnComponentReplaced_2(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * value)
	{
		___OnComponentReplaced_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentReplaced_2), value);
	}

	inline static int32_t get_offset_of_OnEntityReleased_3() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnEntityReleased_3)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get_OnEntityReleased_3() const { return ___OnEntityReleased_3; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of_OnEntityReleased_3() { return &___OnEntityReleased_3; }
	inline void set_OnEntityReleased_3(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		___OnEntityReleased_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnEntityReleased_3), value);
	}

	inline static int32_t get_offset_of_OnDestroyEntity_4() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnDestroyEntity_4)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get_OnDestroyEntity_4() const { return ___OnDestroyEntity_4; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of_OnDestroyEntity_4() { return &___OnDestroyEntity_4; }
	inline void set_OnDestroyEntity_4(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		___OnDestroyEntity_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEntity_4), value);
	}

	inline static int32_t get_offset_of__componentBuffer_5() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentBuffer_5)); }
	inline List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * get__componentBuffer_5() const { return ____componentBuffer_5; }
	inline List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE ** get_address_of__componentBuffer_5() { return &____componentBuffer_5; }
	inline void set__componentBuffer_5(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * value)
	{
		____componentBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____componentBuffer_5), value);
	}

	inline static int32_t get_offset_of__indexBuffer_6() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____indexBuffer_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__indexBuffer_6() const { return ____indexBuffer_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__indexBuffer_6() { return &____indexBuffer_6; }
	inline void set__indexBuffer_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____indexBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&____indexBuffer_6), value);
	}

	inline static int32_t get_offset_of__creationIndex_7() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____creationIndex_7)); }
	inline int32_t get__creationIndex_7() const { return ____creationIndex_7; }
	inline int32_t* get_address_of__creationIndex_7() { return &____creationIndex_7; }
	inline void set__creationIndex_7(int32_t value)
	{
		____creationIndex_7 = value;
	}

	inline static int32_t get_offset_of__isEnabled_8() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____isEnabled_8)); }
	inline bool get__isEnabled_8() const { return ____isEnabled_8; }
	inline bool* get_address_of__isEnabled_8() { return &____isEnabled_8; }
	inline void set__isEnabled_8(bool value)
	{
		____isEnabled_8 = value;
	}

	inline static int32_t get_offset_of__totalComponents_9() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____totalComponents_9)); }
	inline int32_t get__totalComponents_9() const { return ____totalComponents_9; }
	inline int32_t* get_address_of__totalComponents_9() { return &____totalComponents_9; }
	inline void set__totalComponents_9(int32_t value)
	{
		____totalComponents_9 = value;
	}

	inline static int32_t get_offset_of__components_10() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____components_10)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__components_10() const { return ____components_10; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__components_10() { return &____components_10; }
	inline void set__components_10(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____components_10 = value;
		Il2CppCodeGenWriteBarrier((&____components_10), value);
	}

	inline static int32_t get_offset_of__componentPools_11() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentPools_11)); }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* get__componentPools_11() const { return ____componentPools_11; }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272** get_address_of__componentPools_11() { return &____componentPools_11; }
	inline void set__componentPools_11(Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* value)
	{
		____componentPools_11 = value;
		Il2CppCodeGenWriteBarrier((&____componentPools_11), value);
	}

	inline static int32_t get_offset_of__contextInfo_12() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____contextInfo_12)); }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * get__contextInfo_12() const { return ____contextInfo_12; }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED ** get_address_of__contextInfo_12() { return &____contextInfo_12; }
	inline void set__contextInfo_12(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * value)
	{
		____contextInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&____contextInfo_12), value);
	}

	inline static int32_t get_offset_of__aerc_13() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____aerc_13)); }
	inline RuntimeObject* get__aerc_13() const { return ____aerc_13; }
	inline RuntimeObject** get_address_of__aerc_13() { return &____aerc_13; }
	inline void set__aerc_13(RuntimeObject* value)
	{
		____aerc_13 = value;
		Il2CppCodeGenWriteBarrier((&____aerc_13), value);
	}

	inline static int32_t get_offset_of__componentsCache_14() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentsCache_14)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__componentsCache_14() const { return ____componentsCache_14; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__componentsCache_14() { return &____componentsCache_14; }
	inline void set__componentsCache_14(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____componentsCache_14 = value;
		Il2CppCodeGenWriteBarrier((&____componentsCache_14), value);
	}

	inline static int32_t get_offset_of__componentIndicesCache_15() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentIndicesCache_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__componentIndicesCache_15() const { return ____componentIndicesCache_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__componentIndicesCache_15() { return &____componentIndicesCache_15; }
	inline void set__componentIndicesCache_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____componentIndicesCache_15 = value;
		Il2CppCodeGenWriteBarrier((&____componentIndicesCache_15), value);
	}

	inline static int32_t get_offset_of__toStringCache_16() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____toStringCache_16)); }
	inline String_t* get__toStringCache_16() const { return ____toStringCache_16; }
	inline String_t** get_address_of__toStringCache_16() { return &____toStringCache_16; }
	inline void set__toStringCache_16(String_t* value)
	{
		____toStringCache_16 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_16), value);
	}

	inline static int32_t get_offset_of__toStringBuilder_17() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____toStringBuilder_17)); }
	inline StringBuilder_t * get__toStringBuilder_17() const { return ____toStringBuilder_17; }
	inline StringBuilder_t ** get_address_of__toStringBuilder_17() { return &____toStringBuilder_17; }
	inline void set__toStringBuilder_17(StringBuilder_t * value)
	{
		____toStringBuilder_17 = value;
		Il2CppCodeGenWriteBarrier((&____toStringBuilder_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#ifndef SAFEAERC_TC3A4274F22895D6896C084185BA5E145EF097A30_H
#define SAFEAERC_TC3A4274F22895D6896C084185BA5E145EF097A30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.SafeAERC
struct  SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30  : public RuntimeObject
{
public:
	// Entitas.IEntity Entitas.SafeAERC::_entity
	RuntimeObject* ____entity_0;
	// System.Collections.Generic.HashSet`1<System.Object> Entitas.SafeAERC::_owners
	HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * ____owners_1;

public:
	inline static int32_t get_offset_of__entity_0() { return static_cast<int32_t>(offsetof(SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30, ____entity_0)); }
	inline RuntimeObject* get__entity_0() const { return ____entity_0; }
	inline RuntimeObject** get_address_of__entity_0() { return &____entity_0; }
	inline void set__entity_0(RuntimeObject* value)
	{
		____entity_0 = value;
		Il2CppCodeGenWriteBarrier((&____entity_0), value);
	}

	inline static int32_t get_offset_of__owners_1() { return static_cast<int32_t>(offsetof(SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30, ____owners_1)); }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * get__owners_1() const { return ____owners_1; }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 ** get_address_of__owners_1() { return &____owners_1; }
	inline void set__owners_1(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * value)
	{
		____owners_1 = value;
		Il2CppCodeGenWriteBarrier((&____owners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEAERC_TC3A4274F22895D6896C084185BA5E145EF097A30_H
#ifndef SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#define SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Systems
struct  Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Entitas.IInitializeSystem> Entitas.Systems::_initializeSystems
	List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * ____initializeSystems_0;
	// System.Collections.Generic.List`1<Entitas.IExecuteSystem> Entitas.Systems::_executeSystems
	List_1_tE232269578E5E48549D25DD0C9823B612D968293 * ____executeSystems_1;
	// System.Collections.Generic.List`1<Entitas.ICleanupSystem> Entitas.Systems::_cleanupSystems
	List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * ____cleanupSystems_2;
	// System.Collections.Generic.List`1<Entitas.ITearDownSystem> Entitas.Systems::_tearDownSystems
	List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * ____tearDownSystems_3;

public:
	inline static int32_t get_offset_of__initializeSystems_0() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____initializeSystems_0)); }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * get__initializeSystems_0() const { return ____initializeSystems_0; }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C ** get_address_of__initializeSystems_0() { return &____initializeSystems_0; }
	inline void set__initializeSystems_0(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * value)
	{
		____initializeSystems_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializeSystems_0), value);
	}

	inline static int32_t get_offset_of__executeSystems_1() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____executeSystems_1)); }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 * get__executeSystems_1() const { return ____executeSystems_1; }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 ** get_address_of__executeSystems_1() { return &____executeSystems_1; }
	inline void set__executeSystems_1(List_1_tE232269578E5E48549D25DD0C9823B612D968293 * value)
	{
		____executeSystems_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeSystems_1), value);
	}

	inline static int32_t get_offset_of__cleanupSystems_2() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____cleanupSystems_2)); }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * get__cleanupSystems_2() const { return ____cleanupSystems_2; }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC ** get_address_of__cleanupSystems_2() { return &____cleanupSystems_2; }
	inline void set__cleanupSystems_2(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * value)
	{
		____cleanupSystems_2 = value;
		Il2CppCodeGenWriteBarrier((&____cleanupSystems_2), value);
	}

	inline static int32_t get_offset_of__tearDownSystems_3() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____tearDownSystems_3)); }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * get__tearDownSystems_3() const { return ____tearDownSystems_3; }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 ** get_address_of__tearDownSystems_3() { return &____tearDownSystems_3; }
	inline void set__tearDownSystems_3(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * value)
	{
		____tearDownSystems_3 = value;
		Il2CppCodeGenWriteBarrier((&____tearDownSystems_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifndef TRIGGERONEVENTMATCHEREXTENSION_TEF975E8E9F0D4FEE53123912F9A8837705E5FF25_H
#define TRIGGERONEVENTMATCHEREXTENSION_TEF975E8E9F0D4FEE53123912F9A8837705E5FF25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.TriggerOnEventMatcherExtension
struct  TriggerOnEventMatcherExtension_tEF975E8E9F0D4FEE53123912F9A8837705E5FF25  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERONEVENTMATCHEREXTENSION_TEF975E8E9F0D4FEE53123912F9A8837705E5FF25_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef HASHSET_1_T297CD7F944846107B388993164FCD9E317A338A3_H
#define HASHSET_1_T297CD7F944846107B388993164FCD9E317A338A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1<System.Object>
struct  HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____buckets_7;
	// System.Collections.Generic.HashSet`1_Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_t434AC0EDDFC7F65D36C63DBE35C2B09A5EFF6B84* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ____siInfo_14;

public:
	inline static int32_t get_offset_of__buckets_7() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____buckets_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__buckets_7() const { return ____buckets_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__buckets_7() { return &____buckets_7; }
	inline void set__buckets_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____buckets_7 = value;
		Il2CppCodeGenWriteBarrier((&____buckets_7), value);
	}

	inline static int32_t get_offset_of__slots_8() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____slots_8)); }
	inline SlotU5BU5D_t434AC0EDDFC7F65D36C63DBE35C2B09A5EFF6B84* get__slots_8() const { return ____slots_8; }
	inline SlotU5BU5D_t434AC0EDDFC7F65D36C63DBE35C2B09A5EFF6B84** get_address_of__slots_8() { return &____slots_8; }
	inline void set__slots_8(SlotU5BU5D_t434AC0EDDFC7F65D36C63DBE35C2B09A5EFF6B84* value)
	{
		____slots_8 = value;
		Il2CppCodeGenWriteBarrier((&____slots_8), value);
	}

	inline static int32_t get_offset_of__count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____count_9)); }
	inline int32_t get__count_9() const { return ____count_9; }
	inline int32_t* get_address_of__count_9() { return &____count_9; }
	inline void set__count_9(int32_t value)
	{
		____count_9 = value;
	}

	inline static int32_t get_offset_of__lastIndex_10() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____lastIndex_10)); }
	inline int32_t get__lastIndex_10() const { return ____lastIndex_10; }
	inline int32_t* get_address_of__lastIndex_10() { return &____lastIndex_10; }
	inline void set__lastIndex_10(int32_t value)
	{
		____lastIndex_10 = value;
	}

	inline static int32_t get_offset_of__freeList_11() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____freeList_11)); }
	inline int32_t get__freeList_11() const { return ____freeList_11; }
	inline int32_t* get_address_of__freeList_11() { return &____freeList_11; }
	inline void set__freeList_11(int32_t value)
	{
		____freeList_11 = value;
	}

	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____comparer_12)); }
	inline RuntimeObject* get__comparer_12() const { return ____comparer_12; }
	inline RuntimeObject** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(RuntimeObject* value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_12), value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}

	inline static int32_t get_offset_of__siInfo_14() { return static_cast<int32_t>(offsetof(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3, ____siInfo_14)); }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * get__siInfo_14() const { return ____siInfo_14; }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 ** get_address_of__siInfo_14() { return &____siInfo_14; }
	inline void set__siInfo_14(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * value)
	{
		____siInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&____siInfo_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_1_T297CD7F944846107B388993164FCD9E317A338A3_H
#ifndef LIST_1_T3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC_H
#define LIST_1_T3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Entitas.ICleanupSystem>
struct  List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC, ____items_1)); }
	inline ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3* get__items_1() const { return ____items_1; }
	inline ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC_StaticFields, ____emptyArray_5)); }
	inline ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ICleanupSystemU5BU5D_t12139326A545D50B4540E1F2E817A11C413CE3A3* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC_H
#ifndef LIST_1_TD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE_H
#define LIST_1_TD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Entitas.IComponent>
struct  List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE, ____items_1)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__items_1() const { return ____items_1; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE_StaticFields, ____emptyArray_5)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__emptyArray_5() const { return ____emptyArray_5; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE_H
#ifndef LIST_1_TE232269578E5E48549D25DD0C9823B612D968293_H
#define LIST_1_TE232269578E5E48549D25DD0C9823B612D968293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Entitas.IExecuteSystem>
struct  List_1_tE232269578E5E48549D25DD0C9823B612D968293  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE232269578E5E48549D25DD0C9823B612D968293, ____items_1)); }
	inline IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2* get__items_1() const { return ____items_1; }
	inline IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE232269578E5E48549D25DD0C9823B612D968293, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE232269578E5E48549D25DD0C9823B612D968293, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE232269578E5E48549D25DD0C9823B612D968293, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE232269578E5E48549D25DD0C9823B612D968293_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE232269578E5E48549D25DD0C9823B612D968293_StaticFields, ____emptyArray_5)); }
	inline IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(IExecuteSystemU5BU5D_tEF45EDCAEF770E4C1C0028B090F00188F7152DB2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE232269578E5E48549D25DD0C9823B612D968293_H
#ifndef LIST_1_T1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C_H
#define LIST_1_T1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Entitas.IInitializeSystem>
struct  List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C, ____items_1)); }
	inline IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882* get__items_1() const { return ____items_1; }
	inline IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C_StaticFields, ____emptyArray_5)); }
	inline IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882* get__emptyArray_5() const { return ____emptyArray_5; }
	inline IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(IInitializeSystemU5BU5D_tDBAFD530779916D65FE7ACDDA4C2BF4CA1E6F882* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C_H
#ifndef LIST_1_T78F50ABA4688AD6A1255F66AB89EE8BAFD069913_H
#define LIST_1_T78F50ABA4688AD6A1255F66AB89EE8BAFD069913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Entitas.ITearDownSystem>
struct  List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913, ____items_1)); }
	inline ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F* get__items_1() const { return ____items_1; }
	inline ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913_StaticFields, ____emptyArray_5)); }
	inline ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ITearDownSystemU5BU5D_t345F094DC0B34E76CE9025066EB3006BD898CC2F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T78F50ABA4688AD6A1255F66AB89EE8BAFD069913_H
#ifndef LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#define LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____items_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TE1526161A558A17A39A8B69D8EEF3801393B6226_H
#ifndef STACK_1_T0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5_H
#define STACK_1_T0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<Entitas.IComponent>
struct  Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5, ____array_0)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__array_0() const { return ____array_0; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkChars_0), value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChunkPrevious_1), value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ENTITASEXCEPTION_T71786D33342FC2A2F61B3AAC7FD34318ABC7CD33_H
#define ENTITASEXCEPTION_T71786D33342FC2A2F61B3AAC7FD34318ABC7CD33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntitasException
struct  EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITASEXCEPTION_T71786D33342FC2A2F61B3AAC7FD34318ABC7CD33_H
#ifndef MATCHEREXCEPTION_T753A013446CEF9B87BE52DDE80A5536442489653_H
#define MATCHEREXCEPTION_T753A013446CEF9B87BE52DDE80A5536442489653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.MatcherException
struct  MatcherException_t753A013446CEF9B87BE52DDE80A5536442489653  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHEREXCEPTION_T753A013446CEF9B87BE52DDE80A5536442489653_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLLECTOREXCEPTION_TC99C67E8041C8145EFA76944442A2E52DD916E84_H
#define COLLECTOREXCEPTION_TC99C67E8041C8145EFA76944442A2E52DD916E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CollectorException
struct  CollectorException_tC99C67E8041C8145EFA76944442A2E52DD916E84  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTOREXCEPTION_TC99C67E8041C8145EFA76944442A2E52DD916E84_H
#ifndef CONTEXTDOESNOTCONTAINENTITYEXCEPTION_T7080D5711853883454B78F851D81DFE846DE5B95_H
#define CONTEXTDOESNOTCONTAINENTITYEXCEPTION_T7080D5711853883454B78F851D81DFE846DE5B95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextDoesNotContainEntityException
struct  ContextDoesNotContainEntityException_t7080D5711853883454B78F851D81DFE846DE5B95  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTDOESNOTCONTAINENTITYEXCEPTION_T7080D5711853883454B78F851D81DFE846DE5B95_H
#ifndef CONTEXTENTITYINDEXDOESALREADYEXISTEXCEPTION_TCB4C94A55764B9852F030A20334B50BE8299E6BC_H
#define CONTEXTENTITYINDEXDOESALREADYEXISTEXCEPTION_TCB4C94A55764B9852F030A20334B50BE8299E6BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextEntityIndexDoesAlreadyExistException
struct  ContextEntityIndexDoesAlreadyExistException_tCB4C94A55764B9852F030A20334B50BE8299E6BC  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTENTITYINDEXDOESALREADYEXISTEXCEPTION_TCB4C94A55764B9852F030A20334B50BE8299E6BC_H
#ifndef CONTEXTENTITYINDEXDOESNOTEXISTEXCEPTION_T2191FE494E5293B1BA0242B7D67CCA9C05602672_H
#define CONTEXTENTITYINDEXDOESNOTEXISTEXCEPTION_T2191FE494E5293B1BA0242B7D67CCA9C05602672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextEntityIndexDoesNotExistException
struct  ContextEntityIndexDoesNotExistException_t2191FE494E5293B1BA0242B7D67CCA9C05602672  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTENTITYINDEXDOESNOTEXISTEXCEPTION_T2191FE494E5293B1BA0242B7D67CCA9C05602672_H
#ifndef CONTEXTINFOEXCEPTION_T5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381_H
#define CONTEXTINFOEXCEPTION_T5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextInfoException
struct  ContextInfoException_t5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTINFOEXCEPTION_T5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381_H
#ifndef CONTEXTSTILLHASRETAINEDENTITIESEXCEPTION_TFDD6AAA582B14187B08C2B3A07787308DE518C17_H
#define CONTEXTSTILLHASRETAINEDENTITIESEXCEPTION_TFDD6AAA582B14187B08C2B3A07787308DE518C17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextStillHasRetainedEntitiesException
struct  ContextStillHasRetainedEntitiesException_tFDD6AAA582B14187B08C2B3A07787308DE518C17  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTSTILLHASRETAINEDENTITIESEXCEPTION_TFDD6AAA582B14187B08C2B3A07787308DE518C17_H
#ifndef ENTITYALREADYHASCOMPONENTEXCEPTION_T30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_H
#define ENTITYALREADYHASCOMPONENTEXCEPTION_T30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityAlreadyHasComponentException
struct  EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYALREADYHASCOMPONENTEXCEPTION_T30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_H
#ifndef ENTITYDOESNOTHAVECOMPONENTEXCEPTION_TE224D88B637576CC426C2510B406781E58AE209C_H
#define ENTITYDOESNOTHAVECOMPONENTEXCEPTION_TE224D88B637576CC426C2510B406781E58AE209C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityDoesNotHaveComponentException
struct  EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYDOESNOTHAVECOMPONENTEXCEPTION_TE224D88B637576CC426C2510B406781E58AE209C_H
#ifndef ENTITYINDEXEXCEPTION_T6834F599460536EA81AFA69CA928AA2022334EF4_H
#define ENTITYINDEXEXCEPTION_T6834F599460536EA81AFA69CA928AA2022334EF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIndexException
struct  EntityIndexException_t6834F599460536EA81AFA69CA928AA2022334EF4  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEXEXCEPTION_T6834F599460536EA81AFA69CA928AA2022334EF4_H
#ifndef ENTITYISALREADYRETAINEDBYOWNEREXCEPTION_T48896BC8B21BDFFFD7F85D819233996F6E8D2783_H
#define ENTITYISALREADYRETAINEDBYOWNEREXCEPTION_T48896BC8B21BDFFFD7F85D819233996F6E8D2783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsAlreadyRetainedByOwnerException
struct  EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISALREADYRETAINEDBYOWNEREXCEPTION_T48896BC8B21BDFFFD7F85D819233996F6E8D2783_H
#ifndef ENTITYISNOTDESTROYEDEXCEPTION_TE5F9909534769331523B7F21128EF075CE107364_H
#define ENTITYISNOTDESTROYEDEXCEPTION_TE5F9909534769331523B7F21128EF075CE107364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsNotDestroyedException
struct  EntityIsNotDestroyedException_tE5F9909534769331523B7F21128EF075CE107364  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISNOTDESTROYEDEXCEPTION_TE5F9909534769331523B7F21128EF075CE107364_H
#ifndef ENTITYISNOTENABLEDEXCEPTION_T7AE4AB9E6B2414C4063824D87F5C4674046B2C11_H
#define ENTITYISNOTENABLEDEXCEPTION_T7AE4AB9E6B2414C4063824D87F5C4674046B2C11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsNotEnabledException
struct  EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISNOTENABLEDEXCEPTION_T7AE4AB9E6B2414C4063824D87F5C4674046B2C11_H
#ifndef ENTITYISNOTRETAINEDBYOWNEREXCEPTION_T3553575E99A69F8AD64A5BAC2DC898E6954DC225_H
#define ENTITYISNOTRETAINEDBYOWNEREXCEPTION_T3553575E99A69F8AD64A5BAC2DC898E6954DC225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIsNotRetainedByOwnerException
struct  EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225  : public EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYISNOTRETAINEDBYOWNEREXCEPTION_T3553575E99A69F8AD64A5BAC2DC898E6954DC225_H
#ifndef GROUPEVENT_T5D8EAED9A0E5DF01072E622340ED850672F54891_H
#define GROUPEVENT_T5D8EAED9A0E5DF01072E622340ED850672F54891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.GroupEvent
struct  GroupEvent_t5D8EAED9A0E5DF01072E622340ED850672F54891 
{
public:
	// System.Byte Entitas.GroupEvent::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GroupEvent_t5D8EAED9A0E5DF01072E622340ED850672F54891, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPEVENT_T5D8EAED9A0E5DF01072E622340ED850672F54891_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef CONTEXTENTITYCHANGED_T1C8C691CEE56C008F944E90383D7D7400E111EC1_H
#define CONTEXTENTITYCHANGED_T1C8C691CEE56C008F944E90383D7D7400E111EC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextEntityChanged
struct  ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTENTITYCHANGED_T1C8C691CEE56C008F944E90383D7D7400E111EC1_H
#ifndef CONTEXTGROUPCHANGED_TDF404FDADBE283C90826E021920179C9C5986728_H
#define CONTEXTGROUPCHANGED_TDF404FDADBE283C90826E021920179C9C5986728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ContextGroupChanged
struct  ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTGROUPCHANGED_TDF404FDADBE283C90826E021920179C9C5986728_H
#ifndef ENTITYCOMPONENTCHANGED_T326B845F3277940DACBBC8836618A83196065E1A_H
#define ENTITYCOMPONENTCHANGED_T326B845F3277940DACBBC8836618A83196065E1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityComponentChanged
struct  EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYCOMPONENTCHANGED_T326B845F3277940DACBBC8836618A83196065E1A_H
#ifndef ENTITYCOMPONENTREPLACED_TECE10545D16DE6AECA3B03B47FA2582FE7A11B57_H
#define ENTITYCOMPONENTREPLACED_TECE10545D16DE6AECA3B03B47FA2582FE7A11B57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityComponentReplaced
struct  EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYCOMPONENTREPLACED_TECE10545D16DE6AECA3B03B47FA2582FE7A11B57_H
#ifndef ENTITYEVENT_TE7578C70965872DD79E91A748EBBCD674AEAE1D6_H
#define ENTITYEVENT_TE7578C70965872DD79E91A748EBBCD674AEAE1D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityEvent
struct  EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYEVENT_TE7578C70965872DD79E91A748EBBCD674AEAE1D6_H
#ifndef ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#define ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_H
#ifndef FUNC_2_T1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE_H
#define FUNC_2_T1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<Entitas.IEntity,System.String>
struct  Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE_H
#ifndef FUNC_2_T44B347E67E515867D995E8BD5EFD67FA88CE53CF_H
#define FUNC_2_T44B347E67E515867D995E8BD5EFD67FA88CE53CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.String>
struct  Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T44B347E67E515867D995E8BD5EFD67FA88CE53CF_H
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Entitas.IEntity[]
struct IEntityU5BU5D_t67E0A22209E555D9A801B4B2D36DFC5D75A409F9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.Stack`1<Entitas.IComponent>[]
struct Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * m_Items[1];

public:
	inline Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Entitas.IComponent[]
struct IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m93DBD723B5A365BD92FAF21BECDDCAFF67D0CA72_gshared (RuntimeObject* p0, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * p1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Enumerable_ToArray_TisRuntimeObject_mF33F5D8045B820AE98F4EEE7524A58F9B52436DA_gshared (RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C" IL2CPP_METHOD_ATTR void Stack_1_Push_mB892D933D8982A0702F4E09E2F0D7B0C33E2A4E1_gshared (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* List_1_ToArray_m801D4DEF3587F60F463F04EEABE5CBE711FE5612_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Stack_1__ctor_m54114F5D347F44F2C0FD45AF09974A5B55EC5373_gshared (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t Stack_1_get_Count_m8803B4178385D39338A1EDDC39FE6D8152F01A1D_gshared (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Stack_1_Pop_mD632EB4DA13E5CAEC62EECFAD1C88818F1223E20_gshared (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t HashSet_1_get_Count_m4594EB8357F5E4068DEE99E351ADE10202D2AF10_gshared (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HashSet_1__ctor_m8A209D312FD08A21AFB5551881E7A6946A07C0D5_gshared (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR bool HashSet_1_Add_m1A3167674F8646F908B2AF4977144E4D8175D695_gshared (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool HashSet_1_Remove_mDEE995DC0C479BC045A7490A0F7921B9ACBB4790_gshared (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);

// System.Void Entitas.EntitasException::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26 (EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33 * __this, String_t* ___message0, String_t* ___hint1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07 (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Join(System.String,System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4 (String_t* p0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC (RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void System.Func`2<Entitas.IEntity,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mE84225DB2E17CE18158B8FA6B70FACCC34D63E9B (Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared)(__this, p0, p1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<Entitas.IEntity,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisIEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379_TisString_t_m99E1CC3914417788EAA75FCC8E287A367BF57A0D (RuntimeObject* p0, Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * p1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE *, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m93DBD723B5A365BD92FAF21BECDDCAFF67D0CA72_gshared)(p0, p1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC (RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_mF33F5D8045B820AE98F4EEE7524A58F9B52436DA_gshared)(p0, method);
}
// System.Void Entitas.ContextStillHasRetainedEntitiesException/<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mF625972485DA32052CFE5822E1841CDB60E36858 (U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * __this, const RuntimeMethod* method);
// System.Collections.Generic.HashSet`1<System.Object> Entitas.SafeAERC::get_owners()
extern "C" IL2CPP_METHOD_ATTR HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * SafeAERC_get_owners_mCCC99093D93E29B2A5853290A46CEA1DF22E9566 (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m0E11CA9B34D5352759D42FDEA69CC14E7C949427 (Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mE2AF7615AD18E9CD92B1909285F5EC5DA8D180C8_gshared)(__this, p0, p1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisRuntimeObject_TisString_t_mF297F0ADF2AF7D55B365D2122F5A887784112832 (RuntimeObject* p0, Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * p1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF *, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisRuntimeObject_m93DBD723B5A365BD92FAF21BECDDCAFF67D0CA72_gshared)(p0, p1, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0 (Exception_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * p0, Delegate_t * p1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * p0, Delegate_t * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Entitas.IComponent>::.ctor()
inline void List_1__ctor_mEF29B452B49300F7653F3A5FC6340E1F01FA889B (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4 (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_gshared)(__this, method);
}
// System.Void Entitas.Entity::Reactivate(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Entity_Reactivate_mB716527C2222DE67B7062E9B5AA379ABE11FAB2F (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___creationIndex0, const RuntimeMethod* method);
// Entitas.ContextInfo Entitas.Entity::createDefaultContextInfo()
extern "C" IL2CPP_METHOD_ATTR ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * Entity_createDefaultContextInfo_m2048E85A3BDC948AAAA3452EEF56475215572790 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method);
// System.Void Entitas.SafeAERC::.ctor(Entitas.IEntity)
extern "C" IL2CPP_METHOD_ATTR void SafeAERC__ctor_mC8216DC64624BDCEB24698DF7ADC85F2A26FDCAB (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * __this, RuntimeObject* ___entity0, const RuntimeMethod* method);
// System.Int32 Entitas.Entity::get_totalComponents()
extern "C" IL2CPP_METHOD_ATTR int32_t Entity_get_totalComponents_mD9BF5DBADC8B58B2F45C0C9575798D166AE78124 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method);
// System.String System.Int32::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02 (int32_t* __this, const RuntimeMethod* method);
// System.Void Entitas.ContextInfo::.ctor(System.String,System.String[],System.Type[])
extern "C" IL2CPP_METHOD_ATTR void ContextInfo__ctor_m5EC411BF37BB763F2BB4476C65DDCFBB672DF0E8 (ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * __this, String_t* ___name0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___componentNames1, TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___componentTypes2, const RuntimeMethod* method);
// System.Void Entitas.EntityIsNotEnabledException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313 (EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Boolean Entitas.Entity::HasComponent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Entity_HasComponent_m03AEC990CB4263D7419B6CCD6C06416FF02D7711 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Entitas.EntityAlreadyHasComponentException::.ctor(System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityAlreadyHasComponentException__ctor_m19F540EA19E143D39F2366773AECB8E831D1182E (EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB * __this, int32_t ___index0, String_t* ___message1, String_t* ___hint2, const RuntimeMethod* method);
// System.Void Entitas.EntityComponentChanged::Invoke(Entitas.IEntity,System.Int32,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentChanged_Invoke_mB3A0E91AE760C571AF6E5AAD6EAD388B59628D49 (EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * __this, RuntimeObject* ___entity0, int32_t ___index1, RuntimeObject* ___component2, const RuntimeMethod* method);
// System.Void Entitas.EntityDoesNotHaveComponentException::.ctor(System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityDoesNotHaveComponentException__ctor_m15C877635E6695C5DF57F1A45D28A91CE24EDB65 (EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C * __this, int32_t ___index0, String_t* ___message1, String_t* ___hint2, const RuntimeMethod* method);
// System.Void Entitas.Entity::replaceComponent(System.Int32,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void Entity_replaceComponent_mC4CA5CCC0D1EDD836ED494D8DEEE470DB4B1A1C7 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, RuntimeObject* ___replacement1, const RuntimeMethod* method);
// System.Void Entitas.Entity::AddComponent(System.Int32,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, RuntimeObject* ___component1, const RuntimeMethod* method);
// System.Void Entitas.EntityComponentReplaced::Invoke(Entitas.IEntity,System.Int32,Entitas.IComponent,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentReplaced_Invoke_mAE15CE1DDD539E5CA7C66B3BD5E5078C99FD5488 (EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * __this, RuntimeObject* ___entity0, int32_t ___index1, RuntimeObject* ___previousComponent2, RuntimeObject* ___newComponent3, const RuntimeMethod* method);
// System.Collections.Generic.Stack`1<Entitas.IComponent> Entitas.Entity::GetComponentPool(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * Entity_GetComponentPool_m09EFD65E02F8FAC721807E1640C7B5AE10AA8957 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<Entitas.IComponent>::Push(!0)
inline void Stack_1_Push_m90E9E0ABAB946873DC74924DAC696403DA669B84 (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 *, RuntimeObject*, const RuntimeMethod*))Stack_1_Push_mB892D933D8982A0702F4E09E2F0D7B0C33E2A4E1_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.IComponent>::Add(!0)
inline void List_1_Add_m88E063CFB9F1ED228CAB4ECCFC08E5BF80CF234D (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// !0[] System.Collections.Generic.List`1<Entitas.IComponent>::ToArray()
inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* List_1_ToArray_m7BEC5A680E5ED7915147F1C2D158922831DA59D2 (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * __this, const RuntimeMethod* method)
{
	return ((  IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* (*) (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE *, const RuntimeMethod*))List_1_ToArray_m801D4DEF3587F60F463F04EEABE5CBE711FE5612_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.IComponent>::Clear()
inline void List_1_Clear_m786D360E04623387E0CB0FCC17A815D1ED1CCB74 (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Stack`1<Entitas.IComponent>::.ctor()
inline void Stack_1__ctor_mD6EE87D90C123FC7ED2D0BE5B6CADE1704BE3A09 (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * __this, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 *, const RuntimeMethod*))Stack_1__ctor_m54114F5D347F44F2C0FD45AF09974A5B55EC5373_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.Stack`1<Entitas.IComponent>::get_Count()
inline int32_t Stack_1_get_Count_m89BAF285F607EF62CC860B779D8F40495BE24D8A (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 *, const RuntimeMethod*))Stack_1_get_Count_m8803B4178385D39338A1EDDC39FE6D8152F01A1D_gshared)(__this, method);
}
// System.Object System.Activator::CreateInstance(System.Type)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Activator_CreateInstance_mD06EE47879F606317C6DA91FB63E678CABAC6A16 (Type_t * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<Entitas.IComponent>::Pop()
inline RuntimeObject* Stack_1_Pop_m252F7AF64D8E273A7A921A037342F8BA121F8C93 (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 *, const RuntimeMethod*))Stack_1_Pop_mD632EB4DA13E5CAEC62EECFAD1C88818F1223E20_gshared)(__this, method);
}
// System.Void Entitas.EntityEvent::Invoke(Entitas.IEntity)
extern "C" IL2CPP_METHOD_ATTR void EntityEvent_Invoke_m48C29E9B35B6C7A22AAE173C82482DB1307FE905 (EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * __this, RuntimeObject* ___entity0, const RuntimeMethod* method);
// System.Void Entitas.Entity::RemoveAllComponents()
extern "C" IL2CPP_METHOD_ATTR void Entity_RemoveAllComponents_mAD49966DC9C3DAC58F6D49A27FE5664D084E35CC (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B (StringBuilder_t * __this, int32_t p0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Int32)
extern "C" IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m85874CFF9E4B152DB2A91936C6F2CA3E9B1EFF84 (StringBuilder_t * __this, int32_t p0, const RuntimeMethod* method);
// Entitas.IComponent[] Entitas.Entity::GetComponents()
extern "C" IL2CPP_METHOD_ATTR IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* Entity_GetComponents_m4B0440F78D4D6148DF032841238C06BEA38E6532 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
inline int32_t HashSet_1_get_Count_m4594EB8357F5E4068DEE99E351ADE10202D2AF10 (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 *, const RuntimeMethod*))HashSet_1_get_Count_m4594EB8357F5E4068DEE99E351ADE10202D2AF10_gshared)(__this, method);
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
inline void HashSet_1__ctor_m8A209D312FD08A21AFB5551881E7A6946A07C0D5 (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 *, const RuntimeMethod*))HashSet_1__ctor_m8A209D312FD08A21AFB5551881E7A6946A07C0D5_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
inline bool HashSet_1_Add_m1A3167674F8646F908B2AF4977144E4D8175D695 (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, RuntimeObject * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 *, RuntimeObject *, const RuntimeMethod*))HashSet_1_Add_m1A3167674F8646F908B2AF4977144E4D8175D695_gshared)(__this, p0, method);
}
// System.Void Entitas.EntityIsAlreadyRetainedByOwnerException::.ctor(Entitas.IEntity,System.Object)
extern "C" IL2CPP_METHOD_ATTR void EntityIsAlreadyRetainedByOwnerException__ctor_mFBDE6BDC3650783F8C26EA2EB82F8579C7278784 (EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783 * __this, RuntimeObject* ___entity0, RuntimeObject * ___owner1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(!0)
inline bool HashSet_1_Remove_mDEE995DC0C479BC045A7490A0F7921B9ACBB4790 (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * __this, RuntimeObject * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 *, RuntimeObject *, const RuntimeMethod*))HashSet_1_Remove_mDEE995DC0C479BC045A7490A0F7921B9ACBB4790_gshared)(__this, p0, method);
}
// System.Void Entitas.EntityIsNotRetainedByOwnerException::.ctor(Entitas.IEntity,System.Object)
extern "C" IL2CPP_METHOD_ATTR void EntityIsNotRetainedByOwnerException__ctor_mB6FFC485F1A74C09C39BC2A4CBAD2E065E622E8B (EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225 * __this, RuntimeObject* ___entity0, RuntimeObject * ___owner1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Entitas.IInitializeSystem>::.ctor()
inline void List_1__ctor_m54489866F749EC6026BA519D0B0EBEBC4BC2CAD1 (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.IExecuteSystem>::.ctor()
inline void List_1__ctor_mB36C11BB84D2C63932AB8C91C902593368F62AAE (List_1_tE232269578E5E48549D25DD0C9823B612D968293 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE232269578E5E48549D25DD0C9823B612D968293 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.ICleanupSystem>::.ctor()
inline void List_1__ctor_m3851B7F861C2057D9C3BC286F44772F35A8575B0 (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.ITearDownSystem>::.ctor()
inline void List_1__ctor_m9587932254DEE91014C9C59AA0F5AE5E017BA7D0 (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.IInitializeSystem>::Add(!0)
inline void List_1_Add_mA730CED3CC3D80FCC82584EBE806ED022613B069 (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.IExecuteSystem>::Add(!0)
inline void List_1_Add_mE9E33EB2ED285DBCE781434C34464D5B6A5DF800 (List_1_tE232269578E5E48549D25DD0C9823B612D968293 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE232269578E5E48549D25DD0C9823B612D968293 *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.ICleanupSystem>::Add(!0)
inline void List_1_Add_m0BBC37E3FDCD60F153B75D9E56F2260FEEB41165 (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Entitas.ITearDownSystem>::Add(!0)
inline void List_1_Add_m3E18D0DFE1373E92D4D23B8CF9F1F2CC67E19AB7 (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, p0, method);
}
// !0 System.Collections.Generic.List`1<Entitas.IInitializeSystem>::get_Item(System.Int32)
inline RuntimeObject* List_1_get_Item_m75CEF9AEFEB69CD96795F522D1AB9B2FFC163BBE (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<Entitas.IInitializeSystem>::get_Count()
inline int32_t List_1_get_Count_m7DF025BDC3A448B58F52D4FE6329F8CE2D6CA04C (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<Entitas.IExecuteSystem>::get_Item(System.Int32)
inline RuntimeObject* List_1_get_Item_mB7AD700D64EDBCB425370286E3EAB603539BC41C (List_1_tE232269578E5E48549D25DD0C9823B612D968293 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (List_1_tE232269578E5E48549D25DD0C9823B612D968293 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<Entitas.IExecuteSystem>::get_Count()
inline int32_t List_1_get_Count_m05018813FF4A80F1342F8692ACAFAE1A03DC5512 (List_1_tE232269578E5E48549D25DD0C9823B612D968293 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tE232269578E5E48549D25DD0C9823B612D968293 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<Entitas.ICleanupSystem>::get_Item(System.Int32)
inline RuntimeObject* List_1_get_Item_m5EEBC49C9FBF9326533FFB51F0BF3424BF333B70 (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<Entitas.ICleanupSystem>::get_Count()
inline int32_t List_1_get_Count_m1043386622543BFD25CA715705DA1D97D4F074D9 (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<Entitas.ITearDownSystem>::get_Item(System.Int32)
inline RuntimeObject* List_1_get_Item_m92C03E07978B485072E96FC891797B8ADBBD15DD (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<Entitas.ITearDownSystem>::get_Count()
inline int32_t List_1_get_Count_m357AD56D8549B5DBDEEE32F070A942A8FFDF5E9C (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// System.Void Entitas.Systems::DeactivateReactiveSystems()
extern "C" IL2CPP_METHOD_ATTR void Systems_DeactivateReactiveSystems_m2D9A6B73CA9A9BE0DC3D5CC10ADA3F2EDDFDE060 (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.CollectorException::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void CollectorException__ctor_mA23305F8BD0D1F342F917D5C469B988C71DDFEA1 (CollectorException_tC99C67E8041C8145EFA76944442A2E52DD916E84 * __this, String_t* ___message0, String_t* ___hint1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___hint1;
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextDoesNotContainEntityException::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void ContextDoesNotContainEntityException__ctor_m1180F70AA578AF59952BF63E5FF7140BC030A64C (ContextDoesNotContainEntityException_t7080D5711853883454B78F851D81DFE846DE5B95 * __this, String_t* ___message0, String_t* ___hint1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContextDoesNotContainEntityException__ctor_m1180F70AA578AF59952BF63E5FF7140BC030A64C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_0, _stringLiteralF26F7059D0DDD056FED7BBE4138911C0690AE6C7, /*hidden argument*/NULL);
		String_t* L_2 = ___hint1;
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextEntityChanged::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ContextEntityChanged__ctor_mE6D99286AA0BBC1DD2BC1C01CF7DCAAAEEA8FAF9 (ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Entitas.ContextEntityChanged::Invoke(Entitas.IContext,Entitas.IEntity)
extern "C" IL2CPP_METHOD_ATTR void ContextEntityChanged_Invoke_mB8F58D5CE70D0520F42890638DD22BE8F4169E21 (ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * __this, RuntimeObject* ___context0, RuntimeObject* ___entity1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___context0, ___entity1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___entity1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___entity1);
							else
								GenericVirtActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___entity1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___context0, ___entity1);
							else
								VirtActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___context0, ___entity1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___context0, ___entity1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___context0, ___entity1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___entity1);
							else
								GenericVirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___entity1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___context0, ___entity1);
							else
								VirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___context0, ___entity1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___entity1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___context0, ___entity1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___entity1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___entity1);
						else
							GenericVirtActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___entity1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___context0, ___entity1);
						else
							VirtActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___context0, ___entity1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___context0, ___entity1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___context0, ___entity1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___entity1);
						else
							GenericVirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___entity1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___context0, ___entity1);
						else
							VirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___context0, ___entity1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___entity1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Entitas.ContextEntityChanged::BeginInvoke(Entitas.IContext,Entitas.IEntity,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ContextEntityChanged_BeginInvoke_m9891D28F8D68C5AC25E0833FB89152675173AFAA (ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * __this, RuntimeObject* ___context0, RuntimeObject* ___entity1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___context0;
	__d_args[1] = ___entity1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void Entitas.ContextEntityChanged::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void ContextEntityChanged_EndInvoke_m11AF93D1EA35E77EAD802FEBA15D065C0C0FFE05 (ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextEntityIndexDoesAlreadyExistException::.ctor(Entitas.IContext,System.String)
extern "C" IL2CPP_METHOD_ATTR void ContextEntityIndexDoesAlreadyExistException__ctor_m9CC32949340F4D7F68A1398E98C959D20B89E2E6 (ContextEntityIndexDoesAlreadyExistException_tCB4C94A55764B9852F030A20334B50BE8299E6BC * __this, RuntimeObject* ___context0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContextEntityIndexDoesAlreadyExistException__ctor_m9CC32949340F4D7F68A1398E98C959D20B89E2E6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralD10AC531A571FA3F469A9795BF8BD1BC2440497E);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralD10AC531A571FA3F469A9795BF8BD1BC2440497E);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		String_t* L_3 = ___name1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral015AA75E8DFA1E2F0E82A7FB2CAFA09A121B183F);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral015AA75E8DFA1E2F0E82A7FB2CAFA09A121B183F);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		RuntimeObject* L_6 = ___context0;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2654F5CCEAF52997DFCFCED105D22EF40DEF62CC);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral2654F5CCEAF52997DFCFCED105D22EF40DEF62CC);
		String_t* L_8 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_7, /*hidden argument*/NULL);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_8, _stringLiteral24A5A60DC72D713F359E119B485D26F18CA208AA, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextEntityIndexDoesNotExistException::.ctor(Entitas.IContext,System.String)
extern "C" IL2CPP_METHOD_ATTR void ContextEntityIndexDoesNotExistException__ctor_mA058884DA2C4915AFEE6751A9C2883E2EB092451 (ContextEntityIndexDoesNotExistException_t2191FE494E5293B1BA0242B7D67CCA9C05602672 * __this, RuntimeObject* ___context0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContextEntityIndexDoesNotExistException__ctor_mA058884DA2C4915AFEE6751A9C2883E2EB092451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralCF236536D0E19572D619A88C16CC2605B22B1348);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralCF236536D0E19572D619A88C16CC2605B22B1348);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		String_t* L_3 = ___name1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral215541285235990E03ADE4EB1C1F83199DBE566D);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral215541285235990E03ADE4EB1C1F83199DBE566D);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		RuntimeObject* L_6 = ___context0;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2654F5CCEAF52997DFCFCED105D22EF40DEF62CC);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral2654F5CCEAF52997DFCFCED105D22EF40DEF62CC);
		String_t* L_8 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_7, /*hidden argument*/NULL);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_8, _stringLiteralCA2FF84A6A92D9849061664B0317CC18E1D3E5C1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextGroupChanged::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ContextGroupChanged__ctor_m0D8AB36526AC4B129C75A61352A336D4EC225296 (ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Entitas.ContextGroupChanged::Invoke(Entitas.IContext,Entitas.IGroup)
extern "C" IL2CPP_METHOD_ATTR void ContextGroupChanged_Invoke_m9839E6538CF1CA4D2D4EF7BCA668A09783872702 (ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 * __this, RuntimeObject* ___context0, RuntimeObject* ___group1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 2)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___context0, ___group1, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___group1, targetMethod);
				}
			}
			else if (___parameterCount != 2)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___group1);
							else
								GenericVirtActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___group1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___context0, ___group1);
							else
								VirtActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___context0, ___group1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___context0, ___group1, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___context0, ___group1, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___group1);
							else
								GenericVirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___group1);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___context0, ___group1);
							else
								VirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___context0, ___group1);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___group1, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___context0, ___group1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___group1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___group1);
						else
							GenericVirtActionInvoker1< RuntimeObject* >::Invoke(targetMethod, ___context0, ___group1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___context0, ___group1);
						else
							VirtActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___context0, ___group1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___context0, ___group1, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___context0, ___group1, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___group1);
						else
							GenericVirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___context0, ___group1);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___context0, ___group1);
						else
							VirtActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___context0, ___group1);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___context0, ___group1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Entitas.ContextGroupChanged::BeginInvoke(Entitas.IContext,Entitas.IGroup,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ContextGroupChanged_BeginInvoke_mB7073E1AE242D662EB9BD0C5024836814E2462AC (ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 * __this, RuntimeObject* ___context0, RuntimeObject* ___group1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___context0;
	__d_args[1] = ___group1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void Entitas.ContextGroupChanged::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void ContextGroupChanged_EndInvoke_mAD0BB26662E26CE121EE6FEEFE5710C9E27EEF8E (ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextInfo::.ctor(System.String,System.String[],System.Type[])
extern "C" IL2CPP_METHOD_ATTR void ContextInfo__ctor_m5EC411BF37BB763F2BB4476C65DDCFBB672DF0E8 (ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * __this, String_t* ___name0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___componentNames1, TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___componentTypes2, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_name_0(L_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = ___componentNames1;
		__this->set_componentNames_1(L_1);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_2 = ___componentTypes2;
		__this->set_componentTypes_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextInfoException::.ctor(Entitas.IContext,Entitas.ContextInfo)
extern "C" IL2CPP_METHOD_ATTR void ContextInfoException__ctor_m1B9C16ECF443D2A2FA4A1209EC24BEF4B5A01CCD (ContextInfoException_t5E142D0B2B6C87CF5CC9A70D38CF5D40FB1EA381 * __this, RuntimeObject* ___context0, ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * ___contextInfo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContextInfoException__ctor_m1B9C16ECF443D2A2FA4A1209EC24BEF4B5A01CCD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)7);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral3D2A14F50A1337B537062A89C8E70F3391F3FA85);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3D2A14F50A1337B537062A89C8E70F3391F3FA85);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		RuntimeObject* L_3 = ___context0;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralBAFB61407AE607E32387AE40DB637CDD10DDBC24);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteralBAFB61407AE607E32387AE40DB637CDD10DDBC24);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		RuntimeObject* L_6 = ___context0;
		NullCheck(L_6);
		int32_t L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Entitas.IContext::get_totalComponents() */, IContext_t620F28E017D438765172A4D4551A2D82B0448C50_il2cpp_TypeInfo_var, L_6);
		int32_t L_8 = L_7;
		RuntimeObject * L_9 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_9);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_9);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_5;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral200C16EBADD4D3D62FBE907CE5A69F089AD3CC62);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral200C16EBADD4D3D62FBE907CE5A69F089AD3CC62);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_10;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_12 = ___contextInfo1;
		NullCheck(L_12);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_13 = L_12->get_componentNames_1();
		NullCheck(L_13);
		int32_t L_14 = (((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))));
		RuntimeObject * L_15 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_11;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9);
		String_t* L_17 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_16, /*hidden argument*/NULL);
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_18 = ___contextInfo1;
		NullCheck(L_18);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_19 = L_18->get_componentNames_1();
		String_t* L_20 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(_stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC, L_19, /*hidden argument*/NULL);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_17, L_20, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextStillHasRetainedEntitiesException::.ctor(Entitas.IContext,Entitas.IEntity[])
extern "C" IL2CPP_METHOD_ATTR void ContextStillHasRetainedEntitiesException__ctor_m60B798F5D9F0358BB815AF0235A5614BD0FD1CBF (ContextStillHasRetainedEntitiesException_tFDD6AAA582B14187B08C2B3A07787308DE518C17 * __this, RuntimeObject* ___context0, IEntityU5BU5D_t67E0A22209E555D9A801B4B2D36DFC5D75A409F9* ___entities1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContextStillHasRetainedEntitiesException__ctor_m60B798F5D9F0358BB815AF0235A5614BD0FD1CBF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * G_B2_0 = NULL;
	IEntityU5BU5D_t67E0A22209E555D9A801B4B2D36DFC5D75A409F9* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	ContextStillHasRetainedEntitiesException_tFDD6AAA582B14187B08C2B3A07787308DE518C17 * G_B2_5 = NULL;
	Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * G_B1_0 = NULL;
	IEntityU5BU5D_t67E0A22209E555D9A801B4B2D36DFC5D75A409F9* G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	ContextStillHasRetainedEntitiesException_tFDD6AAA582B14187B08C2B3A07787308DE518C17 * G_B1_5 = NULL;
	{
		RuntimeObject* L_0 = ___context0;
		String_t* L_1 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8, L_0, _stringLiteral98F4C5700762C3501ED70A29E6ECA7B964F15C8F, /*hidden argument*/NULL);
		IEntityU5BU5D_t67E0A22209E555D9A801B4B2D36DFC5D75A409F9* L_2 = ___entities1;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var);
		Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * L_3 = ((U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var))->get_U3CU3E9__0_0_2();
		Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * L_4 = L_3;
		G_B1_0 = L_4;
		G_B1_1 = L_2;
		G_B1_2 = _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC;
		G_B1_3 = _stringLiteralA18BB4E7033B240C56F8A9283F7FD28202E0C7E5;
		G_B1_4 = L_1;
		G_B1_5 = __this;
		if (L_4)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_2;
			G_B2_2 = _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC;
			G_B2_3 = _stringLiteralA18BB4E7033B240C56F8A9283F7FD28202E0C7E5;
			G_B2_4 = L_1;
			G_B2_5 = __this;
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var);
		U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * L_5 = ((U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * L_6 = (Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE *)il2cpp_codegen_object_new(Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE_il2cpp_TypeInfo_var);
		Func_2__ctor_mE84225DB2E17CE18158B8FA6B70FACCC34D63E9B(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec_U3C_ctorU3Eb__0_0_m033F2367172299A2B12EB3C390AB85C948623E4D_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mE84225DB2E17CE18158B8FA6B70FACCC34D63E9B_RuntimeMethod_var);
		Func_2_t1FF357CE5BDF87DFFA00204A6E5BE7F5FA1251EE * L_7 = L_6;
		((U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var))->set_U3CU3E9__0_0_2(L_7);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
		G_B2_4 = G_B1_4;
		G_B2_5 = G_B1_5;
	}

IL_003b:
	{
		RuntimeObject* L_8 = Enumerable_Select_TisIEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379_TisString_t_m99E1CC3914417788EAA75FCC8E287A367BF57A0D((RuntimeObject*)(RuntimeObject*)G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Select_TisIEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379_TisString_t_m99E1CC3914417788EAA75FCC8E287A367BF57A0D_RuntimeMethod_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_9 = Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC(L_8, /*hidden argument*/Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC_RuntimeMethod_var);
		String_t* L_10 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(G_B2_2, L_9, /*hidden argument*/NULL);
		String_t* L_11 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(G_B2_3, L_10, /*hidden argument*/NULL);
		NullCheck(G_B2_5);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(G_B2_5, G_B2_4, L_11, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.ContextStillHasRetainedEntitiesException_<>c::.cctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mC9FA5A53082CB6983B0F4EB3FFE42C08122EF9E1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mC9FA5A53082CB6983B0F4EB3FFE42C08122EF9E1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * L_0 = (U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 *)il2cpp_codegen_object_new(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mF625972485DA32052CFE5822E1841CDB60E36858(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Entitas.ContextStillHasRetainedEntitiesException_<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mF625972485DA32052CFE5822E1841CDB60E36858 (U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Entitas.ContextStillHasRetainedEntitiesException_<>c::<.ctor>b__0_0(Entitas.IEntity)
extern "C" IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3C_ctorU3Eb__0_0_m033F2367172299A2B12EB3C390AB85C948623E4D (U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * __this, RuntimeObject* ___e0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3C_ctorU3Eb__0_0_m033F2367172299A2B12EB3C390AB85C948623E4D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * V_0 = NULL;
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * G_B3_0 = NULL;
	HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	String_t* G_B3_3 = NULL;
	RuntimeObject* G_B3_4 = NULL;
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * G_B2_0 = NULL;
	HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	RuntimeObject* G_B2_4 = NULL;
	{
		RuntimeObject* L_0 = ___e0;
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(12 /* Entitas.IAERC Entitas.IEntity::get_aerc() */, IEntity_tB710DED3F76286AAA2C85314F575D57C65F1F379_il2cpp_TypeInfo_var, L_0);
		V_0 = ((SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 *)IsInstSealed((RuntimeObject*)L_1, SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30_il2cpp_TypeInfo_var));
		SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		RuntimeObject* L_3 = ___e0;
		SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * L_4 = V_0;
		NullCheck(L_4);
		HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * L_5 = SafeAERC_get_owners_mCCC99093D93E29B2A5853290A46CEA1DF22E9566(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var);
		Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * L_6 = ((U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var))->get_U3CU3E9__0_1_1();
		Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * L_7 = L_6;
		G_B2_0 = L_7;
		G_B2_1 = L_5;
		G_B2_2 = _stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46;
		G_B2_3 = _stringLiteralFC02E199EAA2A6900AEC454AD22BAA13B6E8F8E6;
		G_B2_4 = L_3;
		if (L_7)
		{
			G_B3_0 = L_7;
			G_B3_1 = L_5;
			G_B3_2 = _stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46;
			G_B3_3 = _stringLiteralFC02E199EAA2A6900AEC454AD22BAA13B6E8F8E6;
			G_B3_4 = L_3;
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var);
		U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * L_8 = ((U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * L_9 = (Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF *)il2cpp_codegen_object_new(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF_il2cpp_TypeInfo_var);
		Func_2__ctor_m0E11CA9B34D5352759D42FDEA69CC14E7C949427(L_9, L_8, (intptr_t)((intptr_t)U3CU3Ec_U3C_ctorU3Eb__0_1_mEFED69862F44B43EC8B1F68715A25CF1EE04DC6E_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m0E11CA9B34D5352759D42FDEA69CC14E7C949427_RuntimeMethod_var);
		Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * L_10 = L_9;
		((U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4_il2cpp_TypeInfo_var))->set_U3CU3E9__0_1_1(L_10);
		G_B3_0 = L_10;
		G_B3_1 = G_B2_1;
		G_B3_2 = G_B2_2;
		G_B3_3 = G_B2_3;
		G_B3_4 = G_B2_4;
	}

IL_003f:
	{
		RuntimeObject* L_11 = Enumerable_Select_TisRuntimeObject_TisString_t_mF297F0ADF2AF7D55B365D2122F5A887784112832(G_B3_1, G_B3_0, /*hidden argument*/Enumerable_Select_TisRuntimeObject_TisString_t_mF297F0ADF2AF7D55B365D2122F5A887784112832_RuntimeMethod_var);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_12 = Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC(L_11, /*hidden argument*/Enumerable_ToArray_TisString_t_m1BAD76FB02571EB3CCC5BB00D4D581399F8523FC_RuntimeMethod_var);
		String_t* L_13 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(G_B3_2, L_12, /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(G_B3_4, G_B3_3, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0054:
	{
		RuntimeObject* L_15 = ___e0;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		return L_16;
	}
}
// System.String Entitas.ContextStillHasRetainedEntitiesException_<>c::<.ctor>b__0_1(System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3C_ctorU3Eb__0_1_mEFED69862F44B43EC8B1F68715A25CF1EE04DC6E (U3CU3Ec_tBB75825E443987A79032BCE92971925042E081B4 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___o0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntitasException::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26 (EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33 * __this, String_t* ___message0, String_t* ___hint1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33 * G_B2_0 = NULL;
	EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	EntitasException_t71786D33342FC2A2F61B3AAC7FD34318ABC7CD33 * G_B3_1 = NULL;
	{
		String_t* L_0 = ___hint1;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0007;
		}
	}
	{
		String_t* L_1 = ___message0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0013;
	}

IL_0007:
	{
		String_t* L_2 = ___message0;
		String_t* L_3 = ___hint1;
		String_t* L_4 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_2, _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0013:
	{
		NullCheck(G_B3_1);
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.Entity::add_OnComponentAdded(Entitas.EntityComponentChanged)
extern "C" IL2CPP_METHOD_ATTR void Entity_add_OnComponentAdded_m1BDF039DEDB9F7DC262F6FA071FC98BCB77C5B15 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_add_OnComponentAdded_m1BDF039DEDB9F7DC262F6FA071FC98BCB77C5B15_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_0 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_1 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_2 = NULL;
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_0 = __this->get_OnComponentAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_1 = V_0;
		V_1 = L_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_2 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)CastclassSealed((RuntimeObject*)L_4, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A_il2cpp_TypeInfo_var));
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** L_5 = __this->get_address_of_OnComponentAdded_0();
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_6 = V_2;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_7 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_8 = InterlockedCompareExchangeImpl<EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *>((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_9 = V_0;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_9) == ((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::remove_OnComponentAdded(Entitas.EntityComponentChanged)
extern "C" IL2CPP_METHOD_ATTR void Entity_remove_OnComponentAdded_m50B58A37A8D0E4E525FC1DDC362F35A11CE2A1CE (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_remove_OnComponentAdded_m50B58A37A8D0E4E525FC1DDC362F35A11CE2A1CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_0 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_1 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_2 = NULL;
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_0 = __this->get_OnComponentAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_1 = V_0;
		V_1 = L_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_2 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)CastclassSealed((RuntimeObject*)L_4, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A_il2cpp_TypeInfo_var));
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** L_5 = __this->get_address_of_OnComponentAdded_0();
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_6 = V_2;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_7 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_8 = InterlockedCompareExchangeImpl<EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *>((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_9 = V_0;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_9) == ((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::add_OnComponentRemoved(Entitas.EntityComponentChanged)
extern "C" IL2CPP_METHOD_ATTR void Entity_add_OnComponentRemoved_mEB6B350C7BABEABAAD960B6DCC1B333355416968 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_add_OnComponentRemoved_mEB6B350C7BABEABAAD960B6DCC1B333355416968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_0 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_1 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_2 = NULL;
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_0 = __this->get_OnComponentRemoved_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_1 = V_0;
		V_1 = L_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_2 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)CastclassSealed((RuntimeObject*)L_4, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A_il2cpp_TypeInfo_var));
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** L_5 = __this->get_address_of_OnComponentRemoved_1();
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_6 = V_2;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_7 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_8 = InterlockedCompareExchangeImpl<EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *>((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_9 = V_0;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_9) == ((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::remove_OnComponentRemoved(Entitas.EntityComponentChanged)
extern "C" IL2CPP_METHOD_ATTR void Entity_remove_OnComponentRemoved_m338E77474CED2C1645AE6E62D08473B3A8F0CE66 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_remove_OnComponentRemoved_m338E77474CED2C1645AE6E62D08473B3A8F0CE66_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_0 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_1 = NULL;
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * V_2 = NULL;
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_0 = __this->get_OnComponentRemoved_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_1 = V_0;
		V_1 = L_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_2 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)CastclassSealed((RuntimeObject*)L_4, EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A_il2cpp_TypeInfo_var));
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** L_5 = __this->get_address_of_OnComponentRemoved_1();
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_6 = V_2;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_7 = V_1;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_8 = InterlockedCompareExchangeImpl<EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *>((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_9 = V_0;
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_9) == ((RuntimeObject*)(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::add_OnComponentReplaced(Entitas.EntityComponentReplaced)
extern "C" IL2CPP_METHOD_ATTR void Entity_add_OnComponentReplaced_m7643566509BC9F1B3B499C1B887C01BF16D703EC (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_add_OnComponentReplaced_m7643566509BC9F1B3B499C1B887C01BF16D703EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * V_0 = NULL;
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * V_1 = NULL;
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * V_2 = NULL;
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_0 = __this->get_OnComponentReplaced_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_1 = V_0;
		V_1 = L_1;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_2 = V_1;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *)CastclassSealed((RuntimeObject*)L_4, EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57_il2cpp_TypeInfo_var));
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 ** L_5 = __this->get_address_of_OnComponentReplaced_2();
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_6 = V_2;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_7 = V_1;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_8 = InterlockedCompareExchangeImpl<EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *>((EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_9 = V_0;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *)L_9) == ((RuntimeObject*)(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::remove_OnComponentReplaced(Entitas.EntityComponentReplaced)
extern "C" IL2CPP_METHOD_ATTR void Entity_remove_OnComponentReplaced_m64A8B6F732CF0D2BDCF2727EFDDEFACBC961F7C5 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_remove_OnComponentReplaced_m64A8B6F732CF0D2BDCF2727EFDDEFACBC961F7C5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * V_0 = NULL;
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * V_1 = NULL;
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * V_2 = NULL;
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_0 = __this->get_OnComponentReplaced_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_1 = V_0;
		V_1 = L_1;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_2 = V_1;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *)CastclassSealed((RuntimeObject*)L_4, EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57_il2cpp_TypeInfo_var));
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 ** L_5 = __this->get_address_of_OnComponentReplaced_2();
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_6 = V_2;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_7 = V_1;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_8 = InterlockedCompareExchangeImpl<EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *>((EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_9 = V_0;
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *)L_9) == ((RuntimeObject*)(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::add_OnEntityReleased(Entitas.EntityEvent)
extern "C" IL2CPP_METHOD_ATTR void Entity_add_OnEntityReleased_m3B8C04DB39AFF7555FE2332869C2E1F129E66CA9 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_add_OnEntityReleased_m3B8C04DB39AFF7555FE2332869C2E1F129E66CA9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_0 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_1 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_2 = NULL;
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_0 = __this->get_OnEntityReleased_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_1 = V_0;
		V_1 = L_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_2 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)CastclassSealed((RuntimeObject*)L_4, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6_il2cpp_TypeInfo_var));
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** L_5 = __this->get_address_of_OnEntityReleased_3();
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_6 = V_2;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_7 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_8 = InterlockedCompareExchangeImpl<EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *>((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_9 = V_0;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_9) == ((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::remove_OnEntityReleased(Entitas.EntityEvent)
extern "C" IL2CPP_METHOD_ATTR void Entity_remove_OnEntityReleased_m81698CA73D8419E6D4D35166599BBF36A0F88471 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_remove_OnEntityReleased_m81698CA73D8419E6D4D35166599BBF36A0F88471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_0 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_1 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_2 = NULL;
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_0 = __this->get_OnEntityReleased_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_1 = V_0;
		V_1 = L_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_2 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)CastclassSealed((RuntimeObject*)L_4, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6_il2cpp_TypeInfo_var));
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** L_5 = __this->get_address_of_OnEntityReleased_3();
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_6 = V_2;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_7 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_8 = InterlockedCompareExchangeImpl<EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *>((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_9 = V_0;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_9) == ((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::add_OnDestroyEntity(Entitas.EntityEvent)
extern "C" IL2CPP_METHOD_ATTR void Entity_add_OnDestroyEntity_m78113890D1F8393C4729151C57655BDCB5665E21 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_add_OnDestroyEntity_m78113890D1F8393C4729151C57655BDCB5665E21_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_0 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_1 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_2 = NULL;
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_0 = __this->get_OnDestroyEntity_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_1 = V_0;
		V_1 = L_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_2 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)CastclassSealed((RuntimeObject*)L_4, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6_il2cpp_TypeInfo_var));
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** L_5 = __this->get_address_of_OnDestroyEntity_4();
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_6 = V_2;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_7 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_8 = InterlockedCompareExchangeImpl<EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *>((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_9 = V_0;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_9) == ((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Entity::remove_OnDestroyEntity(Entitas.EntityEvent)
extern "C" IL2CPP_METHOD_ATTR void Entity_remove_OnDestroyEntity_mEFC765AEF6EA2FD6631028EFB307BB58DD0A72AC (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_remove_OnDestroyEntity_mEFC765AEF6EA2FD6631028EFB307BB58DD0A72AC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_0 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_1 = NULL;
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * V_2 = NULL;
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_0 = __this->get_OnDestroyEntity_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_1 = V_0;
		V_1 = L_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_2 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)CastclassSealed((RuntimeObject*)L_4, EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6_il2cpp_TypeInfo_var));
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** L_5 = __this->get_address_of_OnDestroyEntity_4();
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_6 = V_2;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_7 = V_1;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_8 = InterlockedCompareExchangeImpl<EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *>((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 **)L_5, L_6, L_7);
		V_0 = L_8;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_9 = V_0;
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_9) == ((RuntimeObject*)(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Int32 Entitas.Entity::get_totalComponents()
extern "C" IL2CPP_METHOD_ATTR int32_t Entity_get_totalComponents_mD9BF5DBADC8B58B2F45C0C9575798D166AE78124 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__totalComponents_9();
		return L_0;
	}
}
// System.Int32 Entitas.Entity::get_creationIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t Entity_get_creationIndex_mF12B387C5E3EAF377F82B9589437C8C3963DEC9F (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__creationIndex_7();
		return L_0;
	}
}
// System.Boolean Entitas.Entity::get_isEnabled()
extern "C" IL2CPP_METHOD_ATTR bool Entity_get_isEnabled_m316B1C0EB9096A8C94399C36FEC337DB8CDA343C (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get__isEnabled_8();
		return L_0;
	}
}
// Entitas.IAERC Entitas.Entity::get_aerc()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Entity_get_aerc_mCBF1900C3ABBA5F7033AA4F080008B300871FE9F (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = __this->get__aerc_13();
		return L_0;
	}
}
// System.Void Entitas.Entity::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Entity__ctor_mE4651D95541B7C3E38134B265724325762D842F7 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity__ctor_mE4651D95541B7C3E38134B265724325762D842F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * L_0 = (List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE *)il2cpp_codegen_object_new(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE_il2cpp_TypeInfo_var);
		List_1__ctor_mEF29B452B49300F7653F3A5FC6340E1F01FA889B(L_0, /*hidden argument*/List_1__ctor_mEF29B452B49300F7653F3A5FC6340E1F01FA889B_RuntimeMethod_var);
		__this->set__componentBuffer_5(L_0);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)il2cpp_codegen_object_new(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_il2cpp_TypeInfo_var);
		List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4(L_1, /*hidden argument*/List_1__ctor_mA7F9F92F641CEECFD9D8CFDC667568A05FFD27B4_RuntimeMethod_var);
		__this->set__indexBuffer_6(L_1);
		return;
	}
}
// System.Void Entitas.Entity::Initialize(System.Int32,System.Int32,System.Collections.Generic.Stack`1<Entitas.IComponent>[],Entitas.ContextInfo,Entitas.IAERC)
extern "C" IL2CPP_METHOD_ATTR void Entity_Initialize_mE01A79AC651F7DA6314799654310BBF01680F75E (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___creationIndex0, int32_t ___totalComponents1, Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* ___componentPools2, ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * ___contextInfo3, RuntimeObject* ___aerc4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_Initialize_mE01A79AC651F7DA6314799654310BBF01680F75E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * G_B2_0 = NULL;
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * G_B2_1 = NULL;
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * G_B1_0 = NULL;
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * G_B1_1 = NULL;
	RuntimeObject* G_B4_0 = NULL;
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * G_B4_1 = NULL;
	RuntimeObject* G_B3_0 = NULL;
	Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * G_B3_1 = NULL;
	{
		int32_t L_0 = ___creationIndex0;
		Entity_Reactivate_mB716527C2222DE67B7062E9B5AA379ABE11FAB2F(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___totalComponents1;
		__this->set__totalComponents_9(L_1);
		int32_t L_2 = ___totalComponents1;
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_3 = (IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B*)SZArrayNew(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B_il2cpp_TypeInfo_var, (uint32_t)L_2);
		__this->set__components_10(L_3);
		Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* L_4 = ___componentPools2;
		__this->set__componentPools_11(L_4);
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_5 = ___contextInfo3;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_6 = L_5;
		G_B1_0 = L_6;
		G_B1_1 = __this;
		if (L_6)
		{
			G_B2_0 = L_6;
			G_B2_1 = __this;
			goto IL_002e;
		}
	}
	{
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_7 = Entity_createDefaultContextInfo_m2048E85A3BDC948AAAA3452EEF56475215572790(__this, /*hidden argument*/NULL);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
	}

IL_002e:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__contextInfo_12(G_B2_0);
		RuntimeObject* L_8 = ___aerc4;
		RuntimeObject* L_9 = L_8;
		G_B3_0 = L_9;
		G_B3_1 = __this;
		if (L_9)
		{
			G_B4_0 = L_9;
			G_B4_1 = __this;
			goto IL_0040;
		}
	}
	{
		SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * L_10 = (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 *)il2cpp_codegen_object_new(SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30_il2cpp_TypeInfo_var);
		SafeAERC__ctor_mC8216DC64624BDCEB24698DF7ADC85F2A26FDCAB(L_10, __this, /*hidden argument*/NULL);
		G_B4_0 = ((RuntimeObject*)(L_10));
		G_B4_1 = G_B3_1;
	}

IL_0040:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__aerc_13(G_B4_0);
		return;
	}
}
// Entitas.ContextInfo Entitas.Entity::createDefaultContextInfo()
extern "C" IL2CPP_METHOD_ATTR ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * Entity_createDefaultContextInfo_m2048E85A3BDC948AAAA3452EEF56475215572790 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_createDefaultContextInfo_m2048E85A3BDC948AAAA3452EEF56475215572790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Entity_get_totalComponents_mD9BF5DBADC8B58B2F45C0C9575798D166AE78124(__this, /*hidden argument*/NULL);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_001e;
	}

IL_0010:
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = V_0;
		int32_t L_3 = V_1;
		String_t* L_4 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&V_1), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (String_t*)L_4);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_001e:
	{
		int32_t L_6 = V_1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = V_0;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_9 = (ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED *)il2cpp_codegen_object_new(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED_il2cpp_TypeInfo_var);
		ContextInfo__ctor_m5EC411BF37BB763F2BB4476C65DDCFBB672DF0E8(L_9, _stringLiteral44E0A393E108EFD549601652D0713A316EE64399, L_8, (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)NULL, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Entitas.Entity::Reactivate(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Entity_Reactivate_mB716527C2222DE67B7062E9B5AA379ABE11FAB2F (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___creationIndex0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___creationIndex0;
		__this->set__creationIndex_7(L_0);
		__this->set__isEnabled_8((bool)1);
		return;
	}
}
// System.Void Entitas.Entity::AddComponent(System.Int32,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, RuntimeObject* ___component1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__isEnabled_8();
		if (L_0)
		{
			goto IL_0045;
		}
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral876322E252252218BA31C7C5A2685D0A3638BA7F);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral876322E252252218BA31C7C5A2685D0A3638BA7F);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_2;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_4 = __this->get__contextInfo_12();
		NullCheck(L_4);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_4->get_componentNames_1();
		int32_t L_6 = ___index0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_8);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_3;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral60534582FC064596609725F08086D4D4C0B1EE94);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral60534582FC064596609725F08086D4D4C0B1EE94);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, __this);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_12 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_11, /*hidden argument*/NULL);
		EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 * L_13 = (EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 *)il2cpp_codegen_object_new(EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11_il2cpp_TypeInfo_var);
		EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313(L_13, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, NULL, Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7_RuntimeMethod_var);
	}

IL_0045:
	{
		int32_t L_14 = ___index0;
		bool L_15 = Entity_HasComponent_m03AEC990CB4263D7419B6CCD6C06416FF02D7711(__this, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_16 = ___index0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral876322E252252218BA31C7C5A2685D0A3638BA7F);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral876322E252252218BA31C7C5A2685D0A3638BA7F);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_19 = L_18;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_20 = __this->get__contextInfo_12();
		NullCheck(L_20);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = L_20->get_componentNames_1();
		int32_t L_22 = ___index0;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		String_t* L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_24);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_25 = L_19;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral60534582FC064596609725F08086D4D4C0B1EE94);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral60534582FC064596609725F08086D4D4C0B1EE94);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, __this);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_28 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_27, /*hidden argument*/NULL);
		EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB * L_29 = (EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB *)il2cpp_codegen_object_new(EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB_il2cpp_TypeInfo_var);
		EntityAlreadyHasComponentException__ctor_m19F540EA19E143D39F2366773AECB8E831D1182E(L_29, L_16, L_28, _stringLiteral10DCA3B1CB1BBE244825A82F943245CCD0DBD298, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29, NULL, Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7_RuntimeMethod_var);
	}

IL_0091:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_30 = __this->get__components_10();
		int32_t L_31 = ___index0;
		RuntimeObject* L_32 = ___component1;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_31), (RuntimeObject*)L_32);
		__this->set__componentsCache_14((IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B*)NULL);
		__this->set__componentIndicesCache_15((Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)NULL);
		__this->set__toStringCache_16((String_t*)NULL);
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_33 = __this->get_OnComponentAdded_0();
		if (!L_33)
		{
			goto IL_00c5;
		}
	}
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_34 = __this->get_OnComponentAdded_0();
		int32_t L_35 = ___index0;
		RuntimeObject* L_36 = ___component1;
		NullCheck(L_34);
		EntityComponentChanged_Invoke_mB3A0E91AE760C571AF6E5AAD6EAD388B59628D49(L_34, __this, L_35, L_36, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Void Entitas.Entity::RemoveComponent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Entity_RemoveComponent_m15668325C50C7F44C6FA106733C71EB0A678AB4B (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_RemoveComponent_m15668325C50C7F44C6FA106733C71EB0A678AB4B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__isEnabled_8();
		if (L_0)
		{
			goto IL_0045;
		}
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteralF472D17A616AE4E9F937C0C1CF4AFC000038C596);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralF472D17A616AE4E9F937C0C1CF4AFC000038C596);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_2;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_4 = __this->get__contextInfo_12();
		NullCheck(L_4);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_4->get_componentNames_1();
		int32_t L_6 = ___index0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_8);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_3;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteralAB66D26BBBCB986D88182E749B51F0AB7AFEAA37);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteralAB66D26BBBCB986D88182E749B51F0AB7AFEAA37);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, __this);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_12 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_11, /*hidden argument*/NULL);
		EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 * L_13 = (EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 *)il2cpp_codegen_object_new(EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11_il2cpp_TypeInfo_var);
		EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313(L_13, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, NULL, Entity_RemoveComponent_m15668325C50C7F44C6FA106733C71EB0A678AB4B_RuntimeMethod_var);
	}

IL_0045:
	{
		int32_t L_14 = ___index0;
		bool L_15 = Entity_HasComponent_m03AEC990CB4263D7419B6CCD6C06416FF02D7711(__this, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_16 = ___index0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteralF472D17A616AE4E9F937C0C1CF4AFC000038C596);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralF472D17A616AE4E9F937C0C1CF4AFC000038C596);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_19 = L_18;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_20 = __this->get__contextInfo_12();
		NullCheck(L_20);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = L_20->get_componentNames_1();
		int32_t L_22 = ___index0;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		String_t* L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_24);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_25 = L_19;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteralAB66D26BBBCB986D88182E749B51F0AB7AFEAA37);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteralAB66D26BBBCB986D88182E749B51F0AB7AFEAA37);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, __this);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_27 = L_26;
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_28 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_27, /*hidden argument*/NULL);
		EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C * L_29 = (EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C *)il2cpp_codegen_object_new(EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C_il2cpp_TypeInfo_var);
		EntityDoesNotHaveComponentException__ctor_m15C877635E6695C5DF57F1A45D28A91CE24EDB65(L_29, L_16, L_28, _stringLiteral05CA1CA9FDDFFA322568053773207EFDDBBCA308, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29, NULL, Entity_RemoveComponent_m15668325C50C7F44C6FA106733C71EB0A678AB4B_RuntimeMethod_var);
	}

IL_0091:
	{
		int32_t L_30 = ___index0;
		Entity_replaceComponent_mC4CA5CCC0D1EDD836ED494D8DEEE470DB4B1A1C7(__this, L_30, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Entitas.Entity::ReplaceComponent(System.Int32,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void Entity_ReplaceComponent_mA8B52E87C7EB944E6CF811218F94B1D9CCBE9D6C (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, RuntimeObject* ___component1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_ReplaceComponent_mA8B52E87C7EB944E6CF811218F94B1D9CCBE9D6C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__isEnabled_8();
		if (L_0)
		{
			goto IL_0045;
		}
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral73BB7503BA2D3D447953641B975197DBAD0B2EA8);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral73BB7503BA2D3D447953641B975197DBAD0B2EA8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_2;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_4 = __this->get__contextInfo_12();
		NullCheck(L_4);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = L_4->get_componentNames_1();
		int32_t L_6 = ___index0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_8);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_3;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral204D98988F3FEA6ACD89D4D7B7A64988BDC544F9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral204D98988F3FEA6ACD89D4D7B7A64988BDC544F9);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, __this);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_12 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_11, /*hidden argument*/NULL);
		EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 * L_13 = (EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 *)il2cpp_codegen_object_new(EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11_il2cpp_TypeInfo_var);
		EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313(L_13, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, NULL, Entity_ReplaceComponent_mA8B52E87C7EB944E6CF811218F94B1D9CCBE9D6C_RuntimeMethod_var);
	}

IL_0045:
	{
		int32_t L_14 = ___index0;
		bool L_15 = Entity_HasComponent_m03AEC990CB4263D7419B6CCD6C06416FF02D7711(__this, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_16 = ___index0;
		RuntimeObject* L_17 = ___component1;
		Entity_replaceComponent_mC4CA5CCC0D1EDD836ED494D8DEEE470DB4B1A1C7(__this, L_16, L_17, /*hidden argument*/NULL);
		return;
	}

IL_0057:
	{
		RuntimeObject* L_18 = ___component1;
		if (!L_18)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_19 = ___index0;
		RuntimeObject* L_20 = ___component1;
		Entity_AddComponent_m0CFD48EBEECB93DA4407AED5C2177E1933657FE7(__this, L_19, L_20, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void Entitas.Entity::replaceComponent(System.Int32,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void Entity_replaceComponent_mC4CA5CCC0D1EDD836ED494D8DEEE470DB4B1A1C7 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, RuntimeObject* ___replacement1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_replaceComponent_mC4CA5CCC0D1EDD836ED494D8DEEE470DB4B1A1C7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_0 = __this->get__components_10();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RuntimeObject* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		RuntimeObject* L_4 = ___replacement1;
		RuntimeObject* L_5 = V_0;
		if ((((RuntimeObject*)(RuntimeObject*)L_4) == ((RuntimeObject*)(RuntimeObject*)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_6 = __this->get__components_10();
		int32_t L_7 = ___index0;
		RuntimeObject* L_8 = ___replacement1;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		__this->set__componentsCache_14((IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B*)NULL);
		RuntimeObject* L_9 = ___replacement1;
		if (!L_9)
		{
			goto IL_0039;
		}
	}
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_10 = __this->get_OnComponentReplaced_2();
		if (!L_10)
		{
			goto IL_005d;
		}
	}
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_11 = __this->get_OnComponentReplaced_2();
		int32_t L_12 = ___index0;
		RuntimeObject* L_13 = V_0;
		RuntimeObject* L_14 = ___replacement1;
		NullCheck(L_11);
		EntityComponentReplaced_Invoke_mAE15CE1DDD539E5CA7C66B3BD5E5078C99FD5488(L_11, __this, L_12, L_13, L_14, /*hidden argument*/NULL);
		goto IL_005d;
	}

IL_0039:
	{
		__this->set__componentIndicesCache_15((Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)NULL);
		__this->set__toStringCache_16((String_t*)NULL);
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_15 = __this->get_OnComponentRemoved_1();
		if (!L_15)
		{
			goto IL_005d;
		}
	}
	{
		EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * L_16 = __this->get_OnComponentRemoved_1();
		int32_t L_17 = ___index0;
		RuntimeObject* L_18 = V_0;
		NullCheck(L_16);
		EntityComponentChanged_Invoke_mB3A0E91AE760C571AF6E5AAD6EAD388B59628D49(L_16, __this, L_17, L_18, /*hidden argument*/NULL);
	}

IL_005d:
	{
		int32_t L_19 = ___index0;
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_20 = Entity_GetComponentPool_m09EFD65E02F8FAC721807E1640C7B5AE10AA8957(__this, L_19, /*hidden argument*/NULL);
		RuntimeObject* L_21 = V_0;
		NullCheck(L_20);
		Stack_1_Push_m90E9E0ABAB946873DC74924DAC696403DA669B84(L_20, L_21, /*hidden argument*/Stack_1_Push_m90E9E0ABAB946873DC74924DAC696403DA669B84_RuntimeMethod_var);
		return;
	}

IL_006b:
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_22 = __this->get_OnComponentReplaced_2();
		if (!L_22)
		{
			goto IL_0082;
		}
	}
	{
		EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * L_23 = __this->get_OnComponentReplaced_2();
		int32_t L_24 = ___index0;
		RuntimeObject* L_25 = V_0;
		RuntimeObject* L_26 = ___replacement1;
		NullCheck(L_23);
		EntityComponentReplaced_Invoke_mAE15CE1DDD539E5CA7C66B3BD5E5078C99FD5488(L_23, __this, L_24, L_25, L_26, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// Entitas.IComponent Entitas.Entity::GetComponent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Entity_GetComponent_m65E4F66188239D9C7CFC1B264DDF49669881189B (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_GetComponent_m65E4F66188239D9C7CFC1B264DDF49669881189B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		bool L_1 = Entity_HasComponent_m03AEC990CB4263D7419B6CCD6C06416FF02D7711(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_2 = ___index0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralE3C7E409DA731FE17ED5D291402B93A5030C5875);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralE3C7E409DA731FE17ED5D291402B93A5030C5875);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * L_6 = __this->get__contextInfo_12();
		NullCheck(L_6);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_7 = L_6->get_componentNames_1();
		int32_t L_8 = ___index0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		String_t* L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_10);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_5;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteralAB66D26BBBCB986D88182E749B51F0AB7AFEAA37);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteralAB66D26BBBCB986D88182E749B51F0AB7AFEAA37);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, __this);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_14 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_13, /*hidden argument*/NULL);
		EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C * L_15 = (EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C *)il2cpp_codegen_object_new(EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C_il2cpp_TypeInfo_var);
		EntityDoesNotHaveComponentException__ctor_m15C877635E6695C5DF57F1A45D28A91CE24EDB65(L_15, L_2, L_14, _stringLiteral89EF6D88E54CD9F1E1A7D7F7AF51C6CDCBDA6755, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, NULL, Entity_GetComponent_m65E4F66188239D9C7CFC1B264DDF49669881189B_RuntimeMethod_var);
	}

IL_004c:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_16 = __this->get__components_10();
		int32_t L_17 = ___index0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		RuntimeObject* L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		return L_19;
	}
}
// Entitas.IComponent[] Entitas.Entity::GetComponents()
extern "C" IL2CPP_METHOD_ATTR IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* Entity_GetComponents_m4B0440F78D4D6148DF032841238C06BEA38E6532 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_GetComponents_m4B0440F78D4D6148DF032841238C06BEA38E6532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_0 = __this->get__componentsCache_14();
		if (L_0)
		{
			goto IL_004f;
		}
	}
	{
		V_0 = 0;
		goto IL_0028;
	}

IL_000c:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_1 = __this->get__components_10();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		RuntimeObject* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		RuntimeObject* L_5 = V_1;
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * L_6 = __this->get__componentBuffer_5();
		RuntimeObject* L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m88E063CFB9F1ED228CAB4ECCFC08E5BF80CF234D(L_6, L_7, /*hidden argument*/List_1_Add_m88E063CFB9F1ED228CAB4ECCFC08E5BF80CF234D_RuntimeMethod_var);
	}

IL_0024:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_0;
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_10 = __this->get__components_10();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_000c;
		}
	}
	{
		List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * L_11 = __this->get__componentBuffer_5();
		NullCheck(L_11);
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_12 = List_1_ToArray_m7BEC5A680E5ED7915147F1C2D158922831DA59D2(L_11, /*hidden argument*/List_1_ToArray_m7BEC5A680E5ED7915147F1C2D158922831DA59D2_RuntimeMethod_var);
		__this->set__componentsCache_14(L_12);
		List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * L_13 = __this->get__componentBuffer_5();
		NullCheck(L_13);
		List_1_Clear_m786D360E04623387E0CB0FCC17A815D1ED1CCB74(L_13, /*hidden argument*/List_1_Clear_m786D360E04623387E0CB0FCC17A815D1ED1CCB74_RuntimeMethod_var);
	}

IL_004f:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_14 = __this->get__componentsCache_14();
		return L_14;
	}
}
// System.Boolean Entitas.Entity::HasComponent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Entity_HasComponent_m03AEC990CB4263D7419B6CCD6C06416FF02D7711 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_0 = __this->get__components_10();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RuntimeObject* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return (bool)((!(((RuntimeObject*)(RuntimeObject*)L_3) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Boolean Entitas.Entity::HasComponents(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR bool Entity_HasComponents_mFC3E4350335C669517BE058F9118F18FA7DA4613 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___indices0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0016;
	}

IL_0004:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_0 = __this->get__components_10();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_1 = ___indices0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_0);
		int32_t L_5 = L_4;
		RuntimeObject* L_6 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		if (L_6)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0016:
	{
		int32_t L_8 = V_0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_9 = ___indices0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean Entitas.Entity::HasAnyComponent(System.Int32[])
extern "C" IL2CPP_METHOD_ATTR bool Entity_HasAnyComponent_m9A0D295E787AD56EC08EEBFE67E5002FBCBE2BA6 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___indices0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0016;
	}

IL_0004:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_0 = __this->get__components_10();
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_1 = ___indices0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int32_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_0);
		int32_t L_5 = L_4;
		RuntimeObject* L_6 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		if (!L_6)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)1;
	}

IL_0012:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0016:
	{
		int32_t L_8 = V_0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_9 = ___indices0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void Entitas.Entity::RemoveAllComponents()
extern "C" IL2CPP_METHOD_ATTR void Entity_RemoveAllComponents_mAD49966DC9C3DAC58F6D49A27FE5664D084E35CC (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		__this->set__toStringCache_16((String_t*)NULL);
		V_0 = 0;
		goto IL_0021;
	}

IL_000b:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_0 = __this->get__components_10();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		RuntimeObject* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_4 = V_0;
		Entity_replaceComponent_mC4CA5CCC0D1EDD836ED494D8DEEE470DB4B1A1C7(__this, L_4, (RuntimeObject*)NULL, /*hidden argument*/NULL);
	}

IL_001d:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_6 = V_0;
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_7 = __this->get__components_10();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.Stack`1<Entitas.IComponent> Entitas.Entity::GetComponentPool(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * Entity_GetComponentPool_m09EFD65E02F8FAC721807E1640C7B5AE10AA8957 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_GetComponentPool_m09EFD65E02F8FAC721807E1640C7B5AE10AA8957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * V_0 = NULL;
	{
		Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* L_0 = __this->get__componentPools_11();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_3 = (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 *)(L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_4 = V_0;
		if (L_4)
		{
			goto IL_001b;
		}
	}
	{
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_5 = (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 *)il2cpp_codegen_object_new(Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5_il2cpp_TypeInfo_var);
		Stack_1__ctor_mD6EE87D90C123FC7ED2D0BE5B6CADE1704BE3A09(L_5, /*hidden argument*/Stack_1__ctor_mD6EE87D90C123FC7ED2D0BE5B6CADE1704BE3A09_RuntimeMethod_var);
		V_0 = L_5;
		Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* L_6 = __this->get__componentPools_11();
		int32_t L_7 = ___index0;
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_8 = V_0;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 *)L_8);
	}

IL_001b:
	{
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_9 = V_0;
		return L_9;
	}
}
// Entitas.IComponent Entitas.Entity::CreateComponent(System.Int32,System.Type)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Entity_CreateComponent_mBD244433F9F7931E685D923FEEB0904E07F90059 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, int32_t ___index0, Type_t * ___type1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_CreateComponent_mBD244433F9F7931E685D923FEEB0904E07F90059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_1 = Entity_GetComponentPool_m09EFD65E02F8FAC721807E1640C7B5AE10AA8957(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = Stack_1_get_Count_m89BAF285F607EF62CC860B779D8F40495BE24D8A(L_2, /*hidden argument*/Stack_1_get_Count_m89BAF285F607EF62CC860B779D8F40495BE24D8A_RuntimeMethod_var);
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		Type_t * L_4 = ___type1;
		RuntimeObject * L_5 = Activator_CreateInstance_mD06EE47879F606317C6DA91FB63E678CABAC6A16(L_4, /*hidden argument*/NULL);
		return ((RuntimeObject*)Castclass((RuntimeObject*)L_5, IComponent_tE1E43FF3BDF6A6960F83258783FE4CEADD8B85DC_il2cpp_TypeInfo_var));
	}

IL_001d:
	{
		Stack_1_t0E3C970EB88AE8DB9C5463C7C8BE49C3A28B33E5 * L_6 = V_0;
		NullCheck(L_6);
		RuntimeObject* L_7 = Stack_1_Pop_m252F7AF64D8E273A7A921A037342F8BA121F8C93(L_6, /*hidden argument*/Stack_1_Pop_m252F7AF64D8E273A7A921A037342F8BA121F8C93_RuntimeMethod_var);
		return L_7;
	}
}
// System.Int32 Entitas.Entity::get_retainCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Entity_get_retainCount_m6BCD89D9543984D57AE4B1159580AA529790D8F8 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_get_retainCount_m6BCD89D9543984D57AE4B1159580AA529790D8F8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get__aerc_13();
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Entitas.IAERC::get_retainCount() */, IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void Entitas.Entity::Retain(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Entity_Retain_m0DA441090A622236AC87E40ABEE0EAEFF05EA84A (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, RuntimeObject * ___owner0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_Retain_m0DA441090A622236AC87E40ABEE0EAEFF05EA84A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get__aerc_13();
		RuntimeObject * L_1 = ___owner0;
		NullCheck(L_0);
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(1 /* System.Void Entitas.IAERC::Retain(System.Object) */, IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void Entitas.Entity::Release(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Entity_Release_m081A6C423132BCE072791579FE78FE9548AA9ACC (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, RuntimeObject * ___owner0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_Release_m081A6C423132BCE072791579FE78FE9548AA9ACC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get__aerc_13();
		RuntimeObject * L_1 = ___owner0;
		NullCheck(L_0);
		InterfaceActionInvoker1< RuntimeObject * >::Invoke(2 /* System.Void Entitas.IAERC::Release(System.Object) */, IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0_il2cpp_TypeInfo_var, L_0, L_1);
		RuntimeObject* L_2 = __this->get__aerc_13();
		NullCheck(L_2);
		int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Entitas.IAERC::get_retainCount() */, IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0_il2cpp_TypeInfo_var, L_2);
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_4 = __this->get_OnEntityReleased_3();
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_5 = __this->get_OnEntityReleased_3();
		NullCheck(L_5);
		EntityEvent_Invoke_m48C29E9B35B6C7A22AAE173C82482DB1307FE905(L_5, __this, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void Entitas.Entity::Destroy()
extern "C" IL2CPP_METHOD_ATTR void Entity_Destroy_m4DDC656E3B45AA164F7A0220BB66C4E66D6B6F04 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_Destroy_m4DDC656E3B45AA164F7A0220BB66C4E66D6B6F04_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__isEnabled_8();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_1 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteral171503CAE2A9F0079A6FDBFE8E5FBD88806C33CA, __this, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D, /*hidden argument*/NULL);
		EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 * L_2 = (EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 *)il2cpp_codegen_object_new(EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11_il2cpp_TypeInfo_var);
		EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, Entity_Destroy_m4DDC656E3B45AA164F7A0220BB66C4E66D6B6F04_RuntimeMethod_var);
	}

IL_001e:
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_3 = __this->get_OnDestroyEntity_4();
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * L_4 = __this->get_OnDestroyEntity_4();
		NullCheck(L_4);
		EntityEvent_Invoke_m48C29E9B35B6C7A22AAE173C82482DB1307FE905(L_4, __this, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Entitas.Entity::InternalDestroy()
extern "C" IL2CPP_METHOD_ATTR void Entity_InternalDestroy_m9E47D5B879C64160AF4B864671D1B13111CAC019 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	{
		__this->set__isEnabled_8((bool)0);
		Entity_RemoveAllComponents_mAD49966DC9C3DAC58F6D49A27FE5664D084E35CC(__this, /*hidden argument*/NULL);
		__this->set_OnComponentAdded_0((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)NULL);
		__this->set_OnComponentReplaced_2((EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 *)NULL);
		__this->set_OnComponentRemoved_1((EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A *)NULL);
		__this->set_OnDestroyEntity_4((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)NULL);
		return;
	}
}
// System.Void Entitas.Entity::RemoveAllOnEntityReleasedHandlers()
extern "C" IL2CPP_METHOD_ATTR void Entity_RemoveAllOnEntityReleasedHandlers_m35584D886835E86789D4141F9534B072C1BA95AE (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	{
		__this->set_OnEntityReleased_3((EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 *)NULL);
		return;
	}
}
// System.String Entitas.Entity::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Entity_ToString_mA67A8FED351F17559E633EE71BEA2474E5B81077 (Entity_tB86FED06A87B5FEA836FF73B89D5168789557783 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Entity_ToString_mA67A8FED351F17559E633EE71BEA2474E5B81077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RuntimeObject* V_3 = NULL;
	{
		String_t* L_0 = __this->get__toStringCache_16();
		if (L_0)
		{
			goto IL_00c6;
		}
	}
	{
		StringBuilder_t * L_1 = __this->get__toStringBuilder_17();
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t * L_2 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_2, /*hidden argument*/NULL);
		__this->set__toStringBuilder_17(L_2);
	}

IL_001e:
	{
		StringBuilder_t * L_3 = __this->get__toStringBuilder_17();
		NullCheck(L_3);
		StringBuilder_set_Length_m84AF318230AE5C3D0D48F1CE7C2170F6F5C19F5B(L_3, 0, /*hidden argument*/NULL);
		StringBuilder_t * L_4 = __this->get__toStringBuilder_17();
		NullCheck(L_4);
		StringBuilder_t * L_5 = StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_4, _stringLiteralEDD998E00DFD4094167A7ED68E52AAB09DA5CE23, /*hidden argument*/NULL);
		int32_t L_6 = __this->get__creationIndex_7();
		NullCheck(L_5);
		StringBuilder_t * L_7 = StringBuilder_Append_m85874CFF9E4B152DB2A91936C6F2CA3E9B1EFF84(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_7, _stringLiteral28ED3A797DA3C48C309A4EF792147F3C56CFEC40, /*hidden argument*/NULL);
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_8 = Entity_GetComponents_m4B0440F78D4D6148DF032841238C06BEA38E6532(__this, /*hidden argument*/NULL);
		V_0 = L_8;
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_9 = V_0;
		NullCheck(L_9);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))), (int32_t)1));
		V_2 = 0;
		goto IL_009e;
	}

IL_0061:
	{
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		RuntimeObject* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_3 = L_13;
		RuntimeObject* L_14 = V_3;
		NullCheck(L_14);
		Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_14, /*hidden argument*/NULL);
		__this->set__toStringCache_16((String_t*)NULL);
		StringBuilder_t * L_15 = __this->get__toStringBuilder_17();
		RuntimeObject* L_16 = V_3;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		NullCheck(L_15);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_15, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) >= ((int32_t)L_19)))
		{
			goto IL_009a;
		}
	}
	{
		StringBuilder_t * L_20 = __this->get__toStringBuilder_17();
		NullCheck(L_20);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_20, _stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46, /*hidden argument*/NULL);
	}

IL_009a:
	{
		int32_t L_21 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_009e:
	{
		int32_t L_22 = V_2;
		IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* L_23 = V_0;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_0061;
		}
	}
	{
		StringBuilder_t * L_24 = __this->get__toStringBuilder_17();
		NullCheck(L_24);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_24, _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A, /*hidden argument*/NULL);
		StringBuilder_t * L_25 = __this->get__toStringBuilder_17();
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		__this->set__toStringCache_16(L_26);
	}

IL_00c6:
	{
		String_t* L_27 = __this->get__toStringCache_16();
		return L_27;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityAlreadyHasComponentException::.ctor(System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityAlreadyHasComponentException__ctor_m19F540EA19E143D39F2366773AECB8E831D1182E (EntityAlreadyHasComponentException_t30825CB8B4D13727D4ABF3EBB518E8BC590D4BFB * __this, int32_t ___index0, String_t* ___message1, String_t* ___hint2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityAlreadyHasComponentException__ctor_m19F540EA19E143D39F2366773AECB8E831D1182E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		String_t* L_2 = ___message1;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralF8D7FCEE6F5BC8DFA3075A2BC78D27F3E63D099B);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteralF8D7FCEE6F5BC8DFA3075A2BC78D27F3E63D099B);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		int32_t L_5 = ___index0;
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_8 = L_4;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_9 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_8, /*hidden argument*/NULL);
		String_t* L_10 = ___hint2;
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityComponentChanged::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentChanged__ctor_m1CE40BA3C77489FB9AEB85292A89828B9B8EFBA0 (EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Entitas.EntityComponentChanged::Invoke(Entitas.IEntity,System.Int32,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentChanged_Invoke_mB3A0E91AE760C571AF6E5AAD6EAD388B59628D49 (EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * __this, RuntimeObject* ___entity0, int32_t ___index1, RuntimeObject* ___component2, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 3)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___component2, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___component2, targetMethod);
				}
			}
			else if (___parameterCount != 3)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker2< int32_t, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___component2);
							else
								GenericVirtActionInvoker2< int32_t, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___component2);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker2< int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___entity0, ___index1, ___component2);
							else
								VirtActionInvoker2< int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___entity0, ___index1, ___component2);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___component2, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___component2, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___component2);
							else
								GenericVirtActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___component2);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___entity0, ___index1, ___component2);
							else
								VirtActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___entity0, ___index1, ___component2);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___component2, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___component2, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___component2, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker2< int32_t, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___component2);
						else
							GenericVirtActionInvoker2< int32_t, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___component2);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker2< int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___entity0, ___index1, ___component2);
						else
							VirtActionInvoker2< int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___entity0, ___index1, ___component2);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___component2, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___component2, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___component2);
						else
							GenericVirtActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___component2);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___entity0, ___index1, ___component2);
						else
							VirtActionInvoker3< RuntimeObject*, int32_t, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___entity0, ___index1, ___component2);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___component2, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Entitas.EntityComponentChanged::BeginInvoke(Entitas.IEntity,System.Int32,Entitas.IComponent,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EntityComponentChanged_BeginInvoke_mD3E2C3818F65EC85DAA713E73DBB93E190FEBCCC (EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * __this, RuntimeObject* ___entity0, int32_t ___index1, RuntimeObject* ___component2, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityComponentChanged_BeginInvoke_mD3E2C3818F65EC85DAA713E73DBB93E190FEBCCC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___entity0;
	__d_args[1] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___index1);
	__d_args[2] = ___component2;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void Entitas.EntityComponentChanged::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentChanged_EndInvoke_m1804F21AD94198EBAD1CDD605C1AD1B3327BB485 (EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityComponentReplaced::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentReplaced__ctor_m3E633104757045B28E77E27A64FC9280A564DF25 (EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Entitas.EntityComponentReplaced::Invoke(Entitas.IEntity,System.Int32,Entitas.IComponent,Entitas.IComponent)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentReplaced_Invoke_mAE15CE1DDD539E5CA7C66B3BD5E5078C99FD5488 (EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * __this, RuntimeObject* ___entity0, int32_t ___index1, RuntimeObject* ___previousComponent2, RuntimeObject* ___newComponent3, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 4)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
				}
			}
			else if (___parameterCount != 4)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
							else
								GenericVirtActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___entity0, ___index1, ___previousComponent2, ___newComponent3);
							else
								VirtActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
							else
								GenericVirtActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
							else
								VirtActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 4)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
			}
		}
		else if (___parameterCount != 4)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						else
							GenericVirtActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						else
							VirtActionInvoker3< int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___entity0, ___index1, ___previousComponent2, ___newComponent3);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						else
							GenericVirtActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
						else
							VirtActionInvoker4< RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, int32_t, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, ___index1, ___previousComponent2, ___newComponent3, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Entitas.EntityComponentReplaced::BeginInvoke(Entitas.IEntity,System.Int32,Entitas.IComponent,Entitas.IComponent,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EntityComponentReplaced_BeginInvoke_m9399F0F64C4C28659BA1C766DEEAF62246E62962 (EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * __this, RuntimeObject* ___entity0, int32_t ___index1, RuntimeObject* ___previousComponent2, RuntimeObject* ___newComponent3, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback4, RuntimeObject * ___object5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityComponentReplaced_BeginInvoke_m9399F0F64C4C28659BA1C766DEEAF62246E62962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___entity0;
	__d_args[1] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___index1);
	__d_args[2] = ___previousComponent2;
	__d_args[3] = ___newComponent3;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback4, (RuntimeObject*)___object5);
}
// System.Void Entitas.EntityComponentReplaced::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void EntityComponentReplaced_EndInvoke_m89CA7F267EB96D3DEE6D62BB4F7AA9AEE25B18C3 (EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityDoesNotHaveComponentException::.ctor(System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityDoesNotHaveComponentException__ctor_m15C877635E6695C5DF57F1A45D28A91CE24EDB65 (EntityDoesNotHaveComponentException_tE224D88B637576CC426C2510B406781E58AE209C * __this, int32_t ___index0, String_t* ___message1, String_t* ___hint2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityDoesNotHaveComponentException__ctor_m15C877635E6695C5DF57F1A45D28A91CE24EDB65_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		String_t* L_2 = ___message1;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteralFBFD64C7AE07B85E8C75803841C6E669258128A8);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteralFBFD64C7AE07B85E8C75803841C6E669258128A8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		int32_t L_5 = ___index0;
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_8 = L_4;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)_stringLiteral0AB8318ACAF6E678DD02E2B5C343ED41111B393D);
		String_t* L_9 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_8, /*hidden argument*/NULL);
		String_t* L_10 = ___hint2;
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityEvent::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void EntityEvent__ctor_mAF2EF8C3AB83533BE26F3708926AD4131C55C84F (EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Entitas.EntityEvent::Invoke(Entitas.IEntity)
extern "C" IL2CPP_METHOD_ATTR void EntityEvent_Invoke_m48C29E9B35B6C7A22AAE173C82482DB1307FE905 (EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * __this, RuntimeObject* ___entity0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			if (!il2cpp_codegen_method_is_virtual(targetMethod))
			{
				il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			}
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
			if (___methodIsStatic)
			{
				if (___parameterCount == 1)
				{
					// open
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___entity0, targetMethod);
				}
				else
				{
					// closed
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, targetMethod);
				}
			}
			else if (___parameterCount != 1)
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, ___entity0);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, ___entity0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___entity0);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___entity0);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___entity0, targetMethod);
				}
			}
			else
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (targetThis == NULL)
						{
							typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
							((FunctionPointerType)targetMethodPointer)(___entity0, targetMethod);
						}
						else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0);
							else
								GenericVirtActionInvoker1< RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___entity0);
							else
								VirtActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___entity0);
						}
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, targetMethod);
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___entity0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, ___entity0);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, ___entity0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___entity0);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___entity0);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___entity0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (targetThis == NULL)
					{
						typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___entity0, targetMethod);
					}
					else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0);
						else
							GenericVirtActionInvoker1< RuntimeObject* >::Invoke(targetMethod, targetThis, ___entity0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___entity0);
						else
							VirtActionInvoker1< RuntimeObject* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___entity0);
					}
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___entity0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Entitas.EntityEvent::BeginInvoke(Entitas.IEntity,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* EntityEvent_BeginInvoke_m490D3390F4FB535DAA83B1FA269FD6CDCAAC3855 (EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * __this, RuntimeObject* ___entity0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___entity0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Entitas.EntityEvent::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void EntityEvent_EndInvoke_m516E740F64A82B07B7587F6FD2632025766A0403 (EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityIndexException::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityIndexException__ctor_m64DE487CC7F62D041B700D805881DE1F15C17FC5 (EntityIndexException_t6834F599460536EA81AFA69CA928AA2022334EF4 * __this, String_t* ___message0, String_t* ___hint1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___hint1;
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityIsAlreadyRetainedByOwnerException::.ctor(Entitas.IEntity,System.Object)
extern "C" IL2CPP_METHOD_ATTR void EntityIsAlreadyRetainedByOwnerException__ctor_mFBDE6BDC3650783F8C26EA2EB82F8579C7278784 (EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783 * __this, RuntimeObject* ___entity0, RuntimeObject * ___owner1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityIsAlreadyRetainedByOwnerException__ctor_mFBDE6BDC3650783F8C26EA2EB82F8579C7278784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		RuntimeObject * L_3 = ___owner1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral1059E91D018E70A11B850239FDD5D56E56B7EDFA);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1059E91D018E70A11B850239FDD5D56E56B7EDFA);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		RuntimeObject* L_6 = ___entity0;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2C8B7D70BF0882F9D0D9E2D8489B0C3B616E0E9A);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral2C8B7D70BF0882F9D0D9E2D8489B0C3B616E0E9A);
		String_t* L_8 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_7, /*hidden argument*/NULL);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_8, _stringLiteral80ACF505DD6D69F9053713807E97F7897D7C6EFD, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityIsNotDestroyedException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityIsNotDestroyedException__ctor_mE4794016DBA07D0BDF895C822BA4FC763039451E (EntityIsNotDestroyedException_tE5F9909534769331523B7F21128EF075CE107364 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityIsNotDestroyedException__ctor_mE4794016DBA07D0BDF895C822BA4FC763039451E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_0, _stringLiteral0F4B4026945A92B9D60527EEBE5F70F186EB6BD8, /*hidden argument*/NULL);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_1, _stringLiteral53E651D1B2D0C1DD91A2A708D7D0D2574ACDC519, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityIsNotEnabledException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313 (EntityIsNotEnabledException_t7AE4AB9E6B2414C4063824D87F5C4674046B2C11 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityIsNotEnabledException__ctor_mBFD8876B5BE1D0179E80C03427517FF35AB2D313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_0, _stringLiteralF5928667734E3310B1C6A9AF93EC9A6C14D9F052, /*hidden argument*/NULL);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_1, _stringLiteralCE291E2FB2AF53CF866E83465E4230F8FDFD0D12, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.EntityIsNotRetainedByOwnerException::.ctor(Entitas.IEntity,System.Object)
extern "C" IL2CPP_METHOD_ATTR void EntityIsNotRetainedByOwnerException__ctor_mB6FFC485F1A74C09C39BC2A4CBAD2E065E622E8B (EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225 * __this, RuntimeObject* ___entity0, RuntimeObject * ___owner1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EntityIsNotRetainedByOwnerException__ctor_mB6FFC485F1A74C09C39BC2A4CBAD2E065E622E8B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		RuntimeObject * L_3 = ___owner1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_3);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral971386486EB94529C3F863133E53C85C69E5E91B);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral971386486EB94529C3F863133E53C85C69E5E91B);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_4;
		RuntimeObject* L_6 = ___entity0;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteralC24D66C37AB36CD7593C8EE4978736439B099D18);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteralC24D66C37AB36CD7593C8EE4978736439B099D18);
		String_t* L_8 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_7, /*hidden argument*/NULL);
		EntitasException__ctor_m188226B62385EB6881B352AEFE14D7A5542DFE26(__this, L_8, _stringLiteralFFDE6F5EFE67D4B479474DBA3C4E14667A62A474, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.MatcherException::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void MatcherException__ctor_m49E9717E224C80519E5AD189E7D945F55CFFD83B (MatcherException_t753A013446CEF9B87BE52DDE80A5536442489653 * __this, int32_t ___indices0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MatcherException__ctor_m49E9717E224C80519E5AD189E7D945F55CFFD83B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___indices0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral791D879F08C51DDE8430935280105E22F3179D13, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Entitas.SafeAERC::get_retainCount()
extern "C" IL2CPP_METHOD_ATTR int32_t SafeAERC_get_retainCount_m7E100D325FA89EBB03BB0F877BEC6C8122CCC152 (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeAERC_get_retainCount_m7E100D325FA89EBB03BB0F877BEC6C8122CCC152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * L_0 = __this->get__owners_1();
		NullCheck(L_0);
		int32_t L_1 = HashSet_1_get_Count_m4594EB8357F5E4068DEE99E351ADE10202D2AF10(L_0, /*hidden argument*/HashSet_1_get_Count_m4594EB8357F5E4068DEE99E351ADE10202D2AF10_RuntimeMethod_var);
		return L_1;
	}
}
// System.Collections.Generic.HashSet`1<System.Object> Entitas.SafeAERC::get_owners()
extern "C" IL2CPP_METHOD_ATTR HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * SafeAERC_get_owners_mCCC99093D93E29B2A5853290A46CEA1DF22E9566 (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * __this, const RuntimeMethod* method)
{
	{
		HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * L_0 = __this->get__owners_1();
		return L_0;
	}
}
// System.Void Entitas.SafeAERC::.ctor(Entitas.IEntity)
extern "C" IL2CPP_METHOD_ATTR void SafeAERC__ctor_mC8216DC64624BDCEB24698DF7ADC85F2A26FDCAB (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * __this, RuntimeObject* ___entity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeAERC__ctor_mC8216DC64624BDCEB24698DF7ADC85F2A26FDCAB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * L_0 = (HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 *)il2cpp_codegen_object_new(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m8A209D312FD08A21AFB5551881E7A6946A07C0D5(L_0, /*hidden argument*/HashSet_1__ctor_m8A209D312FD08A21AFB5551881E7A6946A07C0D5_RuntimeMethod_var);
		__this->set__owners_1(L_0);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___entity0;
		__this->set__entity_0(L_1);
		return;
	}
}
// System.Void Entitas.SafeAERC::Retain(System.Object)
extern "C" IL2CPP_METHOD_ATTR void SafeAERC_Retain_m24D271B54B5548342860BF177D10B05DFAA9C468 (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * __this, RuntimeObject * ___owner0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeAERC_Retain_m24D271B54B5548342860BF177D10B05DFAA9C468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * L_0 = SafeAERC_get_owners_mCCC99093D93E29B2A5853290A46CEA1DF22E9566(__this, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___owner0;
		NullCheck(L_0);
		bool L_2 = HashSet_1_Add_m1A3167674F8646F908B2AF4977144E4D8175D695(L_0, L_1, /*hidden argument*/HashSet_1_Add_m1A3167674F8646F908B2AF4977144E4D8175D695_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_3 = __this->get__entity_0();
		RuntimeObject * L_4 = ___owner0;
		EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783 * L_5 = (EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783 *)il2cpp_codegen_object_new(EntityIsAlreadyRetainedByOwnerException_t48896BC8B21BDFFFD7F85D819233996F6E8D2783_il2cpp_TypeInfo_var);
		EntityIsAlreadyRetainedByOwnerException__ctor_mFBDE6BDC3650783F8C26EA2EB82F8579C7278784(L_5, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, SafeAERC_Retain_m24D271B54B5548342860BF177D10B05DFAA9C468_RuntimeMethod_var);
	}

IL_001b:
	{
		return;
	}
}
// System.Void Entitas.SafeAERC::Release(System.Object)
extern "C" IL2CPP_METHOD_ATTR void SafeAERC_Release_m556C922E1281A319B1C2748B147D4BB2793A30BE (SafeAERC_tC3A4274F22895D6896C084185BA5E145EF097A30 * __this, RuntimeObject * ___owner0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeAERC_Release_m556C922E1281A319B1C2748B147D4BB2793A30BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * L_0 = SafeAERC_get_owners_mCCC99093D93E29B2A5853290A46CEA1DF22E9566(__this, /*hidden argument*/NULL);
		RuntimeObject * L_1 = ___owner0;
		NullCheck(L_0);
		bool L_2 = HashSet_1_Remove_mDEE995DC0C479BC045A7490A0F7921B9ACBB4790(L_0, L_1, /*hidden argument*/HashSet_1_Remove_mDEE995DC0C479BC045A7490A0F7921B9ACBB4790_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		RuntimeObject* L_3 = __this->get__entity_0();
		RuntimeObject * L_4 = ___owner0;
		EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225 * L_5 = (EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225 *)il2cpp_codegen_object_new(EntityIsNotRetainedByOwnerException_t3553575E99A69F8AD64A5BAC2DC898E6954DC225_il2cpp_TypeInfo_var);
		EntityIsNotRetainedByOwnerException__ctor_mB6FFC485F1A74C09C39BC2A4CBAD2E065E622E8B(L_5, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, SafeAERC_Release_m556C922E1281A319B1C2748B147D4BB2793A30BE_RuntimeMethod_var);
	}

IL_001b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entitas.Systems::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Systems__ctor_m1D75E40819D0A23654A05858BEF0C978128DF947 (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Systems__ctor_m1D75E40819D0A23654A05858BEF0C978128DF947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * L_0 = (List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C *)il2cpp_codegen_object_new(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C_il2cpp_TypeInfo_var);
		List_1__ctor_m54489866F749EC6026BA519D0B0EBEBC4BC2CAD1(L_0, /*hidden argument*/List_1__ctor_m54489866F749EC6026BA519D0B0EBEBC4BC2CAD1_RuntimeMethod_var);
		__this->set__initializeSystems_0(L_0);
		List_1_tE232269578E5E48549D25DD0C9823B612D968293 * L_1 = (List_1_tE232269578E5E48549D25DD0C9823B612D968293 *)il2cpp_codegen_object_new(List_1_tE232269578E5E48549D25DD0C9823B612D968293_il2cpp_TypeInfo_var);
		List_1__ctor_mB36C11BB84D2C63932AB8C91C902593368F62AAE(L_1, /*hidden argument*/List_1__ctor_mB36C11BB84D2C63932AB8C91C902593368F62AAE_RuntimeMethod_var);
		__this->set__executeSystems_1(L_1);
		List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * L_2 = (List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC *)il2cpp_codegen_object_new(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC_il2cpp_TypeInfo_var);
		List_1__ctor_m3851B7F861C2057D9C3BC286F44772F35A8575B0(L_2, /*hidden argument*/List_1__ctor_m3851B7F861C2057D9C3BC286F44772F35A8575B0_RuntimeMethod_var);
		__this->set__cleanupSystems_2(L_2);
		List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * L_3 = (List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 *)il2cpp_codegen_object_new(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913_il2cpp_TypeInfo_var);
		List_1__ctor_m9587932254DEE91014C9C59AA0F5AE5E017BA7D0(L_3, /*hidden argument*/List_1__ctor_m9587932254DEE91014C9C59AA0F5AE5E017BA7D0_RuntimeMethod_var);
		__this->set__tearDownSystems_3(L_3);
		return;
	}
}
// Entitas.Systems Entitas.Systems::Add(Entitas.ISystem)
extern "C" IL2CPP_METHOD_ATTR Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * Systems_Add_m993517FBD7AB1189A89B5F7A69C9C8FC7339E82C (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, RuntimeObject* ___system0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Systems_Add_m993517FBD7AB1189A89B5F7A69C9C8FC7339E82C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	{
		RuntimeObject* L_0 = ___system0;
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IInitializeSystem_tE88D6DB439FB771759B22891FF5C34DFE84F92E9_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * L_2 = __this->get__initializeSystems_0();
		RuntimeObject* L_3 = V_0;
		NullCheck(L_2);
		List_1_Add_mA730CED3CC3D80FCC82584EBE806ED022613B069(L_2, L_3, /*hidden argument*/List_1_Add_mA730CED3CC3D80FCC82584EBE806ED022613B069_RuntimeMethod_var);
	}

IL_0016:
	{
		RuntimeObject* L_4 = ___system0;
		V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_4, IExecuteSystem_t5B9A85420E9C4FA897B1F7D27EEC5137B9AD461D_il2cpp_TypeInfo_var));
		RuntimeObject* L_5 = V_1;
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		List_1_tE232269578E5E48549D25DD0C9823B612D968293 * L_6 = __this->get__executeSystems_1();
		RuntimeObject* L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_mE9E33EB2ED285DBCE781434C34464D5B6A5DF800(L_6, L_7, /*hidden argument*/List_1_Add_mE9E33EB2ED285DBCE781434C34464D5B6A5DF800_RuntimeMethod_var);
	}

IL_002c:
	{
		RuntimeObject* L_8 = ___system0;
		V_2 = ((RuntimeObject*)IsInst((RuntimeObject*)L_8, ICleanupSystem_tFB9ECEEEA8A40D25C420257F34FB7363362A076E_il2cpp_TypeInfo_var));
		RuntimeObject* L_9 = V_2;
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * L_10 = __this->get__cleanupSystems_2();
		RuntimeObject* L_11 = V_2;
		NullCheck(L_10);
		List_1_Add_m0BBC37E3FDCD60F153B75D9E56F2260FEEB41165(L_10, L_11, /*hidden argument*/List_1_Add_m0BBC37E3FDCD60F153B75D9E56F2260FEEB41165_RuntimeMethod_var);
	}

IL_0042:
	{
		RuntimeObject* L_12 = ___system0;
		V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_12, ITearDownSystem_t86A3B6BAA8C08944EBB0BD477551943AE70D4688_il2cpp_TypeInfo_var));
		RuntimeObject* L_13 = V_3;
		if (!L_13)
		{
			goto IL_0058;
		}
	}
	{
		List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * L_14 = __this->get__tearDownSystems_3();
		RuntimeObject* L_15 = V_3;
		NullCheck(L_14);
		List_1_Add_m3E18D0DFE1373E92D4D23B8CF9F1F2CC67E19AB7(L_14, L_15, /*hidden argument*/List_1_Add_m3E18D0DFE1373E92D4D23B8CF9F1F2CC67E19AB7_RuntimeMethod_var);
	}

IL_0058:
	{
		return __this;
	}
}
// System.Void Entitas.Systems::Initialize()
extern "C" IL2CPP_METHOD_ATTR void Systems_Initialize_m7697AE097B5DBEE3A8F525C2E96612584C3B937B (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Systems_Initialize_m7697AE097B5DBEE3A8F525C2E96612584C3B937B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0019;
	}

IL_0004:
	{
		List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * L_0 = __this->get__initializeSystems_0();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		RuntimeObject* L_2 = List_1_get_Item_m75CEF9AEFEB69CD96795F522D1AB9B2FFC163BBE(L_0, L_1, /*hidden argument*/List_1_get_Item_m75CEF9AEFEB69CD96795F522D1AB9B2FFC163BBE_RuntimeMethod_var);
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Entitas.IInitializeSystem::Initialize() */, IInitializeSystem_tE88D6DB439FB771759B22891FF5C34DFE84F92E9_il2cpp_TypeInfo_var, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * L_5 = __this->get__initializeSystems_0();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m7DF025BDC3A448B58F52D4FE6329F8CE2D6CA04C(L_5, /*hidden argument*/List_1_get_Count_m7DF025BDC3A448B58F52D4FE6329F8CE2D6CA04C_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Systems::Execute()
extern "C" IL2CPP_METHOD_ATTR void Systems_Execute_m3388F82671E5DCF5980371AD7302596FE83A3FA6 (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Systems_Execute_m3388F82671E5DCF5980371AD7302596FE83A3FA6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0019;
	}

IL_0004:
	{
		List_1_tE232269578E5E48549D25DD0C9823B612D968293 * L_0 = __this->get__executeSystems_1();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		RuntimeObject* L_2 = List_1_get_Item_mB7AD700D64EDBCB425370286E3EAB603539BC41C(L_0, L_1, /*hidden argument*/List_1_get_Item_mB7AD700D64EDBCB425370286E3EAB603539BC41C_RuntimeMethod_var);
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Entitas.IExecuteSystem::Execute() */, IExecuteSystem_t5B9A85420E9C4FA897B1F7D27EEC5137B9AD461D_il2cpp_TypeInfo_var, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		List_1_tE232269578E5E48549D25DD0C9823B612D968293 * L_5 = __this->get__executeSystems_1();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m05018813FF4A80F1342F8692ACAFAE1A03DC5512(L_5, /*hidden argument*/List_1_get_Count_m05018813FF4A80F1342F8692ACAFAE1A03DC5512_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Systems::Cleanup()
extern "C" IL2CPP_METHOD_ATTR void Systems_Cleanup_m18D647EE9ED3008C297FC58C068B278D45050B90 (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Systems_Cleanup_m18D647EE9ED3008C297FC58C068B278D45050B90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0019;
	}

IL_0004:
	{
		List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * L_0 = __this->get__cleanupSystems_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		RuntimeObject* L_2 = List_1_get_Item_m5EEBC49C9FBF9326533FFB51F0BF3424BF333B70(L_0, L_1, /*hidden argument*/List_1_get_Item_m5EEBC49C9FBF9326533FFB51F0BF3424BF333B70_RuntimeMethod_var);
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Entitas.ICleanupSystem::Cleanup() */, ICleanupSystem_tFB9ECEEEA8A40D25C420257F34FB7363362A076E_il2cpp_TypeInfo_var, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * L_5 = __this->get__cleanupSystems_2();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m1043386622543BFD25CA715705DA1D97D4F074D9(L_5, /*hidden argument*/List_1_get_Count_m1043386622543BFD25CA715705DA1D97D4F074D9_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Systems::TearDown()
extern "C" IL2CPP_METHOD_ATTR void Systems_TearDown_mFFB66A28D21CA8C6B0C721F06CC76806AB77EEB7 (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Systems_TearDown_mFFB66A28D21CA8C6B0C721F06CC76806AB77EEB7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0019;
	}

IL_0004:
	{
		List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * L_0 = __this->get__tearDownSystems_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		RuntimeObject* L_2 = List_1_get_Item_m92C03E07978B485072E96FC891797B8ADBBD15DD(L_0, L_1, /*hidden argument*/List_1_get_Item_m92C03E07978B485072E96FC891797B8ADBBD15DD_RuntimeMethod_var);
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Entitas.ITearDownSystem::TearDown() */, ITearDownSystem_t86A3B6BAA8C08944EBB0BD477551943AE70D4688_il2cpp_TypeInfo_var, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * L_5 = __this->get__tearDownSystems_3();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m357AD56D8549B5DBDEEE32F070A942A8FFDF5E9C(L_5, /*hidden argument*/List_1_get_Count_m357AD56D8549B5DBDEEE32F070A942A8FFDF5E9C_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void Entitas.Systems::DeactivateReactiveSystems()
extern "C" IL2CPP_METHOD_ATTR void Systems_DeactivateReactiveSystems_m2D9A6B73CA9A9BE0DC3D5CC10ADA3F2EDDFDE060 (Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Systems_DeactivateReactiveSystems_m2D9A6B73CA9A9BE0DC3D5CC10ADA3F2EDDFDE060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * V_2 = NULL;
	RuntimeObject* G_B3_0 = NULL;
	RuntimeObject* G_B2_0 = NULL;
	{
		V_0 = 0;
		goto IL_0033;
	}

IL_0004:
	{
		List_1_tE232269578E5E48549D25DD0C9823B612D968293 * L_0 = __this->get__executeSystems_1();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		RuntimeObject* L_2 = List_1_get_Item_mB7AD700D64EDBCB425370286E3EAB603539BC41C(L_0, L_1, /*hidden argument*/List_1_get_Item_mB7AD700D64EDBCB425370286E3EAB603539BC41C_RuntimeMethod_var);
		RuntimeObject* L_3 = L_2;
		V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_3, IReactiveSystem_tB39B6CD38C789CC6001B15F7F2589E635B81241C_il2cpp_TypeInfo_var));
		RuntimeObject* L_4 = V_1;
		G_B2_0 = L_3;
		if (!L_4)
		{
			G_B3_0 = L_3;
			goto IL_0020;
		}
	}
	{
		RuntimeObject* L_5 = V_1;
		NullCheck(L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Entitas.IReactiveSystem::Deactivate() */, IReactiveSystem_tB39B6CD38C789CC6001B15F7F2589E635B81241C_il2cpp_TypeInfo_var, L_5);
		G_B3_0 = G_B2_0;
	}

IL_0020:
	{
		V_2 = ((Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 *)IsInstClass((RuntimeObject*)G_B3_0, Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9_il2cpp_TypeInfo_var));
		Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * L_6 = V_2;
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9 * L_7 = V_2;
		NullCheck(L_7);
		Systems_DeactivateReactiveSystems_m2D9A6B73CA9A9BE0DC3D5CC10ADA3F2EDDFDE060(L_7, /*hidden argument*/NULL);
	}

IL_002f:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0033:
	{
		int32_t L_9 = V_0;
		List_1_tE232269578E5E48549D25DD0C9823B612D968293 * L_10 = __this->get__executeSystems_1();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m05018813FF4A80F1342F8692ACAFAE1A03DC5512(L_10, /*hidden argument*/List_1_get_Count_m05018813FF4A80F1342F8692ACAFAE1A03DC5512_RuntimeMethod_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
