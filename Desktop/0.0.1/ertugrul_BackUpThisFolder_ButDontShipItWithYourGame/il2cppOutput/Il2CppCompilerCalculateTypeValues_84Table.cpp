﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ChapterModel[]
struct ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317;
// ClaimTournamentRewardEvent
struct ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63;
// CounterSystem
struct CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5;
// CounterVO
struct CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069;
// DataDictionary
struct DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606;
// GameSettingsSO
struct GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C;
// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GlobalSO
struct GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273;
// HelpSupplied
struct HelpSupplied_t0F977C7E29DA7D04D93F72052DD477EC1C65EE36;
// IconSelected
struct IconSelected_tF1620B66301F74CB9CC1684778C4E3FF0CA0C069;
// InternetConnection
struct InternetConnection_tD7C85354B3E4EDF7985D998BA1FB6C9DA115B830;
// LanguageChoosedEvent
struct LanguageChoosedEvent_t497E228421617873B2E867E6DEE93F9AD8AD4C34;
// LevelModel[]
struct LevelModelU5BU5D_tC0711C4A332B0AF163B4C92FAF063F71483EB3DB;
// RewardModel
struct RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44;
// RewardModel[]
struct RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11;
// RotaryHeart.Lib.SerializableDictionary.ReorderableList
struct ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0;
// RotaryHeart.Lib.SerializableDictionary.RequiredReferences
struct RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0;
// ServerLogin
struct ServerLogin_t779BC560CBB2746BEE648D1AAA502B53558CFD72;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,AssetData>
struct Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1;
// System.Collections.Generic.Dictionary`2<System.String,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>
struct Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2;
// System.Collections.Generic.Dictionary`2<System.String,ConfModel>
struct Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D;
// System.Collections.Generic.Dictionary`2<System.String,CounterEvent>
struct Dictionary_2_t0F9A5A968B25355290BC45A62FB464C7D6550ADF;
// System.Collections.Generic.Dictionary`2<System.String,QuestVO>
struct Dictionary_2_tE31C956C5D45FAF9F37A475F6A996B1D749D06D8;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t5ACFAE415D71C68A067C617A03DAF1DF7EC36EF7;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,TournamentModel>
struct Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A;
// System.Collections.Generic.List`1<AssetData>
struct List_1_t4C0E5E5429A980785DA4734722012505FB30D287;
// System.Collections.Generic.List`1<ChestItemModel>
struct List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F;
// System.Collections.Generic.List`1<CustomTutorialModel>
struct List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C;
// System.Collections.Generic.List`1<EffectModel>
struct List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB;
// System.Collections.Generic.List`1<EffectVisual>
struct List_1_tA366AAFE63D066448A3C35B822F880C014E71660;
// System.Collections.Generic.List`1<EffectVisualModel>
struct List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984;
// System.Collections.Generic.List`1<ItemContentModel>
struct List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27;
// System.Collections.Generic.List`1<ItemModel>
struct List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084;
// System.Collections.Generic.List`1<LevelObjective>
struct List_1_tAC67B08C4FE1AD9ACB9D2602E02CD117768BA945;
// System.Collections.Generic.List`1<PerLevelTutorialModel>
struct List_1_tBE32162E196BAE003F483B65C07526E97560480C;
// System.Collections.Generic.List`1<RewardModel>
struct List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827;
// System.Collections.Generic.List`1<SpellModel>
struct List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<TowerModel>
struct List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F;
// System.Collections.Generic.List`1<TrapModel>
struct List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7;
// System.Collections.Generic.List`1<UnitsModel>
struct List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Tayr.TSystemsManager
struct TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462;
// Tayr.TriggerModel
struct TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E;
// Tayr.VOSaver
struct VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F;
// Tayr.VariableChangedEvent
struct VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4;
// TournamentResultCommandEvent
struct TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012;
// TournamentsVO
struct TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.DisposableManager
struct DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASSETDATA_T155B7C20CE31B42FB840C26A65CBBE11D4D16D7A_H
#define ASSETDATA_T155B7C20CE31B42FB840C26A65CBBE11D4D16D7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetData
struct  AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A  : public RuntimeObject
{
public:
	// System.String AssetData::Bundle
	String_t* ___Bundle_0;
	// System.String AssetData::Prefab
	String_t* ___Prefab_1;
	// System.Int32 AssetData::WarmUp
	int32_t ___WarmUp_2;

public:
	inline static int32_t get_offset_of_Bundle_0() { return static_cast<int32_t>(offsetof(AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A, ___Bundle_0)); }
	inline String_t* get_Bundle_0() const { return ___Bundle_0; }
	inline String_t** get_address_of_Bundle_0() { return &___Bundle_0; }
	inline void set_Bundle_0(String_t* value)
	{
		___Bundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A, ___Prefab_1)); }
	inline String_t* get_Prefab_1() const { return ___Prefab_1; }
	inline String_t** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(String_t* value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}

	inline static int32_t get_offset_of_WarmUp_2() { return static_cast<int32_t>(offsetof(AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A, ___WarmUp_2)); }
	inline int32_t get_WarmUp_2() const { return ___WarmUp_2; }
	inline int32_t* get_address_of_WarmUp_2() { return &___WarmUp_2; }
	inline void set_WarmUp_2(int32_t value)
	{
		___WarmUp_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETDATA_T155B7C20CE31B42FB840C26A65CBBE11D4D16D7A_H
#ifndef BATTLECONSTANTS_T9FB60E5A8095C645C4A9066C587967D9B457C162_H
#define BATTLECONSTANTS_T9FB60E5A8095C645C4A9066C587967D9B457C162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleConstants
struct  BattleConstants_t9FB60E5A8095C645C4A9066C587967D9B457C162  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLECONSTANTS_T9FB60E5A8095C645C4A9066C587967D9B457C162_H
#ifndef CHAPTERMODEL_T128F63AB2A81208183CD6AC72466B96980DA0494_H
#define CHAPTERMODEL_T128F63AB2A81208183CD6AC72466B96980DA0494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChapterModel
struct  ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494  : public RuntimeObject
{
public:
	// System.String ChapterModel::Bundle
	String_t* ___Bundle_0;
	// System.Int32 ChapterModel::MaxLevel
	int32_t ___MaxLevel_1;

public:
	inline static int32_t get_offset_of_Bundle_0() { return static_cast<int32_t>(offsetof(ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494, ___Bundle_0)); }
	inline String_t* get_Bundle_0() const { return ___Bundle_0; }
	inline String_t** get_address_of_Bundle_0() { return &___Bundle_0; }
	inline void set_Bundle_0(String_t* value)
	{
		___Bundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_0), value);
	}

	inline static int32_t get_offset_of_MaxLevel_1() { return static_cast<int32_t>(offsetof(ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494, ___MaxLevel_1)); }
	inline int32_t get_MaxLevel_1() const { return ___MaxLevel_1; }
	inline int32_t* get_address_of_MaxLevel_1() { return &___MaxLevel_1; }
	inline void set_MaxLevel_1(int32_t value)
	{
		___MaxLevel_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAPTERMODEL_T128F63AB2A81208183CD6AC72466B96980DA0494_H
#ifndef CHAPTERSSO_T6D25AB47CA5A386B1455B167AA805B0CAB2B24C1_H
#define CHAPTERSSO_T6D25AB47CA5A386B1455B167AA805B0CAB2B24C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChaptersSO
struct  ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1  : public RuntimeObject
{
public:
	// ChapterModel[] ChaptersSO::Chapters
	ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317* ___Chapters_0;

public:
	inline static int32_t get_offset_of_Chapters_0() { return static_cast<int32_t>(offsetof(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1, ___Chapters_0)); }
	inline ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317* get_Chapters_0() const { return ___Chapters_0; }
	inline ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317** get_address_of_Chapters_0() { return &___Chapters_0; }
	inline void set_Chapters_0(ChapterModelU5BU5D_t157A022851AA288B91C80E539D65F11D1F063317* value)
	{
		___Chapters_0 = value;
		Il2CppCodeGenWriteBarrier((&___Chapters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAPTERSSO_T6D25AB47CA5A386B1455B167AA805B0CAB2B24C1_H
#ifndef CHESTITEMMODEL_TFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8_H
#define CHESTITEMMODEL_TFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestItemModel
struct  ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ItemContentModel> ChestItemModel::Content
	List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * ___Content_0;
	// System.Int32 ChestItemModel::Percentage
	int32_t ___Percentage_1;
	// System.Int32 ChestItemModel::Number
	int32_t ___Number_2;
	// System.Boolean ChestItemModel::IsUnique
	bool ___IsUnique_3;

public:
	inline static int32_t get_offset_of_Content_0() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___Content_0)); }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * get_Content_0() const { return ___Content_0; }
	inline List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 ** get_address_of_Content_0() { return &___Content_0; }
	inline void set_Content_0(List_1_t2C6CF342B96E130723D45845A1FD2053CBAF0F27 * value)
	{
		___Content_0 = value;
		Il2CppCodeGenWriteBarrier((&___Content_0), value);
	}

	inline static int32_t get_offset_of_Percentage_1() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___Percentage_1)); }
	inline int32_t get_Percentage_1() const { return ___Percentage_1; }
	inline int32_t* get_address_of_Percentage_1() { return &___Percentage_1; }
	inline void set_Percentage_1(int32_t value)
	{
		___Percentage_1 = value;
	}

	inline static int32_t get_offset_of_Number_2() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___Number_2)); }
	inline int32_t get_Number_2() const { return ___Number_2; }
	inline int32_t* get_address_of_Number_2() { return &___Number_2; }
	inline void set_Number_2(int32_t value)
	{
		___Number_2 = value;
	}

	inline static int32_t get_offset_of_IsUnique_3() { return static_cast<int32_t>(offsetof(ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8, ___IsUnique_3)); }
	inline bool get_IsUnique_3() const { return ___IsUnique_3; }
	inline bool* get_address_of_IsUnique_3() { return &___IsUnique_3; }
	inline void set_IsUnique_3(bool value)
	{
		___IsUnique_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTITEMMODEL_TFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8_H
#ifndef CHESTMODELSO_T78F97E6AC8F488AD3C18236C4080E78CCF29B2A2_H
#define CHESTMODELSO_T78F97E6AC8F488AD3C18236C4080E78CCF29B2A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChestModelSO
struct  ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ChestItemModel> ChestModelSO::Items
	List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F * ___Items_0;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2, ___Items_0)); }
	inline List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F * get_Items_0() const { return ___Items_0; }
	inline List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F ** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(List_1_t82FEF585FAFA799806FF83BC03E51C393CD8004F * value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHESTMODELSO_T78F97E6AC8F488AD3C18236C4080E78CCF29B2A2_H
#ifndef CLAIMTOURNAMENTREWARDCOMMANDMODEL_T4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D_H
#define CLAIMTOURNAMENTREWARDCOMMANDMODEL_T4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardCommandModel
struct  ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D  : public RuntimeObject
{
public:
	// System.String ClaimTournamentRewardCommandModel::TournamentId
	String_t* ___TournamentId_0;
	// GameSparks.Core.GSData ClaimTournamentRewardCommandModel::ResponseData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___ResponseData_1;

public:
	inline static int32_t get_offset_of_TournamentId_0() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D, ___TournamentId_0)); }
	inline String_t* get_TournamentId_0() const { return ___TournamentId_0; }
	inline String_t** get_address_of_TournamentId_0() { return &___TournamentId_0; }
	inline void set_TournamentId_0(String_t* value)
	{
		___TournamentId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TournamentId_0), value);
	}

	inline static int32_t get_offset_of_ResponseData_1() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D, ___ResponseData_1)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_ResponseData_1() const { return ___ResponseData_1; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_ResponseData_1() { return &___ResponseData_1; }
	inline void set_ResponseData_1(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___ResponseData_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResponseData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDCOMMANDMODEL_T4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D_H
#ifndef CLAIMTOURNAMENTREWARDCOMMANDRESULT_TECE3DBA1983CEB89B4011D09194CF0ABE5EFD193_H
#define CLAIMTOURNAMENTREWARDCOMMANDRESULT_TECE3DBA1983CEB89B4011D09194CF0ABE5EFD193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardCommandResult
struct  ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<RewardModel> ClaimTournamentRewardCommandResult::Rewards
	List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 * ___Rewards_0;

public:
	inline static int32_t get_offset_of_Rewards_0() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193, ___Rewards_0)); }
	inline List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 * get_Rewards_0() const { return ___Rewards_0; }
	inline List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 ** get_address_of_Rewards_0() { return &___Rewards_0; }
	inline void set_Rewards_0(List_1_t05E5F1AEA6DCB3F4DE9EF82A15D37F0095D1F827 * value)
	{
		___Rewards_0 = value;
		Il2CppCodeGenWriteBarrier((&___Rewards_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDCOMMANDRESULT_TECE3DBA1983CEB89B4011D09194CF0ABE5EFD193_H
#ifndef CONFMODEL_T22142FE7B971DACF1840795E24BED946C0D8A77F_H
#define CONFMODEL_T22142FE7B971DACF1840795E24BED946C0D8A77F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfModel
struct  ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F  : public RuntimeObject
{
public:
	// System.String ConfModel::FileName
	String_t* ___FileName_0;
	// System.Type ConfModel::Type
	Type_t * ___Type_1;

public:
	inline static int32_t get_offset_of_FileName_0() { return static_cast<int32_t>(offsetof(ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F, ___FileName_0)); }
	inline String_t* get_FileName_0() const { return ___FileName_0; }
	inline String_t** get_address_of_FileName_0() { return &___FileName_0; }
	inline void set_FileName_0(String_t* value)
	{
		___FileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___FileName_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFMODEL_T22142FE7B971DACF1840795E24BED946C0D8A77F_H
#ifndef COUNTERVO_TBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069_H
#define COUNTERVO_TBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterVO
struct  CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt> CounterVO::Counters
	Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 * ___Counters_0;

public:
	inline static int32_t get_offset_of_Counters_0() { return static_cast<int32_t>(offsetof(CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069, ___Counters_0)); }
	inline Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 * get_Counters_0() const { return ___Counters_0; }
	inline Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 ** get_address_of_Counters_0() { return &___Counters_0; }
	inline void set_Counters_0(Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 * value)
	{
		___Counters_0 = value;
		Il2CppCodeGenWriteBarrier((&___Counters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTERVO_TBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069_H
#ifndef EFFECTVISUALMODEL_T4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF_H
#define EFFECTVISUALMODEL_T4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualModel
struct  EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF  : public RuntimeObject
{
public:
	// System.String EffectVisualModel::Id
	String_t* ___Id_0;
	// System.Collections.Generic.List`1<EffectVisual> EffectVisualModel::Visuals
	List_1_tA366AAFE63D066448A3C35B822F880C014E71660 * ___Visuals_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Visuals_1() { return static_cast<int32_t>(offsetof(EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF, ___Visuals_1)); }
	inline List_1_tA366AAFE63D066448A3C35B822F880C014E71660 * get_Visuals_1() const { return ___Visuals_1; }
	inline List_1_tA366AAFE63D066448A3C35B822F880C014E71660 ** get_address_of_Visuals_1() { return &___Visuals_1; }
	inline void set_Visuals_1(List_1_tA366AAFE63D066448A3C35B822F880C014E71660 * value)
	{
		___Visuals_1 = value;
		Il2CppCodeGenWriteBarrier((&___Visuals_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALMODEL_T4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T7A7BD7F79E3AC1F6B21194D6A498110244521FC9_H
#define U3CU3EC__DISPLAYCLASS1_0_T7A7BD7F79E3AC1F6B21194D6A498110244521FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9  : public RuntimeObject
{
public:
	// System.String EffectVisualSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T7A7BD7F79E3AC1F6B21194D6A498110244521FC9_H
#ifndef FEATURESSWITCHCONFIG_TBF1848651D45C05C78F36D68CB647F5CB2F802D3_H
#define FEATURESSWITCHCONFIG_TBF1848651D45C05C78F36D68CB647F5CB2F802D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeaturesSwitchConfig
struct  FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3  : public RuntimeObject
{
public:
	// System.Boolean FeaturesSwitchConfig::TimePressureSystem
	bool ___TimePressureSystem_0;

public:
	inline static int32_t get_offset_of_TimePressureSystem_0() { return static_cast<int32_t>(offsetof(FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3, ___TimePressureSystem_0)); }
	inline bool get_TimePressureSystem_0() const { return ___TimePressureSystem_0; }
	inline bool* get_address_of_TimePressureSystem_0() { return &___TimePressureSystem_0; }
	inline void set_TimePressureSystem_0(bool value)
	{
		___TimePressureSystem_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURESSWITCHCONFIG_TBF1848651D45C05C78F36D68CB647F5CB2F802D3_H
#ifndef GAMECONSTANTS_T1A630C2F5F2E6945735E954CED4C0833EDAE31CC_H
#define GAMECONSTANTS_T1A630C2F5F2E6945735E954CED4C0833EDAE31CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameConstants
struct  GameConstants_t1A630C2F5F2E6945735E954CED4C0833EDAE31CC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONSTANTS_T1A630C2F5F2E6945735E954CED4C0833EDAE31CC_H
#ifndef GAMEDIRESOLVER_T866ECA664BF6463B4E179E3635C89291E8FD8A33_H
#define GAMEDIRESOLVER_T866ECA664BF6463B4E179E3635C89291E8FD8A33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameDiResolver
struct  GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33  : public RuntimeObject
{
public:

public:
};

struct GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields
{
public:
	// Zenject.DiContainer GameDiResolver::_gameContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____gameContainer_0;
	// Zenject.DiContainer GameDiResolver::_battleContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____battleContainer_1;

public:
	inline static int32_t get_offset_of__gameContainer_0() { return static_cast<int32_t>(offsetof(GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields, ____gameContainer_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__gameContainer_0() const { return ____gameContainer_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__gameContainer_0() { return &____gameContainer_0; }
	inline void set__gameContainer_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____gameContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameContainer_0), value);
	}

	inline static int32_t get_offset_of__battleContainer_1() { return static_cast<int32_t>(offsetof(GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields, ____battleContainer_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__battleContainer_1() const { return ____battleContainer_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__battleContainer_1() { return &____battleContainer_1; }
	inline void set__battleContainer_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____battleContainer_1 = value;
		Il2CppCodeGenWriteBarrier((&____battleContainer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEDIRESOLVER_T866ECA664BF6463B4E179E3635C89291E8FD8A33_H
#ifndef GAMEMAINCONFIGS_T0F18F8075F6195A8B2EC2FCB69D934E925EA071A_H
#define GAMEMAINCONFIGS_T0F18F8075F6195A8B2EC2FCB69D934E925EA071A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMainConfigs
struct  GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A  : public RuntimeObject
{
public:

public:
};

struct GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,ConfModel> GameMainConfigs::_gameConfigs
	Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D * ____gameConfigs_0;

public:
	inline static int32_t get_offset_of__gameConfigs_0() { return static_cast<int32_t>(offsetof(GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields, ____gameConfigs_0)); }
	inline Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D * get__gameConfigs_0() const { return ____gameConfigs_0; }
	inline Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D ** get_address_of__gameConfigs_0() { return &____gameConfigs_0; }
	inline void set__gameConfigs_0(Dictionary_2_tF352C3C4DFF013A39485B78BB272A67BB62D3B9D * value)
	{
		____gameConfigs_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameConfigs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMAINCONFIGS_T0F18F8075F6195A8B2EC2FCB69D934E925EA071A_H
#ifndef GAMEPATHRESOLVER_T5EF708FE19C455A8D6940A65EB9A098F50237644_H
#define GAMEPATHRESOLVER_T5EF708FE19C455A8D6940A65EB9A098F50237644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePathResolver
struct  GamePathResolver_t5EF708FE19C455A8D6940A65EB9A098F50237644  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEPATHRESOLVER_T5EF708FE19C455A8D6940A65EB9A098F50237644_H
#ifndef GAMESERVERFACTORY_T5B4121AA6089C9A89938B7DC0CA23675EC4EA487_H
#define GAMESERVERFACTORY_T5B4121AA6089C9A89938B7DC0CA23675EC4EA487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameServerFactory
struct  GameServerFactory_t5B4121AA6089C9A89938B7DC0CA23675EC4EA487  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESERVERFACTORY_T5B4121AA6089C9A89938B7DC0CA23675EC4EA487_H
#ifndef GAMESTATE_TABB839413827DD148BD92CA09A37DA4C24372F36_H
#define GAMESTATE_TABB839413827DD148BD92CA09A37DA4C24372F36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameState
struct  GameState_tABB839413827DD148BD92CA09A37DA4C24372F36  : public RuntimeObject
{
public:

public:
};

struct GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields
{
public:
	// System.Int32 GameState::TeamCount
	int32_t ___TeamCount_0;

public:
	inline static int32_t get_offset_of_TeamCount_0() { return static_cast<int32_t>(offsetof(GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields, ___TeamCount_0)); }
	inline int32_t get_TeamCount_0() const { return ___TeamCount_0; }
	inline int32_t* get_address_of_TeamCount_0() { return &___TeamCount_0; }
	inline void set_TeamCount_0(int32_t value)
	{
		___TeamCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESTATE_TABB839413827DD148BD92CA09A37DA4C24372F36_H
#ifndef GLOBALSO_TACFCA803E1D70E0299E50028D7799CE31F402273_H
#define GLOBALSO_TACFCA803E1D70E0299E50028D7799CE31F402273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalSO
struct  GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273  : public RuntimeObject
{
public:
	// System.String GlobalSO::ServerUrl
	String_t* ___ServerUrl_0;
	// System.String GlobalSO::Version
	String_t* ___Version_1;

public:
	inline static int32_t get_offset_of_ServerUrl_0() { return static_cast<int32_t>(offsetof(GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273, ___ServerUrl_0)); }
	inline String_t* get_ServerUrl_0() const { return ___ServerUrl_0; }
	inline String_t** get_address_of_ServerUrl_0() { return &___ServerUrl_0; }
	inline void set_ServerUrl_0(String_t* value)
	{
		___ServerUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ServerUrl_0), value);
	}

	inline static int32_t get_offset_of_Version_1() { return static_cast<int32_t>(offsetof(GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273, ___Version_1)); }
	inline String_t* get_Version_1() const { return ___Version_1; }
	inline String_t** get_address_of_Version_1() { return &___Version_1; }
	inline void set_Version_1(String_t* value)
	{
		___Version_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALSO_TACFCA803E1D70E0299E50028D7799CE31F402273_H
#ifndef GLOBALVO_T3F00C793D791DA4C9FDF395F616EA34E82E53385_H
#define GLOBALVO_T3F00C793D791DA4C9FDF395F616EA34E82E53385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalVO
struct  GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385  : public RuntimeObject
{
public:
	// System.String GlobalVO::ServerVersion
	String_t* ___ServerVersion_0;
	// System.String GlobalVO::ClientDataVersion
	String_t* ___ClientDataVersion_1;
	// GlobalSO GlobalVO::_globalSO
	GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * ____globalSO_2;

public:
	inline static int32_t get_offset_of_ServerVersion_0() { return static_cast<int32_t>(offsetof(GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385, ___ServerVersion_0)); }
	inline String_t* get_ServerVersion_0() const { return ___ServerVersion_0; }
	inline String_t** get_address_of_ServerVersion_0() { return &___ServerVersion_0; }
	inline void set_ServerVersion_0(String_t* value)
	{
		___ServerVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___ServerVersion_0), value);
	}

	inline static int32_t get_offset_of_ClientDataVersion_1() { return static_cast<int32_t>(offsetof(GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385, ___ClientDataVersion_1)); }
	inline String_t* get_ClientDataVersion_1() const { return ___ClientDataVersion_1; }
	inline String_t** get_address_of_ClientDataVersion_1() { return &___ClientDataVersion_1; }
	inline void set_ClientDataVersion_1(String_t* value)
	{
		___ClientDataVersion_1 = value;
		Il2CppCodeGenWriteBarrier((&___ClientDataVersion_1), value);
	}

	inline static int32_t get_offset_of__globalSO_2() { return static_cast<int32_t>(offsetof(GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385, ____globalSO_2)); }
	inline GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * get__globalSO_2() const { return ____globalSO_2; }
	inline GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 ** get_address_of__globalSO_2() { return &____globalSO_2; }
	inline void set__globalSO_2(GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * value)
	{
		____globalSO_2 = value;
		Il2CppCodeGenWriteBarrier((&____globalSO_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALVO_T3F00C793D791DA4C9FDF395F616EA34E82E53385_H
#ifndef ITEMCONTENTMODEL_T7F429490208DE8F65A9F2278897FF14267F9983D_H
#define ITEMCONTENTMODEL_T7F429490208DE8F65A9F2278897FF14267F9983D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemContentModel
struct  ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D  : public RuntimeObject
{
public:
	// RewardModel ItemContentModel::Reward
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 * ___Reward_0;
	// System.Int32 ItemContentModel::Weight
	int32_t ___Weight_1;
	// System.String ItemContentModel::RequirementStr
	String_t* ___RequirementStr_2;

public:
	inline static int32_t get_offset_of_Reward_0() { return static_cast<int32_t>(offsetof(ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D, ___Reward_0)); }
	inline RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 * get_Reward_0() const { return ___Reward_0; }
	inline RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 ** get_address_of_Reward_0() { return &___Reward_0; }
	inline void set_Reward_0(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44 * value)
	{
		___Reward_0 = value;
		Il2CppCodeGenWriteBarrier((&___Reward_0), value);
	}

	inline static int32_t get_offset_of_Weight_1() { return static_cast<int32_t>(offsetof(ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D, ___Weight_1)); }
	inline int32_t get_Weight_1() const { return ___Weight_1; }
	inline int32_t* get_address_of_Weight_1() { return &___Weight_1; }
	inline void set_Weight_1(int32_t value)
	{
		___Weight_1 = value;
	}

	inline static int32_t get_offset_of_RequirementStr_2() { return static_cast<int32_t>(offsetof(ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D, ___RequirementStr_2)); }
	inline String_t* get_RequirementStr_2() const { return ___RequirementStr_2; }
	inline String_t** get_address_of_RequirementStr_2() { return &___RequirementStr_2; }
	inline void set_RequirementStr_2(String_t* value)
	{
		___RequirementStr_2 = value;
		Il2CppCodeGenWriteBarrier((&___RequirementStr_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMCONTENTMODEL_T7F429490208DE8F65A9F2278897FF14267F9983D_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T1B0AB715C679927C205A4F2E17FCB64373E4C730_H
#define U3CU3EC__DISPLAYCLASS1_0_T1B0AB715C679927C205A4F2E17FCB64373E4C730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730  : public RuntimeObject
{
public:
	// System.String ItemsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T1B0AB715C679927C205A4F2E17FCB64373E4C730_H
#ifndef LEVELMODEL_T5549E807391907F7844D01A4FB4004330AF5E05C_H
#define LEVELMODEL_T5549E807391907F7844D01A4FB4004330AF5E05C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelModel
struct  LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C  : public RuntimeObject
{
public:
	// System.Int32 LevelModel::LevelNumber
	int32_t ___LevelNumber_0;
	// System.String LevelModel::Level
	String_t* ___Level_1;
	// System.String LevelModel::LevelBundle
	String_t* ___LevelBundle_2;
	// System.String LevelModel::Environment
	String_t* ___Environment_3;
	// System.String LevelModel::EnvironmentBundle
	String_t* ___EnvironmentBundle_4;
	// System.Int32 LevelModel::ItemsCount
	int32_t ___ItemsCount_5;
	// System.Collections.Generic.List`1<System.String> LevelModel::ItemsToGenerate
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___ItemsToGenerate_6;
	// System.Collections.Generic.List`1<System.String> LevelModel::InventoryItems
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___InventoryItems_7;
	// System.Collections.Generic.List`1<LevelObjective> LevelModel::Objectives
	List_1_tAC67B08C4FE1AD9ACB9D2602E02CD117768BA945 * ___Objectives_8;
	// System.Int32 LevelModel::ItemsGenerationDelay
	int32_t ___ItemsGenerationDelay_9;
	// System.Single LevelModel::Duration
	float ___Duration_10;

public:
	inline static int32_t get_offset_of_LevelNumber_0() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___LevelNumber_0)); }
	inline int32_t get_LevelNumber_0() const { return ___LevelNumber_0; }
	inline int32_t* get_address_of_LevelNumber_0() { return &___LevelNumber_0; }
	inline void set_LevelNumber_0(int32_t value)
	{
		___LevelNumber_0 = value;
	}

	inline static int32_t get_offset_of_Level_1() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Level_1)); }
	inline String_t* get_Level_1() const { return ___Level_1; }
	inline String_t** get_address_of_Level_1() { return &___Level_1; }
	inline void set_Level_1(String_t* value)
	{
		___Level_1 = value;
		Il2CppCodeGenWriteBarrier((&___Level_1), value);
	}

	inline static int32_t get_offset_of_LevelBundle_2() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___LevelBundle_2)); }
	inline String_t* get_LevelBundle_2() const { return ___LevelBundle_2; }
	inline String_t** get_address_of_LevelBundle_2() { return &___LevelBundle_2; }
	inline void set_LevelBundle_2(String_t* value)
	{
		___LevelBundle_2 = value;
		Il2CppCodeGenWriteBarrier((&___LevelBundle_2), value);
	}

	inline static int32_t get_offset_of_Environment_3() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Environment_3)); }
	inline String_t* get_Environment_3() const { return ___Environment_3; }
	inline String_t** get_address_of_Environment_3() { return &___Environment_3; }
	inline void set_Environment_3(String_t* value)
	{
		___Environment_3 = value;
		Il2CppCodeGenWriteBarrier((&___Environment_3), value);
	}

	inline static int32_t get_offset_of_EnvironmentBundle_4() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___EnvironmentBundle_4)); }
	inline String_t* get_EnvironmentBundle_4() const { return ___EnvironmentBundle_4; }
	inline String_t** get_address_of_EnvironmentBundle_4() { return &___EnvironmentBundle_4; }
	inline void set_EnvironmentBundle_4(String_t* value)
	{
		___EnvironmentBundle_4 = value;
		Il2CppCodeGenWriteBarrier((&___EnvironmentBundle_4), value);
	}

	inline static int32_t get_offset_of_ItemsCount_5() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___ItemsCount_5)); }
	inline int32_t get_ItemsCount_5() const { return ___ItemsCount_5; }
	inline int32_t* get_address_of_ItemsCount_5() { return &___ItemsCount_5; }
	inline void set_ItemsCount_5(int32_t value)
	{
		___ItemsCount_5 = value;
	}

	inline static int32_t get_offset_of_ItemsToGenerate_6() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___ItemsToGenerate_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_ItemsToGenerate_6() const { return ___ItemsToGenerate_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_ItemsToGenerate_6() { return &___ItemsToGenerate_6; }
	inline void set_ItemsToGenerate_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___ItemsToGenerate_6 = value;
		Il2CppCodeGenWriteBarrier((&___ItemsToGenerate_6), value);
	}

	inline static int32_t get_offset_of_InventoryItems_7() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___InventoryItems_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_InventoryItems_7() const { return ___InventoryItems_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_InventoryItems_7() { return &___InventoryItems_7; }
	inline void set_InventoryItems_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___InventoryItems_7 = value;
		Il2CppCodeGenWriteBarrier((&___InventoryItems_7), value);
	}

	inline static int32_t get_offset_of_Objectives_8() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Objectives_8)); }
	inline List_1_tAC67B08C4FE1AD9ACB9D2602E02CD117768BA945 * get_Objectives_8() const { return ___Objectives_8; }
	inline List_1_tAC67B08C4FE1AD9ACB9D2602E02CD117768BA945 ** get_address_of_Objectives_8() { return &___Objectives_8; }
	inline void set_Objectives_8(List_1_tAC67B08C4FE1AD9ACB9D2602E02CD117768BA945 * value)
	{
		___Objectives_8 = value;
		Il2CppCodeGenWriteBarrier((&___Objectives_8), value);
	}

	inline static int32_t get_offset_of_ItemsGenerationDelay_9() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___ItemsGenerationDelay_9)); }
	inline int32_t get_ItemsGenerationDelay_9() const { return ___ItemsGenerationDelay_9; }
	inline int32_t* get_address_of_ItemsGenerationDelay_9() { return &___ItemsGenerationDelay_9; }
	inline void set_ItemsGenerationDelay_9(int32_t value)
	{
		___ItemsGenerationDelay_9 = value;
	}

	inline static int32_t get_offset_of_Duration_10() { return static_cast<int32_t>(offsetof(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C, ___Duration_10)); }
	inline float get_Duration_10() const { return ___Duration_10; }
	inline float* get_address_of_Duration_10() { return &___Duration_10; }
	inline void set_Duration_10(float value)
	{
		___Duration_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELMODEL_T5549E807391907F7844D01A4FB4004330AF5E05C_H
#ifndef PATCHINGVO_TF6810B5A961785E0EB990B4C511101A0A75F3CB4_H
#define PATCHINGVO_TF6810B5A961785E0EB990B4C511101A0A75F3CB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingVO
struct  PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4  : public RuntimeObject
{
public:
	// GlobalSO PatchingVO::_globalSO
	GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * ____globalSO_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PatchingVO::HashVersions
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___HashVersions_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> PatchingVO::HashMissingAssets
	Dictionary_2_t5ACFAE415D71C68A067C617A03DAF1DF7EC36EF7 * ___HashMissingAssets_2;

public:
	inline static int32_t get_offset_of__globalSO_0() { return static_cast<int32_t>(offsetof(PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4, ____globalSO_0)); }
	inline GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * get__globalSO_0() const { return ____globalSO_0; }
	inline GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 ** get_address_of__globalSO_0() { return &____globalSO_0; }
	inline void set__globalSO_0(GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * value)
	{
		____globalSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____globalSO_0), value);
	}

	inline static int32_t get_offset_of_HashVersions_1() { return static_cast<int32_t>(offsetof(PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4, ___HashVersions_1)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_HashVersions_1() const { return ___HashVersions_1; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_HashVersions_1() { return &___HashVersions_1; }
	inline void set_HashVersions_1(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___HashVersions_1 = value;
		Il2CppCodeGenWriteBarrier((&___HashVersions_1), value);
	}

	inline static int32_t get_offset_of_HashMissingAssets_2() { return static_cast<int32_t>(offsetof(PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4, ___HashMissingAssets_2)); }
	inline Dictionary_2_t5ACFAE415D71C68A067C617A03DAF1DF7EC36EF7 * get_HashMissingAssets_2() const { return ___HashMissingAssets_2; }
	inline Dictionary_2_t5ACFAE415D71C68A067C617A03DAF1DF7EC36EF7 ** get_address_of_HashMissingAssets_2() { return &___HashMissingAssets_2; }
	inline void set_HashMissingAssets_2(Dictionary_2_t5ACFAE415D71C68A067C617A03DAF1DF7EC36EF7 * value)
	{
		___HashMissingAssets_2 = value;
		Il2CppCodeGenWriteBarrier((&___HashMissingAssets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGVO_TF6810B5A961785E0EB990B4C511101A0A75F3CB4_H
#ifndef PERLEVELTUTORIALMODEL_T617000CC336C47983E77A6E6171F0DE08379A61C_H
#define PERLEVELTUTORIALMODEL_T617000CC336C47983E77A6E6171F0DE08379A61C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PerLevelTutorialModel
struct  PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C  : public RuntimeObject
{
public:
	// System.String PerLevelTutorialModel::Id
	String_t* ___Id_0;
	// Tayr.TriggerModel PerLevelTutorialModel::_startTigger
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * ____startTigger_1;
	// System.String PerLevelTutorialModel::StartTriggerStr
	String_t* ___StartTriggerStr_2;
	// DataDictionary PerLevelTutorialModel::Data
	DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 * ___Data_3;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of__startTigger_1() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ____startTigger_1)); }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * get__startTigger_1() const { return ____startTigger_1; }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E ** get_address_of__startTigger_1() { return &____startTigger_1; }
	inline void set__startTigger_1(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * value)
	{
		____startTigger_1 = value;
		Il2CppCodeGenWriteBarrier((&____startTigger_1), value);
	}

	inline static int32_t get_offset_of_StartTriggerStr_2() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ___StartTriggerStr_2)); }
	inline String_t* get_StartTriggerStr_2() const { return ___StartTriggerStr_2; }
	inline String_t** get_address_of_StartTriggerStr_2() { return &___StartTriggerStr_2; }
	inline void set_StartTriggerStr_2(String_t* value)
	{
		___StartTriggerStr_2 = value;
		Il2CppCodeGenWriteBarrier((&___StartTriggerStr_2), value);
	}

	inline static int32_t get_offset_of_Data_3() { return static_cast<int32_t>(offsetof(PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C, ___Data_3)); }
	inline DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 * get_Data_3() const { return ___Data_3; }
	inline DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 ** get_address_of_Data_3() { return &___Data_3; }
	inline void set_Data_3(DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606 * value)
	{
		___Data_3 = value;
		Il2CppCodeGenWriteBarrier((&___Data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERLEVELTUTORIALMODEL_T617000CC336C47983E77A6E6171F0DE08379A61C_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T9E186A929A5B89C2CF8480323CF4CCE355ED23F8_H
#define U3CU3EC__DISPLAYCLASS1_0_T9E186A929A5B89C2CF8480323CF4CCE355ED23F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PerLevelTutorialSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8  : public RuntimeObject
{
public:
	// System.String PerLevelTutorialSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T9E186A929A5B89C2CF8480323CF4CCE355ED23F8_H
#ifndef QUESTVO_T80853EC5FEE81D46A730A981B0822AA277450EE6_H
#define QUESTVO_T80853EC5FEE81D46A730A981B0822AA277450EE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestVO
struct  QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6  : public RuntimeObject
{
public:
	// System.String QuestVO::Id
	String_t* ___Id_0;
	// System.Int32 QuestVO::CurrentValue
	int32_t ___CurrentValue_1;
	// System.Int32 QuestVO::TargetValue
	int32_t ___TargetValue_2;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_CurrentValue_1() { return static_cast<int32_t>(offsetof(QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6, ___CurrentValue_1)); }
	inline int32_t get_CurrentValue_1() const { return ___CurrentValue_1; }
	inline int32_t* get_address_of_CurrentValue_1() { return &___CurrentValue_1; }
	inline void set_CurrentValue_1(int32_t value)
	{
		___CurrentValue_1 = value;
	}

	inline static int32_t get_offset_of_TargetValue_2() { return static_cast<int32_t>(offsetof(QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6, ___TargetValue_2)); }
	inline int32_t get_TargetValue_2() const { return ___TargetValue_2; }
	inline int32_t* get_address_of_TargetValue_2() { return &___TargetValue_2; }
	inline void set_TargetValue_2(int32_t value)
	{
		___TargetValue_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTVO_T80853EC5FEE81D46A730A981B0822AA277450EE6_H
#ifndef QUESTSVO_T585A9D6E6540A29D33D1DE147EAFF40417B85429_H
#define QUESTSVO_T585A9D6E6540A29D33D1DE147EAFF40417B85429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestsVO
struct  QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,QuestVO> QuestsVO::CurrentQuests
	Dictionary_2_tE31C956C5D45FAF9F37A475F6A996B1D749D06D8 * ___CurrentQuests_0;

public:
	inline static int32_t get_offset_of_CurrentQuests_0() { return static_cast<int32_t>(offsetof(QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429, ___CurrentQuests_0)); }
	inline Dictionary_2_tE31C956C5D45FAF9F37A475F6A996B1D749D06D8 * get_CurrentQuests_0() const { return ___CurrentQuests_0; }
	inline Dictionary_2_tE31C956C5D45FAF9F37A475F6A996B1D749D06D8 ** get_address_of_CurrentQuests_0() { return &___CurrentQuests_0; }
	inline void set_CurrentQuests_0(Dictionary_2_tE31C956C5D45FAF9F37A475F6A996B1D749D06D8 * value)
	{
		___CurrentQuests_0 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentQuests_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTSVO_T585A9D6E6540A29D33D1DE147EAFF40417B85429_H
#ifndef DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#define DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.DrawableDictionary
struct  DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F  : public RuntimeObject
{
public:
	// RotaryHeart.Lib.SerializableDictionary.ReorderableList RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::reorderableList
	ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * ___reorderableList_0;
	// RotaryHeart.Lib.SerializableDictionary.RequiredReferences RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::reqReferences
	RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * ___reqReferences_1;
	// System.Boolean RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::isExpanded
	bool ___isExpanded_2;

public:
	inline static int32_t get_offset_of_reorderableList_0() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___reorderableList_0)); }
	inline ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * get_reorderableList_0() const { return ___reorderableList_0; }
	inline ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 ** get_address_of_reorderableList_0() { return &___reorderableList_0; }
	inline void set_reorderableList_0(ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * value)
	{
		___reorderableList_0 = value;
		Il2CppCodeGenWriteBarrier((&___reorderableList_0), value);
	}

	inline static int32_t get_offset_of_reqReferences_1() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___reqReferences_1)); }
	inline RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * get_reqReferences_1() const { return ___reqReferences_1; }
	inline RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 ** get_address_of_reqReferences_1() { return &___reqReferences_1; }
	inline void set_reqReferences_1(RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * value)
	{
		___reqReferences_1 = value;
		Il2CppCodeGenWriteBarrier((&___reqReferences_1), value);
	}

	inline static int32_t get_offset_of_isExpanded_2() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___isExpanded_2)); }
	inline bool get_isExpanded_2() const { return ___isExpanded_2; }
	inline bool* get_address_of_isExpanded_2() { return &___isExpanded_2; }
	inline void set_isExpanded_2(bool value)
	{
		___isExpanded_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TC85EADDF907753C4C6CAF2A466928E0776A9754D_H
#define U3CU3EC__DISPLAYCLASS1_0_TC85EADDF907753C4C6CAF2A466928E0776A9754D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D  : public RuntimeObject
{
public:
	// System.String SpellsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TC85EADDF907753C4C6CAF2A466928E0776A9754D_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASICCOMMAND_2_T7396A66A96AF85C35291CC1CAD86F570239D1790_H
#define BASICCOMMAND_2_T7396A66A96AF85C35291CC1CAD86F570239D1790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<ClaimTournamentRewardCommandModel,ClaimTournamentRewardCommandResult>
struct  BasicCommand_2_t7396A66A96AF85C35291CC1CAD86F570239D1790  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T7396A66A96AF85C35291CC1CAD86F570239D1790_H
#ifndef BASICCOMMAND_2_T5F9555B3F91A98758F458742D8E775E977CAF4A5_H
#define BASICCOMMAND_2_T5F9555B3F91A98758F458742D8E775E977CAF4A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<TournamentResultCommandModel,TournamentResultCommandModelResult>
struct  BasicCommand_2_t5F9555B3F91A98758F458742D8E775E977CAF4A5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T5F9555B3F91A98758F458742D8E775E977CAF4A5_H
#ifndef BASICCOMMAND_2_T0921118066C3ABAE6A6B2B11AC724538F4FBAF44_H
#define BASICCOMMAND_2_T0921118066C3ABAE6A6B2B11AC724538F4FBAF44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicCommand`2<TutorialCompletedCommandModel,TutorialCompletedCommandResult>
struct  BasicCommand_2_t0921118066C3ABAE6A6B2B11AC724538F4FBAF44  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCOMMAND_2_T0921118066C3ABAE6A6B2B11AC724538F4FBAF44_H
#ifndef BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#define BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTSystem
struct  BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8  : public RuntimeObject
{
public:
	// Zenject.DisposableManager Tayr.BasicTSystem::_disposableManager
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * ____disposableManager_0;
	// Tayr.TSystemsManager Tayr.BasicTSystem::_systemsManager
	TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * ____systemsManager_1;
	// System.Boolean Tayr.BasicTSystem::<IsInitialized>k__BackingField
	bool ___U3CIsInitializedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__disposableManager_0() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____disposableManager_0)); }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * get__disposableManager_0() const { return ____disposableManager_0; }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC ** get_address_of__disposableManager_0() { return &____disposableManager_0; }
	inline void set__disposableManager_0(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * value)
	{
		____disposableManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposableManager_0), value);
	}

	inline static int32_t get_offset_of__systemsManager_1() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____systemsManager_1)); }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * get__systemsManager_1() const { return ____systemsManager_1; }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 ** get_address_of__systemsManager_1() { return &____systemsManager_1; }
	inline void set__systemsManager_1(TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * value)
	{
		____systemsManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____systemsManager_1), value);
	}

	inline static int32_t get_offset_of_U3CIsInitializedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ___U3CIsInitializedU3Ek__BackingField_2)); }
	inline bool get_U3CIsInitializedU3Ek__BackingField_2() const { return ___U3CIsInitializedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsInitializedU3Ek__BackingField_2() { return &___U3CIsInitializedU3Ek__BackingField_2; }
	inline void set_U3CIsInitializedU3Ek__BackingField_2(bool value)
	{
		___U3CIsInitializedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifndef TOURNAMENTRESULTCOMMANDMODEL_T85DE82B1305AA756766D74891EF4404FF50367B6_H
#define TOURNAMENTRESULTCOMMANDMODEL_T85DE82B1305AA756766D74891EF4404FF50367B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommandModel
struct  TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6  : public RuntimeObject
{
public:
	// GameSparks.Core.GSData TournamentResultCommandModel::ResponseData
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___ResponseData_0;

public:
	inline static int32_t get_offset_of_ResponseData_0() { return static_cast<int32_t>(offsetof(TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6, ___ResponseData_0)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_ResponseData_0() const { return ___ResponseData_0; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_ResponseData_0() { return &___ResponseData_0; }
	inline void set_ResponseData_0(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___ResponseData_0 = value;
		Il2CppCodeGenWriteBarrier((&___ResponseData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMANDMODEL_T85DE82B1305AA756766D74891EF4404FF50367B6_H
#ifndef TOURNAMENTRESULTCOMMANDMODELRESULT_TCCEC4575292DCA9D77CE4E9D594BA31EDE162F77_H
#define TOURNAMENTRESULTCOMMANDMODELRESULT_TCCEC4575292DCA9D77CE4E9D594BA31EDE162F77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommandModelResult
struct  TournamentResultCommandModelResult_tCCEC4575292DCA9D77CE4E9D594BA31EDE162F77  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMANDMODELRESULT_TCCEC4575292DCA9D77CE4E9D594BA31EDE162F77_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE_H
#define U3CU3EC__DISPLAYCLASS1_0_TAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowersSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE  : public RuntimeObject
{
public:
	// System.String TowersSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T04C535B8B5D127AD9A39CBB909D40690C6014560_H
#define U3CU3EC__DISPLAYCLASS1_0_T04C535B8B5D127AD9A39CBB909D40690C6014560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560  : public RuntimeObject
{
public:
	// System.String TrapsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T04C535B8B5D127AD9A39CBB909D40690C6014560_H
#ifndef TUTORIALCOMPLETEDCOMMANDMODEL_T8E735DB75DAEA8DB6988603694DAE3DC5A702561_H
#define TUTORIALCOMPLETEDCOMMANDMODEL_T8E735DB75DAEA8DB6988603694DAE3DC5A702561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedCommandModel
struct  TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561  : public RuntimeObject
{
public:
	// System.String TutorialCompletedCommandModel::TutorialId
	String_t* ___TutorialId_0;

public:
	inline static int32_t get_offset_of_TutorialId_0() { return static_cast<int32_t>(offsetof(TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561, ___TutorialId_0)); }
	inline String_t* get_TutorialId_0() const { return ___TutorialId_0; }
	inline String_t** get_address_of_TutorialId_0() { return &___TutorialId_0; }
	inline void set_TutorialId_0(String_t* value)
	{
		___TutorialId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDCOMMANDMODEL_T8E735DB75DAEA8DB6988603694DAE3DC5A702561_H
#ifndef TUTORIALCOMPLETEDCOMMANDRESULT_T28E5178ED159419CFF37D5F86599383281ABF500_H
#define TUTORIALCOMPLETEDCOMMANDRESULT_T28E5178ED159419CFF37D5F86599383281ABF500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedCommandResult
struct  TutorialCompletedCommandResult_t28E5178ED159419CFF37D5F86599383281ABF500  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDCOMMANDRESULT_T28E5178ED159419CFF37D5F86599383281ABF500_H
#ifndef UNITSMODEL_T01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531_H
#define UNITSMODEL_T01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsModel
struct  UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531  : public RuntimeObject
{
public:
	// System.String UnitsModel::Id
	String_t* ___Id_0;
	// System.String UnitsModel::Bundle
	String_t* ___Bundle_1;
	// System.String UnitsModel::Prefab
	String_t* ___Prefab_2;
	// System.String UnitsModel::Icon
	String_t* ___Icon_3;
	// System.String UnitsModel::AIController
	String_t* ___AIController_4;
	// System.Collections.Generic.List`1<System.String> UnitsModel::Spells
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___Spells_5;
	// System.Int32 UnitsModel::HP
	int32_t ___HP_6;
	// System.Single UnitsModel::AttackDamage
	float ___AttackDamage_7;
	// System.Single UnitsModel::AttackRange
	float ___AttackRange_8;
	// System.Single UnitsModel::AttackFrequency
	float ___AttackFrequency_9;
	// System.Single UnitsModel::Speed
	float ___Speed_10;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___Icon_3)); }
	inline String_t* get_Icon_3() const { return ___Icon_3; }
	inline String_t** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(String_t* value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_3), value);
	}

	inline static int32_t get_offset_of_AIController_4() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___AIController_4)); }
	inline String_t* get_AIController_4() const { return ___AIController_4; }
	inline String_t** get_address_of_AIController_4() { return &___AIController_4; }
	inline void set_AIController_4(String_t* value)
	{
		___AIController_4 = value;
		Il2CppCodeGenWriteBarrier((&___AIController_4), value);
	}

	inline static int32_t get_offset_of_Spells_5() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___Spells_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_Spells_5() const { return ___Spells_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_Spells_5() { return &___Spells_5; }
	inline void set_Spells_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___Spells_5 = value;
		Il2CppCodeGenWriteBarrier((&___Spells_5), value);
	}

	inline static int32_t get_offset_of_HP_6() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___HP_6)); }
	inline int32_t get_HP_6() const { return ___HP_6; }
	inline int32_t* get_address_of_HP_6() { return &___HP_6; }
	inline void set_HP_6(int32_t value)
	{
		___HP_6 = value;
	}

	inline static int32_t get_offset_of_AttackDamage_7() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___AttackDamage_7)); }
	inline float get_AttackDamage_7() const { return ___AttackDamage_7; }
	inline float* get_address_of_AttackDamage_7() { return &___AttackDamage_7; }
	inline void set_AttackDamage_7(float value)
	{
		___AttackDamage_7 = value;
	}

	inline static int32_t get_offset_of_AttackRange_8() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___AttackRange_8)); }
	inline float get_AttackRange_8() const { return ___AttackRange_8; }
	inline float* get_address_of_AttackRange_8() { return &___AttackRange_8; }
	inline void set_AttackRange_8(float value)
	{
		___AttackRange_8 = value;
	}

	inline static int32_t get_offset_of_AttackFrequency_9() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___AttackFrequency_9)); }
	inline float get_AttackFrequency_9() const { return ___AttackFrequency_9; }
	inline float* get_address_of_AttackFrequency_9() { return &___AttackFrequency_9; }
	inline void set_AttackFrequency_9(float value)
	{
		___AttackFrequency_9 = value;
	}

	inline static int32_t get_offset_of_Speed_10() { return static_cast<int32_t>(offsetof(UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531, ___Speed_10)); }
	inline float get_Speed_10() const { return ___Speed_10; }
	inline float* get_address_of_Speed_10() { return &___Speed_10; }
	inline void set_Speed_10(float value)
	{
		___Speed_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITSMODEL_T01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TF59194C24E02C70B72472D026137782318AB9B1B_H
#define U3CU3EC__DISPLAYCLASS1_0_TF59194C24E02C70B72472D026137782318AB9B1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B  : public RuntimeObject
{
public:
	// System.String UnitsSO_<>c__DisplayClass1_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TF59194C24E02C70B72472D026137782318AB9B1B_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef UPDATESETTINGSCOMMANDRESULT_T1C5BAE390EC340A9EED4A392D498941FF9EFAFFE_H
#define UPDATESETTINGSCOMMANDRESULT_T1C5BAE390EC340A9EED4A392D498941FF9EFAFFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateSettingsCommandResult
struct  UpdateSettingsCommandResult_t1C5BAE390EC340A9EED4A392D498941FF9EFAFFE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESETTINGSCOMMANDRESULT_T1C5BAE390EC340A9EED4A392D498941FF9EFAFFE_H
#ifndef USEREVENTS_T029E2F0A52D6507E0686B07050DBEEEA9A32CDF7_H
#define USEREVENTS_T029E2F0A52D6507E0686B07050DBEEEA9A32CDF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserEvents
struct  UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7  : public RuntimeObject
{
public:
	// InternetConnection UserEvents::OnInternetConnection
	InternetConnection_tD7C85354B3E4EDF7985D998BA1FB6C9DA115B830 * ___OnInternetConnection_0;
	// UnityEngine.Events.UnityEvent UserEvents::OnLeaderboardRefreshed
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnLeaderboardRefreshed_1;
	// ServerLogin UserEvents::OnServerLogin
	ServerLogin_t779BC560CBB2746BEE648D1AAA502B53558CFD72 * ___OnServerLogin_2;
	// UnityEngine.Events.UnityEvent UserEvents::OnTeamJoined
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTeamJoined_3;
	// HelpSupplied UserEvents::OnHelpSupplied
	HelpSupplied_t0F977C7E29DA7D04D93F72052DD477EC1C65EE36 * ___OnHelpSupplied_4;
	// UnityEngine.Events.UnityEvent UserEvents::OnTeamLeft
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTeamLeft_5;
	// UnityEngine.Events.UnityEvent UserEvents::OnMemberKicked
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnMemberKicked_6;
	// UnityEngine.Events.UnityEvent UserEvents::OnBeingKicked
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnBeingKicked_7;
	// UnityEngine.Events.UnityEvent UserEvents::OnTeamCreated
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTeamCreated_8;
	// IconSelected UserEvents::OnIconSelected
	IconSelected_tF1620B66301F74CB9CC1684778C4E3FF0CA0C069 * ___OnIconSelected_9;
	// UnityEngine.Events.UnityEvent UserEvents::OnTeamEdited
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTeamEdited_10;
	// UnityEngine.Events.UnityEvent UserEvents::OnTeamPending
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTeamPending_11;
	// UnityEngine.Events.UnityEvent UserEvents::OnBattleWon
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnBattleWon_12;
	// LanguageChoosedEvent UserEvents::OnLanguageChoosed
	LanguageChoosedEvent_t497E228421617873B2E867E6DEE93F9AD8AD4C34 * ___OnLanguageChoosed_13;

public:
	inline static int32_t get_offset_of_OnInternetConnection_0() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnInternetConnection_0)); }
	inline InternetConnection_tD7C85354B3E4EDF7985D998BA1FB6C9DA115B830 * get_OnInternetConnection_0() const { return ___OnInternetConnection_0; }
	inline InternetConnection_tD7C85354B3E4EDF7985D998BA1FB6C9DA115B830 ** get_address_of_OnInternetConnection_0() { return &___OnInternetConnection_0; }
	inline void set_OnInternetConnection_0(InternetConnection_tD7C85354B3E4EDF7985D998BA1FB6C9DA115B830 * value)
	{
		___OnInternetConnection_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnInternetConnection_0), value);
	}

	inline static int32_t get_offset_of_OnLeaderboardRefreshed_1() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnLeaderboardRefreshed_1)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnLeaderboardRefreshed_1() const { return ___OnLeaderboardRefreshed_1; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnLeaderboardRefreshed_1() { return &___OnLeaderboardRefreshed_1; }
	inline void set_OnLeaderboardRefreshed_1(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnLeaderboardRefreshed_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnLeaderboardRefreshed_1), value);
	}

	inline static int32_t get_offset_of_OnServerLogin_2() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnServerLogin_2)); }
	inline ServerLogin_t779BC560CBB2746BEE648D1AAA502B53558CFD72 * get_OnServerLogin_2() const { return ___OnServerLogin_2; }
	inline ServerLogin_t779BC560CBB2746BEE648D1AAA502B53558CFD72 ** get_address_of_OnServerLogin_2() { return &___OnServerLogin_2; }
	inline void set_OnServerLogin_2(ServerLogin_t779BC560CBB2746BEE648D1AAA502B53558CFD72 * value)
	{
		___OnServerLogin_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnServerLogin_2), value);
	}

	inline static int32_t get_offset_of_OnTeamJoined_3() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnTeamJoined_3)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTeamJoined_3() const { return ___OnTeamJoined_3; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTeamJoined_3() { return &___OnTeamJoined_3; }
	inline void set_OnTeamJoined_3(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTeamJoined_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnTeamJoined_3), value);
	}

	inline static int32_t get_offset_of_OnHelpSupplied_4() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnHelpSupplied_4)); }
	inline HelpSupplied_t0F977C7E29DA7D04D93F72052DD477EC1C65EE36 * get_OnHelpSupplied_4() const { return ___OnHelpSupplied_4; }
	inline HelpSupplied_t0F977C7E29DA7D04D93F72052DD477EC1C65EE36 ** get_address_of_OnHelpSupplied_4() { return &___OnHelpSupplied_4; }
	inline void set_OnHelpSupplied_4(HelpSupplied_t0F977C7E29DA7D04D93F72052DD477EC1C65EE36 * value)
	{
		___OnHelpSupplied_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnHelpSupplied_4), value);
	}

	inline static int32_t get_offset_of_OnTeamLeft_5() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnTeamLeft_5)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTeamLeft_5() const { return ___OnTeamLeft_5; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTeamLeft_5() { return &___OnTeamLeft_5; }
	inline void set_OnTeamLeft_5(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTeamLeft_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnTeamLeft_5), value);
	}

	inline static int32_t get_offset_of_OnMemberKicked_6() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnMemberKicked_6)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnMemberKicked_6() const { return ___OnMemberKicked_6; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnMemberKicked_6() { return &___OnMemberKicked_6; }
	inline void set_OnMemberKicked_6(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnMemberKicked_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnMemberKicked_6), value);
	}

	inline static int32_t get_offset_of_OnBeingKicked_7() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnBeingKicked_7)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnBeingKicked_7() const { return ___OnBeingKicked_7; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnBeingKicked_7() { return &___OnBeingKicked_7; }
	inline void set_OnBeingKicked_7(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnBeingKicked_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnBeingKicked_7), value);
	}

	inline static int32_t get_offset_of_OnTeamCreated_8() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnTeamCreated_8)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTeamCreated_8() const { return ___OnTeamCreated_8; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTeamCreated_8() { return &___OnTeamCreated_8; }
	inline void set_OnTeamCreated_8(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTeamCreated_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnTeamCreated_8), value);
	}

	inline static int32_t get_offset_of_OnIconSelected_9() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnIconSelected_9)); }
	inline IconSelected_tF1620B66301F74CB9CC1684778C4E3FF0CA0C069 * get_OnIconSelected_9() const { return ___OnIconSelected_9; }
	inline IconSelected_tF1620B66301F74CB9CC1684778C4E3FF0CA0C069 ** get_address_of_OnIconSelected_9() { return &___OnIconSelected_9; }
	inline void set_OnIconSelected_9(IconSelected_tF1620B66301F74CB9CC1684778C4E3FF0CA0C069 * value)
	{
		___OnIconSelected_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnIconSelected_9), value);
	}

	inline static int32_t get_offset_of_OnTeamEdited_10() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnTeamEdited_10)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTeamEdited_10() const { return ___OnTeamEdited_10; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTeamEdited_10() { return &___OnTeamEdited_10; }
	inline void set_OnTeamEdited_10(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTeamEdited_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnTeamEdited_10), value);
	}

	inline static int32_t get_offset_of_OnTeamPending_11() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnTeamPending_11)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTeamPending_11() const { return ___OnTeamPending_11; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTeamPending_11() { return &___OnTeamPending_11; }
	inline void set_OnTeamPending_11(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTeamPending_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnTeamPending_11), value);
	}

	inline static int32_t get_offset_of_OnBattleWon_12() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnBattleWon_12)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnBattleWon_12() const { return ___OnBattleWon_12; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnBattleWon_12() { return &___OnBattleWon_12; }
	inline void set_OnBattleWon_12(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnBattleWon_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnBattleWon_12), value);
	}

	inline static int32_t get_offset_of_OnLanguageChoosed_13() { return static_cast<int32_t>(offsetof(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7, ___OnLanguageChoosed_13)); }
	inline LanguageChoosedEvent_t497E228421617873B2E867E6DEE93F9AD8AD4C34 * get_OnLanguageChoosed_13() const { return ___OnLanguageChoosed_13; }
	inline LanguageChoosedEvent_t497E228421617873B2E867E6DEE93F9AD8AD4C34 ** get_address_of_OnLanguageChoosed_13() { return &___OnLanguageChoosed_13; }
	inline void set_OnLanguageChoosed_13(LanguageChoosedEvent_t497E228421617873B2E867E6DEE93F9AD8AD4C34 * value)
	{
		___OnLanguageChoosed_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnLanguageChoosed_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEREVENTS_T029E2F0A52D6507E0686B07050DBEEEA9A32CDF7_H
#ifndef CLAIMTOURNAMENTREWARDCOMMAND_T39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_H
#define CLAIMTOURNAMENTREWARDCOMMAND_T39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardCommand
struct  ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF  : public BasicCommand_2_t7396A66A96AF85C35291CC1CAD86F570239D1790
{
public:
	// TournamentsVO ClaimTournamentRewardCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_0;
	// Tayr.VOSaver ClaimTournamentRewardCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__tournamentsVO_0() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF, ____tournamentsVO_0)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_0() const { return ____tournamentsVO_0; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_0() { return &____tournamentsVO_0; }
	inline void set__tournamentsVO_0(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

struct ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields
{
public:
	// ClaimTournamentRewardEvent ClaimTournamentRewardCommand::ClaimTournamentRewardCommandEvent
	ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * ___ClaimTournamentRewardCommandEvent_2;

public:
	inline static int32_t get_offset_of_ClaimTournamentRewardCommandEvent_2() { return static_cast<int32_t>(offsetof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields, ___ClaimTournamentRewardCommandEvent_2)); }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * get_ClaimTournamentRewardCommandEvent_2() const { return ___ClaimTournamentRewardCommandEvent_2; }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 ** get_address_of_ClaimTournamentRewardCommandEvent_2() { return &___ClaimTournamentRewardCommandEvent_2; }
	inline void set_ClaimTournamentRewardCommandEvent_2(ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * value)
	{
		___ClaimTournamentRewardCommandEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ClaimTournamentRewardCommandEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDCOMMAND_T39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_H
#ifndef OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#define OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct  ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::currentCryptoKey
	int32_t ___currentCryptoKey_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::hiddenValue
	int32_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::inited
	bool ___inited_2;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::fakeValue
	int32_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___currentCryptoKey_0)); }
	inline int32_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int32_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int32_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___hiddenValue_1)); }
	inline int32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___fakeValue_3)); }
	inline int32_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int32_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int32_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489_marshaled_com
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#ifndef COUNTERSYSTEM_TEA27491DC859C46A9409C948B5C4E1AC82BEB9D5_H
#define COUNTERSYSTEM_TEA27491DC859C46A9409C948B5C4E1AC82BEB9D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterSystem
struct  CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// CounterVO CounterSystem::_counterVO
	CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069 * ____counterVO_3;
	// System.Collections.Generic.Dictionary`2<System.String,CounterEvent> CounterSystem::_eventMap
	Dictionary_2_t0F9A5A968B25355290BC45A62FB464C7D6550ADF * ____eventMap_4;

public:
	inline static int32_t get_offset_of__counterVO_3() { return static_cast<int32_t>(offsetof(CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5, ____counterVO_3)); }
	inline CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069 * get__counterVO_3() const { return ____counterVO_3; }
	inline CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069 ** get_address_of__counterVO_3() { return &____counterVO_3; }
	inline void set__counterVO_3(CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069 * value)
	{
		____counterVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____counterVO_3), value);
	}

	inline static int32_t get_offset_of__eventMap_4() { return static_cast<int32_t>(offsetof(CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5, ____eventMap_4)); }
	inline Dictionary_2_t0F9A5A968B25355290BC45A62FB464C7D6550ADF * get__eventMap_4() const { return ____eventMap_4; }
	inline Dictionary_2_t0F9A5A968B25355290BC45A62FB464C7D6550ADF ** get_address_of__eventMap_4() { return &____eventMap_4; }
	inline void set__eventMap_4(Dictionary_2_t0F9A5A968B25355290BC45A62FB464C7D6550ADF * value)
	{
		____eventMap_4 = value;
		Il2CppCodeGenWriteBarrier((&____eventMap_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTERSYSTEM_TEA27491DC859C46A9409C948B5C4E1AC82BEB9D5_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_H
#define SERIALIZABLEDICTIONARYBASE_2_T9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,AssetData>
struct  SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t4C0E5E5429A980785DA4734722012505FB30D287 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____dict_3)); }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB, ____values_7)); }
	inline List_1_t4C0E5E5429A980785DA4734722012505FB30D287 * get__values_7() const { return ____values_7; }
	inline List_1_t4C0E5E5429A980785DA4734722012505FB30D287 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t4C0E5E5429A980785DA4734722012505FB30D287 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T4B29E4F36BD124CEA2580573E193FD92E22FC7EE_H
#define SERIALIZABLEDICTIONARYBASE_2_T4B29E4F36BD124CEA2580573E193FD92E22FC7EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,System.String>
struct  SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____dict_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T4B29E4F36BD124CEA2580573E193FD92E22FC7EE_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TOURNAMENTRESULTCOMMAND_T952310C8FDEEE1E034696B42AD609BC49FE86085_H
#define TOURNAMENTRESULTCOMMAND_T952310C8FDEEE1E034696B42AD609BC49FE86085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommand
struct  TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085  : public BasicCommand_2_t5F9555B3F91A98758F458742D8E775E977CAF4A5
{
public:
	// TournamentsVO TournamentResultCommand::_tournamentsVO
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * ____tournamentsVO_0;
	// Tayr.VOSaver TournamentResultCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__tournamentsVO_0() { return static_cast<int32_t>(offsetof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085, ____tournamentsVO_0)); }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * get__tournamentsVO_0() const { return ____tournamentsVO_0; }
	inline TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 ** get_address_of__tournamentsVO_0() { return &____tournamentsVO_0; }
	inline void set__tournamentsVO_0(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016 * value)
	{
		____tournamentsVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____tournamentsVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

struct TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields
{
public:
	// TournamentResultCommandEvent TournamentResultCommand::TournamentResultCommandEvent
	TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 * ___TournamentResultCommandEvent_2;

public:
	inline static int32_t get_offset_of_TournamentResultCommandEvent_2() { return static_cast<int32_t>(offsetof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields, ___TournamentResultCommandEvent_2)); }
	inline TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 * get_TournamentResultCommandEvent_2() const { return ___TournamentResultCommandEvent_2; }
	inline TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 ** get_address_of_TournamentResultCommandEvent_2() { return &___TournamentResultCommandEvent_2; }
	inline void set_TournamentResultCommandEvent_2(TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012 * value)
	{
		___TournamentResultCommandEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___TournamentResultCommandEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMAND_T952310C8FDEEE1E034696B42AD609BC49FE86085_H
#ifndef TUTORIALCOMPLETEDCOMMAND_TC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_H
#define TUTORIALCOMPLETEDCOMMAND_TC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedCommand
struct  TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC  : public BasicCommand_2_t0921118066C3ABAE6A6B2B11AC724538F4FBAF44
{
public:
	// UserVO TutorialCompletedCommand::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_0;
	// Tayr.VOSaver TutorialCompletedCommand::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_1;

public:
	inline static int32_t get_offset_of__userVO_0() { return static_cast<int32_t>(offsetof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC, ____userVO_0)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_0() const { return ____userVO_0; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_0() { return &____userVO_0; }
	inline void set__userVO_0(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_0), value);
	}

	inline static int32_t get_offset_of__voSaver_1() { return static_cast<int32_t>(offsetof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC, ____voSaver_1)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_1() const { return ____voSaver_1; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_1() { return &____voSaver_1; }
	inline void set__voSaver_1(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_1 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_1), value);
	}
};

struct TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields
{
public:
	// ClaimTournamentRewardEvent TutorialCompletedCommand::ClaimTournamentRewardCommandEvent
	ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * ___ClaimTournamentRewardCommandEvent_2;

public:
	inline static int32_t get_offset_of_ClaimTournamentRewardCommandEvent_2() { return static_cast<int32_t>(offsetof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields, ___ClaimTournamentRewardCommandEvent_2)); }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * get_ClaimTournamentRewardCommandEvent_2() const { return ___ClaimTournamentRewardCommandEvent_2; }
	inline ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 ** get_address_of_ClaimTournamentRewardCommandEvent_2() { return &___ClaimTournamentRewardCommandEvent_2; }
	inline void set_ClaimTournamentRewardCommandEvent_2(ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63 * value)
	{
		___ClaimTournamentRewardCommandEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ClaimTournamentRewardCommandEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDCOMMAND_TC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef UNITYEVENT_1_T7E457CD495A67E94765CE61E0A82063AA8BC2B5F_H
#define UNITYEVENT_1_T7E457CD495A67E94765CE61E0A82063AA8BC2B5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<ChatListItemData>
struct  UnityEvent_1_t7E457CD495A67E94765CE61E0A82063AA8BC2B5F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7E457CD495A67E94765CE61E0A82063AA8BC2B5F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7E457CD495A67E94765CE61E0A82063AA8BC2B5F_H
#ifndef UNITYEVENT_1_TE2FEBAB111368B6CFD5C1D869109FB420ADF53E8_H
#define UNITYEVENT_1_TE2FEBAB111368B6CFD5C1D869109FB420ADF53E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<ClaimTournamentRewardCommandResult>
struct  UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TE2FEBAB111368B6CFD5C1D869109FB420ADF53E8_H
#ifndef UNITYEVENT_1_T7825C5DC82A6D3EA21911EE19F0856169B2D1618_H
#define UNITYEVENT_1_T7825C5DC82A6D3EA21911EE19F0856169B2D1618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<FillLivesCommandResultModel>
struct  UnityEvent_1_t7825C5DC82A6D3EA21911EE19F0856169B2D1618  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7825C5DC82A6D3EA21911EE19F0856169B2D1618, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7825C5DC82A6D3EA21911EE19F0856169B2D1618_H
#ifndef UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#define UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifndef UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#define UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifndef UNITYEVENT_1_TACA444CD8B2CBDCD9393629F06117A47C27A8F15_H
#define UNITYEVENT_1_TACA444CD8B2CBDCD9393629F06117A47C27A8F15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_tACA444CD8B2CBDCD9393629F06117A47C27A8F15  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tACA444CD8B2CBDCD9393629F06117A47C27A8F15, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TACA444CD8B2CBDCD9393629F06117A47C27A8F15_H
#ifndef UNITYEVENT_1_T74946EB8080B3ED591EA6608929AA84C3F246D2E_H
#define UNITYEVENT_1_T74946EB8080B3ED591EA6608929AA84C3F246D2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<TournamentResultCommandModelResult>
struct  UnityEvent_1_t74946EB8080B3ED591EA6608929AA84C3F246D2E  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t74946EB8080B3ED591EA6608929AA84C3F246D2E, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T74946EB8080B3ED591EA6608929AA84C3F246D2E_H
#ifndef ASSETDICTIONARY_TC29AFDE2D533D0F9B91BA34778BBF8605137D687_H
#define ASSETDICTIONARY_TC29AFDE2D533D0F9B91BA34778BBF8605137D687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetDictionary
struct  AssetDictionary_tC29AFDE2D533D0F9B91BA34778BBF8605137D687  : public SerializableDictionaryBase_2_t9AF6CD40FC44CF59E24DE40D15BFB1806852FEEB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETDICTIONARY_TC29AFDE2D533D0F9B91BA34778BBF8605137D687_H
#ifndef CLAIMTOURNAMENTREWARDEVENT_T31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63_H
#define CLAIMTOURNAMENTREWARDEVENT_T31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClaimTournamentRewardEvent
struct  ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63  : public UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLAIMTOURNAMENTREWARDEVENT_T31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63_H
#ifndef COUNTEREVENT_T3CEC03B571EB8AC1C5AF0AF416DF2EBEC079DABF_H
#define COUNTEREVENT_T3CEC03B571EB8AC1C5AF0AF416DF2EBEC079DABF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterEvent
struct  CounterEvent_t3CEC03B571EB8AC1C5AF0AF416DF2EBEC079DABF  : public UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTEREVENT_T3CEC03B571EB8AC1C5AF0AF416DF2EBEC079DABF_H
#ifndef COUNTERTYPE_TA3DAF22E472F1B8F4870B6EEC10B3CA142DDD962_H
#define COUNTERTYPE_TA3DAF22E472F1B8F4870B6EEC10B3CA142DDD962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterType
struct  CounterType_tA3DAF22E472F1B8F4870B6EEC10B3CA142DDD962 
{
public:
	// System.Int32 CounterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CounterType_tA3DAF22E472F1B8F4870B6EEC10B3CA142DDD962, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTERTYPE_TA3DAF22E472F1B8F4870B6EEC10B3CA142DDD962_H
#ifndef CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#define CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurrencyType
struct  CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55 
{
public:
	// System.Int32 CurrencyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#ifndef CUSTOMTUTORIALTYPE_TB1BFA15E99AB96E461A01DDDBD92211896AE4968_H
#define CUSTOMTUTORIALTYPE_TB1BFA15E99AB96E461A01DDDBD92211896AE4968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialType
struct  CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968 
{
public:
	// System.Int32 CustomTutorialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTUTORIALTYPE_TB1BFA15E99AB96E461A01DDDBD92211896AE4968_H
#ifndef DATADICTIONARY_T9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606_H
#define DATADICTIONARY_T9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataDictionary
struct  DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606  : public SerializableDictionaryBase_2_t4B29E4F36BD124CEA2580573E193FD92E22FC7EE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATADICTIONARY_T9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606_H
#ifndef EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#define EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectBrain
struct  EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF 
{
public:
	// System.Int32 EffectBrain::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#ifndef EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#define EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectType
struct  EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF 
{
public:
	// System.Int32 EffectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#ifndef EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#define EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualType
struct  EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A 
{
public:
	// System.Int32 EffectVisualType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALTYPE_T026EC7D3087FCA80C407ED9F3797CE097903D15A_H
#ifndef HELPSUPPLIED_T0F977C7E29DA7D04D93F72052DD477EC1C65EE36_H
#define HELPSUPPLIED_T0F977C7E29DA7D04D93F72052DD477EC1C65EE36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelpSupplied
struct  HelpSupplied_t0F977C7E29DA7D04D93F72052DD477EC1C65EE36  : public UnityEvent_1_t7E457CD495A67E94765CE61E0A82063AA8BC2B5F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPSUPPLIED_T0F977C7E29DA7D04D93F72052DD477EC1C65EE36_H
#ifndef ICONSELECTED_TF1620B66301F74CB9CC1684778C4E3FF0CA0C069_H
#define ICONSELECTED_TF1620B66301F74CB9CC1684778C4E3FF0CA0C069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconSelected
struct  IconSelected_tF1620B66301F74CB9CC1684778C4E3FF0CA0C069  : public UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONSELECTED_TF1620B66301F74CB9CC1684778C4E3FF0CA0C069_H
#ifndef INTERNETCONNECTION_TD7C85354B3E4EDF7985D998BA1FB6C9DA115B830_H
#define INTERNETCONNECTION_TD7C85354B3E4EDF7985D998BA1FB6C9DA115B830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InternetConnection
struct  InternetConnection_tD7C85354B3E4EDF7985D998BA1FB6C9DA115B830  : public UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNETCONNECTION_TD7C85354B3E4EDF7985D998BA1FB6C9DA115B830_H
#ifndef INVENTORYVO_TC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9_H
#define INVENTORYVO_TC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryVO
struct  InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9  : public RuntimeObject
{
public:
	// GameSettingsSO InventoryVO::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt InventoryVO::Coins
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___Coins_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt InventoryVO::Hearts
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___Hearts_2;
	// System.DateTime InventoryVO::HeartRecoveryStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___HeartRecoveryStartTime_3;
	// System.Collections.Generic.Dictionary`2<System.String,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt> InventoryVO::Items
	Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 * ___Items_4;

public:
	inline static int32_t get_offset_of__gameSettingsSO_0() { return static_cast<int32_t>(offsetof(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9, ____gameSettingsSO_0)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_0() const { return ____gameSettingsSO_0; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_0() { return &____gameSettingsSO_0; }
	inline void set__gameSettingsSO_0(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_0), value);
	}

	inline static int32_t get_offset_of_Coins_1() { return static_cast<int32_t>(offsetof(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9, ___Coins_1)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_Coins_1() const { return ___Coins_1; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_Coins_1() { return &___Coins_1; }
	inline void set_Coins_1(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___Coins_1 = value;
	}

	inline static int32_t get_offset_of_Hearts_2() { return static_cast<int32_t>(offsetof(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9, ___Hearts_2)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_Hearts_2() const { return ___Hearts_2; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_Hearts_2() { return &___Hearts_2; }
	inline void set_Hearts_2(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___Hearts_2 = value;
	}

	inline static int32_t get_offset_of_HeartRecoveryStartTime_3() { return static_cast<int32_t>(offsetof(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9, ___HeartRecoveryStartTime_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_HeartRecoveryStartTime_3() const { return ___HeartRecoveryStartTime_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_HeartRecoveryStartTime_3() { return &___HeartRecoveryStartTime_3; }
	inline void set_HeartRecoveryStartTime_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___HeartRecoveryStartTime_3 = value;
	}

	inline static int32_t get_offset_of_Items_4() { return static_cast<int32_t>(offsetof(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9, ___Items_4)); }
	inline Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 * get_Items_4() const { return ___Items_4; }
	inline Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 ** get_address_of_Items_4() { return &___Items_4; }
	inline void set_Items_4(Dictionary_2_t23117C477C1F8EACE60312F2D6FF60FFD474A8D2 * value)
	{
		___Items_4 = value;
		Il2CppCodeGenWriteBarrier((&___Items_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYVO_TC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9_H
#ifndef ITEMTYPE_T41090A4C4D2BCA3765F224EA9264EC817755A5A3_H
#define ITEMTYPE_T41090A4C4D2BCA3765F224EA9264EC817755A5A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemType
struct  ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3 
{
public:
	// System.Int32 ItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMTYPE_T41090A4C4D2BCA3765F224EA9264EC817755A5A3_H
#ifndef LANGUAGECHOOSEDEVENT_T497E228421617873B2E867E6DEE93F9AD8AD4C34_H
#define LANGUAGECHOOSEDEVENT_T497E228421617873B2E867E6DEE93F9AD8AD4C34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageChoosedEvent
struct  LanguageChoosedEvent_t497E228421617873B2E867E6DEE93F9AD8AD4C34  : public UnityEvent_1_tACA444CD8B2CBDCD9393629F06117A47C27A8F15
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGECHOOSEDEVENT_T497E228421617873B2E867E6DEE93F9AD8AD4C34_H
#ifndef MAPITEMTYPE_T577937D98B8B61ADC7EB7F0E092F58437801D7BF_H
#define MAPITEMTYPE_T577937D98B8B61ADC7EB7F0E092F58437801D7BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapItemType
struct  MapItemType_t577937D98B8B61ADC7EB7F0E092F58437801D7BF 
{
public:
	// System.Int32 MapItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MapItemType_t577937D98B8B61ADC7EB7F0E092F58437801D7BF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPITEMTYPE_T577937D98B8B61ADC7EB7F0E092F58437801D7BF_H
#ifndef OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#define OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveType
struct  ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28 
{
public:
	// System.Int32 ObjectiveType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#ifndef SERVERLOGIN_T779BC560CBB2746BEE648D1AAA502B53558CFD72_H
#define SERVERLOGIN_T779BC560CBB2746BEE648D1AAA502B53558CFD72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ServerLogin
struct  ServerLogin_t779BC560CBB2746BEE648D1AAA502B53558CFD72  : public UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERLOGIN_T779BC560CBB2746BEE648D1AAA502B53558CFD72_H
#ifndef SPELLAI_T31BA0E89C8EE4E13EC803218DD905CB4693C8366_H
#define SPELLAI_T31BA0E89C8EE4E13EC803218DD905CB4693C8366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellAI
struct  SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366 
{
public:
	// System.Int32 SpellAI::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLAI_T31BA0E89C8EE4E13EC803218DD905CB4693C8366_H
#ifndef SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#define SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellController
struct  SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643 
{
public:
	// System.Int32 SpellController::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLCONTROLLER_TF10DAA53E6D7502133083BF8AD162F9CBEE65643_H
#ifndef SPELLINPUT_T7E941C18CACBD4A789B292019E2341A93AE8739F_H
#define SPELLINPUT_T7E941C18CACBD4A789B292019E2341A93AE8739F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellInput
struct  SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F 
{
public:
	// System.Int32 SpellInput::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLINPUT_T7E941C18CACBD4A789B292019E2341A93AE8739F_H
#ifndef SPELLRUNNER_T504261A93EFC5915A80B9E13A0D004262B424C11_H
#define SPELLRUNNER_T504261A93EFC5915A80B9E13A0D004262B424C11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellRunner
struct  SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11 
{
public:
	// System.Int32 SpellRunner::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLRUNNER_T504261A93EFC5915A80B9E13A0D004262B424C11_H
#ifndef STARTOURNAMENTEVENT_TAC8E51584ABAF5B990762D1E976638088E4B92F2_H
#define STARTOURNAMENTEVENT_TAC8E51584ABAF5B990762D1E976638088E4B92F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarTournamentEvent
struct  StarTournamentEvent_tAC8E51584ABAF5B990762D1E976638088E4B92F2  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTOURNAMENTEVENT_TAC8E51584ABAF5B990762D1E976638088E4B92F2_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_19)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_21)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#define REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.RefreahType
struct  RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE 
{
public:
	// System.Int32 Tayr.RefreahType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifndef TEAMJOINED_TCB8F4B32A19C4DCD6C7B8F895FA4D747EF44B5F3_H
#define TEAMJOINED_TCB8F4B32A19C4DCD6C7B8F895FA4D747EF44B5F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamJoined
struct  TeamJoined_tCB8F4B32A19C4DCD6C7B8F895FA4D747EF44B5F3  : public UnityEvent_1_tACA444CD8B2CBDCD9393629F06117A47C27A8F15
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMJOINED_TCB8F4B32A19C4DCD6C7B8F895FA4D747EF44B5F3_H
#ifndef TOURNAMENTRESULTCOMMANDEVENT_T63203EB3B05D0FB9B44625DF65238A1FDB642012_H
#define TOURNAMENTRESULTCOMMANDEVENT_T63203EB3B05D0FB9B44625DF65238A1FDB642012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentResultCommandEvent
struct  TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012  : public UnityEvent_1_t74946EB8080B3ED591EA6608929AA84C3F246D2E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTRESULTCOMMANDEVENT_T63203EB3B05D0FB9B44625DF65238A1FDB642012_H
#ifndef TOURNAMENTSVO_TCBA9A235AE4069263CE332B1E9150CDABD0E8016_H
#define TOURNAMENTSVO_TCBA9A235AE4069263CE332B1E9150CDABD0E8016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentsVO
struct  TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,TournamentModel> TournamentsVO::Tournaments
	Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A * ___Tournaments_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt TournamentsVO::StarChests
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___StarChests_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt TournamentsVO::LevelChests
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___LevelChests_2;

public:
	inline static int32_t get_offset_of_Tournaments_0() { return static_cast<int32_t>(offsetof(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016, ___Tournaments_0)); }
	inline Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A * get_Tournaments_0() const { return ___Tournaments_0; }
	inline Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A ** get_address_of_Tournaments_0() { return &___Tournaments_0; }
	inline void set_Tournaments_0(Dictionary_2_t0D44CD823085042D11C357BC35A9D64FFB54556A * value)
	{
		___Tournaments_0 = value;
		Il2CppCodeGenWriteBarrier((&___Tournaments_0), value);
	}

	inline static int32_t get_offset_of_StarChests_1() { return static_cast<int32_t>(offsetof(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016, ___StarChests_1)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_StarChests_1() const { return ___StarChests_1; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_StarChests_1() { return &___StarChests_1; }
	inline void set_StarChests_1(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___StarChests_1 = value;
	}

	inline static int32_t get_offset_of_LevelChests_2() { return static_cast<int32_t>(offsetof(TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016, ___LevelChests_2)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_LevelChests_2() const { return ___LevelChests_2; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_LevelChests_2() { return &___LevelChests_2; }
	inline void set_LevelChests_2(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___LevelChests_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTSVO_TCBA9A235AE4069263CE332B1E9150CDABD0E8016_H
#ifndef TUTORIALCOMPLETEDEVENT_T6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE_H
#define TUTORIALCOMPLETEDEVENT_T6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialCompletedEvent
struct  TutorialCompletedEvent_t6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE  : public UnityEvent_1_tE2FEBAB111368B6CFD5C1D869109FB420ADF53E8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALCOMPLETEDEVENT_T6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE_H
#ifndef TUTORIALTYPE_T95A6CD09B8522505F42A6F3FFC2E01CCD692A302_H
#define TUTORIALTYPE_T95A6CD09B8522505F42A6F3FFC2E01CCD692A302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialType
struct  TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302 
{
public:
	// System.Int32 TutorialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALTYPE_T95A6CD09B8522505F42A6F3FFC2E01CCD692A302_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef UPDATESETTINGSEVENT_TBDFC5C0BE56631615A2FE1F8DF1593535637EC8F_H
#define UPDATESETTINGSEVENT_TBDFC5C0BE56631615A2FE1F8DF1593535637EC8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateSettingsEvent
struct  UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F  : public UnityEvent_1_t7825C5DC82A6D3EA21911EE19F0856169B2D1618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESETTINGSEVENT_TBDFC5C0BE56631615A2FE1F8DF1593535637EC8F_H
#ifndef VISUALEFFECTTYPE_T980FA2BBE9E5BA944689AF6AF342230957117921_H
#define VISUALEFFECTTYPE_T980FA2BBE9E5BA944689AF6AF342230957117921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VisualEffectType
struct  VisualEffectType_t980FA2BBE9E5BA944689AF6AF342230957117921 
{
public:
	// System.Int32 VisualEffectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VisualEffectType_t980FA2BBE9E5BA944689AF6AF342230957117921, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALEFFECTTYPE_T980FA2BBE9E5BA944689AF6AF342230957117921_H
#ifndef CUSTOMTUTORIALMODEL_T7F1E6EF5B6AF58AEA9FB2573811B4562E587E635_H
#define CUSTOMTUTORIALMODEL_T7F1E6EF5B6AF58AEA9FB2573811B4562E587E635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialModel
struct  CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635  : public RuntimeObject
{
public:
	// System.String CustomTutorialModel::Id
	String_t* ___Id_0;
	// CustomTutorialType CustomTutorialModel::Type
	int32_t ___Type_1;
	// System.String CustomTutorialModel::Trigger
	String_t* ___Trigger_2;
	// Tayr.TriggerModel CustomTutorialModel::_startTigger
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * ____startTigger_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> CustomTutorialModel::Data
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___Data_4;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Trigger_2() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Trigger_2)); }
	inline String_t* get_Trigger_2() const { return ___Trigger_2; }
	inline String_t** get_address_of_Trigger_2() { return &___Trigger_2; }
	inline void set_Trigger_2(String_t* value)
	{
		___Trigger_2 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger_2), value);
	}

	inline static int32_t get_offset_of__startTigger_3() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ____startTigger_3)); }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * get__startTigger_3() const { return ____startTigger_3; }
	inline TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E ** get_address_of__startTigger_3() { return &____startTigger_3; }
	inline void set__startTigger_3(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E * value)
	{
		____startTigger_3 = value;
		Il2CppCodeGenWriteBarrier((&____startTigger_3), value);
	}

	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635, ___Data_4)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_Data_4() const { return ___Data_4; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTUTORIALMODEL_T7F1E6EF5B6AF58AEA9FB2573811B4562E587E635_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T8A0EA66C8B605616AE4A6A5640B599178BA16E15_H
#define U3CU3EC__DISPLAYCLASS1_0_T8A0EA66C8B605616AE4A6A5640B599178BA16E15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialSO_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15  : public RuntimeObject
{
public:
	// CustomTutorialType CustomTutorialSO_<>c__DisplayClass1_0::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T8A0EA66C8B605616AE4A6A5640B599178BA16E15_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TA198EA948F0DC0F03FB3D83644AAE8654B69EC47_H
#define U3CU3EC__DISPLAYCLASS2_0_TA198EA948F0DC0F03FB3D83644AAE8654B69EC47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialSO_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47  : public RuntimeObject
{
public:
	// CustomTutorialType CustomTutorialSO_<>c__DisplayClass2_0::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TA198EA948F0DC0F03FB3D83644AAE8654B69EC47_H
#ifndef EFFECTMODEL_T1BF2DAB91B75E4435889D8BBCCA583888883223E_H
#define EFFECTMODEL_T1BF2DAB91B75E4435889D8BBCCA583888883223E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectModel
struct  EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E  : public RuntimeObject
{
public:
	// EffectBrain EffectModel::EffectBrain
	int32_t ___EffectBrain_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> EffectModel::EffectBrainData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___EffectBrainData_1;
	// EffectType EffectModel::EffectType
	int32_t ___EffectType_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> EffectModel::EffectData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___EffectData_3;
	// System.String EffectModel::EffectVisualId
	String_t* ___EffectVisualId_4;

public:
	inline static int32_t get_offset_of_EffectBrain_0() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectBrain_0)); }
	inline int32_t get_EffectBrain_0() const { return ___EffectBrain_0; }
	inline int32_t* get_address_of_EffectBrain_0() { return &___EffectBrain_0; }
	inline void set_EffectBrain_0(int32_t value)
	{
		___EffectBrain_0 = value;
	}

	inline static int32_t get_offset_of_EffectBrainData_1() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectBrainData_1)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_EffectBrainData_1() const { return ___EffectBrainData_1; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_EffectBrainData_1() { return &___EffectBrainData_1; }
	inline void set_EffectBrainData_1(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___EffectBrainData_1 = value;
		Il2CppCodeGenWriteBarrier((&___EffectBrainData_1), value);
	}

	inline static int32_t get_offset_of_EffectType_2() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectType_2)); }
	inline int32_t get_EffectType_2() const { return ___EffectType_2; }
	inline int32_t* get_address_of_EffectType_2() { return &___EffectType_2; }
	inline void set_EffectType_2(int32_t value)
	{
		___EffectType_2 = value;
	}

	inline static int32_t get_offset_of_EffectData_3() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectData_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_EffectData_3() const { return ___EffectData_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_EffectData_3() { return &___EffectData_3; }
	inline void set_EffectData_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___EffectData_3 = value;
		Il2CppCodeGenWriteBarrier((&___EffectData_3), value);
	}

	inline static int32_t get_offset_of_EffectVisualId_4() { return static_cast<int32_t>(offsetof(EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E, ___EffectVisualId_4)); }
	inline String_t* get_EffectVisualId_4() const { return ___EffectVisualId_4; }
	inline String_t** get_address_of_EffectVisualId_4() { return &___EffectVisualId_4; }
	inline void set_EffectVisualId_4(String_t* value)
	{
		___EffectVisualId_4 = value;
		Il2CppCodeGenWriteBarrier((&___EffectVisualId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTMODEL_T1BF2DAB91B75E4435889D8BBCCA583888883223E_H
#ifndef EFFECTVISUAL_TE885D61366B46DC715156887B773663B79D6777A_H
#define EFFECTVISUAL_TE885D61366B46DC715156887B773663B79D6777A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisual
struct  EffectVisual_tE885D61366B46DC715156887B773663B79D6777A  : public RuntimeObject
{
public:
	// System.String EffectVisual::Prefab
	String_t* ___Prefab_0;
	// System.String EffectVisual::Bundle
	String_t* ___Bundle_1;
	// EffectVisualType EffectVisual::EffectVisualType
	int32_t ___EffectVisualType_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> EffectVisual::EffectVisualData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___EffectVisualData_3;

public:
	inline static int32_t get_offset_of_Prefab_0() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___Prefab_0)); }
	inline String_t* get_Prefab_0() const { return ___Prefab_0; }
	inline String_t** get_address_of_Prefab_0() { return &___Prefab_0; }
	inline void set_Prefab_0(String_t* value)
	{
		___Prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_EffectVisualType_2() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___EffectVisualType_2)); }
	inline int32_t get_EffectVisualType_2() const { return ___EffectVisualType_2; }
	inline int32_t* get_address_of_EffectVisualType_2() { return &___EffectVisualType_2; }
	inline void set_EffectVisualType_2(int32_t value)
	{
		___EffectVisualType_2 = value;
	}

	inline static int32_t get_offset_of_EffectVisualData_3() { return static_cast<int32_t>(offsetof(EffectVisual_tE885D61366B46DC715156887B773663B79D6777A, ___EffectVisualData_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_EffectVisualData_3() const { return ___EffectVisualData_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_EffectVisualData_3() { return &___EffectVisualData_3; }
	inline void set_EffectVisualData_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___EffectVisualData_3 = value;
		Il2CppCodeGenWriteBarrier((&___EffectVisualData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUAL_TE885D61366B46DC715156887B773663B79D6777A_H
#ifndef ITEMMODEL_T620A13B4D13CD275487D308843103864063DBE1F_H
#define ITEMMODEL_T620A13B4D13CD275487D308843103864063DBE1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemModel
struct  ItemModel_t620A13B4D13CD275487D308843103864063DBE1F  : public RuntimeObject
{
public:
	// System.String ItemModel::Id
	String_t* ___Id_0;
	// System.String ItemModel::Bundle
	String_t* ___Bundle_1;
	// System.String ItemModel::Prefab
	String_t* ___Prefab_2;
	// System.String ItemModel::Icon
	String_t* ___Icon_3;
	// System.String ItemModel::DeathExplosion
	String_t* ___DeathExplosion_4;
	// ItemType ItemModel::Type
	int32_t ___Type_5;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Icon_3)); }
	inline String_t* get_Icon_3() const { return ___Icon_3; }
	inline String_t** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(String_t* value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_3), value);
	}

	inline static int32_t get_offset_of_DeathExplosion_4() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___DeathExplosion_4)); }
	inline String_t* get_DeathExplosion_4() const { return ___DeathExplosion_4; }
	inline String_t** get_address_of_DeathExplosion_4() { return &___DeathExplosion_4; }
	inline void set_DeathExplosion_4(String_t* value)
	{
		___DeathExplosion_4 = value;
		Il2CppCodeGenWriteBarrier((&___DeathExplosion_4), value);
	}

	inline static int32_t get_offset_of_Type_5() { return static_cast<int32_t>(offsetof(ItemModel_t620A13B4D13CD275487D308843103864063DBE1F, ___Type_5)); }
	inline int32_t get_Type_5() const { return ___Type_5; }
	inline int32_t* get_address_of_Type_5() { return &___Type_5; }
	inline void set_Type_5(int32_t value)
	{
		___Type_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMMODEL_T620A13B4D13CD275487D308843103864063DBE1F_H
#ifndef LEVELOBJECTIVE_T694A34150000D3DE3C9341AAC6D6B37667B796EC_H
#define LEVELOBJECTIVE_T694A34150000D3DE3C9341AAC6D6B37667B796EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelObjective
struct  LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC  : public RuntimeObject
{
public:
	// ObjectiveType LevelObjective::Type
	int32_t ___Type_0;
	// System.String LevelObjective::UnitId
	String_t* ___UnitId_1;
	// System.Int32 LevelObjective::Count
	int32_t ___Count_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_UnitId_1() { return static_cast<int32_t>(offsetof(LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC, ___UnitId_1)); }
	inline String_t* get_UnitId_1() const { return ___UnitId_1; }
	inline String_t** get_address_of_UnitId_1() { return &___UnitId_1; }
	inline void set_UnitId_1(String_t* value)
	{
		___UnitId_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnitId_1), value);
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELOBJECTIVE_T694A34150000D3DE3C9341AAC6D6B37667B796EC_H
#ifndef REWARDMODEL_TE09D75F3B47938652450ABB51F21663745C8BC44_H
#define REWARDMODEL_TE09D75F3B47938652450ABB51F21663745C8BC44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RewardModel
struct  RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44  : public RuntimeObject
{
public:
	// CurrencyType RewardModel::Type
	int32_t ___Type_0;
	// System.Int32 RewardModel::Value
	int32_t ___Value_1;
	// System.String RewardModel::Id
	String_t* ___Id_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}

	inline static int32_t get_offset_of_Id_2() { return static_cast<int32_t>(offsetof(RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44, ___Id_2)); }
	inline String_t* get_Id_2() const { return ___Id_2; }
	inline String_t** get_address_of_Id_2() { return &___Id_2; }
	inline void set_Id_2(String_t* value)
	{
		___Id_2 = value;
		Il2CppCodeGenWriteBarrier((&___Id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDMODEL_TE09D75F3B47938652450ABB51F21663745C8BC44_H
#ifndef SPELLMODEL_T50B172E1768260E1736AA33F4ED72DB9CDC35235_H
#define SPELLMODEL_T50B172E1768260E1736AA33F4ED72DB9CDC35235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellModel
struct  SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235  : public RuntimeObject
{
public:
	// System.String SpellModel::Id
	String_t* ___Id_0;
	// SpellInput SpellModel::SpellInput
	int32_t ___SpellInput_1;
	// System.Collections.Generic.Dictionary`2<System.String,AssetData> SpellModel::Assets
	Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * ___Assets_2;
	// SpellController SpellModel::SpellController
	int32_t ___SpellController_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> SpellModel::SpellControllerData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___SpellControllerData_4;
	// System.Collections.Generic.List`1<EffectModel> SpellModel::Effects
	List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * ___Effects_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> SpellModel::poop
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___poop_6;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_SpellInput_1() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___SpellInput_1)); }
	inline int32_t get_SpellInput_1() const { return ___SpellInput_1; }
	inline int32_t* get_address_of_SpellInput_1() { return &___SpellInput_1; }
	inline void set_SpellInput_1(int32_t value)
	{
		___SpellInput_1 = value;
	}

	inline static int32_t get_offset_of_Assets_2() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___Assets_2)); }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * get_Assets_2() const { return ___Assets_2; }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 ** get_address_of_Assets_2() { return &___Assets_2; }
	inline void set_Assets_2(Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * value)
	{
		___Assets_2 = value;
		Il2CppCodeGenWriteBarrier((&___Assets_2), value);
	}

	inline static int32_t get_offset_of_SpellController_3() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___SpellController_3)); }
	inline int32_t get_SpellController_3() const { return ___SpellController_3; }
	inline int32_t* get_address_of_SpellController_3() { return &___SpellController_3; }
	inline void set_SpellController_3(int32_t value)
	{
		___SpellController_3 = value;
	}

	inline static int32_t get_offset_of_SpellControllerData_4() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___SpellControllerData_4)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_SpellControllerData_4() const { return ___SpellControllerData_4; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_SpellControllerData_4() { return &___SpellControllerData_4; }
	inline void set_SpellControllerData_4(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___SpellControllerData_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpellControllerData_4), value);
	}

	inline static int32_t get_offset_of_Effects_5() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___Effects_5)); }
	inline List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * get_Effects_5() const { return ___Effects_5; }
	inline List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB ** get_address_of_Effects_5() { return &___Effects_5; }
	inline void set_Effects_5(List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * value)
	{
		___Effects_5 = value;
		Il2CppCodeGenWriteBarrier((&___Effects_5), value);
	}

	inline static int32_t get_offset_of_poop_6() { return static_cast<int32_t>(offsetof(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235, ___poop_6)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_poop_6() const { return ___poop_6; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_poop_6() { return &___poop_6; }
	inline void set_poop_6(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___poop_6 = value;
		Il2CppCodeGenWriteBarrier((&___poop_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLMODEL_T50B172E1768260E1736AA33F4ED72DB9CDC35235_H
#ifndef VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#define VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.Variable
struct  Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD  : public RuntimeObject
{
public:
	// Tayr.VariableChangedEvent Tayr.Variable::_onVariableChanged
	VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * ____onVariableChanged_0;
	// System.Int32 Tayr.Variable::_value
	int32_t ____value_1;
	// Tayr.RefreahType Tayr.Variable::_refreshType
	int32_t ____refreshType_2;
	// System.Boolean Tayr.Variable::_isInitialized
	bool ____isInitialized_3;

public:
	inline static int32_t get_offset_of__onVariableChanged_0() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____onVariableChanged_0)); }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * get__onVariableChanged_0() const { return ____onVariableChanged_0; }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 ** get_address_of__onVariableChanged_0() { return &____onVariableChanged_0; }
	inline void set__onVariableChanged_0(VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * value)
	{
		____onVariableChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&____onVariableChanged_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____value_1)); }
	inline int32_t get__value_1() const { return ____value_1; }
	inline int32_t* get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(int32_t value)
	{
		____value_1 = value;
	}

	inline static int32_t get_offset_of__refreshType_2() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____refreshType_2)); }
	inline int32_t get__refreshType_2() const { return ____refreshType_2; }
	inline int32_t* get_address_of__refreshType_2() { return &____refreshType_2; }
	inline void set__refreshType_2(int32_t value)
	{
		____refreshType_2 = value;
	}

	inline static int32_t get_offset_of__isInitialized_3() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____isInitialized_3)); }
	inline bool get__isInitialized_3() const { return ____isInitialized_3; }
	inline bool* get_address_of__isInitialized_3() { return &____isInitialized_3; }
	inline void set__isInitialized_3(bool value)
	{
		____isInitialized_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifndef TOWERMODEL_T9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31_H
#define TOWERMODEL_T9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerModel
struct  TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31  : public RuntimeObject
{
public:
	// System.String TowerModel::Id
	String_t* ___Id_0;
	// System.String TowerModel::Bundle
	String_t* ___Bundle_1;
	// System.String TowerModel::Prefab
	String_t* ___Prefab_2;
	// System.String TowerModel::Icon
	String_t* ___Icon_3;
	// SpellAI TowerModel::SpellAI
	int32_t ___SpellAI_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TowerModel::AIData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___AIData_5;
	// System.Single TowerModel::Range
	float ___Range_6;
	// System.String TowerModel::Spell
	String_t* ___Spell_7;
	// System.Int32 TowerModel::HP
	int32_t ___HP_8;
	// System.String TowerModel::DeathExplosion
	String_t* ___DeathExplosion_9;
	// System.Single TowerModel::HealDuration
	float ___HealDuration_10;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Icon_3)); }
	inline String_t* get_Icon_3() const { return ___Icon_3; }
	inline String_t** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(String_t* value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_3), value);
	}

	inline static int32_t get_offset_of_SpellAI_4() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___SpellAI_4)); }
	inline int32_t get_SpellAI_4() const { return ___SpellAI_4; }
	inline int32_t* get_address_of_SpellAI_4() { return &___SpellAI_4; }
	inline void set_SpellAI_4(int32_t value)
	{
		___SpellAI_4 = value;
	}

	inline static int32_t get_offset_of_AIData_5() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___AIData_5)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_AIData_5() const { return ___AIData_5; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_AIData_5() { return &___AIData_5; }
	inline void set_AIData_5(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___AIData_5 = value;
		Il2CppCodeGenWriteBarrier((&___AIData_5), value);
	}

	inline static int32_t get_offset_of_Range_6() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Range_6)); }
	inline float get_Range_6() const { return ___Range_6; }
	inline float* get_address_of_Range_6() { return &___Range_6; }
	inline void set_Range_6(float value)
	{
		___Range_6 = value;
	}

	inline static int32_t get_offset_of_Spell_7() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___Spell_7)); }
	inline String_t* get_Spell_7() const { return ___Spell_7; }
	inline String_t** get_address_of_Spell_7() { return &___Spell_7; }
	inline void set_Spell_7(String_t* value)
	{
		___Spell_7 = value;
		Il2CppCodeGenWriteBarrier((&___Spell_7), value);
	}

	inline static int32_t get_offset_of_HP_8() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___HP_8)); }
	inline int32_t get_HP_8() const { return ___HP_8; }
	inline int32_t* get_address_of_HP_8() { return &___HP_8; }
	inline void set_HP_8(int32_t value)
	{
		___HP_8 = value;
	}

	inline static int32_t get_offset_of_DeathExplosion_9() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___DeathExplosion_9)); }
	inline String_t* get_DeathExplosion_9() const { return ___DeathExplosion_9; }
	inline String_t** get_address_of_DeathExplosion_9() { return &___DeathExplosion_9; }
	inline void set_DeathExplosion_9(String_t* value)
	{
		___DeathExplosion_9 = value;
		Il2CppCodeGenWriteBarrier((&___DeathExplosion_9), value);
	}

	inline static int32_t get_offset_of_HealDuration_10() { return static_cast<int32_t>(offsetof(TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31, ___HealDuration_10)); }
	inline float get_HealDuration_10() const { return ___HealDuration_10; }
	inline float* get_address_of_HealDuration_10() { return &___HealDuration_10; }
	inline void set_HealDuration_10(float value)
	{
		___HealDuration_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERMODEL_T9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31_H
#ifndef TRAPMODEL_TC99E2852889B4321B6754DFDDD005862CDC965EC_H
#define TRAPMODEL_TC99E2852889B4321B6754DFDDD005862CDC965EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapModel
struct  TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC  : public RuntimeObject
{
public:
	// System.String TrapModel::Id
	String_t* ___Id_0;
	// System.String TrapModel::Bundle
	String_t* ___Bundle_1;
	// System.String TrapModel::Prefab
	String_t* ___Prefab_2;
	// System.String TrapModel::Icon
	String_t* ___Icon_3;
	// SpellAI TrapModel::SpellAI
	int32_t ___SpellAI_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> TrapModel::AIData
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___AIData_5;
	// System.Single TrapModel::Range
	float ___Range_6;
	// System.Boolean TrapModel::IsScalable
	bool ___IsScalable_7;
	// System.String TrapModel::Spell
	String_t* ___Spell_8;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Prefab_2)); }
	inline String_t* get_Prefab_2() const { return ___Prefab_2; }
	inline String_t** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(String_t* value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Icon_3() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Icon_3)); }
	inline String_t* get_Icon_3() const { return ___Icon_3; }
	inline String_t** get_address_of_Icon_3() { return &___Icon_3; }
	inline void set_Icon_3(String_t* value)
	{
		___Icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_3), value);
	}

	inline static int32_t get_offset_of_SpellAI_4() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___SpellAI_4)); }
	inline int32_t get_SpellAI_4() const { return ___SpellAI_4; }
	inline int32_t* get_address_of_SpellAI_4() { return &___SpellAI_4; }
	inline void set_SpellAI_4(int32_t value)
	{
		___SpellAI_4 = value;
	}

	inline static int32_t get_offset_of_AIData_5() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___AIData_5)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_AIData_5() const { return ___AIData_5; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_AIData_5() { return &___AIData_5; }
	inline void set_AIData_5(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___AIData_5 = value;
		Il2CppCodeGenWriteBarrier((&___AIData_5), value);
	}

	inline static int32_t get_offset_of_Range_6() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Range_6)); }
	inline float get_Range_6() const { return ___Range_6; }
	inline float* get_address_of_Range_6() { return &___Range_6; }
	inline void set_Range_6(float value)
	{
		___Range_6 = value;
	}

	inline static int32_t get_offset_of_IsScalable_7() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___IsScalable_7)); }
	inline bool get_IsScalable_7() const { return ___IsScalable_7; }
	inline bool* get_address_of_IsScalable_7() { return &___IsScalable_7; }
	inline void set_IsScalable_7(bool value)
	{
		___IsScalable_7 = value;
	}

	inline static int32_t get_offset_of_Spell_8() { return static_cast<int32_t>(offsetof(TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC, ___Spell_8)); }
	inline String_t* get_Spell_8() const { return ___Spell_8; }
	inline String_t** get_address_of_Spell_8() { return &___Spell_8; }
	inline void set_Spell_8(String_t* value)
	{
		___Spell_8 = value;
		Il2CppCodeGenWriteBarrier((&___Spell_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPMODEL_TC99E2852889B4321B6754DFDDD005862CDC965EC_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef USERVO_TA99AB6795F202AC868B7E38E52B808DFAAD4DB94_H
#define USERVO_TA99AB6795F202AC868B7E38E52B808DFAAD4DB94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserVO
struct  UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94  : public RuntimeObject
{
public:
	// System.String UserVO::Hero
	String_t* ___Hero_0;
	// System.String UserVO::UserCountry
	String_t* ___UserCountry_1;
	// System.String UserVO::TeamId
	String_t* ___TeamId_2;
	// System.String UserVO::UserId
	String_t* ___UserId_3;
	// System.Boolean UserVO::Fx
	bool ___Fx_4;
	// System.Boolean UserVO::Music
	bool ___Music_5;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt UserVO::UserLevel
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___UserLevel_6;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt UserVO::UserTrophy
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___UserTrophy_7;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt UserVO::Stars
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___Stars_8;
	// System.Collections.Generic.List`1<System.String> UserVO::TeamsPending
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___TeamsPending_9;
	// System.TimeSpan UserVO::ClientServerOffset
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___ClientServerOffset_10;
	// System.Collections.Generic.List`1<System.String> UserVO::TutorialsCompleted
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___TutorialsCompleted_11;
	// System.Boolean UserVO::Spell1Unlocked
	bool ___Spell1Unlocked_12;
	// System.Boolean UserVO::Spell2Unlocked
	bool ___Spell2Unlocked_13;
	// System.Boolean UserVO::Spell3Unlocked
	bool ___Spell3Unlocked_14;
	// System.Boolean UserVO::Spell4Unlocked
	bool ___Spell4Unlocked_15;

public:
	inline static int32_t get_offset_of_Hero_0() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Hero_0)); }
	inline String_t* get_Hero_0() const { return ___Hero_0; }
	inline String_t** get_address_of_Hero_0() { return &___Hero_0; }
	inline void set_Hero_0(String_t* value)
	{
		___Hero_0 = value;
		Il2CppCodeGenWriteBarrier((&___Hero_0), value);
	}

	inline static int32_t get_offset_of_UserCountry_1() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___UserCountry_1)); }
	inline String_t* get_UserCountry_1() const { return ___UserCountry_1; }
	inline String_t** get_address_of_UserCountry_1() { return &___UserCountry_1; }
	inline void set_UserCountry_1(String_t* value)
	{
		___UserCountry_1 = value;
		Il2CppCodeGenWriteBarrier((&___UserCountry_1), value);
	}

	inline static int32_t get_offset_of_TeamId_2() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___TeamId_2)); }
	inline String_t* get_TeamId_2() const { return ___TeamId_2; }
	inline String_t** get_address_of_TeamId_2() { return &___TeamId_2; }
	inline void set_TeamId_2(String_t* value)
	{
		___TeamId_2 = value;
		Il2CppCodeGenWriteBarrier((&___TeamId_2), value);
	}

	inline static int32_t get_offset_of_UserId_3() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___UserId_3)); }
	inline String_t* get_UserId_3() const { return ___UserId_3; }
	inline String_t** get_address_of_UserId_3() { return &___UserId_3; }
	inline void set_UserId_3(String_t* value)
	{
		___UserId_3 = value;
		Il2CppCodeGenWriteBarrier((&___UserId_3), value);
	}

	inline static int32_t get_offset_of_Fx_4() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Fx_4)); }
	inline bool get_Fx_4() const { return ___Fx_4; }
	inline bool* get_address_of_Fx_4() { return &___Fx_4; }
	inline void set_Fx_4(bool value)
	{
		___Fx_4 = value;
	}

	inline static int32_t get_offset_of_Music_5() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Music_5)); }
	inline bool get_Music_5() const { return ___Music_5; }
	inline bool* get_address_of_Music_5() { return &___Music_5; }
	inline void set_Music_5(bool value)
	{
		___Music_5 = value;
	}

	inline static int32_t get_offset_of_UserLevel_6() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___UserLevel_6)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_UserLevel_6() const { return ___UserLevel_6; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_UserLevel_6() { return &___UserLevel_6; }
	inline void set_UserLevel_6(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___UserLevel_6 = value;
	}

	inline static int32_t get_offset_of_UserTrophy_7() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___UserTrophy_7)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_UserTrophy_7() const { return ___UserTrophy_7; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_UserTrophy_7() { return &___UserTrophy_7; }
	inline void set_UserTrophy_7(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___UserTrophy_7 = value;
	}

	inline static int32_t get_offset_of_Stars_8() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Stars_8)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_Stars_8() const { return ___Stars_8; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_Stars_8() { return &___Stars_8; }
	inline void set_Stars_8(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___Stars_8 = value;
	}

	inline static int32_t get_offset_of_TeamsPending_9() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___TeamsPending_9)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_TeamsPending_9() const { return ___TeamsPending_9; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_TeamsPending_9() { return &___TeamsPending_9; }
	inline void set_TeamsPending_9(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___TeamsPending_9 = value;
		Il2CppCodeGenWriteBarrier((&___TeamsPending_9), value);
	}

	inline static int32_t get_offset_of_ClientServerOffset_10() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___ClientServerOffset_10)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_ClientServerOffset_10() const { return ___ClientServerOffset_10; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_ClientServerOffset_10() { return &___ClientServerOffset_10; }
	inline void set_ClientServerOffset_10(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___ClientServerOffset_10 = value;
	}

	inline static int32_t get_offset_of_TutorialsCompleted_11() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___TutorialsCompleted_11)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_TutorialsCompleted_11() const { return ___TutorialsCompleted_11; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_TutorialsCompleted_11() { return &___TutorialsCompleted_11; }
	inline void set_TutorialsCompleted_11(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___TutorialsCompleted_11 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialsCompleted_11), value);
	}

	inline static int32_t get_offset_of_Spell1Unlocked_12() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Spell1Unlocked_12)); }
	inline bool get_Spell1Unlocked_12() const { return ___Spell1Unlocked_12; }
	inline bool* get_address_of_Spell1Unlocked_12() { return &___Spell1Unlocked_12; }
	inline void set_Spell1Unlocked_12(bool value)
	{
		___Spell1Unlocked_12 = value;
	}

	inline static int32_t get_offset_of_Spell2Unlocked_13() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Spell2Unlocked_13)); }
	inline bool get_Spell2Unlocked_13() const { return ___Spell2Unlocked_13; }
	inline bool* get_address_of_Spell2Unlocked_13() { return &___Spell2Unlocked_13; }
	inline void set_Spell2Unlocked_13(bool value)
	{
		___Spell2Unlocked_13 = value;
	}

	inline static int32_t get_offset_of_Spell3Unlocked_14() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Spell3Unlocked_14)); }
	inline bool get_Spell3Unlocked_14() const { return ___Spell3Unlocked_14; }
	inline bool* get_address_of_Spell3Unlocked_14() { return &___Spell3Unlocked_14; }
	inline void set_Spell3Unlocked_14(bool value)
	{
		___Spell3Unlocked_14 = value;
	}

	inline static int32_t get_offset_of_Spell4Unlocked_15() { return static_cast<int32_t>(offsetof(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94, ___Spell4Unlocked_15)); }
	inline bool get_Spell4Unlocked_15() const { return ___Spell4Unlocked_15; }
	inline bool* get_address_of_Spell4Unlocked_15() { return &___Spell4Unlocked_15; }
	inline void set_Spell4Unlocked_15(bool value)
	{
		___Spell4Unlocked_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERVO_TA99AB6795F202AC868B7E38E52B808DFAAD4DB94_H
#ifndef VISUALEFFECTMODEL_T0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3_H
#define VISUALEFFECTMODEL_T0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VisualEffectModel
struct  VisualEffectModel_t0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3  : public RuntimeObject
{
public:
	// VisualEffectType VisualEffectModel::Type
	int32_t ___Type_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> VisualEffectModel::Data
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___Data_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(VisualEffectModel_t0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Data_1() { return static_cast<int32_t>(offsetof(VisualEffectModel_t0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3, ___Data_1)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_Data_1() const { return ___Data_1; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_Data_1() { return &___Data_1; }
	inline void set_Data_1(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___Data_1 = value;
		Il2CppCodeGenWriteBarrier((&___Data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALEFFECTMODEL_T0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3_H
#ifndef BATTLESETTINGSSO_T49214DEA165282C3E2BD7F72B6223E67DF19A3FE_H
#define BATTLESETTINGSSO_T49214DEA165282C3E2BD7F72B6223E67DF19A3FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleSettingsSO
struct  BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String BattleSettingsSO::DefaultBoost
	String_t* ___DefaultBoost_4;
	// System.String BattleSettingsSO::SpecialBoost
	String_t* ___SpecialBoost_5;
	// System.Int32 BattleSettingsSO::BattleInventorySize
	int32_t ___BattleInventorySize_6;
	// System.Int32 BattleSettingsSO::ThreeStarsPercentage
	int32_t ___ThreeStarsPercentage_7;
	// System.Int32 BattleSettingsSO::TwoStarsPercentage
	int32_t ___TwoStarsPercentage_8;
	// System.String BattleSettingsSO::ReviveSpellId
	String_t* ___ReviveSpellId_9;
	// System.Int32 BattleSettingsSO::ReviveCost
	int32_t ___ReviveCost_10;
	// System.String BattleSettingsSO::Spell1
	String_t* ___Spell1_11;
	// System.Int32 BattleSettingsSO::Spell1Unlock
	int32_t ___Spell1Unlock_12;
	// System.Int32 BattleSettingsSO::Spell1InitialValue
	int32_t ___Spell1InitialValue_13;
	// System.String BattleSettingsSO::Spell2
	String_t* ___Spell2_14;
	// System.Int32 BattleSettingsSO::Spell2Unlock
	int32_t ___Spell2Unlock_15;
	// System.Int32 BattleSettingsSO::Spell2InitialValue
	int32_t ___Spell2InitialValue_16;
	// System.String BattleSettingsSO::Spell3
	String_t* ___Spell3_17;
	// System.Int32 BattleSettingsSO::Spell3Unlock
	int32_t ___Spell3Unlock_18;
	// System.Int32 BattleSettingsSO::Spell3InitialValue
	int32_t ___Spell3InitialValue_19;
	// System.String BattleSettingsSO::Spell4
	String_t* ___Spell4_20;
	// System.Int32 BattleSettingsSO::Spell4Unlock
	int32_t ___Spell4Unlock_21;
	// System.Int32 BattleSettingsSO::Spell4InitialValue
	int32_t ___Spell4InitialValue_22;

public:
	inline static int32_t get_offset_of_DefaultBoost_4() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___DefaultBoost_4)); }
	inline String_t* get_DefaultBoost_4() const { return ___DefaultBoost_4; }
	inline String_t** get_address_of_DefaultBoost_4() { return &___DefaultBoost_4; }
	inline void set_DefaultBoost_4(String_t* value)
	{
		___DefaultBoost_4 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultBoost_4), value);
	}

	inline static int32_t get_offset_of_SpecialBoost_5() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___SpecialBoost_5)); }
	inline String_t* get_SpecialBoost_5() const { return ___SpecialBoost_5; }
	inline String_t** get_address_of_SpecialBoost_5() { return &___SpecialBoost_5; }
	inline void set_SpecialBoost_5(String_t* value)
	{
		___SpecialBoost_5 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialBoost_5), value);
	}

	inline static int32_t get_offset_of_BattleInventorySize_6() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___BattleInventorySize_6)); }
	inline int32_t get_BattleInventorySize_6() const { return ___BattleInventorySize_6; }
	inline int32_t* get_address_of_BattleInventorySize_6() { return &___BattleInventorySize_6; }
	inline void set_BattleInventorySize_6(int32_t value)
	{
		___BattleInventorySize_6 = value;
	}

	inline static int32_t get_offset_of_ThreeStarsPercentage_7() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___ThreeStarsPercentage_7)); }
	inline int32_t get_ThreeStarsPercentage_7() const { return ___ThreeStarsPercentage_7; }
	inline int32_t* get_address_of_ThreeStarsPercentage_7() { return &___ThreeStarsPercentage_7; }
	inline void set_ThreeStarsPercentage_7(int32_t value)
	{
		___ThreeStarsPercentage_7 = value;
	}

	inline static int32_t get_offset_of_TwoStarsPercentage_8() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___TwoStarsPercentage_8)); }
	inline int32_t get_TwoStarsPercentage_8() const { return ___TwoStarsPercentage_8; }
	inline int32_t* get_address_of_TwoStarsPercentage_8() { return &___TwoStarsPercentage_8; }
	inline void set_TwoStarsPercentage_8(int32_t value)
	{
		___TwoStarsPercentage_8 = value;
	}

	inline static int32_t get_offset_of_ReviveSpellId_9() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___ReviveSpellId_9)); }
	inline String_t* get_ReviveSpellId_9() const { return ___ReviveSpellId_9; }
	inline String_t** get_address_of_ReviveSpellId_9() { return &___ReviveSpellId_9; }
	inline void set_ReviveSpellId_9(String_t* value)
	{
		___ReviveSpellId_9 = value;
		Il2CppCodeGenWriteBarrier((&___ReviveSpellId_9), value);
	}

	inline static int32_t get_offset_of_ReviveCost_10() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___ReviveCost_10)); }
	inline int32_t get_ReviveCost_10() const { return ___ReviveCost_10; }
	inline int32_t* get_address_of_ReviveCost_10() { return &___ReviveCost_10; }
	inline void set_ReviveCost_10(int32_t value)
	{
		___ReviveCost_10 = value;
	}

	inline static int32_t get_offset_of_Spell1_11() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell1_11)); }
	inline String_t* get_Spell1_11() const { return ___Spell1_11; }
	inline String_t** get_address_of_Spell1_11() { return &___Spell1_11; }
	inline void set_Spell1_11(String_t* value)
	{
		___Spell1_11 = value;
		Il2CppCodeGenWriteBarrier((&___Spell1_11), value);
	}

	inline static int32_t get_offset_of_Spell1Unlock_12() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell1Unlock_12)); }
	inline int32_t get_Spell1Unlock_12() const { return ___Spell1Unlock_12; }
	inline int32_t* get_address_of_Spell1Unlock_12() { return &___Spell1Unlock_12; }
	inline void set_Spell1Unlock_12(int32_t value)
	{
		___Spell1Unlock_12 = value;
	}

	inline static int32_t get_offset_of_Spell1InitialValue_13() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell1InitialValue_13)); }
	inline int32_t get_Spell1InitialValue_13() const { return ___Spell1InitialValue_13; }
	inline int32_t* get_address_of_Spell1InitialValue_13() { return &___Spell1InitialValue_13; }
	inline void set_Spell1InitialValue_13(int32_t value)
	{
		___Spell1InitialValue_13 = value;
	}

	inline static int32_t get_offset_of_Spell2_14() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell2_14)); }
	inline String_t* get_Spell2_14() const { return ___Spell2_14; }
	inline String_t** get_address_of_Spell2_14() { return &___Spell2_14; }
	inline void set_Spell2_14(String_t* value)
	{
		___Spell2_14 = value;
		Il2CppCodeGenWriteBarrier((&___Spell2_14), value);
	}

	inline static int32_t get_offset_of_Spell2Unlock_15() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell2Unlock_15)); }
	inline int32_t get_Spell2Unlock_15() const { return ___Spell2Unlock_15; }
	inline int32_t* get_address_of_Spell2Unlock_15() { return &___Spell2Unlock_15; }
	inline void set_Spell2Unlock_15(int32_t value)
	{
		___Spell2Unlock_15 = value;
	}

	inline static int32_t get_offset_of_Spell2InitialValue_16() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell2InitialValue_16)); }
	inline int32_t get_Spell2InitialValue_16() const { return ___Spell2InitialValue_16; }
	inline int32_t* get_address_of_Spell2InitialValue_16() { return &___Spell2InitialValue_16; }
	inline void set_Spell2InitialValue_16(int32_t value)
	{
		___Spell2InitialValue_16 = value;
	}

	inline static int32_t get_offset_of_Spell3_17() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell3_17)); }
	inline String_t* get_Spell3_17() const { return ___Spell3_17; }
	inline String_t** get_address_of_Spell3_17() { return &___Spell3_17; }
	inline void set_Spell3_17(String_t* value)
	{
		___Spell3_17 = value;
		Il2CppCodeGenWriteBarrier((&___Spell3_17), value);
	}

	inline static int32_t get_offset_of_Spell3Unlock_18() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell3Unlock_18)); }
	inline int32_t get_Spell3Unlock_18() const { return ___Spell3Unlock_18; }
	inline int32_t* get_address_of_Spell3Unlock_18() { return &___Spell3Unlock_18; }
	inline void set_Spell3Unlock_18(int32_t value)
	{
		___Spell3Unlock_18 = value;
	}

	inline static int32_t get_offset_of_Spell3InitialValue_19() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell3InitialValue_19)); }
	inline int32_t get_Spell3InitialValue_19() const { return ___Spell3InitialValue_19; }
	inline int32_t* get_address_of_Spell3InitialValue_19() { return &___Spell3InitialValue_19; }
	inline void set_Spell3InitialValue_19(int32_t value)
	{
		___Spell3InitialValue_19 = value;
	}

	inline static int32_t get_offset_of_Spell4_20() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell4_20)); }
	inline String_t* get_Spell4_20() const { return ___Spell4_20; }
	inline String_t** get_address_of_Spell4_20() { return &___Spell4_20; }
	inline void set_Spell4_20(String_t* value)
	{
		___Spell4_20 = value;
		Il2CppCodeGenWriteBarrier((&___Spell4_20), value);
	}

	inline static int32_t get_offset_of_Spell4Unlock_21() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell4Unlock_21)); }
	inline int32_t get_Spell4Unlock_21() const { return ___Spell4Unlock_21; }
	inline int32_t* get_address_of_Spell4Unlock_21() { return &___Spell4Unlock_21; }
	inline void set_Spell4Unlock_21(int32_t value)
	{
		___Spell4Unlock_21 = value;
	}

	inline static int32_t get_offset_of_Spell4InitialValue_22() { return static_cast<int32_t>(offsetof(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE, ___Spell4InitialValue_22)); }
	inline int32_t get_Spell4InitialValue_22() const { return ___Spell4InitialValue_22; }
	inline int32_t* get_address_of_Spell4InitialValue_22() { return &___Spell4InitialValue_22; }
	inline void set_Spell4InitialValue_22(int32_t value)
	{
		___Spell4InitialValue_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLESETTINGSSO_T49214DEA165282C3E2BD7F72B6223E67DF19A3FE_H
#ifndef COUNTERVARIABLE_T6E3A5044D7B6B8BCA9F818D51DE3D432EED30468_H
#define COUNTERVARIABLE_T6E3A5044D7B6B8BCA9F818D51DE3D432EED30468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CounterVariable
struct  CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468  : public Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD
{
public:
	// CounterType CounterVariable::_type
	int32_t ____type_4;
	// System.String CounterVariable::_parametre
	String_t* ____parametre_5;
	// CounterSystem CounterVariable::_counterSystem
	CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5 * ____counterSystem_6;

public:
	inline static int32_t get_offset_of__type_4() { return static_cast<int32_t>(offsetof(CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468, ____type_4)); }
	inline int32_t get__type_4() const { return ____type_4; }
	inline int32_t* get_address_of__type_4() { return &____type_4; }
	inline void set__type_4(int32_t value)
	{
		____type_4 = value;
	}

	inline static int32_t get_offset_of__parametre_5() { return static_cast<int32_t>(offsetof(CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468, ____parametre_5)); }
	inline String_t* get__parametre_5() const { return ____parametre_5; }
	inline String_t** get_address_of__parametre_5() { return &____parametre_5; }
	inline void set__parametre_5(String_t* value)
	{
		____parametre_5 = value;
		Il2CppCodeGenWriteBarrier((&____parametre_5), value);
	}

	inline static int32_t get_offset_of__counterSystem_6() { return static_cast<int32_t>(offsetof(CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468, ____counterSystem_6)); }
	inline CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5 * get__counterSystem_6() const { return ____counterSystem_6; }
	inline CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5 ** get_address_of__counterSystem_6() { return &____counterSystem_6; }
	inline void set__counterSystem_6(CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5 * value)
	{
		____counterSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&____counterSystem_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTERVARIABLE_T6E3A5044D7B6B8BCA9F818D51DE3D432EED30468_H
#ifndef CUSTOMTUTORIALSO_T45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D_H
#define CUSTOMTUTORIALSO_T45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomTutorialSO
struct  CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<CustomTutorialModel> CustomTutorialSO::CustomTutorials
	List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C * ___CustomTutorials_4;

public:
	inline static int32_t get_offset_of_CustomTutorials_4() { return static_cast<int32_t>(offsetof(CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D, ___CustomTutorials_4)); }
	inline List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C * get_CustomTutorials_4() const { return ___CustomTutorials_4; }
	inline List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C ** get_address_of_CustomTutorials_4() { return &___CustomTutorials_4; }
	inline void set_CustomTutorials_4(List_1_tA4D8069E4BD0FB418FBF665C39E5CDE768F2CE0C * value)
	{
		___CustomTutorials_4 = value;
		Il2CppCodeGenWriteBarrier((&___CustomTutorials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMTUTORIALSO_T45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D_H
#ifndef EFFECTVISUALSO_T1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC_H
#define EFFECTVISUALSO_T1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualSO
struct  EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<EffectVisualModel> EffectVisualSO::Visuals
	List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 * ___Visuals_4;

public:
	inline static int32_t get_offset_of_Visuals_4() { return static_cast<int32_t>(offsetof(EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC, ___Visuals_4)); }
	inline List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 * get_Visuals_4() const { return ___Visuals_4; }
	inline List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 ** get_address_of_Visuals_4() { return &___Visuals_4; }
	inline void set_Visuals_4(List_1_tB04C00DC79DD1B4D62E9A575C6F56E38DAD22984 * value)
	{
		___Visuals_4 = value;
		Il2CppCodeGenWriteBarrier((&___Visuals_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALSO_T1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC_H
#ifndef GAMESETTINGSSO_TC1B94CA47B375640E013220718E7800109B9058C_H
#define GAMESETTINGSSO_TC1B94CA47B375640E013220718E7800109B9058C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSettingsSO
struct  GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Single GameSettingsSO::LeaderboardUpdateTime
	float ___LeaderboardUpdateTime_4;
	// System.Int32 GameSettingsSO::IconCount
	int32_t ___IconCount_5;
	// System.Int32 GameSettingsSO::InitCoins
	int32_t ___InitCoins_6;
	// System.Int32 GameSettingsSO::InitHearts
	int32_t ___InitHearts_7;
	// System.Int32 GameSettingsSO::MaxHearts
	int32_t ___MaxHearts_8;
	// System.Int32 GameSettingsSO::LiveRecoveryTimeInMinutes
	int32_t ___LiveRecoveryTimeInMinutes_9;
	// System.Int32 GameSettingsSO::FirstTimeJoinTeamReward
	int32_t ___FirstTimeJoinTeamReward_10;
	// System.Int32 GameSettingsSO::MinLevelToJoinTeam
	int32_t ___MinLevelToJoinTeam_11;
	// System.String GameSettingsSO::SurveyUrl
	String_t* ___SurveyUrl_12;
	// System.Single GameSettingsSO::GlobalTweenDuration
	float ___GlobalTweenDuration_13;

public:
	inline static int32_t get_offset_of_LeaderboardUpdateTime_4() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___LeaderboardUpdateTime_4)); }
	inline float get_LeaderboardUpdateTime_4() const { return ___LeaderboardUpdateTime_4; }
	inline float* get_address_of_LeaderboardUpdateTime_4() { return &___LeaderboardUpdateTime_4; }
	inline void set_LeaderboardUpdateTime_4(float value)
	{
		___LeaderboardUpdateTime_4 = value;
	}

	inline static int32_t get_offset_of_IconCount_5() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___IconCount_5)); }
	inline int32_t get_IconCount_5() const { return ___IconCount_5; }
	inline int32_t* get_address_of_IconCount_5() { return &___IconCount_5; }
	inline void set_IconCount_5(int32_t value)
	{
		___IconCount_5 = value;
	}

	inline static int32_t get_offset_of_InitCoins_6() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___InitCoins_6)); }
	inline int32_t get_InitCoins_6() const { return ___InitCoins_6; }
	inline int32_t* get_address_of_InitCoins_6() { return &___InitCoins_6; }
	inline void set_InitCoins_6(int32_t value)
	{
		___InitCoins_6 = value;
	}

	inline static int32_t get_offset_of_InitHearts_7() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___InitHearts_7)); }
	inline int32_t get_InitHearts_7() const { return ___InitHearts_7; }
	inline int32_t* get_address_of_InitHearts_7() { return &___InitHearts_7; }
	inline void set_InitHearts_7(int32_t value)
	{
		___InitHearts_7 = value;
	}

	inline static int32_t get_offset_of_MaxHearts_8() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___MaxHearts_8)); }
	inline int32_t get_MaxHearts_8() const { return ___MaxHearts_8; }
	inline int32_t* get_address_of_MaxHearts_8() { return &___MaxHearts_8; }
	inline void set_MaxHearts_8(int32_t value)
	{
		___MaxHearts_8 = value;
	}

	inline static int32_t get_offset_of_LiveRecoveryTimeInMinutes_9() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___LiveRecoveryTimeInMinutes_9)); }
	inline int32_t get_LiveRecoveryTimeInMinutes_9() const { return ___LiveRecoveryTimeInMinutes_9; }
	inline int32_t* get_address_of_LiveRecoveryTimeInMinutes_9() { return &___LiveRecoveryTimeInMinutes_9; }
	inline void set_LiveRecoveryTimeInMinutes_9(int32_t value)
	{
		___LiveRecoveryTimeInMinutes_9 = value;
	}

	inline static int32_t get_offset_of_FirstTimeJoinTeamReward_10() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___FirstTimeJoinTeamReward_10)); }
	inline int32_t get_FirstTimeJoinTeamReward_10() const { return ___FirstTimeJoinTeamReward_10; }
	inline int32_t* get_address_of_FirstTimeJoinTeamReward_10() { return &___FirstTimeJoinTeamReward_10; }
	inline void set_FirstTimeJoinTeamReward_10(int32_t value)
	{
		___FirstTimeJoinTeamReward_10 = value;
	}

	inline static int32_t get_offset_of_MinLevelToJoinTeam_11() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___MinLevelToJoinTeam_11)); }
	inline int32_t get_MinLevelToJoinTeam_11() const { return ___MinLevelToJoinTeam_11; }
	inline int32_t* get_address_of_MinLevelToJoinTeam_11() { return &___MinLevelToJoinTeam_11; }
	inline void set_MinLevelToJoinTeam_11(int32_t value)
	{
		___MinLevelToJoinTeam_11 = value;
	}

	inline static int32_t get_offset_of_SurveyUrl_12() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___SurveyUrl_12)); }
	inline String_t* get_SurveyUrl_12() const { return ___SurveyUrl_12; }
	inline String_t** get_address_of_SurveyUrl_12() { return &___SurveyUrl_12; }
	inline void set_SurveyUrl_12(String_t* value)
	{
		___SurveyUrl_12 = value;
		Il2CppCodeGenWriteBarrier((&___SurveyUrl_12), value);
	}

	inline static int32_t get_offset_of_GlobalTweenDuration_13() { return static_cast<int32_t>(offsetof(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C, ___GlobalTweenDuration_13)); }
	inline float get_GlobalTweenDuration_13() const { return ___GlobalTweenDuration_13; }
	inline float* get_address_of_GlobalTweenDuration_13() { return &___GlobalTweenDuration_13; }
	inline void set_GlobalTweenDuration_13(float value)
	{
		___GlobalTweenDuration_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESETTINGSSO_TC1B94CA47B375640E013220718E7800109B9058C_H
#ifndef ITEMSSO_TD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A_H
#define ITEMSSO_TD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsSO
struct  ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<ItemModel> ItemsSO::Items
	List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 * ___Items_4;

public:
	inline static int32_t get_offset_of_Items_4() { return static_cast<int32_t>(offsetof(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A, ___Items_4)); }
	inline List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 * get_Items_4() const { return ___Items_4; }
	inline List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 ** get_address_of_Items_4() { return &___Items_4; }
	inline void set_Items_4(List_1_t0B47A19D77897D2BC30A0820609E3BBA2156F084 * value)
	{
		___Items_4 = value;
		Il2CppCodeGenWriteBarrier((&___Items_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSSO_TD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A_H
#ifndef LEVELSSO_T51232A976915E19D66106CE9DD068463ADBAD0EF_H
#define LEVELSSO_T51232A976915E19D66106CE9DD068463ADBAD0EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelsSO
struct  LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// LevelModel[] LevelsSO::Levels
	LevelModelU5BU5D_tC0711C4A332B0AF163B4C92FAF063F71483EB3DB* ___Levels_4;

public:
	inline static int32_t get_offset_of_Levels_4() { return static_cast<int32_t>(offsetof(LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF, ___Levels_4)); }
	inline LevelModelU5BU5D_tC0711C4A332B0AF163B4C92FAF063F71483EB3DB* get_Levels_4() const { return ___Levels_4; }
	inline LevelModelU5BU5D_tC0711C4A332B0AF163B4C92FAF063F71483EB3DB** get_address_of_Levels_4() { return &___Levels_4; }
	inline void set_Levels_4(LevelModelU5BU5D_tC0711C4A332B0AF163B4C92FAF063F71483EB3DB* value)
	{
		___Levels_4 = value;
		Il2CppCodeGenWriteBarrier((&___Levels_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSSO_T51232A976915E19D66106CE9DD068463ADBAD0EF_H
#ifndef PERLEVELTUTORIALSO_TB55F6372D80FCA75DD834538D5137169A7A7E66B_H
#define PERLEVELTUTORIALSO_TB55F6372D80FCA75DD834538D5137169A7A7E66B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PerLevelTutorialSO
struct  PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<PerLevelTutorialModel> PerLevelTutorialSO::Tutorials
	List_1_tBE32162E196BAE003F483B65C07526E97560480C * ___Tutorials_4;

public:
	inline static int32_t get_offset_of_Tutorials_4() { return static_cast<int32_t>(offsetof(PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B, ___Tutorials_4)); }
	inline List_1_tBE32162E196BAE003F483B65C07526E97560480C * get_Tutorials_4() const { return ___Tutorials_4; }
	inline List_1_tBE32162E196BAE003F483B65C07526E97560480C ** get_address_of_Tutorials_4() { return &___Tutorials_4; }
	inline void set_Tutorials_4(List_1_tBE32162E196BAE003F483B65C07526E97560480C * value)
	{
		___Tutorials_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tutorials_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERLEVELTUTORIALSO_TB55F6372D80FCA75DD834538D5137169A7A7E66B_H
#ifndef SOUNDSO_T9B0D26B0A9F986D8019428EBC54B7CC6259E2C73_H
#define SOUNDSO_T9B0D26B0A9F986D8019428EBC54B7CC6259E2C73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundSO
struct  SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String SoundSO::BackgroundMain
	String_t* ___BackgroundMain_4;
	// System.String SoundSO::BackgroundBattle
	String_t* ___BackgroundBattle_5;
	// System.String SoundSO::Button
	String_t* ___Button_6;
	// System.String SoundSO::SummaryWin
	String_t* ___SummaryWin_7;
	// System.String SoundSO::SummaryLose
	String_t* ___SummaryLose_8;
	// System.String SoundSO::Sword
	String_t* ___Sword_9;
	// System.String SoundSO::Damage
	String_t* ___Damage_10;
	// System.Single SoundSO::DamageDelay
	float ___DamageDelay_11;

public:
	inline static int32_t get_offset_of_BackgroundMain_4() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___BackgroundMain_4)); }
	inline String_t* get_BackgroundMain_4() const { return ___BackgroundMain_4; }
	inline String_t** get_address_of_BackgroundMain_4() { return &___BackgroundMain_4; }
	inline void set_BackgroundMain_4(String_t* value)
	{
		___BackgroundMain_4 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundMain_4), value);
	}

	inline static int32_t get_offset_of_BackgroundBattle_5() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___BackgroundBattle_5)); }
	inline String_t* get_BackgroundBattle_5() const { return ___BackgroundBattle_5; }
	inline String_t** get_address_of_BackgroundBattle_5() { return &___BackgroundBattle_5; }
	inline void set_BackgroundBattle_5(String_t* value)
	{
		___BackgroundBattle_5 = value;
		Il2CppCodeGenWriteBarrier((&___BackgroundBattle_5), value);
	}

	inline static int32_t get_offset_of_Button_6() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___Button_6)); }
	inline String_t* get_Button_6() const { return ___Button_6; }
	inline String_t** get_address_of_Button_6() { return &___Button_6; }
	inline void set_Button_6(String_t* value)
	{
		___Button_6 = value;
		Il2CppCodeGenWriteBarrier((&___Button_6), value);
	}

	inline static int32_t get_offset_of_SummaryWin_7() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___SummaryWin_7)); }
	inline String_t* get_SummaryWin_7() const { return ___SummaryWin_7; }
	inline String_t** get_address_of_SummaryWin_7() { return &___SummaryWin_7; }
	inline void set_SummaryWin_7(String_t* value)
	{
		___SummaryWin_7 = value;
		Il2CppCodeGenWriteBarrier((&___SummaryWin_7), value);
	}

	inline static int32_t get_offset_of_SummaryLose_8() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___SummaryLose_8)); }
	inline String_t* get_SummaryLose_8() const { return ___SummaryLose_8; }
	inline String_t** get_address_of_SummaryLose_8() { return &___SummaryLose_8; }
	inline void set_SummaryLose_8(String_t* value)
	{
		___SummaryLose_8 = value;
		Il2CppCodeGenWriteBarrier((&___SummaryLose_8), value);
	}

	inline static int32_t get_offset_of_Sword_9() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___Sword_9)); }
	inline String_t* get_Sword_9() const { return ___Sword_9; }
	inline String_t** get_address_of_Sword_9() { return &___Sword_9; }
	inline void set_Sword_9(String_t* value)
	{
		___Sword_9 = value;
		Il2CppCodeGenWriteBarrier((&___Sword_9), value);
	}

	inline static int32_t get_offset_of_Damage_10() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___Damage_10)); }
	inline String_t* get_Damage_10() const { return ___Damage_10; }
	inline String_t** get_address_of_Damage_10() { return &___Damage_10; }
	inline void set_Damage_10(String_t* value)
	{
		___Damage_10 = value;
		Il2CppCodeGenWriteBarrier((&___Damage_10), value);
	}

	inline static int32_t get_offset_of_DamageDelay_11() { return static_cast<int32_t>(offsetof(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73, ___DamageDelay_11)); }
	inline float get_DamageDelay_11() const { return ___DamageDelay_11; }
	inline float* get_address_of_DamageDelay_11() { return &___DamageDelay_11; }
	inline void set_DamageDelay_11(float value)
	{
		___DamageDelay_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDSO_T9B0D26B0A9F986D8019428EBC54B7CC6259E2C73_H
#ifndef SPELLSSO_TD87C28C149EA2BE80535490B95DC914E94705439_H
#define SPELLSSO_TD87C28C149EA2BE80535490B95DC914E94705439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellsSO
struct  SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<SpellModel> SpellsSO::Spells
	List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F * ___Spells_4;

public:
	inline static int32_t get_offset_of_Spells_4() { return static_cast<int32_t>(offsetof(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439, ___Spells_4)); }
	inline List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F * get_Spells_4() const { return ___Spells_4; }
	inline List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F ** get_address_of_Spells_4() { return &___Spells_4; }
	inline void set_Spells_4(List_1_t3C7DBA87FE72C14B6D9818C99E13D8ED9660373F * value)
	{
		___Spells_4 = value;
		Il2CppCodeGenWriteBarrier((&___Spells_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLSSO_TD87C28C149EA2BE80535490B95DC914E94705439_H
#ifndef TOURNAMENTREWARDSSO_T9121EDF677AEDF239EF74868C27C0879178523E1_H
#define TOURNAMENTREWARDSSO_T9121EDF677AEDF239EF74868C27C0879178523E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TournamentRewardsSO
struct  TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// RewardModel[] TournamentRewardsSO::Rewards
	RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11* ___Rewards_4;

public:
	inline static int32_t get_offset_of_Rewards_4() { return static_cast<int32_t>(offsetof(TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1, ___Rewards_4)); }
	inline RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11* get_Rewards_4() const { return ___Rewards_4; }
	inline RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11** get_address_of_Rewards_4() { return &___Rewards_4; }
	inline void set_Rewards_4(RewardModelU5BU5D_t5B4D5EB45F969FE53860B9964AEB5E519B048C11* value)
	{
		___Rewards_4 = value;
		Il2CppCodeGenWriteBarrier((&___Rewards_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOURNAMENTREWARDSSO_T9121EDF677AEDF239EF74868C27C0879178523E1_H
#ifndef TOWERSSO_T3B7DE821E5F8017950776EEFC42E680B361BCDF0_H
#define TOWERSSO_T3B7DE821E5F8017950776EEFC42E680B361BCDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowersSO
struct  TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<TowerModel> TowersSO::Towers
	List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F * ___Towers_4;

public:
	inline static int32_t get_offset_of_Towers_4() { return static_cast<int32_t>(offsetof(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0, ___Towers_4)); }
	inline List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F * get_Towers_4() const { return ___Towers_4; }
	inline List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F ** get_address_of_Towers_4() { return &___Towers_4; }
	inline void set_Towers_4(List_1_t7D23CBEBEF47512C344C65315325FF324D97FC1F * value)
	{
		___Towers_4 = value;
		Il2CppCodeGenWriteBarrier((&___Towers_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERSSO_T3B7DE821E5F8017950776EEFC42E680B361BCDF0_H
#ifndef TRAPSSO_TFF060852B3652DD05F33E1B19B8EC9166468D9C2_H
#define TRAPSSO_TFF060852B3652DD05F33E1B19B8EC9166468D9C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapsSO
struct  TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<TrapModel> TrapsSO::Traps
	List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 * ___Traps_4;

public:
	inline static int32_t get_offset_of_Traps_4() { return static_cast<int32_t>(offsetof(TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2, ___Traps_4)); }
	inline List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 * get_Traps_4() const { return ___Traps_4; }
	inline List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 ** get_address_of_Traps_4() { return &___Traps_4; }
	inline void set_Traps_4(List_1_t5E828DBB770C7425713DBEAE9366AFC3C1C4ADA7 * value)
	{
		___Traps_4 = value;
		Il2CppCodeGenWriteBarrier((&___Traps_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPSSO_TFF060852B3652DD05F33E1B19B8EC9166468D9C2_H
#ifndef UNITSSO_T8A4174A0E155362BFFAA11BE367BD31F8D001423_H
#define UNITSSO_T8A4174A0E155362BFFAA11BE367BD31F8D001423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitsSO
struct  UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<UnitsModel> UnitsSO::Units
	List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 * ___Units_4;

public:
	inline static int32_t get_offset_of_Units_4() { return static_cast<int32_t>(offsetof(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423, ___Units_4)); }
	inline List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 * get_Units_4() const { return ___Units_4; }
	inline List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 ** get_address_of_Units_4() { return &___Units_4; }
	inline void set_Units_4(List_1_tD552FACB362EB8A9DA9B6EDB71E741DA5D570834 * value)
	{
		___Units_4 = value;
		Il2CppCodeGenWriteBarrier((&___Units_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITSSO_T8A4174A0E155362BFFAA11BE367BD31F8D001423_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8400 = { sizeof (UpdateSettingsCommandResult_t1C5BAE390EC340A9EED4A392D498941FF9EFAFFE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8401 = { sizeof (UpdateSettingsEvent_tBDFC5C0BE56631615A2FE1F8DF1593535637EC8F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8402 = { sizeof (ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF), -1, sizeof(ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8402[3] = 
{
	ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF::get_offset_of__tournamentsVO_0(),
	ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF::get_offset_of__voSaver_1(),
	ClaimTournamentRewardCommand_t39383095AB8EFBB4C86B2DAE8F1A7DE4A474F6DF_StaticFields::get_offset_of_ClaimTournamentRewardCommandEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8403 = { sizeof (ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8403[2] = 
{
	ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D::get_offset_of_TournamentId_0(),
	ClaimTournamentRewardCommandModel_t4ADDCB2DC663FF9A7A7BF212E7D3764861368E1D::get_offset_of_ResponseData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8404 = { sizeof (ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8404[1] = 
{
	ClaimTournamentRewardCommandResult_tECE3DBA1983CEB89B4011D09194CF0ABE5EFD193::get_offset_of_Rewards_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8405 = { sizeof (ClaimTournamentRewardEvent_t31E71F6AB2C04F3BD77BC744E3F0CE51D5CB0D63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8406 = { sizeof (TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085), -1, sizeof(TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8406[3] = 
{
	TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085::get_offset_of__tournamentsVO_0(),
	TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085::get_offset_of__voSaver_1(),
	TournamentResultCommand_t952310C8FDEEE1E034696B42AD609BC49FE86085_StaticFields::get_offset_of_TournamentResultCommandEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8407 = { sizeof (TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8407[1] = 
{
	TournamentResultCommandModel_t85DE82B1305AA756766D74891EF4404FF50367B6::get_offset_of_ResponseData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8408 = { sizeof (TournamentResultCommandModelResult_tCCEC4575292DCA9D77CE4E9D594BA31EDE162F77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8409 = { sizeof (TournamentResultCommandEvent_t63203EB3B05D0FB9B44625DF65238A1FDB642012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8410 = { sizeof (TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC), -1, sizeof(TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8410[3] = 
{
	TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC::get_offset_of__userVO_0(),
	TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC::get_offset_of__voSaver_1(),
	TutorialCompletedCommand_tC0EC8D50886D81D69556E9C075A66E78C3B6A2DC_StaticFields::get_offset_of_ClaimTournamentRewardCommandEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8411 = { sizeof (TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8411[1] = 
{
	TutorialCompletedCommandModel_t8E735DB75DAEA8DB6988603694DAE3DC5A702561::get_offset_of_TutorialId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8412 = { sizeof (TutorialCompletedCommandResult_t28E5178ED159419CFF37D5F86599383281ABF500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8413 = { sizeof (TutorialCompletedEvent_t6DBFFBCD3D5CD8DEA1865FB0036F52F297D85EFE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8414 = { sizeof (BattleConstants_t9FB60E5A8095C645C4A9066C587967D9B457C162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8414[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8415 = { sizeof (GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A), -1, sizeof(GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8415[1] = 
{
	GameMainConfigs_t0F18F8075F6195A8B2EC2FCB69D934E925EA071A_StaticFields::get_offset_of__gameConfigs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8416 = { sizeof (ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8416[2] = 
{
	ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F::get_offset_of_FileName_0(),
	ConfModel_t22142FE7B971DACF1840795E24BED946C0D8A77F::get_offset_of_Type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8417 = { sizeof (GameConstants_t1A630C2F5F2E6945735E954CED4C0833EDAE31CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8417[69] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8418 = { sizeof (GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33), -1, sizeof(GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8418[2] = 
{
	GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields::get_offset_of__gameContainer_0(),
	GameDiResolver_t866ECA664BF6463B4E179E3635C89291E8FD8A33_StaticFields::get_offset_of__battleContainer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8419 = { sizeof (GamePathResolver_t5EF708FE19C455A8D6940A65EB9A098F50237644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8420 = { sizeof (GameState_tABB839413827DD148BD92CA09A37DA4C24372F36), -1, sizeof(GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8420[1] = 
{
	GameState_tABB839413827DD148BD92CA09A37DA4C24372F36_StaticFields::get_offset_of_TeamCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8421 = { sizeof (BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8421[19] = 
{
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_DefaultBoost_4(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_SpecialBoost_5(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_BattleInventorySize_6(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_ThreeStarsPercentage_7(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_TwoStarsPercentage_8(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_ReviveSpellId_9(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_ReviveCost_10(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell1_11(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell1Unlock_12(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell1InitialValue_13(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell2_14(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell2Unlock_15(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell2InitialValue_16(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell3_17(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell3Unlock_18(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell3InitialValue_19(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell4_20(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell4Unlock_21(),
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE::get_offset_of_Spell4InitialValue_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8422 = { sizeof (ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8422[1] = 
{
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1::get_offset_of_Chapters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8423 = { sizeof (ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8423[2] = 
{
	ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494::get_offset_of_Bundle_0(),
	ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494::get_offset_of_MaxLevel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8424 = { sizeof (ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8424[1] = 
{
	ChestModelSO_t78F97E6AC8F488AD3C18236C4080E78CCF29B2A2::get_offset_of_Items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8425 = { sizeof (ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8425[4] = 
{
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_Content_0(),
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_Percentage_1(),
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_Number_2(),
	ChestItemModel_tFB97FEEB81B57C2A0C1DDD9C5662A3965197EFA8::get_offset_of_IsUnique_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8426 = { sizeof (ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8426[3] = 
{
	ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D::get_offset_of_Reward_0(),
	ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D::get_offset_of_Weight_1(),
	ItemContentModel_t7F429490208DE8F65A9F2278897FF14267F9983D::get_offset_of_RequirementStr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8427 = { sizeof (RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8427[3] = 
{
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44::get_offset_of_Type_0(),
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44::get_offset_of_Value_1(),
	RewardModel_tE09D75F3B47938652450ABB51F21663745C8BC44::get_offset_of_Id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8428 = { sizeof (CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8428[1] = 
{
	CustomTutorialSO_t45FC033F62BB26BC5595EAA9FEF1DF50BFA2A41D::get_offset_of_CustomTutorials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8429 = { sizeof (U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8429[1] = 
{
	U3CU3Ec__DisplayClass1_0_t8A0EA66C8B605616AE4A6A5640B599178BA16E15::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8430 = { sizeof (U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8430[1] = 
{
	U3CU3Ec__DisplayClass2_0_tA198EA948F0DC0F03FB3D83644AAE8654B69EC47::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8431 = { sizeof (CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8431[5] = 
{
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Id_0(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Type_1(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Trigger_2(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of__startTigger_3(),
	CustomTutorialModel_t7F1E6EF5B6AF58AEA9FB2573811B4562E587E635::get_offset_of_Data_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8432 = { sizeof (CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8432[6] = 
{
	CustomTutorialType_tB1BFA15E99AB96E461A01DDDBD92211896AE4968::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8433 = { sizeof (EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8433[1] = 
{
	EffectVisualSO_t1C5856B5E821FD66A4C1BD41DB5194A5610E6FFC::get_offset_of_Visuals_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8434 = { sizeof (U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8434[1] = 
{
	U3CU3Ec__DisplayClass1_0_t7A7BD7F79E3AC1F6B21194D6A498110244521FC9::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8435 = { sizeof (EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8435[2] = 
{
	EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF::get_offset_of_Id_0(),
	EffectVisualModel_t4E4CB43B096384FFC7DAC3E6DF1B2DF8DED965DF::get_offset_of_Visuals_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8436 = { sizeof (EffectVisual_tE885D61366B46DC715156887B773663B79D6777A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8436[4] = 
{
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_Prefab_0(),
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_Bundle_1(),
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_EffectVisualType_2(),
	EffectVisual_tE885D61366B46DC715156887B773663B79D6777A::get_offset_of_EffectVisualData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8437 = { sizeof (EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8437[4] = 
{
	EffectVisualType_t026EC7D3087FCA80C407ED9F3797CE097903D15A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8438 = { sizeof (FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8438[1] = 
{
	FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3::get_offset_of_TimePressureSystem_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8439 = { sizeof (GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8439[10] = 
{
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_LeaderboardUpdateTime_4(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_IconCount_5(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_InitCoins_6(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_InitHearts_7(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_MaxHearts_8(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_LiveRecoveryTimeInMinutes_9(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_FirstTimeJoinTeamReward_10(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_MinLevelToJoinTeam_11(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_SurveyUrl_12(),
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C::get_offset_of_GlobalTweenDuration_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8440 = { sizeof (GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8440[2] = 
{
	GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273::get_offset_of_ServerUrl_0(),
	GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273::get_offset_of_Version_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8441 = { sizeof (ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8441[1] = 
{
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A::get_offset_of_Items_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8442 = { sizeof (U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8442[1] = 
{
	U3CU3Ec__DisplayClass1_0_t1B0AB715C679927C205A4F2E17FCB64373E4C730::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8443 = { sizeof (ItemModel_t620A13B4D13CD275487D308843103864063DBE1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8443[6] = 
{
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Id_0(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Bundle_1(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Prefab_2(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Icon_3(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_DeathExplosion_4(),
	ItemModel_t620A13B4D13CD275487D308843103864063DBE1F::get_offset_of_Type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8444 = { sizeof (ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8444[4] = 
{
	ItemType_t41090A4C4D2BCA3765F224EA9264EC817755A5A3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8445 = { sizeof (LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8445[1] = 
{
	LevelsSO_t51232A976915E19D66106CE9DD068463ADBAD0EF::get_offset_of_Levels_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8446 = { sizeof (LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8446[11] = 
{
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_LevelNumber_0(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Level_1(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_LevelBundle_2(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Environment_3(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_EnvironmentBundle_4(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_ItemsCount_5(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_ItemsToGenerate_6(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_InventoryItems_7(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Objectives_8(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_ItemsGenerationDelay_9(),
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C::get_offset_of_Duration_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8447 = { sizeof (LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8447[3] = 
{
	LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC::get_offset_of_Type_0(),
	LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC::get_offset_of_UnitId_1(),
	LevelObjective_t694A34150000D3DE3C9341AAC6D6B37667B796EC::get_offset_of_Count_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8448 = { sizeof (ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8448[5] = 
{
	ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8449 = { sizeof (PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8449[1] = 
{
	PerLevelTutorialSO_tB55F6372D80FCA75DD834538D5137169A7A7E66B::get_offset_of_Tutorials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8450 = { sizeof (U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8450[1] = 
{
	U3CU3Ec__DisplayClass1_0_t9E186A929A5B89C2CF8480323CF4CCE355ED23F8::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8451 = { sizeof (PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8451[4] = 
{
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of_Id_0(),
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of__startTigger_1(),
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of_StartTriggerStr_2(),
	PerLevelTutorialModel_t617000CC336C47983E77A6E6171F0DE08379A61C::get_offset_of_Data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8452 = { sizeof (TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8452[4] = 
{
	TutorialType_t95A6CD09B8522505F42A6F3FFC2E01CCD692A302::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8453 = { sizeof (SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8453[8] = 
{
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_BackgroundMain_4(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_BackgroundBattle_5(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_Button_6(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_SummaryWin_7(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_SummaryLose_8(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_Sword_9(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_Damage_10(),
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73::get_offset_of_DamageDelay_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8454 = { sizeof (SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8454[1] = 
{
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439::get_offset_of_Spells_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8455 = { sizeof (U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8455[1] = 
{
	U3CU3Ec__DisplayClass1_0_tC85EADDF907753C4C6CAF2A466928E0776A9754D::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8456 = { sizeof (SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8456[7] = 
{
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_Id_0(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_SpellInput_1(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_Assets_2(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_SpellController_3(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_SpellControllerData_4(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_Effects_5(),
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235::get_offset_of_poop_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8457 = { sizeof (DataDictionary_t9D4A0BE16CC0324BF2D47F35B50C65CC9B44E606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8458 = { sizeof (AssetDictionary_tC29AFDE2D533D0F9B91BA34778BBF8605137D687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8459 = { sizeof (EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8459[5] = 
{
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectBrain_0(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectBrainData_1(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectType_2(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectData_3(),
	EffectModel_t1BF2DAB91B75E4435889D8BBCCA583888883223E::get_offset_of_EffectVisualId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8460 = { sizeof (AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8460[3] = 
{
	AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A::get_offset_of_Bundle_0(),
	AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A::get_offset_of_Prefab_1(),
	AssetData_t155B7C20CE31B42FB840C26A65CBBE11D4D16D7A::get_offset_of_WarmUp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8461 = { sizeof (SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8461[5] = 
{
	SpellInput_t7E941C18CACBD4A789B292019E2341A93AE8739F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8462 = { sizeof (SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8462[3] = 
{
	SpellRunner_t504261A93EFC5915A80B9E13A0D004262B424C11::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8463 = { sizeof (SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8463[14] = 
{
	SpellController_tF10DAA53E6D7502133083BF8AD162F9CBEE65643::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8464 = { sizeof (EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8464[9] = 
{
	EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8465 = { sizeof (TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8465[1] = 
{
	TournamentRewardsSO_t9121EDF677AEDF239EF74868C27C0879178523E1::get_offset_of_Rewards_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8466 = { sizeof (TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8466[1] = 
{
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0::get_offset_of_Towers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8467 = { sizeof (U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8467[1] = 
{
	U3CU3Ec__DisplayClass1_0_tAE58CA8E50F9CCB21ED71CCCCA1E1EDF56A9F5CE::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8468 = { sizeof (TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8468[11] = 
{
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Id_0(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Bundle_1(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Prefab_2(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Icon_3(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_SpellAI_4(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_AIData_5(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Range_6(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_Spell_7(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_HP_8(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_DeathExplosion_9(),
	TowerModel_t9DD2032CC7655923CBFCAC0EF0DF7B2D11683D31::get_offset_of_HealDuration_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8469 = { sizeof (SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8469[3] = 
{
	SpellAI_t31BA0E89C8EE4E13EC803218DD905CB4693C8366::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8470 = { sizeof (TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8470[1] = 
{
	TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2::get_offset_of_Traps_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8471 = { sizeof (U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8471[1] = 
{
	U3CU3Ec__DisplayClass1_0_t04C535B8B5D127AD9A39CBB909D40690C6014560::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8472 = { sizeof (TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8472[9] = 
{
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Id_0(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Bundle_1(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Prefab_2(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Icon_3(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_SpellAI_4(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_AIData_5(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Range_6(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_IsScalable_7(),
	TrapModel_tC99E2852889B4321B6754DFDDD005862CDC965EC::get_offset_of_Spell_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8473 = { sizeof (UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8473[1] = 
{
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423::get_offset_of_Units_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8474 = { sizeof (U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8474[1] = 
{
	U3CU3Ec__DisplayClass1_0_tF59194C24E02C70B72472D026137782318AB9B1B::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8475 = { sizeof (UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8475[11] = 
{
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_Id_0(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_Bundle_1(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_Prefab_2(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_Icon_3(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_AIController_4(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_Spells_5(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_HP_6(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_AttackDamage_7(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_AttackRange_8(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_AttackFrequency_9(),
	UnitsModel_t01B1C2DD2F89A48A7AEE78DF7CF546031FB6C531::get_offset_of_Speed_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8476 = { sizeof (VisualEffectModel_t0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8476[2] = 
{
	VisualEffectModel_t0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3::get_offset_of_Type_0(),
	VisualEffectModel_t0F39DCBDF286D7E55D950E8DFA2BA50E96B2A6C3::get_offset_of_Data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8477 = { sizeof (VisualEffectType_t980FA2BBE9E5BA944689AF6AF342230957117921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8477[3] = 
{
	VisualEffectType_t980FA2BBE9E5BA944689AF6AF342230957117921::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8478 = { sizeof (MapItemType_t577937D98B8B61ADC7EB7F0E092F58437801D7BF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8478[9] = 
{
	MapItemType_t577937D98B8B61ADC7EB7F0E092F58437801D7BF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8479 = { sizeof (CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8479[1] = 
{
	CounterVO_tBA84D56B4BED1924EF9F4DC96BC4A6D7A60BD069::get_offset_of_Counters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8480 = { sizeof (GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8480[3] = 
{
	GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385::get_offset_of_ServerVersion_0(),
	GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385::get_offset_of_ClientDataVersion_1(),
	GlobalVO_t3F00C793D791DA4C9FDF395F616EA34E82E53385::get_offset_of__globalSO_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8481 = { sizeof (InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8481[5] = 
{
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9::get_offset_of__gameSettingsSO_0(),
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9::get_offset_of_Coins_1(),
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9::get_offset_of_Hearts_2(),
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9::get_offset_of_HeartRecoveryStartTime_3(),
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9::get_offset_of_Items_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8482 = { sizeof (PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8482[3] = 
{
	PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4::get_offset_of__globalSO_0(),
	PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4::get_offset_of_HashVersions_1(),
	PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4::get_offset_of_HashMissingAssets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8483 = { sizeof (QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8483[1] = 
{
	QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429::get_offset_of_CurrentQuests_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8484 = { sizeof (QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8484[3] = 
{
	QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6::get_offset_of_Id_0(),
	QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6::get_offset_of_CurrentValue_1(),
	QuestVO_t80853EC5FEE81D46A730A981B0822AA277450EE6::get_offset_of_TargetValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8485 = { sizeof (TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8485[3] = 
{
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016::get_offset_of_Tournaments_0(),
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016::get_offset_of_StarChests_1(),
	TournamentsVO_tCBA9A235AE4069263CE332B1E9150CDABD0E8016::get_offset_of_LevelChests_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8486 = { sizeof (UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8486[16] = 
{
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Hero_0(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_UserCountry_1(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_TeamId_2(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_UserId_3(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Fx_4(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Music_5(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_UserLevel_6(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_UserTrophy_7(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Stars_8(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_TeamsPending_9(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_ClientServerOffset_10(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_TutorialsCompleted_11(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Spell1Unlocked_12(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Spell2Unlocked_13(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Spell3Unlocked_14(),
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94::get_offset_of_Spell4Unlocked_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8487 = { sizeof (CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8487[2] = 
{
	CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5::get_offset_of__counterVO_3(),
	CounterSystem_tEA27491DC859C46A9409C948B5C4E1AC82BEB9D5::get_offset_of__eventMap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8488 = { sizeof (CounterType_tA3DAF22E472F1B8F4870B6EEC10B3CA142DDD962)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8488[5] = 
{
	CounterType_tA3DAF22E472F1B8F4870B6EEC10B3CA142DDD962::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8489 = { sizeof (CounterEvent_t3CEC03B571EB8AC1C5AF0AF416DF2EBEC079DABF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8490 = { sizeof (CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8490[3] = 
{
	CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468::get_offset_of__type_4(),
	CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468::get_offset_of__parametre_5(),
	CounterVariable_t6E3A5044D7B6B8BCA9F818D51DE3D432EED30468::get_offset_of__counterSystem_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8491 = { sizeof (UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8491[14] = 
{
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnInternetConnection_0(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnLeaderboardRefreshed_1(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnServerLogin_2(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnTeamJoined_3(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnHelpSupplied_4(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnTeamLeft_5(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnMemberKicked_6(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnBeingKicked_7(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnTeamCreated_8(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnIconSelected_9(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnTeamEdited_10(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnTeamPending_11(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnBattleWon_12(),
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7::get_offset_of_OnLanguageChoosed_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8492 = { sizeof (InternetConnection_tD7C85354B3E4EDF7985D998BA1FB6C9DA115B830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8493 = { sizeof (ServerLogin_t779BC560CBB2746BEE648D1AAA502B53558CFD72), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8494 = { sizeof (TeamJoined_tCB8F4B32A19C4DCD6C7B8F895FA4D747EF44B5F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8495 = { sizeof (HelpSupplied_t0F977C7E29DA7D04D93F72052DD477EC1C65EE36), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8496 = { sizeof (IconSelected_tF1620B66301F74CB9CC1684778C4E3FF0CA0C069), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8497 = { sizeof (StarTournamentEvent_tAC8E51584ABAF5B990762D1E976638088E4B92F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8498 = { sizeof (LanguageChoosedEvent_t497E228421617873B2E867E6DEE93F9AD8AD4C34), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8499 = { sizeof (GameServerFactory_t5B4121AA6089C9A89938B7DC0CA23675EC4EA487), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
