﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// StarFxController
struct StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// vfxController
struct vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547;

extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral8F25A859269CA51039632DD6BBA28FB061CFB8D4;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var;
extern const uint32_t StarFxController_Awake_m5668AD662E18E91C889C1B59FB2BFFF7803B8A02_MetadataUsageId;
extern const uint32_t vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA_MetadataUsageId;

struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;


#ifndef U3CMODULEU3E_TD1EFFA6201164FCC6E8DD2EDD5EE15545901839B_H
#define U3CMODULEU3E_TD1EFFA6201164FCC6E8DD2EDD5EE15545901839B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD1EFFA6201164FCC6E8DD2EDD5EE15545901839B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TD1EFFA6201164FCC6E8DD2EDD5EE15545901839B_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef STARFXCONTROLLER_T43DD9475A7C574AD503BCEB33A4C3CBD985AD075_H
#define STARFXCONTROLLER_T43DD9475A7C574AD503BCEB33A4C3CBD985AD075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StarFxController
struct  StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] StarFxController::starFX
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFX_4;
	// System.Int32 StarFxController::ea
	int32_t ___ea_5;
	// System.Int32 StarFxController::currentEa
	int32_t ___currentEa_6;
	// System.Single StarFxController::delay
	float ___delay_7;
	// System.Single StarFxController::currentDelay
	float ___currentDelay_8;
	// System.Boolean StarFxController::isEnd
	bool ___isEnd_9;
	// System.Int32 StarFxController::idStar
	int32_t ___idStar_10;

public:
	inline static int32_t get_offset_of_starFX_4() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___starFX_4)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFX_4() const { return ___starFX_4; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFX_4() { return &___starFX_4; }
	inline void set_starFX_4(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___starFX_4), value);
	}

	inline static int32_t get_offset_of_ea_5() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___ea_5)); }
	inline int32_t get_ea_5() const { return ___ea_5; }
	inline int32_t* get_address_of_ea_5() { return &___ea_5; }
	inline void set_ea_5(int32_t value)
	{
		___ea_5 = value;
	}

	inline static int32_t get_offset_of_currentEa_6() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___currentEa_6)); }
	inline int32_t get_currentEa_6() const { return ___currentEa_6; }
	inline int32_t* get_address_of_currentEa_6() { return &___currentEa_6; }
	inline void set_currentEa_6(int32_t value)
	{
		___currentEa_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_currentDelay_8() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___currentDelay_8)); }
	inline float get_currentDelay_8() const { return ___currentDelay_8; }
	inline float* get_address_of_currentDelay_8() { return &___currentDelay_8; }
	inline void set_currentDelay_8(float value)
	{
		___currentDelay_8 = value;
	}

	inline static int32_t get_offset_of_isEnd_9() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___isEnd_9)); }
	inline bool get_isEnd_9() const { return ___isEnd_9; }
	inline bool* get_address_of_isEnd_9() { return &___isEnd_9; }
	inline void set_isEnd_9(bool value)
	{
		___isEnd_9 = value;
	}

	inline static int32_t get_offset_of_idStar_10() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075, ___idStar_10)); }
	inline int32_t get_idStar_10() const { return ___idStar_10; }
	inline int32_t* get_address_of_idStar_10() { return &___idStar_10; }
	inline void set_idStar_10(int32_t value)
	{
		___idStar_10 = value;
	}
};

struct StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields
{
public:
	// StarFxController StarFxController::myStarFxController
	StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * ___myStarFxController_11;

public:
	inline static int32_t get_offset_of_myStarFxController_11() { return static_cast<int32_t>(offsetof(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields, ___myStarFxController_11)); }
	inline StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * get_myStarFxController_11() const { return ___myStarFxController_11; }
	inline StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 ** get_address_of_myStarFxController_11() { return &___myStarFxController_11; }
	inline void set_myStarFxController_11(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * value)
	{
		___myStarFxController_11 = value;
		Il2CppCodeGenWriteBarrier((&___myStarFxController_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARFXCONTROLLER_T43DD9475A7C574AD503BCEB33A4C3CBD985AD075_H
#ifndef VFXCONTROLLER_TBDD97B953055545A8817FEE3FF2C394707EE7547_H
#define VFXCONTROLLER_TBDD97B953055545A8817FEE3FF2C394707EE7547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// vfxController
struct  vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] vfxController::starFx01Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx01Prefabs_4;
	// UnityEngine.GameObject[] vfxController::starFx02Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx02Prefabs_5;
	// UnityEngine.GameObject[] vfxController::starFx03Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx03Prefabs_6;
	// UnityEngine.GameObject[] vfxController::starFx04Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx04Prefabs_7;
	// UnityEngine.GameObject[] vfxController::starFx05Prefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___starFx05Prefabs_8;
	// UnityEngine.GameObject[] vfxController::DesStarFxObjs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___DesStarFxObjs_9;
	// UnityEngine.GameObject[] vfxController::bgFxPrefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___bgFxPrefabs_10;
	// System.Int32 vfxController::currentStarImage
	int32_t ___currentStarImage_11;
	// System.Int32 vfxController::currentStarFx
	int32_t ___currentStarFx_12;
	// System.Int32 vfxController::currentLevel
	int32_t ___currentLevel_13;
	// System.Int32 vfxController::currentBgFx
	int32_t ___currentBgFx_14;

public:
	inline static int32_t get_offset_of_starFx01Prefabs_4() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx01Prefabs_4)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx01Prefabs_4() const { return ___starFx01Prefabs_4; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx01Prefabs_4() { return &___starFx01Prefabs_4; }
	inline void set_starFx01Prefabs_4(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx01Prefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___starFx01Prefabs_4), value);
	}

	inline static int32_t get_offset_of_starFx02Prefabs_5() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx02Prefabs_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx02Prefabs_5() const { return ___starFx02Prefabs_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx02Prefabs_5() { return &___starFx02Prefabs_5; }
	inline void set_starFx02Prefabs_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx02Prefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&___starFx02Prefabs_5), value);
	}

	inline static int32_t get_offset_of_starFx03Prefabs_6() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx03Prefabs_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx03Prefabs_6() const { return ___starFx03Prefabs_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx03Prefabs_6() { return &___starFx03Prefabs_6; }
	inline void set_starFx03Prefabs_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx03Prefabs_6 = value;
		Il2CppCodeGenWriteBarrier((&___starFx03Prefabs_6), value);
	}

	inline static int32_t get_offset_of_starFx04Prefabs_7() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx04Prefabs_7)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx04Prefabs_7() const { return ___starFx04Prefabs_7; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx04Prefabs_7() { return &___starFx04Prefabs_7; }
	inline void set_starFx04Prefabs_7(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx04Prefabs_7 = value;
		Il2CppCodeGenWriteBarrier((&___starFx04Prefabs_7), value);
	}

	inline static int32_t get_offset_of_starFx05Prefabs_8() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___starFx05Prefabs_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_starFx05Prefabs_8() const { return ___starFx05Prefabs_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_starFx05Prefabs_8() { return &___starFx05Prefabs_8; }
	inline void set_starFx05Prefabs_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___starFx05Prefabs_8 = value;
		Il2CppCodeGenWriteBarrier((&___starFx05Prefabs_8), value);
	}

	inline static int32_t get_offset_of_DesStarFxObjs_9() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___DesStarFxObjs_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_DesStarFxObjs_9() const { return ___DesStarFxObjs_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_DesStarFxObjs_9() { return &___DesStarFxObjs_9; }
	inline void set_DesStarFxObjs_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___DesStarFxObjs_9 = value;
		Il2CppCodeGenWriteBarrier((&___DesStarFxObjs_9), value);
	}

	inline static int32_t get_offset_of_bgFxPrefabs_10() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___bgFxPrefabs_10)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_bgFxPrefabs_10() const { return ___bgFxPrefabs_10; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_bgFxPrefabs_10() { return &___bgFxPrefabs_10; }
	inline void set_bgFxPrefabs_10(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___bgFxPrefabs_10 = value;
		Il2CppCodeGenWriteBarrier((&___bgFxPrefabs_10), value);
	}

	inline static int32_t get_offset_of_currentStarImage_11() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentStarImage_11)); }
	inline int32_t get_currentStarImage_11() const { return ___currentStarImage_11; }
	inline int32_t* get_address_of_currentStarImage_11() { return &___currentStarImage_11; }
	inline void set_currentStarImage_11(int32_t value)
	{
		___currentStarImage_11 = value;
	}

	inline static int32_t get_offset_of_currentStarFx_12() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentStarFx_12)); }
	inline int32_t get_currentStarFx_12() const { return ___currentStarFx_12; }
	inline int32_t* get_address_of_currentStarFx_12() { return &___currentStarFx_12; }
	inline void set_currentStarFx_12(int32_t value)
	{
		___currentStarFx_12 = value;
	}

	inline static int32_t get_offset_of_currentLevel_13() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentLevel_13)); }
	inline int32_t get_currentLevel_13() const { return ___currentLevel_13; }
	inline int32_t* get_address_of_currentLevel_13() { return &___currentLevel_13; }
	inline void set_currentLevel_13(int32_t value)
	{
		___currentLevel_13 = value;
	}

	inline static int32_t get_offset_of_currentBgFx_14() { return static_cast<int32_t>(offsetof(vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547, ___currentBgFx_14)); }
	inline int32_t get_currentBgFx_14() const { return ___currentBgFx_14; }
	inline int32_t* get_address_of_currentBgFx_14() { return &___currentBgFx_14; }
	inline void set_currentBgFx_14(int32_t value)
	{
		___currentBgFx_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VFXCONTROLLER_TBDD97B953055545A8817FEE3FF2C394707EE7547_H
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * m_Items[1];

public:
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mEF511C369E0CA9462FD3427DFC2375E81469570F_gshared (RuntimeObject * p0, const RuntimeMethod* method);

// System.Void StarFxController::Reset()
extern "C" IL2CPP_METHOD_ATTR void StarFxController_Reset_m79F20EB48E5A031FF53080B1698EC3F95FE128C0 (StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKeyDown_mD82B14BB87E1C811668BD1A2CFBC0CF3D4983FEA (int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void vfxController::PlayStarFX()
extern "C" IL2CPP_METHOD_ATTR void vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9 (String_t* p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_get_gameObject_mB8D6D847ABF95430B098554F3F2D63EC1D30C815 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * p0, const RuntimeMethod* method)
{
	return ((  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mEF511C369E0CA9462FD3427DFC2375E81469570F_gshared)(p0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void StarFxController::Awake()
extern "C" IL2CPP_METHOD_ATTR void StarFxController_Awake_m5668AD662E18E91C889C1B59FB2BFFF7803B8A02 (StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StarFxController_Awake_m5668AD662E18E91C889C1B59FB2BFFF7803B8A02_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields*)il2cpp_codegen_static_fields_for(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_il2cpp_TypeInfo_var))->set_myStarFxController_11(__this);
		return;
	}
}
// System.Void StarFxController::Start()
extern "C" IL2CPP_METHOD_ATTR void StarFxController_Start_m71E8F1337455D8660C3C18350D773467B4082750 (StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * __this, const RuntimeMethod* method)
{
	{
		StarFxController_Reset_m79F20EB48E5A031FF53080B1698EC3F95FE128C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StarFxController::Update()
extern "C" IL2CPP_METHOD_ATTR void StarFxController_Update_m813580226C1268D4147196EFB372BB5887D500E0 (StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isEnd_9();
		if (L_0)
		{
			goto IL_007e;
		}
	}
	{
		float L_1 = __this->get_currentDelay_8();
		float L_2 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_currentDelay_8(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
		float L_3 = __this->get_currentDelay_8();
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_4 = __this->get_currentEa_6();
		int32_t L_5 = __this->get_ea_5();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0064;
		}
	}
	{
		float L_6 = __this->get_delay_7();
		__this->set_currentDelay_8(L_6);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_7 = __this->get_starFX_4();
		int32_t L_8 = __this->get_currentEa_6();
		NullCheck(L_7);
		int32_t L_9 = L_8;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)1, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_currentEa_6();
		__this->set_currentEa_6(((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1)));
		goto IL_007e;
	}

IL_0064:
	{
		__this->set_isEnd_9((bool)1);
		float L_12 = __this->get_delay_7();
		__this->set_currentDelay_8(L_12);
		__this->set_currentEa_6(0);
	}

IL_007e:
	{
		bool L_13 = Input_GetKeyDown_mD82B14BB87E1C811668BD1A2CFBC0CF3D4983FEA(((int32_t)274), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0090;
		}
	}
	{
		StarFxController_Reset_m79F20EB48E5A031FF53080B1698EC3F95FE128C0(__this, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// System.Void StarFxController::Reset()
extern "C" IL2CPP_METHOD_ATTR void StarFxController_Reset_m79F20EB48E5A031FF53080B1698EC3F95FE128C0 (StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0016;
	}

IL_0004:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = __this->get_starFX_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)0, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0016:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)3)))
		{
			goto IL_0004;
		}
	}
	{
		float L_6 = __this->get_delay_7();
		__this->set_currentDelay_8(L_6);
		__this->set_currentEa_6(0);
		__this->set_isEnd_9((bool)0);
		V_1 = 0;
		goto IL_004a;
	}

IL_0038:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_7 = __this->get_starFX_4();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_10, (bool)0, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_004a:
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) < ((int32_t)3)))
		{
			goto IL_0038;
		}
	}
	{
		return;
	}
}
// System.Void StarFxController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void StarFxController__ctor_mD81480C32DAA5274C123588F7F1D67E2583998D5 (StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vfxController::Start()
extern "C" IL2CPP_METHOD_ATTR void vfxController_Start_mF5769EE7F048603A4B451A897AA196C91A3BFC26 (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentStarImage_11(0);
		__this->set_currentStarFx_12(0);
		__this->set_currentLevel_13(3);
		__this->set_currentBgFx_14(1);
		return;
	}
}
// System.Void vfxController::ChangedStarImage(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void vfxController_ChangedStarImage_m910C52F96961E0B20F02C501D102AF2FCF00D6B1 (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, int32_t ___i0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		__this->set_currentStarImage_11(L_0);
		vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vfxController::ChangedStarFX(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void vfxController_ChangedStarFX_m7B6D114AA715BC0F17A57EF04C9F78733BC30483 (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, int32_t ___i0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		__this->set_currentStarFx_12(L_0);
		vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vfxController::ChangedLevel(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void vfxController_ChangedLevel_mBA88CEC3FCC31A33AA483E86BDB3D6ED66FE808B (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, int32_t ___i0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		__this->set_currentLevel_13(L_0);
		vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vfxController::ChangedBgFx(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void vfxController_ChangedBgFx_mC9DE939D1DB4A2C5F19203DC2EEFF7A894E9D6DA (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, int32_t ___i0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___i0;
		__this->set_currentBgFx_14(L_0);
		vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vfxController::PlayStarFX()
extern "C" IL2CPP_METHOD_ATTR void vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vfxController_PlayStarFX_m2A1A612AFF0E72A841F452FB3D884F27DFF4A4AA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* V_0 = NULL;
	int32_t V_1 = 0;
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_0 = GameObject_FindGameObjectsWithTag_mF49A195F19A598C5FD145FFE175ABE5B4885FAD9(_stringLiteral8F25A859269CA51039632DD6BBA28FB061CFB8D4, /*hidden argument*/NULL);
		__this->set_DesStarFxObjs_9(L_0);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_1 = __this->get_DesStarFxObjs_9();
		V_0 = L_1;
		V_1 = 0;
		goto IL_002c;
	}

IL_001b:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = GameObject_get_gameObject_mB8D6D847ABF95430B098554F3F2D63EC1D30C815(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_002c:
	{
		int32_t L_8 = V_1;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_10 = __this->get_currentBgFx_14();
		if (!L_10)
		{
			goto IL_004d;
		}
	}
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_11 = __this->get_bgFxPrefabs_10();
		int32_t L_12 = __this->get_currentBgFx_14();
		NullCheck(L_11);
		int32_t L_13 = L_12;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E(L_14, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var);
	}

IL_004d:
	{
		int32_t L_15 = __this->get_currentStarImage_11();
		V_1 = L_15;
		int32_t L_16 = V_1;
		switch (L_16)
		{
			case 0:
			{
				goto IL_006f;
			}
			case 1:
			{
				goto IL_0093;
			}
			case 2:
			{
				goto IL_00b7;
			}
			case 3:
			{
				goto IL_00db;
			}
			case 4:
			{
				goto IL_00ff;
			}
		}
	}
	{
		return;
	}

IL_006f:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_17 = __this->get_starFx01Prefabs_4();
		int32_t L_18 = __this->get_currentStarFx_12();
		NullCheck(L_17);
		int32_t L_19 = L_18;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E(L_20, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var);
		StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * L_21 = ((StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields*)il2cpp_codegen_static_fields_for(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_il2cpp_TypeInfo_var))->get_myStarFxController_11();
		int32_t L_22 = __this->get_currentLevel_13();
		NullCheck(L_21);
		L_21->set_ea_5(L_22);
		return;
	}

IL_0093:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_23 = __this->get_starFx02Prefabs_5();
		int32_t L_24 = __this->get_currentStarFx_12();
		NullCheck(L_23);
		int32_t L_25 = L_24;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E(L_26, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var);
		StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * L_27 = ((StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields*)il2cpp_codegen_static_fields_for(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_il2cpp_TypeInfo_var))->get_myStarFxController_11();
		int32_t L_28 = __this->get_currentLevel_13();
		NullCheck(L_27);
		L_27->set_ea_5(L_28);
		return;
	}

IL_00b7:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_29 = __this->get_starFx03Prefabs_6();
		int32_t L_30 = __this->get_currentStarFx_12();
		NullCheck(L_29);
		int32_t L_31 = L_30;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E(L_32, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var);
		StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * L_33 = ((StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields*)il2cpp_codegen_static_fields_for(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_il2cpp_TypeInfo_var))->get_myStarFxController_11();
		int32_t L_34 = __this->get_currentLevel_13();
		NullCheck(L_33);
		L_33->set_ea_5(L_34);
		return;
	}

IL_00db:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_35 = __this->get_starFx04Prefabs_7();
		int32_t L_36 = __this->get_currentStarFx_12();
		NullCheck(L_35);
		int32_t L_37 = L_36;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E(L_38, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var);
		StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * L_39 = ((StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields*)il2cpp_codegen_static_fields_for(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_il2cpp_TypeInfo_var))->get_myStarFxController_11();
		int32_t L_40 = __this->get_currentLevel_13();
		NullCheck(L_39);
		L_39->set_ea_5(L_40);
		return;
	}

IL_00ff:
	{
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_41 = __this->get_starFx05Prefabs_8();
		int32_t L_42 = __this->get_currentStarFx_12();
		NullCheck(L_41);
		int32_t L_43 = L_42;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E(L_44, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var);
		StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075 * L_45 = ((StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_StaticFields*)il2cpp_codegen_static_fields_for(StarFxController_t43DD9475A7C574AD503BCEB33A4C3CBD985AD075_il2cpp_TypeInfo_var))->get_myStarFxController_11();
		int32_t L_46 = __this->get_currentLevel_13();
		NullCheck(L_45);
		L_45->set_ea_5(L_46);
		return;
	}
}
// System.Void vfxController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void vfxController__ctor_mA694D51E916719ECB86C76E40746075E2B541695 (vfxController_tBDD97B953055545A8817FEE3FF2C394707EE7547 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
