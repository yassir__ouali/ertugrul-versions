﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB;
// System.Action`2<System.Net.WebClient,System.ComponentModel.AsyncCompletedEventHandler>
struct Action_2_t69AEB644838FDEB34941A1AF142FD305C93A49A7;
// System.Action`2<System.Net.WebClient,System.Net.DownloadDataCompletedEventHandler>
struct Action_2_t083259C45D6A8E2CCD331C122A023C5C4B4D0FE9;
// System.Action`2<System.Net.WebClient,System.Net.DownloadStringCompletedEventHandler>
struct Action_2_t9061DC4CD4B5675DC998F0790CA5D0D15DCC66CE;
// System.Action`2<System.Net.WebClient,System.Net.OpenReadCompletedEventHandler>
struct Action_2_t24C9A91F4BC1ED6D40AE9E798F19DB0CE4CB56C8;
// System.Action`2<System.Net.WebClient,System.Net.OpenWriteCompletedEventHandler>
struct Action_2_t20FA8610429534671D804864BC590CF75935F40D;
// System.Action`2<System.Net.WebClient,System.Net.UploadDataCompletedEventHandler>
struct Action_2_t2C49A0F9A7170A372BEA7535DD431F01A57B0589;
// System.Action`2<System.Net.WebClient,System.Net.UploadFileCompletedEventHandler>
struct Action_2_tE38D6782DC1A490EDE9D47D9C1F03A86B5171036;
// System.Action`2<System.Net.WebClient,System.Net.UploadStringCompletedEventHandler>
struct Action_2_t1168A6A3333D3CF7FDDF720A4DDDDCEA7FB7AFDE;
// System.Action`2<System.Net.WebClient,System.Net.UploadValuesCompletedEventHandler>
struct Action_2_t3EE0A1BD7BD16FCD2E00EADD73080664BAC8A035;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.IDictionary`2<System.String,System.Net.TrackingValidationObjectDictionary/ValidateAndParseValue>
struct IDictionary_2_tE75A8671689537F3F2F2A3F11C75AD31C7CF2301;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1E5681AFFD86E8189077B1EE929272E2AF245A91;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.Queue
struct Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3;
// System.Collections.SortedList
struct SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1;
// System.Collections.Stack
struct Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643;
// System.ComponentModel.AsyncCompletedEventHandler
struct AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6;
// System.ComponentModel.AsyncOperation
struct AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4;
// System.ComponentModel.ISite
struct ISite_t6804B48BC23ABB5F4141903F878589BCEF6097A2;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Exception
struct Exception_t;
// System.Func`2<System.ComponentModel.AsyncCompletedEventArgs,System.Object>
struct Func_2_tDE667922AE5D8E516168594E04038A79091979A9;
// System.Func`2<System.Net.DownloadDataCompletedEventArgs,System.Byte[]>
struct Func_2_tD17A333C073B6B19056A11BF6B7998AE30809197;
// System.Func`2<System.Net.DownloadStringCompletedEventArgs,System.String>
struct Func_2_tBE7D342FE0F39A2241399FBE8864BC632864C619;
// System.Func`2<System.Net.OpenReadCompletedEventArgs,System.IO.Stream>
struct Func_2_t57744E90FE5027161AF05CCEDD66C4AB64724D53;
// System.Func`2<System.Net.OpenWriteCompletedEventArgs,System.IO.Stream>
struct Func_2_t3157BDC0194B05B94A754E301CA0A760CC7292F5;
// System.Func`2<System.Net.UploadDataCompletedEventArgs,System.Byte[]>
struct Func_2_t2D1860C041767F9F7CD15D3B75EC44D9C9188120;
// System.Func`2<System.Net.UploadFileCompletedEventArgs,System.Byte[]>
struct Func_2_t7AAA217876F68374CC848B134DCF1032AB2E47A0;
// System.Func`2<System.Net.UploadStringCompletedEventArgs,System.String>
struct Func_2_t115B8EA7D5806FC5ACB8DA63EBD4B299813BE817;
// System.Func`2<System.Net.UploadValuesCompletedEventArgs,System.Byte[]>
struct Func_2_t3D57FDDC2419906CF7CE57832CA05A230477F790;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.AutoWebProxyScriptEngine
struct AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455;
// System.Net.Base64Stream
struct Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0;
// System.Net.Base64Stream/ReadStateInfo
struct ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9;
// System.Net.Cache.RequestCacheBinding
struct RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61;
// System.Net.Cache.RequestCacheProtocol
struct RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D;
// System.Net.Comparer
struct Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB;
// System.Net.CompletionDelegate
struct CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674;
// System.Net.ConnectionPool
struct ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C;
// System.Net.Cookie
struct Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90;
// System.Net.CookieCollection
struct CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3;
// System.Net.CookieTokenizer
struct CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD;
// System.Net.CookieTokenizer/RecognizedAttribute[]
struct RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55;
// System.Net.CreateConnectionDelegate
struct CreateConnectionDelegate_tAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B;
// System.Net.DigestHeaderParser
struct DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9;
// System.Net.DownloadDataCompletedEventArgs
struct DownloadDataCompletedEventArgs_t5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C;
// System.Net.DownloadDataCompletedEventHandler
struct DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61;
// System.Net.DownloadProgressChangedEventArgs
struct DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522;
// System.Net.DownloadProgressChangedEventHandler
struct DownloadProgressChangedEventHandler_t7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F;
// System.Net.DownloadStringCompletedEventArgs
struct DownloadStringCompletedEventArgs_t9517D2DD80B7233622CBD41E60D06F02E5F327AE;
// System.Net.DownloadStringCompletedEventHandler
struct DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C;
// System.Net.FileWebRequest
struct FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F;
// System.Net.GeneralAsyncDelegate
struct GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92;
// System.Net.HeaderVariantInfo[]
struct HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012;
// System.Net.HttpListenerContext
struct HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA;
// System.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271;
// System.Net.ICredentialPolicy
struct ICredentialPolicy_t09FC19BE60498729D1F75F5EC8B35166F1FDD3C7;
// System.Net.ICredentials
struct ICredentials_t1A41F1096B037CAB53AE01434DF0747881455344;
// System.Net.IPAddress
struct IPAddress_t77F35D21A3027F0CE7B38EA9B56BFD31B28952CE;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t7F25C4C038C43BFDA8EA84969112E82BADC38BC3;
// System.Net.IPEndPoint
struct IPEndPoint_tCD29981135F7B1989C3845BF455AD44EBC13DE3F;
// System.Net.IPHostEntry
struct IPHostEntry_tB00EABDF75DB19AEAD4F3E7D93BFD7BAE558149D;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Net.InterlockedStack
struct InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C;
// System.Net.LazyAsyncResult
struct LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3;
// System.Net.LazyAsyncResult/ThreadContext
struct ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082;
// System.Net.Mime.Base64WriteStateInfo
struct Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366;
// System.Net.MonoChunkStream
struct MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5;
// System.Net.OpenReadCompletedEventArgs
struct OpenReadCompletedEventArgs_tBC33F1F08441BE06D37C9F2DBE7BDA702DB86393;
// System.Net.OpenReadCompletedEventHandler
struct OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A;
// System.Net.OpenWriteCompletedEventArgs
struct OpenWriteCompletedEventArgs_t46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3;
// System.Net.OpenWriteCompletedEventHandler
struct OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD;
// System.Net.PooledStream
struct PooledStream_tA0A798FD29060971887DF12D77E4BB444BE34D1B;
// System.Net.ScatterGatherBuffers
struct ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E;
// System.Net.ServicePoint
struct ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA;
// System.Net.TimerThread/Callback
struct Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3;
// System.Net.TimerThread/Queue
struct Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643;
// System.Net.TimerThread/Timer
struct Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F;
// System.Net.UploadDataCompletedEventArgs
struct UploadDataCompletedEventArgs_t17D573D2463C12EF95555B0CA0135B4FD71A920D;
// System.Net.UploadDataCompletedEventHandler
struct UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33;
// System.Net.UploadFileCompletedEventArgs
struct UploadFileCompletedEventArgs_t85C3133630FCC3D7DD1803046F61F5C3F21F6D0A;
// System.Net.UploadFileCompletedEventHandler
struct UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86;
// System.Net.UploadProgressChangedEventArgs
struct UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E;
// System.Net.UploadProgressChangedEventHandler
struct UploadProgressChangedEventHandler_tD3F687D5C0C93A87BA843B34B11570B2B0530B54;
// System.Net.UploadStringCompletedEventArgs
struct UploadStringCompletedEventArgs_t5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB;
// System.Net.UploadStringCompletedEventHandler
struct UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6;
// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t5BC91E51C2B464B1912C651AAAEEE890F7AD3A74;
// System.Net.UploadValuesCompletedEventHandler
struct UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6;
// System.Net.WebClient
struct WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A;
// System.Net.WebClient/ProgressData
struct ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304;
// System.Net.WebRequest
struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8;
// System.Net.WebRequest/DesignerWebRequestCreate
struct DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3;
// System.Net.WebResponse
struct WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53;
// System.Threading.ExecutionContext
struct ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01;
// System.Threading.Tasks.TaskCompletionSource`1<System.Byte[]>
struct TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72;
// System.Threading.Tasks.TaskCompletionSource`1<System.IO.Stream>
struct TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE;
// System.Threading.Tasks.TaskCompletionSource`1<System.Object>
struct TaskCompletionSource_1_t6C02642279BC7BF03091A8CB685FC6B4E68BED12;
// System.Threading.Tasks.TaskCompletionSource`1<System.String>
struct TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Threading.WaitCallback
struct WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC;
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef STRINGDICTIONARY_T9B6306775C5F70981BCB8A30603B4C93C22844FF_H
#define STRINGDICTIONARY_T9B6306775C5F70981BCB8A30603B4C93C22844FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.StringDictionary
struct  StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.StringDictionary::contents
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___contents_0;

public:
	inline static int32_t get_offset_of_contents_0() { return static_cast<int32_t>(offsetof(StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF, ___contents_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_contents_0() const { return ___contents_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_contents_0() { return &___contents_0; }
	inline void set_contents_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___contents_0 = value;
		Il2CppCodeGenWriteBarrier((&___contents_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGDICTIONARY_T9B6306775C5F70981BCB8A30603B4C93C22844FF_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef AUTHENTICATIONMANAGER_T0C973C7282FB47EAA7E2A239421304D47571B012_H
#define AUTHENTICATIONMANAGER_T0C973C7282FB47EAA7E2A239421304D47571B012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AuthenticationManager
struct  AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012  : public RuntimeObject
{
public:

public:
};

struct AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields
{
public:
	// System.Collections.ArrayList System.Net.AuthenticationManager::modules
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___modules_0;
	// System.Object System.Net.AuthenticationManager::locker
	RuntimeObject * ___locker_1;
	// System.Net.ICredentialPolicy System.Net.AuthenticationManager::credential_policy
	RuntimeObject* ___credential_policy_2;

public:
	inline static int32_t get_offset_of_modules_0() { return static_cast<int32_t>(offsetof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields, ___modules_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_modules_0() const { return ___modules_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_modules_0() { return &___modules_0; }
	inline void set_modules_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___modules_0 = value;
		Il2CppCodeGenWriteBarrier((&___modules_0), value);
	}

	inline static int32_t get_offset_of_locker_1() { return static_cast<int32_t>(offsetof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields, ___locker_1)); }
	inline RuntimeObject * get_locker_1() const { return ___locker_1; }
	inline RuntimeObject ** get_address_of_locker_1() { return &___locker_1; }
	inline void set_locker_1(RuntimeObject * value)
	{
		___locker_1 = value;
		Il2CppCodeGenWriteBarrier((&___locker_1), value);
	}

	inline static int32_t get_offset_of_credential_policy_2() { return static_cast<int32_t>(offsetof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields, ___credential_policy_2)); }
	inline RuntimeObject* get_credential_policy_2() const { return ___credential_policy_2; }
	inline RuntimeObject** get_address_of_credential_policy_2() { return &___credential_policy_2; }
	inline void set_credential_policy_2(RuntimeObject* value)
	{
		___credential_policy_2 = value;
		Il2CppCodeGenWriteBarrier((&___credential_policy_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONMANAGER_T0C973C7282FB47EAA7E2A239421304D47571B012_H
#ifndef AUTOWEBPROXYSCRIPTENGINE_TA3B7EF6B73AD21A750868072B07936408AB3B455_H
#define AUTOWEBPROXYSCRIPTENGINE_TA3B7EF6B73AD21A750868072B07936408AB3B455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AutoWebProxyScriptEngine
struct  AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455  : public RuntimeObject
{
public:
	// System.Uri System.Net.AutoWebProxyScriptEngine::<AutomaticConfigurationScript>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CAutomaticConfigurationScriptU3Ek__BackingField_0;
	// System.Boolean System.Net.AutoWebProxyScriptEngine::<AutomaticallyDetectSettings>k__BackingField
	bool ___U3CAutomaticallyDetectSettingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAutomaticConfigurationScriptU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455, ___U3CAutomaticConfigurationScriptU3Ek__BackingField_0)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CAutomaticConfigurationScriptU3Ek__BackingField_0() const { return ___U3CAutomaticConfigurationScriptU3Ek__BackingField_0; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CAutomaticConfigurationScriptU3Ek__BackingField_0() { return &___U3CAutomaticConfigurationScriptU3Ek__BackingField_0; }
	inline void set_U3CAutomaticConfigurationScriptU3Ek__BackingField_0(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CAutomaticConfigurationScriptU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAutomaticConfigurationScriptU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAutomaticallyDetectSettingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455, ___U3CAutomaticallyDetectSettingsU3Ek__BackingField_1)); }
	inline bool get_U3CAutomaticallyDetectSettingsU3Ek__BackingField_1() const { return ___U3CAutomaticallyDetectSettingsU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CAutomaticallyDetectSettingsU3Ek__BackingField_1() { return &___U3CAutomaticallyDetectSettingsU3Ek__BackingField_1; }
	inline void set_U3CAutomaticallyDetectSettingsU3Ek__BackingField_1(bool value)
	{
		___U3CAutomaticallyDetectSettingsU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOWEBPROXYSCRIPTENGINE_TA3B7EF6B73AD21A750868072B07936408AB3B455_H
#ifndef READSTATEINFO_T565A55D472ED85BBB4A76B7544030B84B8912DB9_H
#define READSTATEINFO_T565A55D472ED85BBB4A76B7544030B84B8912DB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Base64Stream_ReadStateInfo
struct  ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9  : public RuntimeObject
{
public:
	// System.Byte System.Net.Base64Stream_ReadStateInfo::val
	uint8_t ___val_0;
	// System.Byte System.Net.Base64Stream_ReadStateInfo::pos
	uint8_t ___pos_1;

public:
	inline static int32_t get_offset_of_val_0() { return static_cast<int32_t>(offsetof(ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9, ___val_0)); }
	inline uint8_t get_val_0() const { return ___val_0; }
	inline uint8_t* get_address_of_val_0() { return &___val_0; }
	inline void set_val_0(uint8_t value)
	{
		___val_0 = value;
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9, ___pos_1)); }
	inline uint8_t get_pos_1() const { return ___pos_1; }
	inline uint8_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(uint8_t value)
	{
		___pos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATEINFO_T565A55D472ED85BBB4A76B7544030B84B8912DB9_H
#ifndef BASICCLIENT_T691369603F87465F4B5A78CD356545B56ABCA18C_H
#define BASICCLIENT_T691369603F87465F4B5A78CD356545B56ABCA18C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BasicClient
struct  BasicClient_t691369603F87465F4B5A78CD356545B56ABCA18C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCLIENT_T691369603F87465F4B5A78CD356545B56ABCA18C_H
#ifndef READBUFFERSTATE_TBD41DF9062E5FA86A5BFC35E08C1B9224E00585B_H
#define READBUFFERSTATE_TBD41DF9062E5FA86A5BFC35E08C1B9224E00585B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkedInputStream_ReadBufferState
struct  ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.ChunkedInputStream_ReadBufferState::Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Buffer_0;
	// System.Int32 System.Net.ChunkedInputStream_ReadBufferState::Offset
	int32_t ___Offset_1;
	// System.Int32 System.Net.ChunkedInputStream_ReadBufferState::Count
	int32_t ___Count_2;
	// System.Int32 System.Net.ChunkedInputStream_ReadBufferState::InitialCount
	int32_t ___InitialCount_3;
	// System.Net.HttpStreamAsyncResult System.Net.ChunkedInputStream_ReadBufferState::Ares
	HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 * ___Ares_4;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Offset_1)); }
	inline int32_t get_Offset_1() const { return ___Offset_1; }
	inline int32_t* get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(int32_t value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}

	inline static int32_t get_offset_of_InitialCount_3() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___InitialCount_3)); }
	inline int32_t get_InitialCount_3() const { return ___InitialCount_3; }
	inline int32_t* get_address_of_InitialCount_3() { return &___InitialCount_3; }
	inline void set_InitialCount_3(int32_t value)
	{
		___InitialCount_3 = value;
	}

	inline static int32_t get_offset_of_Ares_4() { return static_cast<int32_t>(offsetof(ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B, ___Ares_4)); }
	inline HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 * get_Ares_4() const { return ___Ares_4; }
	inline HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 ** get_address_of_Ares_4() { return &___Ares_4; }
	inline void set_Ares_4(HttpStreamAsyncResult_t3CCE017E175E89575780F29152E61D6DE5D10271 * value)
	{
		___Ares_4 = value;
		Il2CppCodeGenWriteBarrier((&___Ares_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READBUFFERSTATE_TBD41DF9062E5FA86A5BFC35E08C1B9224E00585B_H
#ifndef COMPARER_TFC5265AD65740F9DB39C75F1E3EF8032982F45AB_H
#define COMPARER_TFC5265AD65740F9DB39C75F1E3EF8032982F45AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Comparer
struct  Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARER_TFC5265AD65740F9DB39C75F1E3EF8032982F45AB_H
#ifndef ASYNCCONNECTIONPOOLREQUEST_TC39BE0320275AE9734B38A518415794109CEF1AA_H
#define ASYNCCONNECTIONPOOLREQUEST_TC39BE0320275AE9734B38A518415794109CEF1AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ConnectionPool_AsyncConnectionPoolRequest
struct  AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA  : public RuntimeObject
{
public:
	// System.Object System.Net.ConnectionPool_AsyncConnectionPoolRequest::OwningObject
	RuntimeObject * ___OwningObject_0;
	// System.Net.GeneralAsyncDelegate System.Net.ConnectionPool_AsyncConnectionPoolRequest::AsyncCallback
	GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 * ___AsyncCallback_1;
	// System.Net.ConnectionPool System.Net.ConnectionPool_AsyncConnectionPoolRequest::Pool
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C * ___Pool_2;
	// System.Int32 System.Net.ConnectionPool_AsyncConnectionPoolRequest::CreationTimeout
	int32_t ___CreationTimeout_3;

public:
	inline static int32_t get_offset_of_OwningObject_0() { return static_cast<int32_t>(offsetof(AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA, ___OwningObject_0)); }
	inline RuntimeObject * get_OwningObject_0() const { return ___OwningObject_0; }
	inline RuntimeObject ** get_address_of_OwningObject_0() { return &___OwningObject_0; }
	inline void set_OwningObject_0(RuntimeObject * value)
	{
		___OwningObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___OwningObject_0), value);
	}

	inline static int32_t get_offset_of_AsyncCallback_1() { return static_cast<int32_t>(offsetof(AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA, ___AsyncCallback_1)); }
	inline GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 * get_AsyncCallback_1() const { return ___AsyncCallback_1; }
	inline GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 ** get_address_of_AsyncCallback_1() { return &___AsyncCallback_1; }
	inline void set_AsyncCallback_1(GeneralAsyncDelegate_tD535232B4C5C7E31A01C18A2A072F80C1C085D92 * value)
	{
		___AsyncCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___AsyncCallback_1), value);
	}

	inline static int32_t get_offset_of_Pool_2() { return static_cast<int32_t>(offsetof(AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA, ___Pool_2)); }
	inline ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C * get_Pool_2() const { return ___Pool_2; }
	inline ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C ** get_address_of_Pool_2() { return &___Pool_2; }
	inline void set_Pool_2(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C * value)
	{
		___Pool_2 = value;
		Il2CppCodeGenWriteBarrier((&___Pool_2), value);
	}

	inline static int32_t get_offset_of_CreationTimeout_3() { return static_cast<int32_t>(offsetof(AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA, ___CreationTimeout_3)); }
	inline int32_t get_CreationTimeout_3() const { return ___CreationTimeout_3; }
	inline int32_t* get_address_of_CreationTimeout_3() { return &___CreationTimeout_3; }
	inline void set_CreationTimeout_3(int32_t value)
	{
		___CreationTimeout_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCONNECTIONPOOLREQUEST_TC39BE0320275AE9734B38A518415794109CEF1AA_H
#ifndef COOKIECOLLECTIONENUMERATOR_TDADB2721F8B45D4F815C846DCE2EF92E3760A48D_H
#define COOKIECOLLECTIONENUMERATOR_TDADB2721F8B45D4F815C846DCE2EF92E3760A48D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection_CookieCollectionEnumerator
struct  CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D  : public RuntimeObject
{
public:
	// System.Net.CookieCollection System.Net.CookieCollection_CookieCollectionEnumerator::m_cookies
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * ___m_cookies_0;
	// System.Int32 System.Net.CookieCollection_CookieCollectionEnumerator::m_count
	int32_t ___m_count_1;
	// System.Int32 System.Net.CookieCollection_CookieCollectionEnumerator::m_index
	int32_t ___m_index_2;
	// System.Int32 System.Net.CookieCollection_CookieCollectionEnumerator::m_version
	int32_t ___m_version_3;

public:
	inline static int32_t get_offset_of_m_cookies_0() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_cookies_0)); }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * get_m_cookies_0() const { return ___m_cookies_0; }
	inline CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 ** get_address_of_m_cookies_0() { return &___m_cookies_0; }
	inline void set_m_cookies_0(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3 * value)
	{
		___m_cookies_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_cookies_0), value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}

	inline static int32_t get_offset_of_m_index_2() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_index_2)); }
	inline int32_t get_m_index_2() const { return ___m_index_2; }
	inline int32_t* get_address_of_m_index_2() { return &___m_index_2; }
	inline void set_m_index_2(int32_t value)
	{
		___m_index_2 = value;
	}

	inline static int32_t get_offset_of_m_version_3() { return static_cast<int32_t>(offsetof(CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D, ___m_version_3)); }
	inline int32_t get_m_version_3() const { return ___m_version_3; }
	inline int32_t* get_address_of_m_version_3() { return &___m_version_3; }
	inline void set_m_version_3(int32_t value)
	{
		___m_version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTIONENUMERATOR_TDADB2721F8B45D4F815C846DCE2EF92E3760A48D_H
#ifndef COOKIECONTAINER_T7E062D04BAED9F3B30DDEC14B09660BB506A2A73_H
#define COOKIECONTAINER_T7E062D04BAED9F3B30DDEC14B09660BB506A2A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieContainer
struct  CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Net.CookieContainer::m_domainTable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_domainTable_4;
	// System.Int32 System.Net.CookieContainer::m_maxCookieSize
	int32_t ___m_maxCookieSize_5;
	// System.Int32 System.Net.CookieContainer::m_maxCookies
	int32_t ___m_maxCookies_6;
	// System.Int32 System.Net.CookieContainer::m_maxCookiesPerDomain
	int32_t ___m_maxCookiesPerDomain_7;
	// System.Int32 System.Net.CookieContainer::m_count
	int32_t ___m_count_8;
	// System.String System.Net.CookieContainer::m_fqdnMyDomain
	String_t* ___m_fqdnMyDomain_9;

public:
	inline static int32_t get_offset_of_m_domainTable_4() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_domainTable_4)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_domainTable_4() const { return ___m_domainTable_4; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_domainTable_4() { return &___m_domainTable_4; }
	inline void set_m_domainTable_4(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_domainTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_domainTable_4), value);
	}

	inline static int32_t get_offset_of_m_maxCookieSize_5() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_maxCookieSize_5)); }
	inline int32_t get_m_maxCookieSize_5() const { return ___m_maxCookieSize_5; }
	inline int32_t* get_address_of_m_maxCookieSize_5() { return &___m_maxCookieSize_5; }
	inline void set_m_maxCookieSize_5(int32_t value)
	{
		___m_maxCookieSize_5 = value;
	}

	inline static int32_t get_offset_of_m_maxCookies_6() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_maxCookies_6)); }
	inline int32_t get_m_maxCookies_6() const { return ___m_maxCookies_6; }
	inline int32_t* get_address_of_m_maxCookies_6() { return &___m_maxCookies_6; }
	inline void set_m_maxCookies_6(int32_t value)
	{
		___m_maxCookies_6 = value;
	}

	inline static int32_t get_offset_of_m_maxCookiesPerDomain_7() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_maxCookiesPerDomain_7)); }
	inline int32_t get_m_maxCookiesPerDomain_7() const { return ___m_maxCookiesPerDomain_7; }
	inline int32_t* get_address_of_m_maxCookiesPerDomain_7() { return &___m_maxCookiesPerDomain_7; }
	inline void set_m_maxCookiesPerDomain_7(int32_t value)
	{
		___m_maxCookiesPerDomain_7 = value;
	}

	inline static int32_t get_offset_of_m_count_8() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_count_8)); }
	inline int32_t get_m_count_8() const { return ___m_count_8; }
	inline int32_t* get_address_of_m_count_8() { return &___m_count_8; }
	inline void set_m_count_8(int32_t value)
	{
		___m_count_8 = value;
	}

	inline static int32_t get_offset_of_m_fqdnMyDomain_9() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73, ___m_fqdnMyDomain_9)); }
	inline String_t* get_m_fqdnMyDomain_9() const { return ___m_fqdnMyDomain_9; }
	inline String_t** get_address_of_m_fqdnMyDomain_9() { return &___m_fqdnMyDomain_9; }
	inline void set_m_fqdnMyDomain_9(String_t* value)
	{
		___m_fqdnMyDomain_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_fqdnMyDomain_9), value);
	}
};

struct CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields
{
public:
	// System.Net.HeaderVariantInfo[] System.Net.CookieContainer::HeaderInfo
	HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012* ___HeaderInfo_3;

public:
	inline static int32_t get_offset_of_HeaderInfo_3() { return static_cast<int32_t>(offsetof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields, ___HeaderInfo_3)); }
	inline HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012* get_HeaderInfo_3() const { return ___HeaderInfo_3; }
	inline HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012** get_address_of_HeaderInfo_3() { return &___HeaderInfo_3; }
	inline void set_HeaderInfo_3(HeaderVariantInfoU5BU5D_t0E01B2AC4A3A836E5AC79344A8F0CBD547CC8012* value)
	{
		___HeaderInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___HeaderInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECONTAINER_T7E062D04BAED9F3B30DDEC14B09660BB506A2A73_H
#ifndef COOKIEPARSER_T6034725CF7B5A3842FEC753620D331478F74B396_H
#define COOKIEPARSER_T6034725CF7B5A3842FEC753620D331478F74B396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieParser
struct  CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396  : public RuntimeObject
{
public:
	// System.Net.CookieTokenizer System.Net.CookieParser::m_tokenizer
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD * ___m_tokenizer_0;
	// System.Net.Cookie System.Net.CookieParser::m_savedCookie
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90 * ___m_savedCookie_1;

public:
	inline static int32_t get_offset_of_m_tokenizer_0() { return static_cast<int32_t>(offsetof(CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396, ___m_tokenizer_0)); }
	inline CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD * get_m_tokenizer_0() const { return ___m_tokenizer_0; }
	inline CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD ** get_address_of_m_tokenizer_0() { return &___m_tokenizer_0; }
	inline void set_m_tokenizer_0(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD * value)
	{
		___m_tokenizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_tokenizer_0), value);
	}

	inline static int32_t get_offset_of_m_savedCookie_1() { return static_cast<int32_t>(offsetof(CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396, ___m_savedCookie_1)); }
	inline Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90 * get_m_savedCookie_1() const { return ___m_savedCookie_1; }
	inline Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90 ** get_address_of_m_savedCookie_1() { return &___m_savedCookie_1; }
	inline void set_m_savedCookie_1(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90 * value)
	{
		___m_savedCookie_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_savedCookie_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEPARSER_T6034725CF7B5A3842FEC753620D331478F74B396_H
#ifndef DEFAULTCERTIFICATEPOLICY_T2B28BF921CE86F4956EF998580E48C314B14A674_H
#define DEFAULTCERTIFICATEPOLICY_T2B28BF921CE86F4956EF998580E48C314B14A674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DefaultCertificatePolicy
struct  DefaultCertificatePolicy_t2B28BF921CE86F4956EF998580E48C314B14A674  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCERTIFICATEPOLICY_T2B28BF921CE86F4956EF998580E48C314B14A674_H
#ifndef DIGESTCLIENT_T2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_H
#define DIGESTCLIENT_T2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestClient
struct  DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C  : public RuntimeObject
{
public:

public:
};

struct DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.DigestClient::cache
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields, ___cache_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_cache_0() const { return ___cache_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTCLIENT_T2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_H
#ifndef DIGESTHEADERPARSER_T86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_H
#define DIGESTHEADERPARSER_T86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestHeaderParser
struct  DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9  : public RuntimeObject
{
public:
	// System.String System.Net.DigestHeaderParser::header
	String_t* ___header_0;
	// System.Int32 System.Net.DigestHeaderParser::length
	int32_t ___length_1;
	// System.Int32 System.Net.DigestHeaderParser::pos
	int32_t ___pos_2;
	// System.String[] System.Net.DigestHeaderParser::values
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___values_4;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}

	inline static int32_t get_offset_of_values_4() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9, ___values_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_values_4() const { return ___values_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_values_4() { return &___values_4; }
	inline void set_values_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___values_4 = value;
		Il2CppCodeGenWriteBarrier((&___values_4), value);
	}
};

struct DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields
{
public:
	// System.String[] System.Net.DigestHeaderParser::keywords
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___keywords_3;

public:
	inline static int32_t get_offset_of_keywords_3() { return static_cast<int32_t>(offsetof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields, ___keywords_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_keywords_3() const { return ___keywords_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_keywords_3() { return &___keywords_3; }
	inline void set_keywords_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___keywords_3 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTHEADERPARSER_T86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_H
#ifndef DNS_T0E6B5B77C654107F106B577875FE899BAF8ADCF9_H
#define DNS_T0E6B5B77C654107F106B577875FE899BAF8ADCF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns
struct  Dns_t0E6B5B77C654107F106B577875FE899BAF8ADCF9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DNS_T0E6B5B77C654107F106B577875FE899BAF8ADCF9_H
#ifndef FILEWEBREQUESTCREATOR_TC02A1A70722C45B078D759F22AE10256A6900C6D_H
#define FILEWEBREQUESTCREATOR_TC02A1A70722C45B078D759F22AE10256A6900C6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequestCreator
struct  FileWebRequestCreator_tC02A1A70722C45B078D759F22AE10256A6900C6D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUESTCREATOR_TC02A1A70722C45B078D759F22AE10256A6900C6D_H
#ifndef HTTPSYSSETTINGS_TD4022F248385DE2D6DC0E56ED065C427791663F4_H
#define HTTPSYSSETTINGS_TD4022F248385DE2D6DC0E56ED065C427791663F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpSysSettings
struct  HttpSysSettings_tD4022F248385DE2D6DC0E56ED065C427791663F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSYSSETTINGS_TD4022F248385DE2D6DC0E56ED065C427791663F4_H
#ifndef INTERLOCKEDSTACK_TFCF31BB75AAF94EE95CC970515F20CD84745CB2C_H
#define INTERLOCKEDSTACK_TFCF31BB75AAF94EE95CC970515F20CD84745CB2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.InterlockedStack
struct  InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C  : public RuntimeObject
{
public:
	// System.Collections.Stack System.Net.InterlockedStack::_stack
	Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * ____stack_0;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C, ____stack_0)); }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * get__stack_0() const { return ____stack_0; }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((&____stack_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERLOCKEDSTACK_TFCF31BB75AAF94EE95CC970515F20CD84745CB2C_H
#ifndef LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#define LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.LazyAsyncResult
struct  LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3  : public RuntimeObject
{
public:
	// System.Object System.Net.LazyAsyncResult::m_AsyncObject
	RuntimeObject * ___m_AsyncObject_3;
	// System.Object System.Net.LazyAsyncResult::m_AsyncState
	RuntimeObject * ___m_AsyncState_4;
	// System.AsyncCallback System.Net.LazyAsyncResult::m_AsyncCallback
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___m_AsyncCallback_5;
	// System.Object System.Net.LazyAsyncResult::m_Result
	RuntimeObject * ___m_Result_6;
	// System.Int32 System.Net.LazyAsyncResult::m_ErrorCode
	int32_t ___m_ErrorCode_7;
	// System.Int32 System.Net.LazyAsyncResult::m_IntCompleted
	int32_t ___m_IntCompleted_8;
	// System.Boolean System.Net.LazyAsyncResult::m_EndCalled
	bool ___m_EndCalled_9;
	// System.Boolean System.Net.LazyAsyncResult::m_UserEvent
	bool ___m_UserEvent_10;
	// System.Object System.Net.LazyAsyncResult::m_Event
	RuntimeObject * ___m_Event_11;

public:
	inline static int32_t get_offset_of_m_AsyncObject_3() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncObject_3)); }
	inline RuntimeObject * get_m_AsyncObject_3() const { return ___m_AsyncObject_3; }
	inline RuntimeObject ** get_address_of_m_AsyncObject_3() { return &___m_AsyncObject_3; }
	inline void set_m_AsyncObject_3(RuntimeObject * value)
	{
		___m_AsyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncObject_3), value);
	}

	inline static int32_t get_offset_of_m_AsyncState_4() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncState_4)); }
	inline RuntimeObject * get_m_AsyncState_4() const { return ___m_AsyncState_4; }
	inline RuntimeObject ** get_address_of_m_AsyncState_4() { return &___m_AsyncState_4; }
	inline void set_m_AsyncState_4(RuntimeObject * value)
	{
		___m_AsyncState_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncState_4), value);
	}

	inline static int32_t get_offset_of_m_AsyncCallback_5() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_AsyncCallback_5)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_m_AsyncCallback_5() const { return ___m_AsyncCallback_5; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_m_AsyncCallback_5() { return &___m_AsyncCallback_5; }
	inline void set_m_AsyncCallback_5(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___m_AsyncCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncCallback_5), value);
	}

	inline static int32_t get_offset_of_m_Result_6() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_Result_6)); }
	inline RuntimeObject * get_m_Result_6() const { return ___m_Result_6; }
	inline RuntimeObject ** get_address_of_m_Result_6() { return &___m_Result_6; }
	inline void set_m_Result_6(RuntimeObject * value)
	{
		___m_Result_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_6), value);
	}

	inline static int32_t get_offset_of_m_ErrorCode_7() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_ErrorCode_7)); }
	inline int32_t get_m_ErrorCode_7() const { return ___m_ErrorCode_7; }
	inline int32_t* get_address_of_m_ErrorCode_7() { return &___m_ErrorCode_7; }
	inline void set_m_ErrorCode_7(int32_t value)
	{
		___m_ErrorCode_7 = value;
	}

	inline static int32_t get_offset_of_m_IntCompleted_8() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_IntCompleted_8)); }
	inline int32_t get_m_IntCompleted_8() const { return ___m_IntCompleted_8; }
	inline int32_t* get_address_of_m_IntCompleted_8() { return &___m_IntCompleted_8; }
	inline void set_m_IntCompleted_8(int32_t value)
	{
		___m_IntCompleted_8 = value;
	}

	inline static int32_t get_offset_of_m_EndCalled_9() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_EndCalled_9)); }
	inline bool get_m_EndCalled_9() const { return ___m_EndCalled_9; }
	inline bool* get_address_of_m_EndCalled_9() { return &___m_EndCalled_9; }
	inline void set_m_EndCalled_9(bool value)
	{
		___m_EndCalled_9 = value;
	}

	inline static int32_t get_offset_of_m_UserEvent_10() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_UserEvent_10)); }
	inline bool get_m_UserEvent_10() const { return ___m_UserEvent_10; }
	inline bool* get_address_of_m_UserEvent_10() { return &___m_UserEvent_10; }
	inline void set_m_UserEvent_10(bool value)
	{
		___m_UserEvent_10 = value;
	}

	inline static int32_t get_offset_of_m_Event_11() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3, ___m_Event_11)); }
	inline RuntimeObject * get_m_Event_11() const { return ___m_Event_11; }
	inline RuntimeObject ** get_address_of_m_Event_11() { return &___m_Event_11; }
	inline void set_m_Event_11(RuntimeObject * value)
	{
		___m_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Event_11), value);
	}
};

struct LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields
{
public:
	// System.Net.LazyAsyncResult_ThreadContext System.Net.LazyAsyncResult::t_ThreadContext
	ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * ___t_ThreadContext_2;

public:
	inline static int32_t get_offset_of_t_ThreadContext_2() { return static_cast<int32_t>(offsetof(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3_ThreadStaticFields, ___t_ThreadContext_2)); }
	inline ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * get_t_ThreadContext_2() const { return ___t_ThreadContext_2; }
	inline ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 ** get_address_of_t_ThreadContext_2() { return &___t_ThreadContext_2; }
	inline void set_t_ThreadContext_2(ThreadContext_tCC2E1DE0DDF550CCA67AE22CDF6F1AD426DC9082 * value)
	{
		___t_ThreadContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_ThreadContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYASYNCRESULT_T6D867D275402699126BB3DC89093BD94CFFDA5D3_H
#ifndef LOGGING_T16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_H
#define LOGGING_T16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Logging
struct  Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70  : public RuntimeObject
{
public:

public:
};

struct Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields
{
public:
	// System.Boolean System.Net.Logging::On
	bool ___On_0;

public:
	inline static int32_t get_offset_of_On_0() { return static_cast<int32_t>(offsetof(Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields, ___On_0)); }
	inline bool get_On_0() const { return ___On_0; }
	inline bool* get_address_of_On_0() { return &___On_0; }
	inline void set_On_0(bool value)
	{
		___On_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGING_T16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_H
#ifndef PATHLIST_TE89F0E044B0D96268DB20C9B0FC852C690C2DC8A_H
#define PATHLIST_TE89F0E044B0D96268DB20C9B0FC852C690C2DC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.PathList
struct  PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A  : public RuntimeObject
{
public:
	// System.Collections.SortedList System.Net.PathList::m_list
	SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * ___m_list_0;

public:
	inline static int32_t get_offset_of_m_list_0() { return static_cast<int32_t>(offsetof(PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A, ___m_list_0)); }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * get_m_list_0() const { return ___m_list_0; }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E ** get_address_of_m_list_0() { return &___m_list_0; }
	inline void set_m_list_0(SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * value)
	{
		___m_list_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHLIST_TE89F0E044B0D96268DB20C9B0FC852C690C2DC8A_H
#ifndef PATHLISTCOMPARER_TDBE17A93599D72911D22973B739F500E8C26ADCF_H
#define PATHLISTCOMPARER_TDBE17A93599D72911D22973B739F500E8C26ADCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.PathList_PathListComparer
struct  PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF  : public RuntimeObject
{
public:

public:
};

struct PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields
{
public:
	// System.Net.PathList_PathListComparer System.Net.PathList_PathListComparer::StaticInstance
	PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF * ___StaticInstance_0;

public:
	inline static int32_t get_offset_of_StaticInstance_0() { return static_cast<int32_t>(offsetof(PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields, ___StaticInstance_0)); }
	inline PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF * get_StaticInstance_0() const { return ___StaticInstance_0; }
	inline PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF ** get_address_of_StaticInstance_0() { return &___StaticInstance_0; }
	inline void set_StaticInstance_0(PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF * value)
	{
		___StaticInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___StaticInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHLISTCOMPARER_TDBE17A93599D72911D22973B739F500E8C26ADCF_H
#ifndef SERVERCERTVALIDATIONCALLBACK_T431E949AECAE20901007813737F5B26311F5F9FB_H
#define SERVERCERTVALIDATIONCALLBACK_T431E949AECAE20901007813737F5B26311F5F9FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback
struct  ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB  : public RuntimeObject
{
public:
	// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServerCertValidationCallback::m_ValidationCallback
	RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * ___m_ValidationCallback_0;
	// System.Threading.ExecutionContext System.Net.ServerCertValidationCallback::m_Context
	ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * ___m_Context_1;

public:
	inline static int32_t get_offset_of_m_ValidationCallback_0() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB, ___m_ValidationCallback_0)); }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * get_m_ValidationCallback_0() const { return ___m_ValidationCallback_0; }
	inline RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E ** get_address_of_m_ValidationCallback_0() { return &___m_ValidationCallback_0; }
	inline void set_m_ValidationCallback_0(RemoteCertificateValidationCallback_t9C6BA19681BAA3CD78E6674293A57FF5DF62831E * value)
	{
		___m_ValidationCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidationCallback_0), value);
	}

	inline static int32_t get_offset_of_m_Context_1() { return static_cast<int32_t>(offsetof(ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB, ___m_Context_1)); }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * get_m_Context_1() const { return ___m_Context_1; }
	inline ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 ** get_address_of_m_Context_1() { return &___m_Context_1; }
	inline void set_m_Context_1(ExecutionContext_t0E11C30308A4CC964D8A2EA9132F9BDCE5362C70 * value)
	{
		___m_Context_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCERTVALIDATIONCALLBACK_T431E949AECAE20901007813737F5B26311F5F9FB_H
#ifndef TRACESOURCE_TB542C6FBC9B31C22CB10F33E872951BADCFA2633_H
#define TRACESOURCE_TB542C6FBC9B31C22CB10F33E872951BADCFA2633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TraceSource
struct  TraceSource_tB542C6FBC9B31C22CB10F33E872951BADCFA2633  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACESOURCE_TB542C6FBC9B31C22CB10F33E872951BADCFA2633_H
#ifndef UNSAFENCLNATIVEMETHODS_T6130A140E270DE50A48DAA13D71A71F5BD9F1537_H
#define UNSAFENCLNATIVEMETHODS_T6130A140E270DE50A48DAA13D71A71F5BD9F1537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods
struct  UnsafeNclNativeMethods_t6130A140E270DE50A48DAA13D71A71F5BD9F1537  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSAFENCLNATIVEMETHODS_T6130A140E270DE50A48DAA13D71A71F5BD9F1537_H
#ifndef HTTPAPI_TDD21E6C468ACB472A3D1CE51865417E65113B12F_H
#define HTTPAPI_TDD21E6C468ACB472A3D1CE51865417E65113B12F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods_HttpApi
struct  HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F  : public RuntimeObject
{
public:

public:
};

struct HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields
{
public:
	// System.String[] System.Net.UnsafeNclNativeMethods_HttpApi::m_Strings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_Strings_2;

public:
	inline static int32_t get_offset_of_m_Strings_2() { return static_cast<int32_t>(offsetof(HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields, ___m_Strings_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_Strings_2() const { return ___m_Strings_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_Strings_2() { return &___m_Strings_2; }
	inline void set_m_Strings_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_Strings_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Strings_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPAPI_TDD21E6C468ACB472A3D1CE51865417E65113B12F_H
#ifndef HTTP_REQUEST_HEADER_ID_TB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_H
#define HTTP_REQUEST_HEADER_ID_TB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods_HttpApi_HTTP_REQUEST_HEADER_ID
struct  HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE  : public RuntimeObject
{
public:

public:
};

struct HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields
{
public:
	// System.String[] System.Net.UnsafeNclNativeMethods_HttpApi_HTTP_REQUEST_HEADER_ID::m_Strings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_Strings_0;

public:
	inline static int32_t get_offset_of_m_Strings_0() { return static_cast<int32_t>(offsetof(HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields, ___m_Strings_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_Strings_0() const { return ___m_Strings_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_Strings_0() { return &___m_Strings_0; }
	inline void set_m_Strings_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_Strings_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Strings_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP_REQUEST_HEADER_ID_TB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_H
#ifndef HTTP_RESPONSE_HEADER_ID_TA74859FBE9C85DA67FD3342E8540DE020868BA79_H
#define HTTP_RESPONSE_HEADER_ID_TA74859FBE9C85DA67FD3342E8540DE020868BA79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods_HttpApi_HTTP_RESPONSE_HEADER_ID
struct  HTTP_RESPONSE_HEADER_ID_tA74859FBE9C85DA67FD3342E8540DE020868BA79  : public RuntimeObject
{
public:

public:
};

struct HTTP_RESPONSE_HEADER_ID_tA74859FBE9C85DA67FD3342E8540DE020868BA79_StaticFields
{
public:
	// System.Collections.Hashtable System.Net.UnsafeNclNativeMethods_HttpApi_HTTP_RESPONSE_HEADER_ID::m_Hashtable
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Hashtable_0;

public:
	inline static int32_t get_offset_of_m_Hashtable_0() { return static_cast<int32_t>(offsetof(HTTP_RESPONSE_HEADER_ID_tA74859FBE9C85DA67FD3342E8540DE020868BA79_StaticFields, ___m_Hashtable_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Hashtable_0() const { return ___m_Hashtable_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Hashtable_0() { return &___m_Hashtable_0; }
	inline void set_m_Hashtable_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Hashtable_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hashtable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTP_RESPONSE_HEADER_ID_TA74859FBE9C85DA67FD3342E8540DE020868BA79_H
#ifndef SECURESTRINGHELPER_T9F5A5E822AB08545A97B612C217CC6C760FF78F5_H
#define SECURESTRINGHELPER_T9F5A5E822AB08545A97B612C217CC6C760FF78F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods_SecureStringHelper
struct  SecureStringHelper_t9F5A5E822AB08545A97B612C217CC6C760FF78F5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURESTRINGHELPER_T9F5A5E822AB08545A97B612C217CC6C760FF78F5_H
#ifndef U3CU3EC_TA25D420E0876D9D904FC49A6C21CA7C82EB06703_H
#define U3CU3EC_TA25D420E0876D9D904FC49A6C21CA7C82EB06703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c
struct  U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields
{
public:
	// System.Net.WebClient_<>c System.Net.WebClient_<>c::<>9
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703 * ___U3CU3E9_0;
	// System.Func`2<System.Net.DownloadStringCompletedEventArgs,System.String> System.Net.WebClient_<>c::<>9__219_1
	Func_2_tBE7D342FE0F39A2241399FBE8864BC632864C619 * ___U3CU3E9__219_1_1;
	// System.Action`2<System.Net.WebClient,System.Net.DownloadStringCompletedEventHandler> System.Net.WebClient_<>c::<>9__219_2
	Action_2_t9061DC4CD4B5675DC998F0790CA5D0D15DCC66CE * ___U3CU3E9__219_2_2;
	// System.Func`2<System.Net.OpenReadCompletedEventArgs,System.IO.Stream> System.Net.WebClient_<>c::<>9__221_1
	Func_2_t57744E90FE5027161AF05CCEDD66C4AB64724D53 * ___U3CU3E9__221_1_3;
	// System.Action`2<System.Net.WebClient,System.Net.OpenReadCompletedEventHandler> System.Net.WebClient_<>c::<>9__221_2
	Action_2_t24C9A91F4BC1ED6D40AE9E798F19DB0CE4CB56C8 * ___U3CU3E9__221_2_4;
	// System.Func`2<System.Net.OpenWriteCompletedEventArgs,System.IO.Stream> System.Net.WebClient_<>c::<>9__225_1
	Func_2_t3157BDC0194B05B94A754E301CA0A760CC7292F5 * ___U3CU3E9__225_1_5;
	// System.Action`2<System.Net.WebClient,System.Net.OpenWriteCompletedEventHandler> System.Net.WebClient_<>c::<>9__225_2
	Action_2_t20FA8610429534671D804864BC590CF75935F40D * ___U3CU3E9__225_2_6;
	// System.Func`2<System.Net.UploadStringCompletedEventArgs,System.String> System.Net.WebClient_<>c::<>9__229_1
	Func_2_t115B8EA7D5806FC5ACB8DA63EBD4B299813BE817 * ___U3CU3E9__229_1_7;
	// System.Action`2<System.Net.WebClient,System.Net.UploadStringCompletedEventHandler> System.Net.WebClient_<>c::<>9__229_2
	Action_2_t1168A6A3333D3CF7FDDF720A4DDDDCEA7FB7AFDE * ___U3CU3E9__229_2_8;
	// System.Func`2<System.Net.DownloadDataCompletedEventArgs,System.Byte[]> System.Net.WebClient_<>c::<>9__231_1
	Func_2_tD17A333C073B6B19056A11BF6B7998AE30809197 * ___U3CU3E9__231_1_9;
	// System.Action`2<System.Net.WebClient,System.Net.DownloadDataCompletedEventHandler> System.Net.WebClient_<>c::<>9__231_2
	Action_2_t083259C45D6A8E2CCD331C122A023C5C4B4D0FE9 * ___U3CU3E9__231_2_10;
	// System.Func`2<System.ComponentModel.AsyncCompletedEventArgs,System.Object> System.Net.WebClient_<>c::<>9__233_1
	Func_2_tDE667922AE5D8E516168594E04038A79091979A9 * ___U3CU3E9__233_1_11;
	// System.Action`2<System.Net.WebClient,System.ComponentModel.AsyncCompletedEventHandler> System.Net.WebClient_<>c::<>9__233_2
	Action_2_t69AEB644838FDEB34941A1AF142FD305C93A49A7 * ___U3CU3E9__233_2_12;
	// System.Func`2<System.Net.UploadDataCompletedEventArgs,System.Byte[]> System.Net.WebClient_<>c::<>9__237_1
	Func_2_t2D1860C041767F9F7CD15D3B75EC44D9C9188120 * ___U3CU3E9__237_1_13;
	// System.Action`2<System.Net.WebClient,System.Net.UploadDataCompletedEventHandler> System.Net.WebClient_<>c::<>9__237_2
	Action_2_t2C49A0F9A7170A372BEA7535DD431F01A57B0589 * ___U3CU3E9__237_2_14;
	// System.Func`2<System.Net.UploadFileCompletedEventArgs,System.Byte[]> System.Net.WebClient_<>c::<>9__241_1
	Func_2_t7AAA217876F68374CC848B134DCF1032AB2E47A0 * ___U3CU3E9__241_1_15;
	// System.Action`2<System.Net.WebClient,System.Net.UploadFileCompletedEventHandler> System.Net.WebClient_<>c::<>9__241_2
	Action_2_tE38D6782DC1A490EDE9D47D9C1F03A86B5171036 * ___U3CU3E9__241_2_16;
	// System.Func`2<System.Net.UploadValuesCompletedEventArgs,System.Byte[]> System.Net.WebClient_<>c::<>9__245_1
	Func_2_t3D57FDDC2419906CF7CE57832CA05A230477F790 * ___U3CU3E9__245_1_17;
	// System.Action`2<System.Net.WebClient,System.Net.UploadValuesCompletedEventHandler> System.Net.WebClient_<>c::<>9__245_2
	Action_2_t3EE0A1BD7BD16FCD2E00EADD73080664BAC8A035 * ___U3CU3E9__245_2_18;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__219_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__219_1_1)); }
	inline Func_2_tBE7D342FE0F39A2241399FBE8864BC632864C619 * get_U3CU3E9__219_1_1() const { return ___U3CU3E9__219_1_1; }
	inline Func_2_tBE7D342FE0F39A2241399FBE8864BC632864C619 ** get_address_of_U3CU3E9__219_1_1() { return &___U3CU3E9__219_1_1; }
	inline void set_U3CU3E9__219_1_1(Func_2_tBE7D342FE0F39A2241399FBE8864BC632864C619 * value)
	{
		___U3CU3E9__219_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__219_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__219_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__219_2_2)); }
	inline Action_2_t9061DC4CD4B5675DC998F0790CA5D0D15DCC66CE * get_U3CU3E9__219_2_2() const { return ___U3CU3E9__219_2_2; }
	inline Action_2_t9061DC4CD4B5675DC998F0790CA5D0D15DCC66CE ** get_address_of_U3CU3E9__219_2_2() { return &___U3CU3E9__219_2_2; }
	inline void set_U3CU3E9__219_2_2(Action_2_t9061DC4CD4B5675DC998F0790CA5D0D15DCC66CE * value)
	{
		___U3CU3E9__219_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__219_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__221_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__221_1_3)); }
	inline Func_2_t57744E90FE5027161AF05CCEDD66C4AB64724D53 * get_U3CU3E9__221_1_3() const { return ___U3CU3E9__221_1_3; }
	inline Func_2_t57744E90FE5027161AF05CCEDD66C4AB64724D53 ** get_address_of_U3CU3E9__221_1_3() { return &___U3CU3E9__221_1_3; }
	inline void set_U3CU3E9__221_1_3(Func_2_t57744E90FE5027161AF05CCEDD66C4AB64724D53 * value)
	{
		___U3CU3E9__221_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__221_1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__221_2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__221_2_4)); }
	inline Action_2_t24C9A91F4BC1ED6D40AE9E798F19DB0CE4CB56C8 * get_U3CU3E9__221_2_4() const { return ___U3CU3E9__221_2_4; }
	inline Action_2_t24C9A91F4BC1ED6D40AE9E798F19DB0CE4CB56C8 ** get_address_of_U3CU3E9__221_2_4() { return &___U3CU3E9__221_2_4; }
	inline void set_U3CU3E9__221_2_4(Action_2_t24C9A91F4BC1ED6D40AE9E798F19DB0CE4CB56C8 * value)
	{
		___U3CU3E9__221_2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__221_2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__225_1_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__225_1_5)); }
	inline Func_2_t3157BDC0194B05B94A754E301CA0A760CC7292F5 * get_U3CU3E9__225_1_5() const { return ___U3CU3E9__225_1_5; }
	inline Func_2_t3157BDC0194B05B94A754E301CA0A760CC7292F5 ** get_address_of_U3CU3E9__225_1_5() { return &___U3CU3E9__225_1_5; }
	inline void set_U3CU3E9__225_1_5(Func_2_t3157BDC0194B05B94A754E301CA0A760CC7292F5 * value)
	{
		___U3CU3E9__225_1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__225_1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__225_2_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__225_2_6)); }
	inline Action_2_t20FA8610429534671D804864BC590CF75935F40D * get_U3CU3E9__225_2_6() const { return ___U3CU3E9__225_2_6; }
	inline Action_2_t20FA8610429534671D804864BC590CF75935F40D ** get_address_of_U3CU3E9__225_2_6() { return &___U3CU3E9__225_2_6; }
	inline void set_U3CU3E9__225_2_6(Action_2_t20FA8610429534671D804864BC590CF75935F40D * value)
	{
		___U3CU3E9__225_2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__225_2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__229_1_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__229_1_7)); }
	inline Func_2_t115B8EA7D5806FC5ACB8DA63EBD4B299813BE817 * get_U3CU3E9__229_1_7() const { return ___U3CU3E9__229_1_7; }
	inline Func_2_t115B8EA7D5806FC5ACB8DA63EBD4B299813BE817 ** get_address_of_U3CU3E9__229_1_7() { return &___U3CU3E9__229_1_7; }
	inline void set_U3CU3E9__229_1_7(Func_2_t115B8EA7D5806FC5ACB8DA63EBD4B299813BE817 * value)
	{
		___U3CU3E9__229_1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__229_1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__229_2_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__229_2_8)); }
	inline Action_2_t1168A6A3333D3CF7FDDF720A4DDDDCEA7FB7AFDE * get_U3CU3E9__229_2_8() const { return ___U3CU3E9__229_2_8; }
	inline Action_2_t1168A6A3333D3CF7FDDF720A4DDDDCEA7FB7AFDE ** get_address_of_U3CU3E9__229_2_8() { return &___U3CU3E9__229_2_8; }
	inline void set_U3CU3E9__229_2_8(Action_2_t1168A6A3333D3CF7FDDF720A4DDDDCEA7FB7AFDE * value)
	{
		___U3CU3E9__229_2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__229_2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__231_1_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__231_1_9)); }
	inline Func_2_tD17A333C073B6B19056A11BF6B7998AE30809197 * get_U3CU3E9__231_1_9() const { return ___U3CU3E9__231_1_9; }
	inline Func_2_tD17A333C073B6B19056A11BF6B7998AE30809197 ** get_address_of_U3CU3E9__231_1_9() { return &___U3CU3E9__231_1_9; }
	inline void set_U3CU3E9__231_1_9(Func_2_tD17A333C073B6B19056A11BF6B7998AE30809197 * value)
	{
		___U3CU3E9__231_1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__231_1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__231_2_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__231_2_10)); }
	inline Action_2_t083259C45D6A8E2CCD331C122A023C5C4B4D0FE9 * get_U3CU3E9__231_2_10() const { return ___U3CU3E9__231_2_10; }
	inline Action_2_t083259C45D6A8E2CCD331C122A023C5C4B4D0FE9 ** get_address_of_U3CU3E9__231_2_10() { return &___U3CU3E9__231_2_10; }
	inline void set_U3CU3E9__231_2_10(Action_2_t083259C45D6A8E2CCD331C122A023C5C4B4D0FE9 * value)
	{
		___U3CU3E9__231_2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__231_2_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__233_1_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__233_1_11)); }
	inline Func_2_tDE667922AE5D8E516168594E04038A79091979A9 * get_U3CU3E9__233_1_11() const { return ___U3CU3E9__233_1_11; }
	inline Func_2_tDE667922AE5D8E516168594E04038A79091979A9 ** get_address_of_U3CU3E9__233_1_11() { return &___U3CU3E9__233_1_11; }
	inline void set_U3CU3E9__233_1_11(Func_2_tDE667922AE5D8E516168594E04038A79091979A9 * value)
	{
		___U3CU3E9__233_1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__233_1_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__233_2_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__233_2_12)); }
	inline Action_2_t69AEB644838FDEB34941A1AF142FD305C93A49A7 * get_U3CU3E9__233_2_12() const { return ___U3CU3E9__233_2_12; }
	inline Action_2_t69AEB644838FDEB34941A1AF142FD305C93A49A7 ** get_address_of_U3CU3E9__233_2_12() { return &___U3CU3E9__233_2_12; }
	inline void set_U3CU3E9__233_2_12(Action_2_t69AEB644838FDEB34941A1AF142FD305C93A49A7 * value)
	{
		___U3CU3E9__233_2_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__233_2_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__237_1_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__237_1_13)); }
	inline Func_2_t2D1860C041767F9F7CD15D3B75EC44D9C9188120 * get_U3CU3E9__237_1_13() const { return ___U3CU3E9__237_1_13; }
	inline Func_2_t2D1860C041767F9F7CD15D3B75EC44D9C9188120 ** get_address_of_U3CU3E9__237_1_13() { return &___U3CU3E9__237_1_13; }
	inline void set_U3CU3E9__237_1_13(Func_2_t2D1860C041767F9F7CD15D3B75EC44D9C9188120 * value)
	{
		___U3CU3E9__237_1_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__237_1_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__237_2_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__237_2_14)); }
	inline Action_2_t2C49A0F9A7170A372BEA7535DD431F01A57B0589 * get_U3CU3E9__237_2_14() const { return ___U3CU3E9__237_2_14; }
	inline Action_2_t2C49A0F9A7170A372BEA7535DD431F01A57B0589 ** get_address_of_U3CU3E9__237_2_14() { return &___U3CU3E9__237_2_14; }
	inline void set_U3CU3E9__237_2_14(Action_2_t2C49A0F9A7170A372BEA7535DD431F01A57B0589 * value)
	{
		___U3CU3E9__237_2_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__237_2_14), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__241_1_15() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__241_1_15)); }
	inline Func_2_t7AAA217876F68374CC848B134DCF1032AB2E47A0 * get_U3CU3E9__241_1_15() const { return ___U3CU3E9__241_1_15; }
	inline Func_2_t7AAA217876F68374CC848B134DCF1032AB2E47A0 ** get_address_of_U3CU3E9__241_1_15() { return &___U3CU3E9__241_1_15; }
	inline void set_U3CU3E9__241_1_15(Func_2_t7AAA217876F68374CC848B134DCF1032AB2E47A0 * value)
	{
		___U3CU3E9__241_1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__241_1_15), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__241_2_16() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__241_2_16)); }
	inline Action_2_tE38D6782DC1A490EDE9D47D9C1F03A86B5171036 * get_U3CU3E9__241_2_16() const { return ___U3CU3E9__241_2_16; }
	inline Action_2_tE38D6782DC1A490EDE9D47D9C1F03A86B5171036 ** get_address_of_U3CU3E9__241_2_16() { return &___U3CU3E9__241_2_16; }
	inline void set_U3CU3E9__241_2_16(Action_2_tE38D6782DC1A490EDE9D47D9C1F03A86B5171036 * value)
	{
		___U3CU3E9__241_2_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__241_2_16), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__245_1_17() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__245_1_17)); }
	inline Func_2_t3D57FDDC2419906CF7CE57832CA05A230477F790 * get_U3CU3E9__245_1_17() const { return ___U3CU3E9__245_1_17; }
	inline Func_2_t3D57FDDC2419906CF7CE57832CA05A230477F790 ** get_address_of_U3CU3E9__245_1_17() { return &___U3CU3E9__245_1_17; }
	inline void set_U3CU3E9__245_1_17(Func_2_t3D57FDDC2419906CF7CE57832CA05A230477F790 * value)
	{
		___U3CU3E9__245_1_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__245_1_17), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__245_2_18() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields, ___U3CU3E9__245_2_18)); }
	inline Action_2_t3EE0A1BD7BD16FCD2E00EADD73080664BAC8A035 * get_U3CU3E9__245_2_18() const { return ___U3CU3E9__245_2_18; }
	inline Action_2_t3EE0A1BD7BD16FCD2E00EADD73080664BAC8A035 ** get_address_of_U3CU3E9__245_2_18() { return &___U3CU3E9__245_2_18; }
	inline void set_U3CU3E9__245_2_18(Action_2_t3EE0A1BD7BD16FCD2E00EADD73080664BAC8A035 * value)
	{
		___U3CU3E9__245_2_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__245_2_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA25D420E0876D9D904FC49A6C21CA7C82EB06703_H
#ifndef U3CU3EC__DISPLAYCLASS219_0_T7B73AAA6A5D162D4621884B5989B86427FC9F326_H
#define U3CU3EC__DISPLAYCLASS219_0_T7B73AAA6A5D162D4621884B5989B86427FC9F326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass219_0
struct  U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass219_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.String> System.Net.WebClient_<>c__DisplayClass219_0::tcs
	TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * ___tcs_1;
	// System.Net.DownloadStringCompletedEventHandler System.Net.WebClient_<>c__DisplayClass219_0::handler
	DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326, ___tcs_1)); }
	inline TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326, ___handler_2)); }
	inline DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C * get_handler_2() const { return ___handler_2; }
	inline DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS219_0_T7B73AAA6A5D162D4621884B5989B86427FC9F326_H
#ifndef U3CU3EC__DISPLAYCLASS221_0_T531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA_H
#define U3CU3EC__DISPLAYCLASS221_0_T531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass221_0
struct  U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass221_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.IO.Stream> System.Net.WebClient_<>c__DisplayClass221_0::tcs
	TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE * ___tcs_1;
	// System.Net.OpenReadCompletedEventHandler System.Net.WebClient_<>c__DisplayClass221_0::handler
	OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA, ___tcs_1)); }
	inline TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA, ___handler_2)); }
	inline OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A * get_handler_2() const { return ___handler_2; }
	inline OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS221_0_T531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA_H
#ifndef U3CU3EC__DISPLAYCLASS225_0_T822CCE40F09D735E6EB7DC8C900BFD468673FEB7_H
#define U3CU3EC__DISPLAYCLASS225_0_T822CCE40F09D735E6EB7DC8C900BFD468673FEB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass225_0
struct  U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass225_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.IO.Stream> System.Net.WebClient_<>c__DisplayClass225_0::tcs
	TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE * ___tcs_1;
	// System.Net.OpenWriteCompletedEventHandler System.Net.WebClient_<>c__DisplayClass225_0::handler
	OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7, ___tcs_1)); }
	inline TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_t543507037C82FF5AA340154F51415CF2727BF9BE * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7, ___handler_2)); }
	inline OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD * get_handler_2() const { return ___handler_2; }
	inline OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS225_0_T822CCE40F09D735E6EB7DC8C900BFD468673FEB7_H
#ifndef U3CU3EC__DISPLAYCLASS229_0_T4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2_H
#define U3CU3EC__DISPLAYCLASS229_0_T4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass229_0
struct  U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass229_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.String> System.Net.WebClient_<>c__DisplayClass229_0::tcs
	TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * ___tcs_1;
	// System.Net.UploadStringCompletedEventHandler System.Net.WebClient_<>c__DisplayClass229_0::handler
	UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2, ___tcs_1)); }
	inline TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_t740D6B4559A97E4E740413E4EB4F1F90A84CB317 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2, ___handler_2)); }
	inline UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 * get_handler_2() const { return ___handler_2; }
	inline UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS229_0_T4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2_H
#ifndef U3CU3EC__DISPLAYCLASS231_0_T58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC_H
#define U3CU3EC__DISPLAYCLASS231_0_T58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass231_0
struct  U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass231_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.Byte[]> System.Net.WebClient_<>c__DisplayClass231_0::tcs
	TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * ___tcs_1;
	// System.Net.DownloadDataCompletedEventHandler System.Net.WebClient_<>c__DisplayClass231_0::handler
	DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC, ___tcs_1)); }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC, ___handler_2)); }
	inline DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 * get_handler_2() const { return ___handler_2; }
	inline DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS231_0_T58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC_H
#ifndef U3CU3EC__DISPLAYCLASS233_0_TC1EE9A5459D05903741819AB7D146C9FDA0CA7A0_H
#define U3CU3EC__DISPLAYCLASS233_0_TC1EE9A5459D05903741819AB7D146C9FDA0CA7A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass233_0
struct  U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass233_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.Object> System.Net.WebClient_<>c__DisplayClass233_0::tcs
	TaskCompletionSource_1_t6C02642279BC7BF03091A8CB685FC6B4E68BED12 * ___tcs_1;
	// System.ComponentModel.AsyncCompletedEventHandler System.Net.WebClient_<>c__DisplayClass233_0::handler
	AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0, ___tcs_1)); }
	inline TaskCompletionSource_1_t6C02642279BC7BF03091A8CB685FC6B4E68BED12 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_t6C02642279BC7BF03091A8CB685FC6B4E68BED12 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_t6C02642279BC7BF03091A8CB685FC6B4E68BED12 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0, ___handler_2)); }
	inline AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 * get_handler_2() const { return ___handler_2; }
	inline AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS233_0_TC1EE9A5459D05903741819AB7D146C9FDA0CA7A0_H
#ifndef U3CU3EC__DISPLAYCLASS237_0_TE92157534B53E3C62BED67AE88D68D30FF73C196_H
#define U3CU3EC__DISPLAYCLASS237_0_TE92157534B53E3C62BED67AE88D68D30FF73C196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass237_0
struct  U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass237_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.Byte[]> System.Net.WebClient_<>c__DisplayClass237_0::tcs
	TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * ___tcs_1;
	// System.Net.UploadDataCompletedEventHandler System.Net.WebClient_<>c__DisplayClass237_0::handler
	UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196, ___tcs_1)); }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196, ___handler_2)); }
	inline UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 * get_handler_2() const { return ___handler_2; }
	inline UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS237_0_TE92157534B53E3C62BED67AE88D68D30FF73C196_H
#ifndef U3CU3EC__DISPLAYCLASS241_0_TA756424A2D82104ABD6894645FEE056AC0C54341_H
#define U3CU3EC__DISPLAYCLASS241_0_TA756424A2D82104ABD6894645FEE056AC0C54341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass241_0
struct  U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass241_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.Byte[]> System.Net.WebClient_<>c__DisplayClass241_0::tcs
	TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * ___tcs_1;
	// System.Net.UploadFileCompletedEventHandler System.Net.WebClient_<>c__DisplayClass241_0::handler
	UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341, ___tcs_1)); }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341, ___handler_2)); }
	inline UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 * get_handler_2() const { return ___handler_2; }
	inline UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS241_0_TA756424A2D82104ABD6894645FEE056AC0C54341_H
#ifndef U3CU3EC__DISPLAYCLASS245_0_TA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED_H
#define U3CU3EC__DISPLAYCLASS245_0_TA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_<>c__DisplayClass245_0
struct  U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_<>c__DisplayClass245_0::<>4__this
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<System.Byte[]> System.Net.WebClient_<>c__DisplayClass245_0::tcs
	TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * ___tcs_1;
	// System.Net.UploadValuesCompletedEventHandler System.Net.WebClient_<>c__DisplayClass245_0::handler
	UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 * ___handler_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED, ___U3CU3E4__this_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED, ___tcs_1)); }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_tB4013FB3A5C28FCEF0646F3ABE1C6F7C35A30C72 * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((&___tcs_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED, ___handler_2)); }
	inline UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 * get_handler_2() const { return ___handler_2; }
	inline UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS245_0_TA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED_H
#ifndef DOWNLOADBITSSTATE_T294B3B4DB78D93D45120CA51692E089D96B9DB44_H
#define DOWNLOADBITSSTATE_T294B3B4DB78D93D45120CA51692E089D96B9DB44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_DownloadBitsState
struct  DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44  : public RuntimeObject
{
public:
	// System.Net.WebClient System.Net.WebClient_DownloadBitsState::WebClient
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___WebClient_0;
	// System.IO.Stream System.Net.WebClient_DownloadBitsState::WriteStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___WriteStream_1;
	// System.Byte[] System.Net.WebClient_DownloadBitsState::InnerBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___InnerBuffer_2;
	// System.ComponentModel.AsyncOperation System.Net.WebClient_DownloadBitsState::AsyncOp
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * ___AsyncOp_3;
	// System.Net.WebRequest System.Net.WebClient_DownloadBitsState::Request
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___Request_4;
	// System.Net.CompletionDelegate System.Net.WebClient_DownloadBitsState::CompletionDelegate
	CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * ___CompletionDelegate_5;
	// System.IO.Stream System.Net.WebClient_DownloadBitsState::ReadStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___ReadStream_6;
	// System.Net.ScatterGatherBuffers System.Net.WebClient_DownloadBitsState::SgBuffers
	ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B * ___SgBuffers_7;
	// System.Int64 System.Net.WebClient_DownloadBitsState::ContentLength
	int64_t ___ContentLength_8;
	// System.Int64 System.Net.WebClient_DownloadBitsState::Length
	int64_t ___Length_9;
	// System.Net.WebClient_ProgressData System.Net.WebClient_DownloadBitsState::Progress
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * ___Progress_11;

public:
	inline static int32_t get_offset_of_WebClient_0() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___WebClient_0)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_WebClient_0() const { return ___WebClient_0; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_WebClient_0() { return &___WebClient_0; }
	inline void set_WebClient_0(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___WebClient_0 = value;
		Il2CppCodeGenWriteBarrier((&___WebClient_0), value);
	}

	inline static int32_t get_offset_of_WriteStream_1() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___WriteStream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_WriteStream_1() const { return ___WriteStream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_WriteStream_1() { return &___WriteStream_1; }
	inline void set_WriteStream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___WriteStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___WriteStream_1), value);
	}

	inline static int32_t get_offset_of_InnerBuffer_2() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___InnerBuffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_InnerBuffer_2() const { return ___InnerBuffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_InnerBuffer_2() { return &___InnerBuffer_2; }
	inline void set_InnerBuffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___InnerBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___InnerBuffer_2), value);
	}

	inline static int32_t get_offset_of_AsyncOp_3() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___AsyncOp_3)); }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * get_AsyncOp_3() const { return ___AsyncOp_3; }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 ** get_address_of_AsyncOp_3() { return &___AsyncOp_3; }
	inline void set_AsyncOp_3(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * value)
	{
		___AsyncOp_3 = value;
		Il2CppCodeGenWriteBarrier((&___AsyncOp_3), value);
	}

	inline static int32_t get_offset_of_Request_4() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___Request_4)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_Request_4() const { return ___Request_4; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_Request_4() { return &___Request_4; }
	inline void set_Request_4(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___Request_4 = value;
		Il2CppCodeGenWriteBarrier((&___Request_4), value);
	}

	inline static int32_t get_offset_of_CompletionDelegate_5() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___CompletionDelegate_5)); }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * get_CompletionDelegate_5() const { return ___CompletionDelegate_5; }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 ** get_address_of_CompletionDelegate_5() { return &___CompletionDelegate_5; }
	inline void set_CompletionDelegate_5(CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * value)
	{
		___CompletionDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((&___CompletionDelegate_5), value);
	}

	inline static int32_t get_offset_of_ReadStream_6() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___ReadStream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_ReadStream_6() const { return ___ReadStream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_ReadStream_6() { return &___ReadStream_6; }
	inline void set_ReadStream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___ReadStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___ReadStream_6), value);
	}

	inline static int32_t get_offset_of_SgBuffers_7() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___SgBuffers_7)); }
	inline ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B * get_SgBuffers_7() const { return ___SgBuffers_7; }
	inline ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B ** get_address_of_SgBuffers_7() { return &___SgBuffers_7; }
	inline void set_SgBuffers_7(ScatterGatherBuffers_t5090EA13155952525395C6141ACA51BDBE255E6B * value)
	{
		___SgBuffers_7 = value;
		Il2CppCodeGenWriteBarrier((&___SgBuffers_7), value);
	}

	inline static int32_t get_offset_of_ContentLength_8() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___ContentLength_8)); }
	inline int64_t get_ContentLength_8() const { return ___ContentLength_8; }
	inline int64_t* get_address_of_ContentLength_8() { return &___ContentLength_8; }
	inline void set_ContentLength_8(int64_t value)
	{
		___ContentLength_8 = value;
	}

	inline static int32_t get_offset_of_Length_9() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___Length_9)); }
	inline int64_t get_Length_9() const { return ___Length_9; }
	inline int64_t* get_address_of_Length_9() { return &___Length_9; }
	inline void set_Length_9(int64_t value)
	{
		___Length_9 = value;
	}

	inline static int32_t get_offset_of_Progress_11() { return static_cast<int32_t>(offsetof(DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44, ___Progress_11)); }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * get_Progress_11() const { return ___Progress_11; }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 ** get_address_of_Progress_11() { return &___Progress_11; }
	inline void set_Progress_11(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * value)
	{
		___Progress_11 = value;
		Il2CppCodeGenWriteBarrier((&___Progress_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADBITSSTATE_T294B3B4DB78D93D45120CA51692E089D96B9DB44_H
#ifndef PROGRESSDATA_TE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20_H
#define PROGRESSDATA_TE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_ProgressData
struct  ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20  : public RuntimeObject
{
public:
	// System.Int64 System.Net.WebClient_ProgressData::BytesSent
	int64_t ___BytesSent_0;
	// System.Int64 System.Net.WebClient_ProgressData::TotalBytesToSend
	int64_t ___TotalBytesToSend_1;
	// System.Int64 System.Net.WebClient_ProgressData::BytesReceived
	int64_t ___BytesReceived_2;
	// System.Int64 System.Net.WebClient_ProgressData::TotalBytesToReceive
	int64_t ___TotalBytesToReceive_3;
	// System.Boolean System.Net.WebClient_ProgressData::HasUploadPhase
	bool ___HasUploadPhase_4;

public:
	inline static int32_t get_offset_of_BytesSent_0() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___BytesSent_0)); }
	inline int64_t get_BytesSent_0() const { return ___BytesSent_0; }
	inline int64_t* get_address_of_BytesSent_0() { return &___BytesSent_0; }
	inline void set_BytesSent_0(int64_t value)
	{
		___BytesSent_0 = value;
	}

	inline static int32_t get_offset_of_TotalBytesToSend_1() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___TotalBytesToSend_1)); }
	inline int64_t get_TotalBytesToSend_1() const { return ___TotalBytesToSend_1; }
	inline int64_t* get_address_of_TotalBytesToSend_1() { return &___TotalBytesToSend_1; }
	inline void set_TotalBytesToSend_1(int64_t value)
	{
		___TotalBytesToSend_1 = value;
	}

	inline static int32_t get_offset_of_BytesReceived_2() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___BytesReceived_2)); }
	inline int64_t get_BytesReceived_2() const { return ___BytesReceived_2; }
	inline int64_t* get_address_of_BytesReceived_2() { return &___BytesReceived_2; }
	inline void set_BytesReceived_2(int64_t value)
	{
		___BytesReceived_2 = value;
	}

	inline static int32_t get_offset_of_TotalBytesToReceive_3() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___TotalBytesToReceive_3)); }
	inline int64_t get_TotalBytesToReceive_3() const { return ___TotalBytesToReceive_3; }
	inline int64_t* get_address_of_TotalBytesToReceive_3() { return &___TotalBytesToReceive_3; }
	inline void set_TotalBytesToReceive_3(int64_t value)
	{
		___TotalBytesToReceive_3 = value;
	}

	inline static int32_t get_offset_of_HasUploadPhase_4() { return static_cast<int32_t>(offsetof(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20, ___HasUploadPhase_4)); }
	inline bool get_HasUploadPhase_4() const { return ___HasUploadPhase_4; }
	inline bool* get_address_of_HasUploadPhase_4() { return &___HasUploadPhase_4; }
	inline void set_HasUploadPhase_4(bool value)
	{
		___HasUploadPhase_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSDATA_TE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20_H
#ifndef UPLOADBITSSTATE_TA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48_H
#define UPLOADBITSSTATE_TA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_UploadBitsState
struct  UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48  : public RuntimeObject
{
public:
	// System.Int32 System.Net.WebClient_UploadBitsState::m_ChunkSize
	int32_t ___m_ChunkSize_0;
	// System.Int32 System.Net.WebClient_UploadBitsState::m_BufferWritePosition
	int32_t ___m_BufferWritePosition_1;
	// System.Net.WebClient System.Net.WebClient_UploadBitsState::WebClient
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___WebClient_2;
	// System.IO.Stream System.Net.WebClient_UploadBitsState::WriteStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___WriteStream_3;
	// System.Byte[] System.Net.WebClient_UploadBitsState::InnerBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___InnerBuffer_4;
	// System.Byte[] System.Net.WebClient_UploadBitsState::Header
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Header_5;
	// System.Byte[] System.Net.WebClient_UploadBitsState::Footer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Footer_6;
	// System.ComponentModel.AsyncOperation System.Net.WebClient_UploadBitsState::AsyncOp
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * ___AsyncOp_7;
	// System.Net.WebRequest System.Net.WebClient_UploadBitsState::Request
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___Request_8;
	// System.Net.CompletionDelegate System.Net.WebClient_UploadBitsState::UploadCompletionDelegate
	CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * ___UploadCompletionDelegate_9;
	// System.Net.CompletionDelegate System.Net.WebClient_UploadBitsState::DownloadCompletionDelegate
	CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * ___DownloadCompletionDelegate_10;
	// System.IO.Stream System.Net.WebClient_UploadBitsState::ReadStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___ReadStream_11;
	// System.Net.WebClient_ProgressData System.Net.WebClient_UploadBitsState::Progress
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * ___Progress_12;

public:
	inline static int32_t get_offset_of_m_ChunkSize_0() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___m_ChunkSize_0)); }
	inline int32_t get_m_ChunkSize_0() const { return ___m_ChunkSize_0; }
	inline int32_t* get_address_of_m_ChunkSize_0() { return &___m_ChunkSize_0; }
	inline void set_m_ChunkSize_0(int32_t value)
	{
		___m_ChunkSize_0 = value;
	}

	inline static int32_t get_offset_of_m_BufferWritePosition_1() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___m_BufferWritePosition_1)); }
	inline int32_t get_m_BufferWritePosition_1() const { return ___m_BufferWritePosition_1; }
	inline int32_t* get_address_of_m_BufferWritePosition_1() { return &___m_BufferWritePosition_1; }
	inline void set_m_BufferWritePosition_1(int32_t value)
	{
		___m_BufferWritePosition_1 = value;
	}

	inline static int32_t get_offset_of_WebClient_2() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___WebClient_2)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_WebClient_2() const { return ___WebClient_2; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_WebClient_2() { return &___WebClient_2; }
	inline void set_WebClient_2(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___WebClient_2 = value;
		Il2CppCodeGenWriteBarrier((&___WebClient_2), value);
	}

	inline static int32_t get_offset_of_WriteStream_3() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___WriteStream_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_WriteStream_3() const { return ___WriteStream_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_WriteStream_3() { return &___WriteStream_3; }
	inline void set_WriteStream_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___WriteStream_3 = value;
		Il2CppCodeGenWriteBarrier((&___WriteStream_3), value);
	}

	inline static int32_t get_offset_of_InnerBuffer_4() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___InnerBuffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_InnerBuffer_4() const { return ___InnerBuffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_InnerBuffer_4() { return &___InnerBuffer_4; }
	inline void set_InnerBuffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___InnerBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InnerBuffer_4), value);
	}

	inline static int32_t get_offset_of_Header_5() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___Header_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Header_5() const { return ___Header_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Header_5() { return &___Header_5; }
	inline void set_Header_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Header_5 = value;
		Il2CppCodeGenWriteBarrier((&___Header_5), value);
	}

	inline static int32_t get_offset_of_Footer_6() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___Footer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Footer_6() const { return ___Footer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Footer_6() { return &___Footer_6; }
	inline void set_Footer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Footer_6 = value;
		Il2CppCodeGenWriteBarrier((&___Footer_6), value);
	}

	inline static int32_t get_offset_of_AsyncOp_7() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___AsyncOp_7)); }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * get_AsyncOp_7() const { return ___AsyncOp_7; }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 ** get_address_of_AsyncOp_7() { return &___AsyncOp_7; }
	inline void set_AsyncOp_7(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * value)
	{
		___AsyncOp_7 = value;
		Il2CppCodeGenWriteBarrier((&___AsyncOp_7), value);
	}

	inline static int32_t get_offset_of_Request_8() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___Request_8)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_Request_8() const { return ___Request_8; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_Request_8() { return &___Request_8; }
	inline void set_Request_8(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___Request_8 = value;
		Il2CppCodeGenWriteBarrier((&___Request_8), value);
	}

	inline static int32_t get_offset_of_UploadCompletionDelegate_9() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___UploadCompletionDelegate_9)); }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * get_UploadCompletionDelegate_9() const { return ___UploadCompletionDelegate_9; }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 ** get_address_of_UploadCompletionDelegate_9() { return &___UploadCompletionDelegate_9; }
	inline void set_UploadCompletionDelegate_9(CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * value)
	{
		___UploadCompletionDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___UploadCompletionDelegate_9), value);
	}

	inline static int32_t get_offset_of_DownloadCompletionDelegate_10() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___DownloadCompletionDelegate_10)); }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * get_DownloadCompletionDelegate_10() const { return ___DownloadCompletionDelegate_10; }
	inline CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 ** get_address_of_DownloadCompletionDelegate_10() { return &___DownloadCompletionDelegate_10; }
	inline void set_DownloadCompletionDelegate_10(CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674 * value)
	{
		___DownloadCompletionDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadCompletionDelegate_10), value);
	}

	inline static int32_t get_offset_of_ReadStream_11() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___ReadStream_11)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_ReadStream_11() const { return ___ReadStream_11; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_ReadStream_11() { return &___ReadStream_11; }
	inline void set_ReadStream_11(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___ReadStream_11 = value;
		Il2CppCodeGenWriteBarrier((&___ReadStream_11), value);
	}

	inline static int32_t get_offset_of_Progress_12() { return static_cast<int32_t>(offsetof(UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48, ___Progress_12)); }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * get_Progress_12() const { return ___Progress_12; }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 ** get_address_of_Progress_12() { return &___Progress_12; }
	inline void set_Progress_12(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * value)
	{
		___Progress_12 = value;
		Il2CppCodeGenWriteBarrier((&___Progress_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADBITSSTATE_TA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48_H
#ifndef WEBPROXY_T075305900B1D4D8BC8FAB08852842E42BC000417_H
#define WEBPROXY_T075305900B1D4D8BC8FAB08852842E42BC000417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxy
struct  WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417  : public RuntimeObject
{
public:
	// System.Boolean System.Net.WebProxy::_UseRegistry
	bool ____UseRegistry_0;
	// System.Boolean System.Net.WebProxy::_BypassOnLocal
	bool ____BypassOnLocal_1;
	// System.Boolean System.Net.WebProxy::m_EnableAutoproxy
	bool ___m_EnableAutoproxy_2;
	// System.Uri System.Net.WebProxy::_ProxyAddress
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ____ProxyAddress_3;
	// System.Collections.ArrayList System.Net.WebProxy::_BypassList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____BypassList_4;
	// System.Net.ICredentials System.Net.WebProxy::_Credentials
	RuntimeObject* ____Credentials_5;
	// System.Text.RegularExpressions.Regex[] System.Net.WebProxy::_RegExBypassList
	RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53* ____RegExBypassList_6;
	// System.Collections.Hashtable System.Net.WebProxy::_ProxyHostAddresses
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____ProxyHostAddresses_7;
	// System.Net.AutoWebProxyScriptEngine System.Net.WebProxy::m_ScriptEngine
	AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 * ___m_ScriptEngine_8;

public:
	inline static int32_t get_offset_of__UseRegistry_0() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____UseRegistry_0)); }
	inline bool get__UseRegistry_0() const { return ____UseRegistry_0; }
	inline bool* get_address_of__UseRegistry_0() { return &____UseRegistry_0; }
	inline void set__UseRegistry_0(bool value)
	{
		____UseRegistry_0 = value;
	}

	inline static int32_t get_offset_of__BypassOnLocal_1() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____BypassOnLocal_1)); }
	inline bool get__BypassOnLocal_1() const { return ____BypassOnLocal_1; }
	inline bool* get_address_of__BypassOnLocal_1() { return &____BypassOnLocal_1; }
	inline void set__BypassOnLocal_1(bool value)
	{
		____BypassOnLocal_1 = value;
	}

	inline static int32_t get_offset_of_m_EnableAutoproxy_2() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ___m_EnableAutoproxy_2)); }
	inline bool get_m_EnableAutoproxy_2() const { return ___m_EnableAutoproxy_2; }
	inline bool* get_address_of_m_EnableAutoproxy_2() { return &___m_EnableAutoproxy_2; }
	inline void set_m_EnableAutoproxy_2(bool value)
	{
		___m_EnableAutoproxy_2 = value;
	}

	inline static int32_t get_offset_of__ProxyAddress_3() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____ProxyAddress_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get__ProxyAddress_3() const { return ____ProxyAddress_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of__ProxyAddress_3() { return &____ProxyAddress_3; }
	inline void set__ProxyAddress_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		____ProxyAddress_3 = value;
		Il2CppCodeGenWriteBarrier((&____ProxyAddress_3), value);
	}

	inline static int32_t get_offset_of__BypassList_4() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____BypassList_4)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__BypassList_4() const { return ____BypassList_4; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__BypassList_4() { return &____BypassList_4; }
	inline void set__BypassList_4(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____BypassList_4 = value;
		Il2CppCodeGenWriteBarrier((&____BypassList_4), value);
	}

	inline static int32_t get_offset_of__Credentials_5() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____Credentials_5)); }
	inline RuntimeObject* get__Credentials_5() const { return ____Credentials_5; }
	inline RuntimeObject** get_address_of__Credentials_5() { return &____Credentials_5; }
	inline void set__Credentials_5(RuntimeObject* value)
	{
		____Credentials_5 = value;
		Il2CppCodeGenWriteBarrier((&____Credentials_5), value);
	}

	inline static int32_t get_offset_of__RegExBypassList_6() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____RegExBypassList_6)); }
	inline RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53* get__RegExBypassList_6() const { return ____RegExBypassList_6; }
	inline RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53** get_address_of__RegExBypassList_6() { return &____RegExBypassList_6; }
	inline void set__RegExBypassList_6(RegexU5BU5D_t9CA70F985DE1C94823B06BD0B2FCCC97927E6C53* value)
	{
		____RegExBypassList_6 = value;
		Il2CppCodeGenWriteBarrier((&____RegExBypassList_6), value);
	}

	inline static int32_t get_offset_of__ProxyHostAddresses_7() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ____ProxyHostAddresses_7)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__ProxyHostAddresses_7() const { return ____ProxyHostAddresses_7; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__ProxyHostAddresses_7() { return &____ProxyHostAddresses_7; }
	inline void set__ProxyHostAddresses_7(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____ProxyHostAddresses_7 = value;
		Il2CppCodeGenWriteBarrier((&____ProxyHostAddresses_7), value);
	}

	inline static int32_t get_offset_of_m_ScriptEngine_8() { return static_cast<int32_t>(offsetof(WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417, ___m_ScriptEngine_8)); }
	inline AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 * get_m_ScriptEngine_8() const { return ___m_ScriptEngine_8; }
	inline AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 ** get_address_of_m_ScriptEngine_8() { return &___m_ScriptEngine_8; }
	inline void set_m_ScriptEngine_8(AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455 * value)
	{
		___m_ScriptEngine_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScriptEngine_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXY_T075305900B1D4D8BC8FAB08852842E42BC000417_H
#ifndef WEBPROXYDATA_T12D22F3302B5ED2B04EA88CE36650BB097E46922_H
#define WEBPROXYDATA_T12D22F3302B5ED2B04EA88CE36650BB097E46922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebProxyData
struct  WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922  : public RuntimeObject
{
public:
	// System.Boolean System.Net.WebProxyData::bypassOnLocal
	bool ___bypassOnLocal_0;
	// System.Boolean System.Net.WebProxyData::automaticallyDetectSettings
	bool ___automaticallyDetectSettings_1;
	// System.Uri System.Net.WebProxyData::proxyAddress
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___proxyAddress_2;
	// System.Collections.Hashtable System.Net.WebProxyData::proxyHostAddresses
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___proxyHostAddresses_3;
	// System.Uri System.Net.WebProxyData::scriptLocation
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___scriptLocation_4;
	// System.Collections.ArrayList System.Net.WebProxyData::bypassList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___bypassList_5;

public:
	inline static int32_t get_offset_of_bypassOnLocal_0() { return static_cast<int32_t>(offsetof(WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922, ___bypassOnLocal_0)); }
	inline bool get_bypassOnLocal_0() const { return ___bypassOnLocal_0; }
	inline bool* get_address_of_bypassOnLocal_0() { return &___bypassOnLocal_0; }
	inline void set_bypassOnLocal_0(bool value)
	{
		___bypassOnLocal_0 = value;
	}

	inline static int32_t get_offset_of_automaticallyDetectSettings_1() { return static_cast<int32_t>(offsetof(WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922, ___automaticallyDetectSettings_1)); }
	inline bool get_automaticallyDetectSettings_1() const { return ___automaticallyDetectSettings_1; }
	inline bool* get_address_of_automaticallyDetectSettings_1() { return &___automaticallyDetectSettings_1; }
	inline void set_automaticallyDetectSettings_1(bool value)
	{
		___automaticallyDetectSettings_1 = value;
	}

	inline static int32_t get_offset_of_proxyAddress_2() { return static_cast<int32_t>(offsetof(WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922, ___proxyAddress_2)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_proxyAddress_2() const { return ___proxyAddress_2; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_proxyAddress_2() { return &___proxyAddress_2; }
	inline void set_proxyAddress_2(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___proxyAddress_2 = value;
		Il2CppCodeGenWriteBarrier((&___proxyAddress_2), value);
	}

	inline static int32_t get_offset_of_proxyHostAddresses_3() { return static_cast<int32_t>(offsetof(WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922, ___proxyHostAddresses_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_proxyHostAddresses_3() const { return ___proxyHostAddresses_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_proxyHostAddresses_3() { return &___proxyHostAddresses_3; }
	inline void set_proxyHostAddresses_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___proxyHostAddresses_3 = value;
		Il2CppCodeGenWriteBarrier((&___proxyHostAddresses_3), value);
	}

	inline static int32_t get_offset_of_scriptLocation_4() { return static_cast<int32_t>(offsetof(WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922, ___scriptLocation_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_scriptLocation_4() const { return ___scriptLocation_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_scriptLocation_4() { return &___scriptLocation_4; }
	inline void set_scriptLocation_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___scriptLocation_4 = value;
		Il2CppCodeGenWriteBarrier((&___scriptLocation_4), value);
	}

	inline static int32_t get_offset_of_bypassList_5() { return static_cast<int32_t>(offsetof(WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922, ___bypassList_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_bypassList_5() const { return ___bypassList_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_bypassList_5() { return &___bypassList_5; }
	inline void set_bypassList_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___bypassList_5 = value;
		Il2CppCodeGenWriteBarrier((&___bypassList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYDATA_T12D22F3302B5ED2B04EA88CE36650BB097E46922_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#define ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AsyncCompletedEventArgs
struct  AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Exception System.ComponentModel.AsyncCompletedEventArgs::error
	Exception_t * ___error_1;
	// System.Boolean System.ComponentModel.AsyncCompletedEventArgs::cancelled
	bool ___cancelled_2;
	// System.Object System.ComponentModel.AsyncCompletedEventArgs::userState
	RuntimeObject * ___userState_3;

public:
	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___error_1)); }
	inline Exception_t * get_error_1() const { return ___error_1; }
	inline Exception_t ** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(Exception_t * value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier((&___error_1), value);
	}

	inline static int32_t get_offset_of_cancelled_2() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___cancelled_2)); }
	inline bool get_cancelled_2() const { return ___cancelled_2; }
	inline bool* get_address_of_cancelled_2() { return &___cancelled_2; }
	inline void set_cancelled_2(bool value)
	{
		___cancelled_2 = value;
	}

	inline static int32_t get_offset_of_userState_3() { return static_cast<int32_t>(offsetof(AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7, ___userState_3)); }
	inline RuntimeObject * get_userState_3() const { return ___userState_3; }
	inline RuntimeObject ** get_address_of_userState_3() { return &___userState_3; }
	inline void set_userState_3(RuntimeObject * value)
	{
		___userState_3 = value;
		Il2CppCodeGenWriteBarrier((&___userState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCOMPLETEDEVENTARGS_TADCBE47368F4D8B198B46D332B27EE38C8B5F8B7_H
#ifndef COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#define COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.ComponentModel.ISite System.ComponentModel.Component::site
	RuntimeObject* ___site_2;
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::events
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * ___events_3;

public:
	inline static int32_t get_offset_of_site_2() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473, ___site_2)); }
	inline RuntimeObject* get_site_2() const { return ___site_2; }
	inline RuntimeObject** get_address_of_site_2() { return &___site_2; }
	inline void set_site_2(RuntimeObject* value)
	{
		___site_2 = value;
		Il2CppCodeGenWriteBarrier((&___site_2), value);
	}

	inline static int32_t get_offset_of_events_3() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473, ___events_3)); }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * get_events_3() const { return ___events_3; }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 ** get_address_of_events_3() { return &___events_3; }
	inline void set_events_3(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * value)
	{
		___events_3 = value;
		Il2CppCodeGenWriteBarrier((&___events_3), value);
	}
};

struct Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields
{
public:
	// System.Object System.ComponentModel.Component::EventDisposed
	RuntimeObject * ___EventDisposed_1;

public:
	inline static int32_t get_offset_of_EventDisposed_1() { return static_cast<int32_t>(offsetof(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473_StaticFields, ___EventDisposed_1)); }
	inline RuntimeObject * get_EventDisposed_1() const { return ___EventDisposed_1; }
	inline RuntimeObject ** get_address_of_EventDisposed_1() { return &___EventDisposed_1; }
	inline void set_EventDisposed_1(RuntimeObject * value)
	{
		___EventDisposed_1 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T7AEFE153F6778CF52E1981BC3E811A9604B29473_H
#ifndef PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#define PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ProgressChangedEventArgs
struct  ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Int32 System.ComponentModel.ProgressChangedEventArgs::progressPercentage
	int32_t ___progressPercentage_1;
	// System.Object System.ComponentModel.ProgressChangedEventArgs::userState
	RuntimeObject * ___userState_2;

public:
	inline static int32_t get_offset_of_progressPercentage_1() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F, ___progressPercentage_1)); }
	inline int32_t get_progressPercentage_1() const { return ___progressPercentage_1; }
	inline int32_t* get_address_of_progressPercentage_1() { return &___progressPercentage_1; }
	inline void set_progressPercentage_1(int32_t value)
	{
		___progressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_userState_2() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F, ___userState_2)); }
	inline RuntimeObject * get_userState_2() const { return ___userState_2; }
	inline RuntimeObject ** get_address_of_userState_2() { return &___userState_2; }
	inline void set_userState_2(RuntimeObject * value)
	{
		___userState_2 = value;
		Il2CppCodeGenWriteBarrier((&___userState_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef READASYNCRESULT_T7E8F5D3534825385A0F0386E23DCCB4F81D16463_H
#define READASYNCRESULT_T7E8F5D3534825385A0F0386E23DCCB4F81D16463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Base64Stream_ReadAsyncResult
struct  ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463  : public LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3
{
public:
	// System.Net.Base64Stream System.Net.Base64Stream_ReadAsyncResult::parent
	Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 * ___parent_12;
	// System.Byte[] System.Net.Base64Stream_ReadAsyncResult::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_13;
	// System.Int32 System.Net.Base64Stream_ReadAsyncResult::offset
	int32_t ___offset_14;
	// System.Int32 System.Net.Base64Stream_ReadAsyncResult::count
	int32_t ___count_15;
	// System.Int32 System.Net.Base64Stream_ReadAsyncResult::read
	int32_t ___read_16;

public:
	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463, ___parent_12)); }
	inline Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 * get_parent_12() const { return ___parent_12; }
	inline Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier((&___parent_12), value);
	}

	inline static int32_t get_offset_of_buffer_13() { return static_cast<int32_t>(offsetof(ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463, ___buffer_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_13() const { return ___buffer_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_13() { return &___buffer_13; }
	inline void set_buffer_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_13 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_13), value);
	}

	inline static int32_t get_offset_of_offset_14() { return static_cast<int32_t>(offsetof(ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463, ___offset_14)); }
	inline int32_t get_offset_14() const { return ___offset_14; }
	inline int32_t* get_address_of_offset_14() { return &___offset_14; }
	inline void set_offset_14(int32_t value)
	{
		___offset_14 = value;
	}

	inline static int32_t get_offset_of_count_15() { return static_cast<int32_t>(offsetof(ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463, ___count_15)); }
	inline int32_t get_count_15() const { return ___count_15; }
	inline int32_t* get_address_of_count_15() { return &___count_15; }
	inline void set_count_15(int32_t value)
	{
		___count_15 = value;
	}

	inline static int32_t get_offset_of_read_16() { return static_cast<int32_t>(offsetof(ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463, ___read_16)); }
	inline int32_t get_read_16() const { return ___read_16; }
	inline int32_t* get_address_of_read_16() { return &___read_16; }
	inline void set_read_16(int32_t value)
	{
		___read_16 = value;
	}
};

struct ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463_StaticFields
{
public:
	// System.AsyncCallback System.Net.Base64Stream_ReadAsyncResult::onRead
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___onRead_17;

public:
	inline static int32_t get_offset_of_onRead_17() { return static_cast<int32_t>(offsetof(ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463_StaticFields, ___onRead_17)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_onRead_17() const { return ___onRead_17; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_onRead_17() { return &___onRead_17; }
	inline void set_onRead_17(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___onRead_17 = value;
		Il2CppCodeGenWriteBarrier((&___onRead_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READASYNCRESULT_T7E8F5D3534825385A0F0386E23DCCB4F81D16463_H
#ifndef WRITEASYNCRESULT_T797D3F470103C5836D4BE6B14D6CD4EA6797E6CB_H
#define WRITEASYNCRESULT_T797D3F470103C5836D4BE6B14D6CD4EA6797E6CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Base64Stream_WriteAsyncResult
struct  WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB  : public LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3
{
public:
	// System.Net.Base64Stream System.Net.Base64Stream_WriteAsyncResult::parent
	Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 * ___parent_12;
	// System.Byte[] System.Net.Base64Stream_WriteAsyncResult::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_13;
	// System.Int32 System.Net.Base64Stream_WriteAsyncResult::offset
	int32_t ___offset_14;
	// System.Int32 System.Net.Base64Stream_WriteAsyncResult::count
	int32_t ___count_15;
	// System.Int32 System.Net.Base64Stream_WriteAsyncResult::written
	int32_t ___written_17;

public:
	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB, ___parent_12)); }
	inline Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 * get_parent_12() const { return ___parent_12; }
	inline Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier((&___parent_12), value);
	}

	inline static int32_t get_offset_of_buffer_13() { return static_cast<int32_t>(offsetof(WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB, ___buffer_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_13() const { return ___buffer_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_13() { return &___buffer_13; }
	inline void set_buffer_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_13 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_13), value);
	}

	inline static int32_t get_offset_of_offset_14() { return static_cast<int32_t>(offsetof(WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB, ___offset_14)); }
	inline int32_t get_offset_14() const { return ___offset_14; }
	inline int32_t* get_address_of_offset_14() { return &___offset_14; }
	inline void set_offset_14(int32_t value)
	{
		___offset_14 = value;
	}

	inline static int32_t get_offset_of_count_15() { return static_cast<int32_t>(offsetof(WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB, ___count_15)); }
	inline int32_t get_count_15() const { return ___count_15; }
	inline int32_t* get_address_of_count_15() { return &___count_15; }
	inline void set_count_15(int32_t value)
	{
		___count_15 = value;
	}

	inline static int32_t get_offset_of_written_17() { return static_cast<int32_t>(offsetof(WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB, ___written_17)); }
	inline int32_t get_written_17() const { return ___written_17; }
	inline int32_t* get_address_of_written_17() { return &___written_17; }
	inline void set_written_17(int32_t value)
	{
		___written_17 = value;
	}
};

struct WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB_StaticFields
{
public:
	// System.AsyncCallback System.Net.Base64Stream_WriteAsyncResult::onWrite
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___onWrite_16;

public:
	inline static int32_t get_offset_of_onWrite_16() { return static_cast<int32_t>(offsetof(WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB_StaticFields, ___onWrite_16)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_onWrite_16() const { return ___onWrite_16; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_onWrite_16() { return &___onWrite_16; }
	inline void set_onWrite_16(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___onWrite_16 = value;
		Il2CppCodeGenWriteBarrier((&___onWrite_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEASYNCRESULT_T797D3F470103C5836D4BE6B14D6CD4EA6797E6CB_H
#ifndef TRACKINGSTRINGDICTIONARY_T053716745947C37C70895827847C4F194773910E_H
#define TRACKINGSTRINGDICTIONARY_T053716745947C37C70895827847C4F194773910E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TrackingStringDictionary
struct  TrackingStringDictionary_t053716745947C37C70895827847C4F194773910E  : public StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF
{
public:
	// System.Boolean System.Net.TrackingStringDictionary::isChanged
	bool ___isChanged_1;
	// System.Boolean System.Net.TrackingStringDictionary::isReadOnly
	bool ___isReadOnly_2;

public:
	inline static int32_t get_offset_of_isChanged_1() { return static_cast<int32_t>(offsetof(TrackingStringDictionary_t053716745947C37C70895827847C4F194773910E, ___isChanged_1)); }
	inline bool get_isChanged_1() const { return ___isChanged_1; }
	inline bool* get_address_of_isChanged_1() { return &___isChanged_1; }
	inline void set_isChanged_1(bool value)
	{
		___isChanged_1 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_2() { return static_cast<int32_t>(offsetof(TrackingStringDictionary_t053716745947C37C70895827847C4F194773910E, ___isReadOnly_2)); }
	inline bool get_isReadOnly_2() const { return ___isReadOnly_2; }
	inline bool* get_address_of_isReadOnly_2() { return &___isReadOnly_2; }
	inline void set_isReadOnly_2(bool value)
	{
		___isReadOnly_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTRINGDICTIONARY_T053716745947C37C70895827847C4F194773910E_H
#ifndef TRACKINGVALIDATIONOBJECTDICTIONARY_T8B007338BA6B5743538A5FA2F9A187DD52FA9E1C_H
#define TRACKINGVALIDATIONOBJECTDICTIONARY_T8B007338BA6B5743538A5FA2F9A187DD52FA9E1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TrackingValidationObjectDictionary
struct  TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C  : public StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> System.Net.TrackingValidationObjectDictionary::internalObjects
	RuntimeObject* ___internalObjects_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.Net.TrackingValidationObjectDictionary_ValidateAndParseValue> System.Net.TrackingValidationObjectDictionary::validators
	RuntimeObject* ___validators_2;
	// System.Boolean System.Net.TrackingValidationObjectDictionary::<IsChanged>k__BackingField
	bool ___U3CIsChangedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_internalObjects_1() { return static_cast<int32_t>(offsetof(TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C, ___internalObjects_1)); }
	inline RuntimeObject* get_internalObjects_1() const { return ___internalObjects_1; }
	inline RuntimeObject** get_address_of_internalObjects_1() { return &___internalObjects_1; }
	inline void set_internalObjects_1(RuntimeObject* value)
	{
		___internalObjects_1 = value;
		Il2CppCodeGenWriteBarrier((&___internalObjects_1), value);
	}

	inline static int32_t get_offset_of_validators_2() { return static_cast<int32_t>(offsetof(TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C, ___validators_2)); }
	inline RuntimeObject* get_validators_2() const { return ___validators_2; }
	inline RuntimeObject** get_address_of_validators_2() { return &___validators_2; }
	inline void set_validators_2(RuntimeObject* value)
	{
		___validators_2 = value;
		Il2CppCodeGenWriteBarrier((&___validators_2), value);
	}

	inline static int32_t get_offset_of_U3CIsChangedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C, ___U3CIsChangedU3Ek__BackingField_3)); }
	inline bool get_U3CIsChangedU3Ek__BackingField_3() const { return ___U3CIsChangedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsChangedU3Ek__BackingField_3() { return &___U3CIsChangedU3Ek__BackingField_3; }
	inline void set_U3CIsChangedU3Ek__BackingField_3(bool value)
	{
		___U3CIsChangedU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGVALIDATIONOBJECTDICTIONARY_T8B007338BA6B5743538A5FA2F9A187DD52FA9E1C_H
#ifndef WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#define WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebResponse
struct  WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Boolean System.Net.WebResponse::m_IsCacheFresh
	bool ___m_IsCacheFresh_1;
	// System.Boolean System.Net.WebResponse::m_IsFromCache
	bool ___m_IsFromCache_2;

public:
	inline static int32_t get_offset_of_m_IsCacheFresh_1() { return static_cast<int32_t>(offsetof(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD, ___m_IsCacheFresh_1)); }
	inline bool get_m_IsCacheFresh_1() const { return ___m_IsCacheFresh_1; }
	inline bool* get_address_of_m_IsCacheFresh_1() { return &___m_IsCacheFresh_1; }
	inline void set_m_IsCacheFresh_1(bool value)
	{
		___m_IsCacheFresh_1 = value;
	}

	inline static int32_t get_offset_of_m_IsFromCache_2() { return static_cast<int32_t>(offsetof(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD, ___m_IsFromCache_2)); }
	inline bool get_m_IsFromCache_2() const { return ___m_IsFromCache_2; }
	inline bool* get_address_of_m_IsFromCache_2() { return &___m_IsFromCache_2; }
	inline void set_m_IsFromCache_2(bool value)
	{
		___m_IsFromCache_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBRESPONSE_T5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#define FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t2808E076CDE4650AF89F55FD78F49290D0EC5BDC  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#ifndef FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#define FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6_H
#ifndef STATE_T91B9B70A7FEC6854AA0B62E466EEC54C6F7983E1_H
#define STATE_T91B9B70A7FEC6854AA0B62E466EEC54C6F7983E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ConnectionPool_State
struct  State_t91B9B70A7FEC6854AA0B62E466EEC54C6F7983E1 
{
public:
	// System.Int32 System.Net.ConnectionPool_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t91B9B70A7FEC6854AA0B62E466EEC54C6F7983E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T91B9B70A7FEC6854AA0B62E466EEC54C6F7983E1_H
#ifndef COOKIECOLLECTION_T69ADF0ABD99419E54AB4740B341D94F443D995A3_H
#define COOKIECOLLECTION_T69ADF0ABD99419E54AB4740B341D94F443D995A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection
struct  CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3  : public RuntimeObject
{
public:
	// System.Int32 System.Net.CookieCollection::m_version
	int32_t ___m_version_0;
	// System.Collections.ArrayList System.Net.CookieCollection::m_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_list_1;
	// System.DateTime System.Net.CookieCollection::m_TimeStamp
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_TimeStamp_2;
	// System.Boolean System.Net.CookieCollection::m_has_other_versions
	bool ___m_has_other_versions_3;
	// System.Boolean System.Net.CookieCollection::m_IsReadOnly
	bool ___m_IsReadOnly_4;

public:
	inline static int32_t get_offset_of_m_version_0() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_version_0)); }
	inline int32_t get_m_version_0() const { return ___m_version_0; }
	inline int32_t* get_address_of_m_version_0() { return &___m_version_0; }
	inline void set_m_version_0(int32_t value)
	{
		___m_version_0 = value;
	}

	inline static int32_t get_offset_of_m_list_1() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_list_1)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_list_1() const { return ___m_list_1; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_list_1() { return &___m_list_1; }
	inline void set_m_list_1(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_list_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_list_1), value);
	}

	inline static int32_t get_offset_of_m_TimeStamp_2() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_TimeStamp_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_TimeStamp_2() const { return ___m_TimeStamp_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_TimeStamp_2() { return &___m_TimeStamp_2; }
	inline void set_m_TimeStamp_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_TimeStamp_2 = value;
	}

	inline static int32_t get_offset_of_m_has_other_versions_3() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_has_other_versions_3)); }
	inline bool get_m_has_other_versions_3() const { return ___m_has_other_versions_3; }
	inline bool* get_address_of_m_has_other_versions_3() { return &___m_has_other_versions_3; }
	inline void set_m_has_other_versions_3(bool value)
	{
		___m_has_other_versions_3 = value;
	}

	inline static int32_t get_offset_of_m_IsReadOnly_4() { return static_cast<int32_t>(offsetof(CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3, ___m_IsReadOnly_4)); }
	inline bool get_m_IsReadOnly_4() const { return ___m_IsReadOnly_4; }
	inline bool* get_address_of_m_IsReadOnly_4() { return &___m_IsReadOnly_4; }
	inline void set_m_IsReadOnly_4(bool value)
	{
		___m_IsReadOnly_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTION_T69ADF0ABD99419E54AB4740B341D94F443D995A3_H
#ifndef STAMP_T06B0F70FA36D78E86543007609E79740E8BB87BE_H
#define STAMP_T06B0F70FA36D78E86543007609E79740E8BB87BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection_Stamp
struct  Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE 
{
public:
	// System.Int32 System.Net.CookieCollection_Stamp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAMP_T06B0F70FA36D78E86543007609E79740E8BB87BE_H
#ifndef COOKIETOKEN_TB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8_H
#define COOKIETOKEN_TB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieToken
struct  CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8 
{
public:
	// System.Int32 System.Net.CookieToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIETOKEN_TB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8_H
#ifndef COOKIEVARIANT_T896D38AC6FBE01ADFB532B04C5E0E19842256CFC_H
#define COOKIEVARIANT_T896D38AC6FBE01ADFB532B04C5E0E19842256CFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieVariant
struct  CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC 
{
public:
	// System.Int32 System.Net.CookieVariant::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEVARIANT_T896D38AC6FBE01ADFB532B04C5E0E19842256CFC_H
#ifndef DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#define DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DecompressionMethods
struct  DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D 
{
public:
	// System.Int32 System.Net.DecompressionMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifndef DELEGATEDSTREAM_TA1FDA59D05B04F64B575B9D3AC6639D91917BBBC_H
#define DELEGATEDSTREAM_TA1FDA59D05B04F64B575B9D3AC6639D91917BBBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DelegatedStream
struct  DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream System.Net.DelegatedStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Net.Sockets.NetworkStream System.Net.DelegatedStream::netStream
	NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * ___netStream_6;

public:
	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_netStream_6() { return static_cast<int32_t>(offsetof(DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC, ___netStream_6)); }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * get_netStream_6() const { return ___netStream_6; }
	inline NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA ** get_address_of_netStream_6() { return &___netStream_6; }
	inline void set_netStream_6(NetworkStream_t362D0CD0C74C2F5CBD02905C9422E4240872ADCA * value)
	{
		___netStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___netStream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEDSTREAM_TA1FDA59D05B04F64B575B9D3AC6639D91917BBBC_H
#ifndef DIGESTSESSION_TCFF843AD732355E50F57213B29E91E9016A17627_H
#define DIGESTSESSION_TCFF843AD732355E50F57213B29E91E9016A17627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DigestSession
struct  DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627  : public RuntimeObject
{
public:
	// System.DateTime System.Net.DigestSession::lastUse
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastUse_1;
	// System.Int32 System.Net.DigestSession::_nc
	int32_t ____nc_2;
	// System.Security.Cryptography.HashAlgorithm System.Net.DigestSession::hash
	HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * ___hash_3;
	// System.Net.DigestHeaderParser System.Net.DigestSession::parser
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 * ___parser_4;
	// System.String System.Net.DigestSession::_cnonce
	String_t* ____cnonce_5;

public:
	inline static int32_t get_offset_of_lastUse_1() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ___lastUse_1)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastUse_1() const { return ___lastUse_1; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastUse_1() { return &___lastUse_1; }
	inline void set_lastUse_1(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastUse_1 = value;
	}

	inline static int32_t get_offset_of__nc_2() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ____nc_2)); }
	inline int32_t get__nc_2() const { return ____nc_2; }
	inline int32_t* get_address_of__nc_2() { return &____nc_2; }
	inline void set__nc_2(int32_t value)
	{
		____nc_2 = value;
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ___hash_3)); }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * get_hash_3() const { return ___hash_3; }
	inline HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA ** get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(HashAlgorithm_t65659695B16C0BBF05707BF45191A97DC156D6BA * value)
	{
		___hash_3 = value;
		Il2CppCodeGenWriteBarrier((&___hash_3), value);
	}

	inline static int32_t get_offset_of_parser_4() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ___parser_4)); }
	inline DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 * get_parser_4() const { return ___parser_4; }
	inline DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 ** get_address_of_parser_4() { return &___parser_4; }
	inline void set_parser_4(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9 * value)
	{
		___parser_4 = value;
		Il2CppCodeGenWriteBarrier((&___parser_4), value);
	}

	inline static int32_t get_offset_of__cnonce_5() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627, ____cnonce_5)); }
	inline String_t* get__cnonce_5() const { return ____cnonce_5; }
	inline String_t** get_address_of__cnonce_5() { return &____cnonce_5; }
	inline void set__cnonce_5(String_t* value)
	{
		____cnonce_5 = value;
		Il2CppCodeGenWriteBarrier((&____cnonce_5), value);
	}
};

struct DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator System.Net.DigestSession::rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTSESSION_TCFF843AD732355E50F57213B29E91E9016A17627_H
#ifndef DOWNLOADDATACOMPLETEDEVENTARGS_T5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C_H
#define DOWNLOADDATACOMPLETEDEVENTARGS_T5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadDataCompletedEventArgs
struct  DownloadDataCompletedEventArgs_t5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.Byte[] System.Net.DownloadDataCompletedEventArgs::m_Result
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(DownloadDataCompletedEventArgs_t5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C, ___m_Result_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Result_4() const { return ___m_Result_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADDATACOMPLETEDEVENTARGS_T5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C_H
#ifndef DOWNLOADPROGRESSCHANGEDEVENTARGS_TACD8549B8D84C4FBA17C2D19FA87E966CCCD4522_H
#define DOWNLOADPROGRESSCHANGEDEVENTARGS_TACD8549B8D84C4FBA17C2D19FA87E966CCCD4522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadProgressChangedEventArgs
struct  DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522  : public ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F
{
public:
	// System.Int64 System.Net.DownloadProgressChangedEventArgs::m_BytesReceived
	int64_t ___m_BytesReceived_3;
	// System.Int64 System.Net.DownloadProgressChangedEventArgs::m_TotalBytesToReceive
	int64_t ___m_TotalBytesToReceive_4;

public:
	inline static int32_t get_offset_of_m_BytesReceived_3() { return static_cast<int32_t>(offsetof(DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522, ___m_BytesReceived_3)); }
	inline int64_t get_m_BytesReceived_3() const { return ___m_BytesReceived_3; }
	inline int64_t* get_address_of_m_BytesReceived_3() { return &___m_BytesReceived_3; }
	inline void set_m_BytesReceived_3(int64_t value)
	{
		___m_BytesReceived_3 = value;
	}

	inline static int32_t get_offset_of_m_TotalBytesToReceive_4() { return static_cast<int32_t>(offsetof(DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522, ___m_TotalBytesToReceive_4)); }
	inline int64_t get_m_TotalBytesToReceive_4() const { return ___m_TotalBytesToReceive_4; }
	inline int64_t* get_address_of_m_TotalBytesToReceive_4() { return &___m_TotalBytesToReceive_4; }
	inline void set_m_TotalBytesToReceive_4(int64_t value)
	{
		___m_TotalBytesToReceive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADPROGRESSCHANGEDEVENTARGS_TACD8549B8D84C4FBA17C2D19FA87E966CCCD4522_H
#ifndef DOWNLOADSTRINGCOMPLETEDEVENTARGS_T9517D2DD80B7233622CBD41E60D06F02E5F327AE_H
#define DOWNLOADSTRINGCOMPLETEDEVENTARGS_T9517D2DD80B7233622CBD41E60D06F02E5F327AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadStringCompletedEventArgs
struct  DownloadStringCompletedEventArgs_t9517D2DD80B7233622CBD41E60D06F02E5F327AE  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.String System.Net.DownloadStringCompletedEventArgs::m_Result
	String_t* ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(DownloadStringCompletedEventArgs_t9517D2DD80B7233622CBD41E60D06F02E5F327AE, ___m_Result_4)); }
	inline String_t* get_m_Result_4() const { return ___m_Result_4; }
	inline String_t** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(String_t* value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADSTRINGCOMPLETEDEVENTARGS_T9517D2DD80B7233622CBD41E60D06F02E5F327AE_H
#ifndef OPENREADCOMPLETEDEVENTARGS_TBC33F1F08441BE06D37C9F2DBE7BDA702DB86393_H
#define OPENREADCOMPLETEDEVENTARGS_TBC33F1F08441BE06D37C9F2DBE7BDA702DB86393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.OpenReadCompletedEventArgs
struct  OpenReadCompletedEventArgs_tBC33F1F08441BE06D37C9F2DBE7BDA702DB86393  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.IO.Stream System.Net.OpenReadCompletedEventArgs::m_Result
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(OpenReadCompletedEventArgs_tBC33F1F08441BE06D37C9F2DBE7BDA702DB86393, ___m_Result_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_Result_4() const { return ___m_Result_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENREADCOMPLETEDEVENTARGS_TBC33F1F08441BE06D37C9F2DBE7BDA702DB86393_H
#ifndef OPENWRITECOMPLETEDEVENTARGS_T46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3_H
#define OPENWRITECOMPLETEDEVENTARGS_T46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.OpenWriteCompletedEventArgs
struct  OpenWriteCompletedEventArgs_t46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.IO.Stream System.Net.OpenWriteCompletedEventArgs::m_Result
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(OpenWriteCompletedEventArgs_t46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3, ___m_Result_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_Result_4() const { return ___m_Result_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENWRITECOMPLETEDEVENTARGS_T46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3_H
#ifndef REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#define REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.RequestStream
struct  RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.Net.RequestStream::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_5;
	// System.Int32 System.Net.RequestStream::offset
	int32_t ___offset_6;
	// System.Int32 System.Net.RequestStream::length
	int32_t ___length_7;
	// System.Int64 System.Net.RequestStream::remaining_body
	int64_t ___remaining_body_8;
	// System.Boolean System.Net.RequestStream::disposed
	bool ___disposed_9;
	// System.IO.Stream System.Net.RequestStream::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_10;

public:
	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_offset_6() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___offset_6)); }
	inline int32_t get_offset_6() const { return ___offset_6; }
	inline int32_t* get_address_of_offset_6() { return &___offset_6; }
	inline void set_offset_6(int32_t value)
	{
		___offset_6 = value;
	}

	inline static int32_t get_offset_of_length_7() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___length_7)); }
	inline int32_t get_length_7() const { return ___length_7; }
	inline int32_t* get_address_of_length_7() { return &___length_7; }
	inline void set_length_7(int32_t value)
	{
		___length_7 = value;
	}

	inline static int32_t get_offset_of_remaining_body_8() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___remaining_body_8)); }
	inline int64_t get_remaining_body_8() const { return ___remaining_body_8; }
	inline int64_t* get_address_of_remaining_body_8() { return &___remaining_body_8; }
	inline void set_remaining_body_8(int64_t value)
	{
		___remaining_body_8 = value;
	}

	inline static int32_t get_offset_of_disposed_9() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___disposed_9)); }
	inline bool get_disposed_9() const { return ___disposed_9; }
	inline bool* get_address_of_disposed_9() { return &___disposed_9; }
	inline void set_disposed_9(bool value)
	{
		___disposed_9 = value;
	}

	inline static int32_t get_offset_of_stream_10() { return static_cast<int32_t>(offsetof(RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35, ___stream_10)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_10() const { return ___stream_10; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_10() { return &___stream_10; }
	inline void set_stream_10(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_10 = value;
		Il2CppCodeGenWriteBarrier((&___stream_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T4BF6FD18AB7261094F557E867606B91FECF74D35_H
#ifndef AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#define AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.AuthenticationLevel
struct  AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B 
{
public:
	// System.Int32 System.Net.Security.AuthenticationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthenticationLevel_tC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONLEVEL_TC0FE8B3A1A9C4F39798DD6F6C024078BB137F52B_H
#ifndef SSLPOLICYERRORS_TD39D8AA1FDBFBC6745122C5A899F10A1C9258671_H
#define SSLPOLICYERRORS_TD39D8AA1FDBFBC6745122C5A899F10A1C9258671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.SslPolicyErrors
struct  SslPolicyErrors_tD39D8AA1FDBFBC6745122C5A899F10A1C9258671 
{
public:
	// System.Int32 System.Net.Security.SslPolicyErrors::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SslPolicyErrors_tD39D8AA1FDBFBC6745122C5A899F10A1C9258671, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPOLICYERRORS_TD39D8AA1FDBFBC6745122C5A899F10A1C9258671_H
#ifndef ENUM_T0563A3CEB7BF8F17212F91D1AF0DF4EB1BE13918_H
#define ENUM_T0563A3CEB7BF8F17212F91D1AF0DF4EB1BE13918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UnsafeNclNativeMethods_HttpApi_Enum
struct  Enum_t0563A3CEB7BF8F17212F91D1AF0DF4EB1BE13918 
{
public:
	// System.Int32 System.Net.UnsafeNclNativeMethods_HttpApi_Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Enum_t0563A3CEB7BF8F17212F91D1AF0DF4EB1BE13918, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUM_T0563A3CEB7BF8F17212F91D1AF0DF4EB1BE13918_H
#ifndef UPLOADDATACOMPLETEDEVENTARGS_T17D573D2463C12EF95555B0CA0135B4FD71A920D_H
#define UPLOADDATACOMPLETEDEVENTARGS_T17D573D2463C12EF95555B0CA0135B4FD71A920D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadDataCompletedEventArgs
struct  UploadDataCompletedEventArgs_t17D573D2463C12EF95555B0CA0135B4FD71A920D  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.Byte[] System.Net.UploadDataCompletedEventArgs::m_Result
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(UploadDataCompletedEventArgs_t17D573D2463C12EF95555B0CA0135B4FD71A920D, ___m_Result_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Result_4() const { return ___m_Result_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADDATACOMPLETEDEVENTARGS_T17D573D2463C12EF95555B0CA0135B4FD71A920D_H
#ifndef UPLOADFILECOMPLETEDEVENTARGS_T85C3133630FCC3D7DD1803046F61F5C3F21F6D0A_H
#define UPLOADFILECOMPLETEDEVENTARGS_T85C3133630FCC3D7DD1803046F61F5C3F21F6D0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadFileCompletedEventArgs
struct  UploadFileCompletedEventArgs_t85C3133630FCC3D7DD1803046F61F5C3F21F6D0A  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.Byte[] System.Net.UploadFileCompletedEventArgs::m_Result
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(UploadFileCompletedEventArgs_t85C3133630FCC3D7DD1803046F61F5C3F21F6D0A, ___m_Result_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Result_4() const { return ___m_Result_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADFILECOMPLETEDEVENTARGS_T85C3133630FCC3D7DD1803046F61F5C3F21F6D0A_H
#ifndef UPLOADPROGRESSCHANGEDEVENTARGS_TE606124E632B46CA14E4ED59011DBE3203BE975E_H
#define UPLOADPROGRESSCHANGEDEVENTARGS_TE606124E632B46CA14E4ED59011DBE3203BE975E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadProgressChangedEventArgs
struct  UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E  : public ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F
{
public:
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_BytesReceived
	int64_t ___m_BytesReceived_3;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_TotalBytesToReceive
	int64_t ___m_TotalBytesToReceive_4;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_BytesSent
	int64_t ___m_BytesSent_5;
	// System.Int64 System.Net.UploadProgressChangedEventArgs::m_TotalBytesToSend
	int64_t ___m_TotalBytesToSend_6;

public:
	inline static int32_t get_offset_of_m_BytesReceived_3() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_BytesReceived_3)); }
	inline int64_t get_m_BytesReceived_3() const { return ___m_BytesReceived_3; }
	inline int64_t* get_address_of_m_BytesReceived_3() { return &___m_BytesReceived_3; }
	inline void set_m_BytesReceived_3(int64_t value)
	{
		___m_BytesReceived_3 = value;
	}

	inline static int32_t get_offset_of_m_TotalBytesToReceive_4() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_TotalBytesToReceive_4)); }
	inline int64_t get_m_TotalBytesToReceive_4() const { return ___m_TotalBytesToReceive_4; }
	inline int64_t* get_address_of_m_TotalBytesToReceive_4() { return &___m_TotalBytesToReceive_4; }
	inline void set_m_TotalBytesToReceive_4(int64_t value)
	{
		___m_TotalBytesToReceive_4 = value;
	}

	inline static int32_t get_offset_of_m_BytesSent_5() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_BytesSent_5)); }
	inline int64_t get_m_BytesSent_5() const { return ___m_BytesSent_5; }
	inline int64_t* get_address_of_m_BytesSent_5() { return &___m_BytesSent_5; }
	inline void set_m_BytesSent_5(int64_t value)
	{
		___m_BytesSent_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalBytesToSend_6() { return static_cast<int32_t>(offsetof(UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E, ___m_TotalBytesToSend_6)); }
	inline int64_t get_m_TotalBytesToSend_6() const { return ___m_TotalBytesToSend_6; }
	inline int64_t* get_address_of_m_TotalBytesToSend_6() { return &___m_TotalBytesToSend_6; }
	inline void set_m_TotalBytesToSend_6(int64_t value)
	{
		___m_TotalBytesToSend_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADPROGRESSCHANGEDEVENTARGS_TE606124E632B46CA14E4ED59011DBE3203BE975E_H
#ifndef UPLOADSTRINGCOMPLETEDEVENTARGS_T5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB_H
#define UPLOADSTRINGCOMPLETEDEVENTARGS_T5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadStringCompletedEventArgs
struct  UploadStringCompletedEventArgs_t5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.String System.Net.UploadStringCompletedEventArgs::m_Result
	String_t* ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(UploadStringCompletedEventArgs_t5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB, ___m_Result_4)); }
	inline String_t* get_m_Result_4() const { return ___m_Result_4; }
	inline String_t** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(String_t* value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSTRINGCOMPLETEDEVENTARGS_T5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB_H
#ifndef UPLOADVALUESCOMPLETEDEVENTARGS_T5BC91E51C2B464B1912C651AAAEEE890F7AD3A74_H
#define UPLOADVALUESCOMPLETEDEVENTARGS_T5BC91E51C2B464B1912C651AAAEEE890F7AD3A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadValuesCompletedEventArgs
struct  UploadValuesCompletedEventArgs_t5BC91E51C2B464B1912C651AAAEEE890F7AD3A74  : public AsyncCompletedEventArgs_tADCBE47368F4D8B198B46D332B27EE38C8B5F8B7
{
public:
	// System.Byte[] System.Net.UploadValuesCompletedEventArgs::m_Result
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Result_4;

public:
	inline static int32_t get_offset_of_m_Result_4() { return static_cast<int32_t>(offsetof(UploadValuesCompletedEventArgs_t5BC91E51C2B464B1912C651AAAEEE890F7AD3A74, ___m_Result_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Result_4() const { return ___m_Result_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Result_4() { return &___m_Result_4; }
	inline void set_m_Result_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Result_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Result_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADVALUESCOMPLETEDEVENTARGS_T5BC91E51C2B464B1912C651AAAEEE890F7AD3A74_H
#ifndef WEBCLIENT_T0BFD938623FF98FA7EEE0031AB9E8293DD295F6A_H
#define WEBCLIENT_T0BFD938623FF98FA7EEE0031AB9E8293DD295F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient
struct  WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A  : public Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473
{
public:
	// System.Uri System.Net.WebClient::m_baseAddress
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_baseAddress_9;
	// System.Net.ICredentials System.Net.WebClient::m_credentials
	RuntimeObject* ___m_credentials_10;
	// System.Net.WebHeaderCollection System.Net.WebClient::m_headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___m_headers_11;
	// System.Collections.Specialized.NameValueCollection System.Net.WebClient::m_requestParameters
	NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * ___m_requestParameters_12;
	// System.Net.WebResponse System.Net.WebClient::m_WebResponse
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * ___m_WebResponse_13;
	// System.Net.WebRequest System.Net.WebClient::m_WebRequest
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___m_WebRequest_14;
	// System.Text.Encoding System.Net.WebClient::m_Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___m_Encoding_15;
	// System.String System.Net.WebClient::m_Method
	String_t* ___m_Method_16;
	// System.Int64 System.Net.WebClient::m_ContentLength
	int64_t ___m_ContentLength_17;
	// System.Boolean System.Net.WebClient::m_InitWebClientAsync
	bool ___m_InitWebClientAsync_18;
	// System.Boolean System.Net.WebClient::m_Cancelled
	bool ___m_Cancelled_19;
	// System.Net.WebClient_ProgressData System.Net.WebClient::m_Progress
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * ___m_Progress_20;
	// System.Net.IWebProxy System.Net.WebClient::m_Proxy
	RuntimeObject* ___m_Proxy_21;
	// System.Boolean System.Net.WebClient::m_ProxySet
	bool ___m_ProxySet_22;
	// System.Net.Cache.RequestCachePolicy System.Net.WebClient::m_CachePolicy
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ___m_CachePolicy_23;
	// System.Boolean System.Net.WebClient::<AllowReadStreamBuffering>k__BackingField
	bool ___U3CAllowReadStreamBufferingU3Ek__BackingField_24;
	// System.Boolean System.Net.WebClient::<AllowWriteStreamBuffering>k__BackingField
	bool ___U3CAllowWriteStreamBufferingU3Ek__BackingField_25;
	// System.Int32 System.Net.WebClient::m_CallNesting
	int32_t ___m_CallNesting_26;
	// System.ComponentModel.AsyncOperation System.Net.WebClient::m_AsyncOp
	AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * ___m_AsyncOp_27;
	// System.Net.OpenReadCompletedEventHandler System.Net.WebClient::OpenReadCompleted
	OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A * ___OpenReadCompleted_28;
	// System.Threading.SendOrPostCallback System.Net.WebClient::openReadOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___openReadOperationCompleted_29;
	// System.Net.OpenWriteCompletedEventHandler System.Net.WebClient::OpenWriteCompleted
	OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD * ___OpenWriteCompleted_30;
	// System.Threading.SendOrPostCallback System.Net.WebClient::openWriteOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___openWriteOperationCompleted_31;
	// System.Net.DownloadStringCompletedEventHandler System.Net.WebClient::DownloadStringCompleted
	DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C * ___DownloadStringCompleted_32;
	// System.Threading.SendOrPostCallback System.Net.WebClient::downloadStringOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___downloadStringOperationCompleted_33;
	// System.Net.DownloadDataCompletedEventHandler System.Net.WebClient::DownloadDataCompleted
	DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 * ___DownloadDataCompleted_34;
	// System.Threading.SendOrPostCallback System.Net.WebClient::downloadDataOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___downloadDataOperationCompleted_35;
	// System.ComponentModel.AsyncCompletedEventHandler System.Net.WebClient::DownloadFileCompleted
	AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 * ___DownloadFileCompleted_36;
	// System.Threading.SendOrPostCallback System.Net.WebClient::downloadFileOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___downloadFileOperationCompleted_37;
	// System.Net.UploadStringCompletedEventHandler System.Net.WebClient::UploadStringCompleted
	UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 * ___UploadStringCompleted_38;
	// System.Threading.SendOrPostCallback System.Net.WebClient::uploadStringOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___uploadStringOperationCompleted_39;
	// System.Net.UploadDataCompletedEventHandler System.Net.WebClient::UploadDataCompleted
	UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 * ___UploadDataCompleted_40;
	// System.Threading.SendOrPostCallback System.Net.WebClient::uploadDataOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___uploadDataOperationCompleted_41;
	// System.Net.UploadFileCompletedEventHandler System.Net.WebClient::UploadFileCompleted
	UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 * ___UploadFileCompleted_42;
	// System.Threading.SendOrPostCallback System.Net.WebClient::uploadFileOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___uploadFileOperationCompleted_43;
	// System.Net.UploadValuesCompletedEventHandler System.Net.WebClient::UploadValuesCompleted
	UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 * ___UploadValuesCompleted_44;
	// System.Threading.SendOrPostCallback System.Net.WebClient::uploadValuesOperationCompleted
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___uploadValuesOperationCompleted_45;
	// System.Net.DownloadProgressChangedEventHandler System.Net.WebClient::DownloadProgressChanged
	DownloadProgressChangedEventHandler_t7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F * ___DownloadProgressChanged_46;
	// System.Net.UploadProgressChangedEventHandler System.Net.WebClient::UploadProgressChanged
	UploadProgressChangedEventHandler_tD3F687D5C0C93A87BA843B34B11570B2B0530B54 * ___UploadProgressChanged_47;
	// System.Threading.SendOrPostCallback System.Net.WebClient::reportDownloadProgressChanged
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___reportDownloadProgressChanged_48;
	// System.Threading.SendOrPostCallback System.Net.WebClient::reportUploadProgressChanged
	SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * ___reportUploadProgressChanged_49;

public:
	inline static int32_t get_offset_of_m_baseAddress_9() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_baseAddress_9)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_baseAddress_9() const { return ___m_baseAddress_9; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_baseAddress_9() { return &___m_baseAddress_9; }
	inline void set_m_baseAddress_9(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_baseAddress_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_baseAddress_9), value);
	}

	inline static int32_t get_offset_of_m_credentials_10() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_credentials_10)); }
	inline RuntimeObject* get_m_credentials_10() const { return ___m_credentials_10; }
	inline RuntimeObject** get_address_of_m_credentials_10() { return &___m_credentials_10; }
	inline void set_m_credentials_10(RuntimeObject* value)
	{
		___m_credentials_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_credentials_10), value);
	}

	inline static int32_t get_offset_of_m_headers_11() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_headers_11)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_m_headers_11() const { return ___m_headers_11; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_m_headers_11() { return &___m_headers_11; }
	inline void set_m_headers_11(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___m_headers_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_headers_11), value);
	}

	inline static int32_t get_offset_of_m_requestParameters_12() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_requestParameters_12)); }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * get_m_requestParameters_12() const { return ___m_requestParameters_12; }
	inline NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 ** get_address_of_m_requestParameters_12() { return &___m_requestParameters_12; }
	inline void set_m_requestParameters_12(NameValueCollection_t7C7CED43E4C6E997E3C8012F1D2CC4027FAD10D1 * value)
	{
		___m_requestParameters_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_requestParameters_12), value);
	}

	inline static int32_t get_offset_of_m_WebResponse_13() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_WebResponse_13)); }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * get_m_WebResponse_13() const { return ___m_WebResponse_13; }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD ** get_address_of_m_WebResponse_13() { return &___m_WebResponse_13; }
	inline void set_m_WebResponse_13(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * value)
	{
		___m_WebResponse_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebResponse_13), value);
	}

	inline static int32_t get_offset_of_m_WebRequest_14() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_WebRequest_14)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_m_WebRequest_14() const { return ___m_WebRequest_14; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_m_WebRequest_14() { return &___m_WebRequest_14; }
	inline void set_m_WebRequest_14(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___m_WebRequest_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebRequest_14), value);
	}

	inline static int32_t get_offset_of_m_Encoding_15() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Encoding_15)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_m_Encoding_15() const { return ___m_Encoding_15; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_m_Encoding_15() { return &___m_Encoding_15; }
	inline void set_m_Encoding_15(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___m_Encoding_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Encoding_15), value);
	}

	inline static int32_t get_offset_of_m_Method_16() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Method_16)); }
	inline String_t* get_m_Method_16() const { return ___m_Method_16; }
	inline String_t** get_address_of_m_Method_16() { return &___m_Method_16; }
	inline void set_m_Method_16(String_t* value)
	{
		___m_Method_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_16), value);
	}

	inline static int32_t get_offset_of_m_ContentLength_17() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_ContentLength_17)); }
	inline int64_t get_m_ContentLength_17() const { return ___m_ContentLength_17; }
	inline int64_t* get_address_of_m_ContentLength_17() { return &___m_ContentLength_17; }
	inline void set_m_ContentLength_17(int64_t value)
	{
		___m_ContentLength_17 = value;
	}

	inline static int32_t get_offset_of_m_InitWebClientAsync_18() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_InitWebClientAsync_18)); }
	inline bool get_m_InitWebClientAsync_18() const { return ___m_InitWebClientAsync_18; }
	inline bool* get_address_of_m_InitWebClientAsync_18() { return &___m_InitWebClientAsync_18; }
	inline void set_m_InitWebClientAsync_18(bool value)
	{
		___m_InitWebClientAsync_18 = value;
	}

	inline static int32_t get_offset_of_m_Cancelled_19() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Cancelled_19)); }
	inline bool get_m_Cancelled_19() const { return ___m_Cancelled_19; }
	inline bool* get_address_of_m_Cancelled_19() { return &___m_Cancelled_19; }
	inline void set_m_Cancelled_19(bool value)
	{
		___m_Cancelled_19 = value;
	}

	inline static int32_t get_offset_of_m_Progress_20() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Progress_20)); }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * get_m_Progress_20() const { return ___m_Progress_20; }
	inline ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 ** get_address_of_m_Progress_20() { return &___m_Progress_20; }
	inline void set_m_Progress_20(ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20 * value)
	{
		___m_Progress_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_Progress_20), value);
	}

	inline static int32_t get_offset_of_m_Proxy_21() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_Proxy_21)); }
	inline RuntimeObject* get_m_Proxy_21() const { return ___m_Proxy_21; }
	inline RuntimeObject** get_address_of_m_Proxy_21() { return &___m_Proxy_21; }
	inline void set_m_Proxy_21(RuntimeObject* value)
	{
		___m_Proxy_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Proxy_21), value);
	}

	inline static int32_t get_offset_of_m_ProxySet_22() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_ProxySet_22)); }
	inline bool get_m_ProxySet_22() const { return ___m_ProxySet_22; }
	inline bool* get_address_of_m_ProxySet_22() { return &___m_ProxySet_22; }
	inline void set_m_ProxySet_22(bool value)
	{
		___m_ProxySet_22 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_23() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_CachePolicy_23)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get_m_CachePolicy_23() const { return ___m_CachePolicy_23; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of_m_CachePolicy_23() { return &___m_CachePolicy_23; }
	inline void set_m_CachePolicy_23(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		___m_CachePolicy_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePolicy_23), value);
	}

	inline static int32_t get_offset_of_U3CAllowReadStreamBufferingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___U3CAllowReadStreamBufferingU3Ek__BackingField_24)); }
	inline bool get_U3CAllowReadStreamBufferingU3Ek__BackingField_24() const { return ___U3CAllowReadStreamBufferingU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CAllowReadStreamBufferingU3Ek__BackingField_24() { return &___U3CAllowReadStreamBufferingU3Ek__BackingField_24; }
	inline void set_U3CAllowReadStreamBufferingU3Ek__BackingField_24(bool value)
	{
		___U3CAllowReadStreamBufferingU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CAllowWriteStreamBufferingU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___U3CAllowWriteStreamBufferingU3Ek__BackingField_25)); }
	inline bool get_U3CAllowWriteStreamBufferingU3Ek__BackingField_25() const { return ___U3CAllowWriteStreamBufferingU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CAllowWriteStreamBufferingU3Ek__BackingField_25() { return &___U3CAllowWriteStreamBufferingU3Ek__BackingField_25; }
	inline void set_U3CAllowWriteStreamBufferingU3Ek__BackingField_25(bool value)
	{
		___U3CAllowWriteStreamBufferingU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_m_CallNesting_26() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_CallNesting_26)); }
	inline int32_t get_m_CallNesting_26() const { return ___m_CallNesting_26; }
	inline int32_t* get_address_of_m_CallNesting_26() { return &___m_CallNesting_26; }
	inline void set_m_CallNesting_26(int32_t value)
	{
		___m_CallNesting_26 = value;
	}

	inline static int32_t get_offset_of_m_AsyncOp_27() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___m_AsyncOp_27)); }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * get_m_AsyncOp_27() const { return ___m_AsyncOp_27; }
	inline AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 ** get_address_of_m_AsyncOp_27() { return &___m_AsyncOp_27; }
	inline void set_m_AsyncOp_27(AsyncOperation_tFA5B90911F8ABE8B07A3058301687DDDF5F78748 * value)
	{
		___m_AsyncOp_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncOp_27), value);
	}

	inline static int32_t get_offset_of_OpenReadCompleted_28() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___OpenReadCompleted_28)); }
	inline OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A * get_OpenReadCompleted_28() const { return ___OpenReadCompleted_28; }
	inline OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A ** get_address_of_OpenReadCompleted_28() { return &___OpenReadCompleted_28; }
	inline void set_OpenReadCompleted_28(OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A * value)
	{
		___OpenReadCompleted_28 = value;
		Il2CppCodeGenWriteBarrier((&___OpenReadCompleted_28), value);
	}

	inline static int32_t get_offset_of_openReadOperationCompleted_29() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___openReadOperationCompleted_29)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_openReadOperationCompleted_29() const { return ___openReadOperationCompleted_29; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_openReadOperationCompleted_29() { return &___openReadOperationCompleted_29; }
	inline void set_openReadOperationCompleted_29(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___openReadOperationCompleted_29 = value;
		Il2CppCodeGenWriteBarrier((&___openReadOperationCompleted_29), value);
	}

	inline static int32_t get_offset_of_OpenWriteCompleted_30() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___OpenWriteCompleted_30)); }
	inline OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD * get_OpenWriteCompleted_30() const { return ___OpenWriteCompleted_30; }
	inline OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD ** get_address_of_OpenWriteCompleted_30() { return &___OpenWriteCompleted_30; }
	inline void set_OpenWriteCompleted_30(OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD * value)
	{
		___OpenWriteCompleted_30 = value;
		Il2CppCodeGenWriteBarrier((&___OpenWriteCompleted_30), value);
	}

	inline static int32_t get_offset_of_openWriteOperationCompleted_31() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___openWriteOperationCompleted_31)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_openWriteOperationCompleted_31() const { return ___openWriteOperationCompleted_31; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_openWriteOperationCompleted_31() { return &___openWriteOperationCompleted_31; }
	inline void set_openWriteOperationCompleted_31(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___openWriteOperationCompleted_31 = value;
		Il2CppCodeGenWriteBarrier((&___openWriteOperationCompleted_31), value);
	}

	inline static int32_t get_offset_of_DownloadStringCompleted_32() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___DownloadStringCompleted_32)); }
	inline DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C * get_DownloadStringCompleted_32() const { return ___DownloadStringCompleted_32; }
	inline DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C ** get_address_of_DownloadStringCompleted_32() { return &___DownloadStringCompleted_32; }
	inline void set_DownloadStringCompleted_32(DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C * value)
	{
		___DownloadStringCompleted_32 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadStringCompleted_32), value);
	}

	inline static int32_t get_offset_of_downloadStringOperationCompleted_33() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___downloadStringOperationCompleted_33)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_downloadStringOperationCompleted_33() const { return ___downloadStringOperationCompleted_33; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_downloadStringOperationCompleted_33() { return &___downloadStringOperationCompleted_33; }
	inline void set_downloadStringOperationCompleted_33(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___downloadStringOperationCompleted_33 = value;
		Il2CppCodeGenWriteBarrier((&___downloadStringOperationCompleted_33), value);
	}

	inline static int32_t get_offset_of_DownloadDataCompleted_34() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___DownloadDataCompleted_34)); }
	inline DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 * get_DownloadDataCompleted_34() const { return ___DownloadDataCompleted_34; }
	inline DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 ** get_address_of_DownloadDataCompleted_34() { return &___DownloadDataCompleted_34; }
	inline void set_DownloadDataCompleted_34(DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61 * value)
	{
		___DownloadDataCompleted_34 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadDataCompleted_34), value);
	}

	inline static int32_t get_offset_of_downloadDataOperationCompleted_35() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___downloadDataOperationCompleted_35)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_downloadDataOperationCompleted_35() const { return ___downloadDataOperationCompleted_35; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_downloadDataOperationCompleted_35() { return &___downloadDataOperationCompleted_35; }
	inline void set_downloadDataOperationCompleted_35(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___downloadDataOperationCompleted_35 = value;
		Il2CppCodeGenWriteBarrier((&___downloadDataOperationCompleted_35), value);
	}

	inline static int32_t get_offset_of_DownloadFileCompleted_36() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___DownloadFileCompleted_36)); }
	inline AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 * get_DownloadFileCompleted_36() const { return ___DownloadFileCompleted_36; }
	inline AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 ** get_address_of_DownloadFileCompleted_36() { return &___DownloadFileCompleted_36; }
	inline void set_DownloadFileCompleted_36(AsyncCompletedEventHandler_tEC686A4F246B63CF3F780209D5DAFA051DF81DB6 * value)
	{
		___DownloadFileCompleted_36 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadFileCompleted_36), value);
	}

	inline static int32_t get_offset_of_downloadFileOperationCompleted_37() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___downloadFileOperationCompleted_37)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_downloadFileOperationCompleted_37() const { return ___downloadFileOperationCompleted_37; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_downloadFileOperationCompleted_37() { return &___downloadFileOperationCompleted_37; }
	inline void set_downloadFileOperationCompleted_37(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___downloadFileOperationCompleted_37 = value;
		Il2CppCodeGenWriteBarrier((&___downloadFileOperationCompleted_37), value);
	}

	inline static int32_t get_offset_of_UploadStringCompleted_38() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___UploadStringCompleted_38)); }
	inline UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 * get_UploadStringCompleted_38() const { return ___UploadStringCompleted_38; }
	inline UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 ** get_address_of_UploadStringCompleted_38() { return &___UploadStringCompleted_38; }
	inline void set_UploadStringCompleted_38(UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6 * value)
	{
		___UploadStringCompleted_38 = value;
		Il2CppCodeGenWriteBarrier((&___UploadStringCompleted_38), value);
	}

	inline static int32_t get_offset_of_uploadStringOperationCompleted_39() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___uploadStringOperationCompleted_39)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_uploadStringOperationCompleted_39() const { return ___uploadStringOperationCompleted_39; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_uploadStringOperationCompleted_39() { return &___uploadStringOperationCompleted_39; }
	inline void set_uploadStringOperationCompleted_39(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___uploadStringOperationCompleted_39 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStringOperationCompleted_39), value);
	}

	inline static int32_t get_offset_of_UploadDataCompleted_40() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___UploadDataCompleted_40)); }
	inline UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 * get_UploadDataCompleted_40() const { return ___UploadDataCompleted_40; }
	inline UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 ** get_address_of_UploadDataCompleted_40() { return &___UploadDataCompleted_40; }
	inline void set_UploadDataCompleted_40(UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33 * value)
	{
		___UploadDataCompleted_40 = value;
		Il2CppCodeGenWriteBarrier((&___UploadDataCompleted_40), value);
	}

	inline static int32_t get_offset_of_uploadDataOperationCompleted_41() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___uploadDataOperationCompleted_41)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_uploadDataOperationCompleted_41() const { return ___uploadDataOperationCompleted_41; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_uploadDataOperationCompleted_41() { return &___uploadDataOperationCompleted_41; }
	inline void set_uploadDataOperationCompleted_41(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___uploadDataOperationCompleted_41 = value;
		Il2CppCodeGenWriteBarrier((&___uploadDataOperationCompleted_41), value);
	}

	inline static int32_t get_offset_of_UploadFileCompleted_42() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___UploadFileCompleted_42)); }
	inline UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 * get_UploadFileCompleted_42() const { return ___UploadFileCompleted_42; }
	inline UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 ** get_address_of_UploadFileCompleted_42() { return &___UploadFileCompleted_42; }
	inline void set_UploadFileCompleted_42(UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86 * value)
	{
		___UploadFileCompleted_42 = value;
		Il2CppCodeGenWriteBarrier((&___UploadFileCompleted_42), value);
	}

	inline static int32_t get_offset_of_uploadFileOperationCompleted_43() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___uploadFileOperationCompleted_43)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_uploadFileOperationCompleted_43() const { return ___uploadFileOperationCompleted_43; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_uploadFileOperationCompleted_43() { return &___uploadFileOperationCompleted_43; }
	inline void set_uploadFileOperationCompleted_43(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___uploadFileOperationCompleted_43 = value;
		Il2CppCodeGenWriteBarrier((&___uploadFileOperationCompleted_43), value);
	}

	inline static int32_t get_offset_of_UploadValuesCompleted_44() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___UploadValuesCompleted_44)); }
	inline UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 * get_UploadValuesCompleted_44() const { return ___UploadValuesCompleted_44; }
	inline UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 ** get_address_of_UploadValuesCompleted_44() { return &___UploadValuesCompleted_44; }
	inline void set_UploadValuesCompleted_44(UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6 * value)
	{
		___UploadValuesCompleted_44 = value;
		Il2CppCodeGenWriteBarrier((&___UploadValuesCompleted_44), value);
	}

	inline static int32_t get_offset_of_uploadValuesOperationCompleted_45() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___uploadValuesOperationCompleted_45)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_uploadValuesOperationCompleted_45() const { return ___uploadValuesOperationCompleted_45; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_uploadValuesOperationCompleted_45() { return &___uploadValuesOperationCompleted_45; }
	inline void set_uploadValuesOperationCompleted_45(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___uploadValuesOperationCompleted_45 = value;
		Il2CppCodeGenWriteBarrier((&___uploadValuesOperationCompleted_45), value);
	}

	inline static int32_t get_offset_of_DownloadProgressChanged_46() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___DownloadProgressChanged_46)); }
	inline DownloadProgressChangedEventHandler_t7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F * get_DownloadProgressChanged_46() const { return ___DownloadProgressChanged_46; }
	inline DownloadProgressChangedEventHandler_t7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F ** get_address_of_DownloadProgressChanged_46() { return &___DownloadProgressChanged_46; }
	inline void set_DownloadProgressChanged_46(DownloadProgressChangedEventHandler_t7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F * value)
	{
		___DownloadProgressChanged_46 = value;
		Il2CppCodeGenWriteBarrier((&___DownloadProgressChanged_46), value);
	}

	inline static int32_t get_offset_of_UploadProgressChanged_47() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___UploadProgressChanged_47)); }
	inline UploadProgressChangedEventHandler_tD3F687D5C0C93A87BA843B34B11570B2B0530B54 * get_UploadProgressChanged_47() const { return ___UploadProgressChanged_47; }
	inline UploadProgressChangedEventHandler_tD3F687D5C0C93A87BA843B34B11570B2B0530B54 ** get_address_of_UploadProgressChanged_47() { return &___UploadProgressChanged_47; }
	inline void set_UploadProgressChanged_47(UploadProgressChangedEventHandler_tD3F687D5C0C93A87BA843B34B11570B2B0530B54 * value)
	{
		___UploadProgressChanged_47 = value;
		Il2CppCodeGenWriteBarrier((&___UploadProgressChanged_47), value);
	}

	inline static int32_t get_offset_of_reportDownloadProgressChanged_48() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___reportDownloadProgressChanged_48)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_reportDownloadProgressChanged_48() const { return ___reportDownloadProgressChanged_48; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_reportDownloadProgressChanged_48() { return &___reportDownloadProgressChanged_48; }
	inline void set_reportDownloadProgressChanged_48(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___reportDownloadProgressChanged_48 = value;
		Il2CppCodeGenWriteBarrier((&___reportDownloadProgressChanged_48), value);
	}

	inline static int32_t get_offset_of_reportUploadProgressChanged_49() { return static_cast<int32_t>(offsetof(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A, ___reportUploadProgressChanged_49)); }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * get_reportUploadProgressChanged_49() const { return ___reportUploadProgressChanged_49; }
	inline SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 ** get_address_of_reportUploadProgressChanged_49() { return &___reportUploadProgressChanged_49; }
	inline void set_reportUploadProgressChanged_49(SendOrPostCallback_t3F9C0164860E4AA5138DF8B4488DFB0D33147F01 * value)
	{
		___reportUploadProgressChanged_49 = value;
		Il2CppCodeGenWriteBarrier((&___reportUploadProgressChanged_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCLIENT_T0BFD938623FF98FA7EEE0031AB9E8293DD295F6A_H
#ifndef WEBCLIENTWRITESTREAM_T7E060E6D900CCC3256E5BFF30537376635158397_H
#define WEBCLIENTWRITESTREAM_T7E060E6D900CCC3256E5BFF30537376635158397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient_WebClientWriteStream
struct  WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Net.WebRequest System.Net.WebClient_WebClientWriteStream::m_request
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___m_request_5;
	// System.IO.Stream System.Net.WebClient_WebClientWriteStream::m_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_stream_6;
	// System.Net.WebClient System.Net.WebClient_WebClientWriteStream::m_WebClient
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * ___m_WebClient_7;

public:
	inline static int32_t get_offset_of_m_request_5() { return static_cast<int32_t>(offsetof(WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397, ___m_request_5)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_m_request_5() const { return ___m_request_5; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_m_request_5() { return &___m_request_5; }
	inline void set_m_request_5(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___m_request_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_request_5), value);
	}

	inline static int32_t get_offset_of_m_stream_6() { return static_cast<int32_t>(offsetof(WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397, ___m_stream_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_stream_6() const { return ___m_stream_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_stream_6() { return &___m_stream_6; }
	inline void set_m_stream_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_stream_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream_6), value);
	}

	inline static int32_t get_offset_of_m_WebClient_7() { return static_cast<int32_t>(offsetof(WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397, ___m_WebClient_7)); }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * get_m_WebClient_7() const { return ___m_WebClient_7; }
	inline WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A ** get_address_of_m_WebClient_7() { return &___m_WebClient_7; }
	inline void set_m_WebClient_7(WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A * value)
	{
		___m_WebClient_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebClient_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCLIENTWRITESTREAM_T7E060E6D900CCC3256E5BFF30537376635158397_H
#ifndef TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#define TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.TokenImpersonationLevel
struct  TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26 
{
public:
	// System.Int32 System.Security.Principal.TokenImpersonationLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TokenImpersonationLevel_tED478ED25688E978F79556E1A2335F7262023D26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIMPERSONATIONLEVEL_TED478ED25688E978F79556E1A2335F7262023D26_H
#ifndef FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#define FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_7;
	// System.String System.IO.FileStream::name
	String_t* ___name_8;
	// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.FileStream::safeHandle
	SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * ___safeHandle_9;
	// System.Boolean System.IO.FileStream::isExposed
	bool ___isExposed_10;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_11;
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_12;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_13;
	// System.Boolean System.IO.FileStream::async
	bool ___async_14;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_15;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_16;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_17;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_18;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_19;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_20;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_21;

public:
	inline static int32_t get_offset_of_buf_7() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_7() const { return ___buf_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_7() { return &___buf_7; }
	inline void set_buf_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_7 = value;
		Il2CppCodeGenWriteBarrier((&___buf_7), value);
	}

	inline static int32_t get_offset_of_name_8() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___name_8)); }
	inline String_t* get_name_8() const { return ___name_8; }
	inline String_t** get_address_of_name_8() { return &___name_8; }
	inline void set_name_8(String_t* value)
	{
		___name_8 = value;
		Il2CppCodeGenWriteBarrier((&___name_8), value);
	}

	inline static int32_t get_offset_of_safeHandle_9() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___safeHandle_9)); }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * get_safeHandle_9() const { return ___safeHandle_9; }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB ** get_address_of_safeHandle_9() { return &___safeHandle_9; }
	inline void set_safeHandle_9(SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * value)
	{
		___safeHandle_9 = value;
		Il2CppCodeGenWriteBarrier((&___safeHandle_9), value);
	}

	inline static int32_t get_offset_of_isExposed_10() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___isExposed_10)); }
	inline bool get_isExposed_10() const { return ___isExposed_10; }
	inline bool* get_address_of_isExposed_10() { return &___isExposed_10; }
	inline void set_isExposed_10(bool value)
	{
		___isExposed_10 = value;
	}

	inline static int32_t get_offset_of_append_startpos_11() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___append_startpos_11)); }
	inline int64_t get_append_startpos_11() const { return ___append_startpos_11; }
	inline int64_t* get_address_of_append_startpos_11() { return &___append_startpos_11; }
	inline void set_append_startpos_11(int64_t value)
	{
		___append_startpos_11 = value;
	}

	inline static int32_t get_offset_of_access_12() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___access_12)); }
	inline int32_t get_access_12() const { return ___access_12; }
	inline int32_t* get_address_of_access_12() { return &___access_12; }
	inline void set_access_12(int32_t value)
	{
		___access_12 = value;
	}

	inline static int32_t get_offset_of_owner_13() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___owner_13)); }
	inline bool get_owner_13() const { return ___owner_13; }
	inline bool* get_address_of_owner_13() { return &___owner_13; }
	inline void set_owner_13(bool value)
	{
		___owner_13 = value;
	}

	inline static int32_t get_offset_of_async_14() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___async_14)); }
	inline bool get_async_14() const { return ___async_14; }
	inline bool* get_address_of_async_14() { return &___async_14; }
	inline void set_async_14(bool value)
	{
		___async_14 = value;
	}

	inline static int32_t get_offset_of_canseek_15() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___canseek_15)); }
	inline bool get_canseek_15() const { return ___canseek_15; }
	inline bool* get_address_of_canseek_15() { return &___canseek_15; }
	inline void set_canseek_15(bool value)
	{
		___canseek_15 = value;
	}

	inline static int32_t get_offset_of_anonymous_16() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___anonymous_16)); }
	inline bool get_anonymous_16() const { return ___anonymous_16; }
	inline bool* get_address_of_anonymous_16() { return &___anonymous_16; }
	inline void set_anonymous_16(bool value)
	{
		___anonymous_16 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_17() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_dirty_17)); }
	inline bool get_buf_dirty_17() const { return ___buf_dirty_17; }
	inline bool* get_address_of_buf_dirty_17() { return &___buf_dirty_17; }
	inline void set_buf_dirty_17(bool value)
	{
		___buf_dirty_17 = value;
	}

	inline static int32_t get_offset_of_buf_size_18() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_size_18)); }
	inline int32_t get_buf_size_18() const { return ___buf_size_18; }
	inline int32_t* get_address_of_buf_size_18() { return &___buf_size_18; }
	inline void set_buf_size_18(int32_t value)
	{
		___buf_size_18 = value;
	}

	inline static int32_t get_offset_of_buf_length_19() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_length_19)); }
	inline int32_t get_buf_length_19() const { return ___buf_length_19; }
	inline int32_t* get_address_of_buf_length_19() { return &___buf_length_19; }
	inline void set_buf_length_19(int32_t value)
	{
		___buf_length_19 = value;
	}

	inline static int32_t get_offset_of_buf_offset_20() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_offset_20)); }
	inline int32_t get_buf_offset_20() const { return ___buf_offset_20; }
	inline int32_t* get_address_of_buf_offset_20() { return &___buf_offset_20; }
	inline void set_buf_offset_20(int32_t value)
	{
		___buf_offset_20 = value;
	}

	inline static int32_t get_offset_of_buf_start_21() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_start_21)); }
	inline int64_t get_buf_start_21() const { return ___buf_start_21; }
	inline int64_t* get_address_of_buf_start_21() { return &___buf_start_21; }
	inline void set_buf_start_21(int64_t value)
	{
		___buf_start_21 = value;
	}
};

struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields
{
public:
	// System.Byte[] System.IO.FileStream::buf_recycle
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_recycle_5;
	// System.Object System.IO.FileStream::buf_recycle_lock
	RuntimeObject * ___buf_recycle_lock_6;

public:
	inline static int32_t get_offset_of_buf_recycle_5() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_recycle_5() const { return ___buf_recycle_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_recycle_5() { return &___buf_recycle_5; }
	inline void set_buf_recycle_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_recycle_5 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_5), value);
	}

	inline static int32_t get_offset_of_buf_recycle_lock_6() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_lock_6)); }
	inline RuntimeObject * get_buf_recycle_lock_6() const { return ___buf_recycle_lock_6; }
	inline RuntimeObject ** get_address_of_buf_recycle_lock_6() { return &___buf_recycle_lock_6; }
	inline void set_buf_recycle_lock_6(RuntimeObject * value)
	{
		___buf_recycle_lock_6 = value;
		Il2CppCodeGenWriteBarrier((&___buf_recycle_lock_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_TA770BF9AF0906644D43C81B962C7DBC3BC79A418_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef BASE64STREAM_TF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_H
#define BASE64STREAM_TF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Base64Stream
struct  Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0  : public DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC
{
public:
	// System.Int32 System.Net.Base64Stream::lineLength
	int32_t ___lineLength_9;
	// System.Net.Base64Stream_ReadStateInfo System.Net.Base64Stream::readState
	ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9 * ___readState_10;
	// System.Net.Mime.Base64WriteStateInfo System.Net.Base64Stream::writeState
	Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366 * ___writeState_11;

public:
	inline static int32_t get_offset_of_lineLength_9() { return static_cast<int32_t>(offsetof(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0, ___lineLength_9)); }
	inline int32_t get_lineLength_9() const { return ___lineLength_9; }
	inline int32_t* get_address_of_lineLength_9() { return &___lineLength_9; }
	inline void set_lineLength_9(int32_t value)
	{
		___lineLength_9 = value;
	}

	inline static int32_t get_offset_of_readState_10() { return static_cast<int32_t>(offsetof(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0, ___readState_10)); }
	inline ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9 * get_readState_10() const { return ___readState_10; }
	inline ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9 ** get_address_of_readState_10() { return &___readState_10; }
	inline void set_readState_10(ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9 * value)
	{
		___readState_10 = value;
		Il2CppCodeGenWriteBarrier((&___readState_10), value);
	}

	inline static int32_t get_offset_of_writeState_11() { return static_cast<int32_t>(offsetof(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0, ___writeState_11)); }
	inline Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366 * get_writeState_11() const { return ___writeState_11; }
	inline Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366 ** get_address_of_writeState_11() { return &___writeState_11; }
	inline void set_writeState_11(Base64WriteStateInfo_tB45E408314AC5BFE8908E8E6C85BEB10EF29F366 * value)
	{
		___writeState_11 = value;
		Il2CppCodeGenWriteBarrier((&___writeState_11), value);
	}
};

struct Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_StaticFields
{
public:
	// System.Byte[] System.Net.Base64Stream::base64DecodeMap
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___base64DecodeMap_7;
	// System.Byte[] System.Net.Base64Stream::base64EncodeMap
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___base64EncodeMap_8;

public:
	inline static int32_t get_offset_of_base64DecodeMap_7() { return static_cast<int32_t>(offsetof(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_StaticFields, ___base64DecodeMap_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_base64DecodeMap_7() const { return ___base64DecodeMap_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_base64DecodeMap_7() { return &___base64DecodeMap_7; }
	inline void set_base64DecodeMap_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___base64DecodeMap_7 = value;
		Il2CppCodeGenWriteBarrier((&___base64DecodeMap_7), value);
	}

	inline static int32_t get_offset_of_base64EncodeMap_8() { return static_cast<int32_t>(offsetof(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_StaticFields, ___base64EncodeMap_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_base64EncodeMap_8() const { return ___base64EncodeMap_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_base64EncodeMap_8() { return &___base64EncodeMap_8; }
	inline void set_base64EncodeMap_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___base64EncodeMap_8 = value;
		Il2CppCodeGenWriteBarrier((&___base64EncodeMap_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64STREAM_TF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_H
#ifndef CHUNKEDINPUTSTREAM_T10E9B9112B748C89333D9C33D519EDAF17181930_H
#define CHUNKEDINPUTSTREAM_T10E9B9112B748C89333D9C33D519EDAF17181930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkedInputStream
struct  ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930  : public RequestStream_t4BF6FD18AB7261094F557E867606B91FECF74D35
{
public:
	// System.Boolean System.Net.ChunkedInputStream::disposed
	bool ___disposed_11;
	// System.Net.MonoChunkStream System.Net.ChunkedInputStream::decoder
	MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * ___decoder_12;
	// System.Net.HttpListenerContext System.Net.ChunkedInputStream::context
	HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * ___context_13;
	// System.Boolean System.Net.ChunkedInputStream::no_more_data
	bool ___no_more_data_14;

public:
	inline static int32_t get_offset_of_disposed_11() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___disposed_11)); }
	inline bool get_disposed_11() const { return ___disposed_11; }
	inline bool* get_address_of_disposed_11() { return &___disposed_11; }
	inline void set_disposed_11(bool value)
	{
		___disposed_11 = value;
	}

	inline static int32_t get_offset_of_decoder_12() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___decoder_12)); }
	inline MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * get_decoder_12() const { return ___decoder_12; }
	inline MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 ** get_address_of_decoder_12() { return &___decoder_12; }
	inline void set_decoder_12(MonoChunkStream_t33C2B7ECB208D77D1C94F0A9F48EB5AE27DF5AB5 * value)
	{
		___decoder_12 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_12), value);
	}

	inline static int32_t get_offset_of_context_13() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___context_13)); }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * get_context_13() const { return ___context_13; }
	inline HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA ** get_address_of_context_13() { return &___context_13; }
	inline void set_context_13(HttpListenerContext_t438DA3609B8D775884622DF392BD7D72C651FCFA * value)
	{
		___context_13 = value;
		Il2CppCodeGenWriteBarrier((&___context_13), value);
	}

	inline static int32_t get_offset_of_no_more_data_14() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930, ___no_more_data_14)); }
	inline bool get_no_more_data_14() const { return ___no_more_data_14; }
	inline bool* get_address_of_no_more_data_14() { return &___no_more_data_14; }
	inline void set_no_more_data_14(bool value)
	{
		___no_more_data_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKEDINPUTSTREAM_T10E9B9112B748C89333D9C33D519EDAF17181930_H
#ifndef CLOSABLESTREAM_TC64EAEC07968A4B6D2AAABD81A9AB59517F383F3_H
#define CLOSABLESTREAM_TC64EAEC07968A4B6D2AAABD81A9AB59517F383F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ClosableStream
struct  ClosableStream_tC64EAEC07968A4B6D2AAABD81A9AB59517F383F3  : public DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC
{
public:
	// System.EventHandler System.Net.ClosableStream::onClose
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___onClose_7;
	// System.Int32 System.Net.ClosableStream::closed
	int32_t ___closed_8;

public:
	inline static int32_t get_offset_of_onClose_7() { return static_cast<int32_t>(offsetof(ClosableStream_tC64EAEC07968A4B6D2AAABD81A9AB59517F383F3, ___onClose_7)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_onClose_7() const { return ___onClose_7; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_onClose_7() { return &___onClose_7; }
	inline void set_onClose_7(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___onClose_7 = value;
		Il2CppCodeGenWriteBarrier((&___onClose_7), value);
	}

	inline static int32_t get_offset_of_closed_8() { return static_cast<int32_t>(offsetof(ClosableStream_tC64EAEC07968A4B6D2AAABD81A9AB59517F383F3, ___closed_8)); }
	inline int32_t get_closed_8() const { return ___closed_8; }
	inline int32_t* get_address_of_closed_8() { return &___closed_8; }
	inline void set_closed_8(int32_t value)
	{
		___closed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSABLESTREAM_TC64EAEC07968A4B6D2AAABD81A9AB59517F383F3_H
#ifndef CONNECTIONPOOL_T7BEC38A8953B38D2C1262D373089B8915ECE020C_H
#define CONNECTIONPOOL_T7BEC38A8953B38D2C1262D373089B8915ECE020C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ConnectionPool
struct  ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C  : public RuntimeObject
{
public:
	// System.Net.TimerThread_Queue System.Net.ConnectionPool::m_CleanupQueue
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * ___m_CleanupQueue_10;
	// System.Net.ConnectionPool_State System.Net.ConnectionPool::m_State
	int32_t ___m_State_11;
	// System.Net.InterlockedStack System.Net.ConnectionPool::m_StackOld
	InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C * ___m_StackOld_12;
	// System.Net.InterlockedStack System.Net.ConnectionPool::m_StackNew
	InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C * ___m_StackNew_13;
	// System.Int32 System.Net.ConnectionPool::m_WaitCount
	int32_t ___m_WaitCount_14;
	// System.Threading.WaitHandle[] System.Net.ConnectionPool::m_WaitHandles
	WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* ___m_WaitHandles_15;
	// System.Exception System.Net.ConnectionPool::m_ResError
	Exception_t * ___m_ResError_16;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.ConnectionPool::m_ErrorOccured
	bool ___m_ErrorOccured_17;
	// System.Net.TimerThread_Timer System.Net.ConnectionPool::m_ErrorTimer
	Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F * ___m_ErrorTimer_18;
	// System.Collections.ArrayList System.Net.ConnectionPool::m_ObjectList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___m_ObjectList_19;
	// System.Int32 System.Net.ConnectionPool::m_TotalObjects
	int32_t ___m_TotalObjects_20;
	// System.Collections.Queue System.Net.ConnectionPool::m_QueuedRequests
	Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * ___m_QueuedRequests_21;
	// System.Threading.Thread System.Net.ConnectionPool::m_AsyncThread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___m_AsyncThread_22;
	// System.Int32 System.Net.ConnectionPool::m_MaxPoolSize
	int32_t ___m_MaxPoolSize_23;
	// System.Int32 System.Net.ConnectionPool::m_MinPoolSize
	int32_t ___m_MinPoolSize_24;
	// System.Net.ServicePoint System.Net.ConnectionPool::m_ServicePoint
	ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * ___m_ServicePoint_25;
	// System.Net.CreateConnectionDelegate System.Net.ConnectionPool::m_CreateConnectionCallback
	CreateConnectionDelegate_tAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B * ___m_CreateConnectionCallback_26;

public:
	inline static int32_t get_offset_of_m_CleanupQueue_10() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_CleanupQueue_10)); }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * get_m_CleanupQueue_10() const { return ___m_CleanupQueue_10; }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 ** get_address_of_m_CleanupQueue_10() { return &___m_CleanupQueue_10; }
	inline void set_m_CleanupQueue_10(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * value)
	{
		___m_CleanupQueue_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CleanupQueue_10), value);
	}

	inline static int32_t get_offset_of_m_State_11() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_State_11)); }
	inline int32_t get_m_State_11() const { return ___m_State_11; }
	inline int32_t* get_address_of_m_State_11() { return &___m_State_11; }
	inline void set_m_State_11(int32_t value)
	{
		___m_State_11 = value;
	}

	inline static int32_t get_offset_of_m_StackOld_12() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_StackOld_12)); }
	inline InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C * get_m_StackOld_12() const { return ___m_StackOld_12; }
	inline InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C ** get_address_of_m_StackOld_12() { return &___m_StackOld_12; }
	inline void set_m_StackOld_12(InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C * value)
	{
		___m_StackOld_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_StackOld_12), value);
	}

	inline static int32_t get_offset_of_m_StackNew_13() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_StackNew_13)); }
	inline InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C * get_m_StackNew_13() const { return ___m_StackNew_13; }
	inline InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C ** get_address_of_m_StackNew_13() { return &___m_StackNew_13; }
	inline void set_m_StackNew_13(InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C * value)
	{
		___m_StackNew_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_StackNew_13), value);
	}

	inline static int32_t get_offset_of_m_WaitCount_14() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_WaitCount_14)); }
	inline int32_t get_m_WaitCount_14() const { return ___m_WaitCount_14; }
	inline int32_t* get_address_of_m_WaitCount_14() { return &___m_WaitCount_14; }
	inline void set_m_WaitCount_14(int32_t value)
	{
		___m_WaitCount_14 = value;
	}

	inline static int32_t get_offset_of_m_WaitHandles_15() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_WaitHandles_15)); }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* get_m_WaitHandles_15() const { return ___m_WaitHandles_15; }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC** get_address_of_m_WaitHandles_15() { return &___m_WaitHandles_15; }
	inline void set_m_WaitHandles_15(WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* value)
	{
		___m_WaitHandles_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaitHandles_15), value);
	}

	inline static int32_t get_offset_of_m_ResError_16() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_ResError_16)); }
	inline Exception_t * get_m_ResError_16() const { return ___m_ResError_16; }
	inline Exception_t ** get_address_of_m_ResError_16() { return &___m_ResError_16; }
	inline void set_m_ResError_16(Exception_t * value)
	{
		___m_ResError_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ResError_16), value);
	}

	inline static int32_t get_offset_of_m_ErrorOccured_17() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_ErrorOccured_17)); }
	inline bool get_m_ErrorOccured_17() const { return ___m_ErrorOccured_17; }
	inline bool* get_address_of_m_ErrorOccured_17() { return &___m_ErrorOccured_17; }
	inline void set_m_ErrorOccured_17(bool value)
	{
		___m_ErrorOccured_17 = value;
	}

	inline static int32_t get_offset_of_m_ErrorTimer_18() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_ErrorTimer_18)); }
	inline Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F * get_m_ErrorTimer_18() const { return ___m_ErrorTimer_18; }
	inline Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F ** get_address_of_m_ErrorTimer_18() { return &___m_ErrorTimer_18; }
	inline void set_m_ErrorTimer_18(Timer_t3B21B1013E27B5DC9FED14EC0921A5F51230D46F * value)
	{
		___m_ErrorTimer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_ErrorTimer_18), value);
	}

	inline static int32_t get_offset_of_m_ObjectList_19() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_ObjectList_19)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_m_ObjectList_19() const { return ___m_ObjectList_19; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_m_ObjectList_19() { return &___m_ObjectList_19; }
	inline void set_m_ObjectList_19(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___m_ObjectList_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectList_19), value);
	}

	inline static int32_t get_offset_of_m_TotalObjects_20() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_TotalObjects_20)); }
	inline int32_t get_m_TotalObjects_20() const { return ___m_TotalObjects_20; }
	inline int32_t* get_address_of_m_TotalObjects_20() { return &___m_TotalObjects_20; }
	inline void set_m_TotalObjects_20(int32_t value)
	{
		___m_TotalObjects_20 = value;
	}

	inline static int32_t get_offset_of_m_QueuedRequests_21() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_QueuedRequests_21)); }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * get_m_QueuedRequests_21() const { return ___m_QueuedRequests_21; }
	inline Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 ** get_address_of_m_QueuedRequests_21() { return &___m_QueuedRequests_21; }
	inline void set_m_QueuedRequests_21(Queue_tEC6DE7527799C2E4224B469ECD0CDD2B25E8E4F3 * value)
	{
		___m_QueuedRequests_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_QueuedRequests_21), value);
	}

	inline static int32_t get_offset_of_m_AsyncThread_22() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_AsyncThread_22)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_m_AsyncThread_22() const { return ___m_AsyncThread_22; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_m_AsyncThread_22() { return &___m_AsyncThread_22; }
	inline void set_m_AsyncThread_22(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___m_AsyncThread_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncThread_22), value);
	}

	inline static int32_t get_offset_of_m_MaxPoolSize_23() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_MaxPoolSize_23)); }
	inline int32_t get_m_MaxPoolSize_23() const { return ___m_MaxPoolSize_23; }
	inline int32_t* get_address_of_m_MaxPoolSize_23() { return &___m_MaxPoolSize_23; }
	inline void set_m_MaxPoolSize_23(int32_t value)
	{
		___m_MaxPoolSize_23 = value;
	}

	inline static int32_t get_offset_of_m_MinPoolSize_24() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_MinPoolSize_24)); }
	inline int32_t get_m_MinPoolSize_24() const { return ___m_MinPoolSize_24; }
	inline int32_t* get_address_of_m_MinPoolSize_24() { return &___m_MinPoolSize_24; }
	inline void set_m_MinPoolSize_24(int32_t value)
	{
		___m_MinPoolSize_24 = value;
	}

	inline static int32_t get_offset_of_m_ServicePoint_25() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_ServicePoint_25)); }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * get_m_ServicePoint_25() const { return ___m_ServicePoint_25; }
	inline ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 ** get_address_of_m_ServicePoint_25() { return &___m_ServicePoint_25; }
	inline void set_m_ServicePoint_25(ServicePoint_t5F42B1A9D56E09B4B051BE0968C81DE3128E3EB4 * value)
	{
		___m_ServicePoint_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServicePoint_25), value);
	}

	inline static int32_t get_offset_of_m_CreateConnectionCallback_26() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C, ___m_CreateConnectionCallback_26)); }
	inline CreateConnectionDelegate_tAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B * get_m_CreateConnectionCallback_26() const { return ___m_CreateConnectionCallback_26; }
	inline CreateConnectionDelegate_tAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B ** get_address_of_m_CreateConnectionCallback_26() { return &___m_CreateConnectionCallback_26; }
	inline void set_m_CreateConnectionCallback_26(CreateConnectionDelegate_tAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B * value)
	{
		___m_CreateConnectionCallback_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreateConnectionCallback_26), value);
	}
};

struct ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields
{
public:
	// System.Net.TimerThread_Callback System.Net.ConnectionPool::s_CleanupCallback
	Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * ___s_CleanupCallback_0;
	// System.Net.TimerThread_Callback System.Net.ConnectionPool::s_CancelErrorCallback
	Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * ___s_CancelErrorCallback_1;
	// System.Net.TimerThread_Queue System.Net.ConnectionPool::s_CancelErrorQueue
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * ___s_CancelErrorQueue_2;

public:
	inline static int32_t get_offset_of_s_CleanupCallback_0() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields, ___s_CleanupCallback_0)); }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * get_s_CleanupCallback_0() const { return ___s_CleanupCallback_0; }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 ** get_address_of_s_CleanupCallback_0() { return &___s_CleanupCallback_0; }
	inline void set_s_CleanupCallback_0(Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * value)
	{
		___s_CleanupCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CleanupCallback_0), value);
	}

	inline static int32_t get_offset_of_s_CancelErrorCallback_1() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields, ___s_CancelErrorCallback_1)); }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * get_s_CancelErrorCallback_1() const { return ___s_CancelErrorCallback_1; }
	inline Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 ** get_address_of_s_CancelErrorCallback_1() { return &___s_CancelErrorCallback_1; }
	inline void set_s_CancelErrorCallback_1(Callback_t8DF3FD84AB632709C486978BE28ED721EB3A40E3 * value)
	{
		___s_CancelErrorCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelErrorCallback_1), value);
	}

	inline static int32_t get_offset_of_s_CancelErrorQueue_2() { return static_cast<int32_t>(offsetof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields, ___s_CancelErrorQueue_2)); }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * get_s_CancelErrorQueue_2() const { return ___s_CancelErrorQueue_2; }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 ** get_address_of_s_CancelErrorQueue_2() { return &___s_CancelErrorQueue_2; }
	inline void set_s_CancelErrorQueue_2(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * value)
	{
		___s_CancelErrorQueue_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelErrorQueue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPOOL_T7BEC38A8953B38D2C1262D373089B8915ECE020C_H
#ifndef COOKIE_T595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_H
#define COOKIE_T595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cookie
struct  Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90  : public RuntimeObject
{
public:
	// System.String System.Net.Cookie::m_comment
	String_t* ___m_comment_20;
	// System.Uri System.Net.Cookie::m_commentUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_commentUri_21;
	// System.Net.CookieVariant System.Net.Cookie::m_cookieVariant
	int32_t ___m_cookieVariant_22;
	// System.Boolean System.Net.Cookie::m_discard
	bool ___m_discard_23;
	// System.String System.Net.Cookie::m_domain
	String_t* ___m_domain_24;
	// System.Boolean System.Net.Cookie::m_domain_implicit
	bool ___m_domain_implicit_25;
	// System.DateTime System.Net.Cookie::m_expires
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_expires_26;
	// System.String System.Net.Cookie::m_name
	String_t* ___m_name_27;
	// System.String System.Net.Cookie::m_path
	String_t* ___m_path_28;
	// System.Boolean System.Net.Cookie::m_path_implicit
	bool ___m_path_implicit_29;
	// System.String System.Net.Cookie::m_port
	String_t* ___m_port_30;
	// System.Boolean System.Net.Cookie::m_port_implicit
	bool ___m_port_implicit_31;
	// System.Int32[] System.Net.Cookie::m_port_list
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_port_list_32;
	// System.Boolean System.Net.Cookie::m_secure
	bool ___m_secure_33;
	// System.Boolean System.Net.Cookie::m_httpOnly
	bool ___m_httpOnly_34;
	// System.DateTime System.Net.Cookie::m_timeStamp
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_timeStamp_35;
	// System.String System.Net.Cookie::m_value
	String_t* ___m_value_36;
	// System.Int32 System.Net.Cookie::m_version
	int32_t ___m_version_37;
	// System.String System.Net.Cookie::m_domainKey
	String_t* ___m_domainKey_38;
	// System.Boolean System.Net.Cookie::IsQuotedVersion
	bool ___IsQuotedVersion_39;
	// System.Boolean System.Net.Cookie::IsQuotedDomain
	bool ___IsQuotedDomain_40;

public:
	inline static int32_t get_offset_of_m_comment_20() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_comment_20)); }
	inline String_t* get_m_comment_20() const { return ___m_comment_20; }
	inline String_t** get_address_of_m_comment_20() { return &___m_comment_20; }
	inline void set_m_comment_20(String_t* value)
	{
		___m_comment_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_comment_20), value);
	}

	inline static int32_t get_offset_of_m_commentUri_21() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_commentUri_21)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_commentUri_21() const { return ___m_commentUri_21; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_commentUri_21() { return &___m_commentUri_21; }
	inline void set_m_commentUri_21(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_commentUri_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_commentUri_21), value);
	}

	inline static int32_t get_offset_of_m_cookieVariant_22() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_cookieVariant_22)); }
	inline int32_t get_m_cookieVariant_22() const { return ___m_cookieVariant_22; }
	inline int32_t* get_address_of_m_cookieVariant_22() { return &___m_cookieVariant_22; }
	inline void set_m_cookieVariant_22(int32_t value)
	{
		___m_cookieVariant_22 = value;
	}

	inline static int32_t get_offset_of_m_discard_23() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_discard_23)); }
	inline bool get_m_discard_23() const { return ___m_discard_23; }
	inline bool* get_address_of_m_discard_23() { return &___m_discard_23; }
	inline void set_m_discard_23(bool value)
	{
		___m_discard_23 = value;
	}

	inline static int32_t get_offset_of_m_domain_24() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_domain_24)); }
	inline String_t* get_m_domain_24() const { return ___m_domain_24; }
	inline String_t** get_address_of_m_domain_24() { return &___m_domain_24; }
	inline void set_m_domain_24(String_t* value)
	{
		___m_domain_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_domain_24), value);
	}

	inline static int32_t get_offset_of_m_domain_implicit_25() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_domain_implicit_25)); }
	inline bool get_m_domain_implicit_25() const { return ___m_domain_implicit_25; }
	inline bool* get_address_of_m_domain_implicit_25() { return &___m_domain_implicit_25; }
	inline void set_m_domain_implicit_25(bool value)
	{
		___m_domain_implicit_25 = value;
	}

	inline static int32_t get_offset_of_m_expires_26() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_expires_26)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_expires_26() const { return ___m_expires_26; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_expires_26() { return &___m_expires_26; }
	inline void set_m_expires_26(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_expires_26 = value;
	}

	inline static int32_t get_offset_of_m_name_27() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_name_27)); }
	inline String_t* get_m_name_27() const { return ___m_name_27; }
	inline String_t** get_address_of_m_name_27() { return &___m_name_27; }
	inline void set_m_name_27(String_t* value)
	{
		___m_name_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_27), value);
	}

	inline static int32_t get_offset_of_m_path_28() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_path_28)); }
	inline String_t* get_m_path_28() const { return ___m_path_28; }
	inline String_t** get_address_of_m_path_28() { return &___m_path_28; }
	inline void set_m_path_28(String_t* value)
	{
		___m_path_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_path_28), value);
	}

	inline static int32_t get_offset_of_m_path_implicit_29() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_path_implicit_29)); }
	inline bool get_m_path_implicit_29() const { return ___m_path_implicit_29; }
	inline bool* get_address_of_m_path_implicit_29() { return &___m_path_implicit_29; }
	inline void set_m_path_implicit_29(bool value)
	{
		___m_path_implicit_29 = value;
	}

	inline static int32_t get_offset_of_m_port_30() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_port_30)); }
	inline String_t* get_m_port_30() const { return ___m_port_30; }
	inline String_t** get_address_of_m_port_30() { return &___m_port_30; }
	inline void set_m_port_30(String_t* value)
	{
		___m_port_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_port_30), value);
	}

	inline static int32_t get_offset_of_m_port_implicit_31() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_port_implicit_31)); }
	inline bool get_m_port_implicit_31() const { return ___m_port_implicit_31; }
	inline bool* get_address_of_m_port_implicit_31() { return &___m_port_implicit_31; }
	inline void set_m_port_implicit_31(bool value)
	{
		___m_port_implicit_31 = value;
	}

	inline static int32_t get_offset_of_m_port_list_32() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_port_list_32)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_port_list_32() const { return ___m_port_list_32; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_port_list_32() { return &___m_port_list_32; }
	inline void set_m_port_list_32(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_port_list_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_port_list_32), value);
	}

	inline static int32_t get_offset_of_m_secure_33() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_secure_33)); }
	inline bool get_m_secure_33() const { return ___m_secure_33; }
	inline bool* get_address_of_m_secure_33() { return &___m_secure_33; }
	inline void set_m_secure_33(bool value)
	{
		___m_secure_33 = value;
	}

	inline static int32_t get_offset_of_m_httpOnly_34() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_httpOnly_34)); }
	inline bool get_m_httpOnly_34() const { return ___m_httpOnly_34; }
	inline bool* get_address_of_m_httpOnly_34() { return &___m_httpOnly_34; }
	inline void set_m_httpOnly_34(bool value)
	{
		___m_httpOnly_34 = value;
	}

	inline static int32_t get_offset_of_m_timeStamp_35() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_timeStamp_35)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_timeStamp_35() const { return ___m_timeStamp_35; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_timeStamp_35() { return &___m_timeStamp_35; }
	inline void set_m_timeStamp_35(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_timeStamp_35 = value;
	}

	inline static int32_t get_offset_of_m_value_36() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_value_36)); }
	inline String_t* get_m_value_36() const { return ___m_value_36; }
	inline String_t** get_address_of_m_value_36() { return &___m_value_36; }
	inline void set_m_value_36(String_t* value)
	{
		___m_value_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_36), value);
	}

	inline static int32_t get_offset_of_m_version_37() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_version_37)); }
	inline int32_t get_m_version_37() const { return ___m_version_37; }
	inline int32_t* get_address_of_m_version_37() { return &___m_version_37; }
	inline void set_m_version_37(int32_t value)
	{
		___m_version_37 = value;
	}

	inline static int32_t get_offset_of_m_domainKey_38() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___m_domainKey_38)); }
	inline String_t* get_m_domainKey_38() const { return ___m_domainKey_38; }
	inline String_t** get_address_of_m_domainKey_38() { return &___m_domainKey_38; }
	inline void set_m_domainKey_38(String_t* value)
	{
		___m_domainKey_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_domainKey_38), value);
	}

	inline static int32_t get_offset_of_IsQuotedVersion_39() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___IsQuotedVersion_39)); }
	inline bool get_IsQuotedVersion_39() const { return ___IsQuotedVersion_39; }
	inline bool* get_address_of_IsQuotedVersion_39() { return &___IsQuotedVersion_39; }
	inline void set_IsQuotedVersion_39(bool value)
	{
		___IsQuotedVersion_39 = value;
	}

	inline static int32_t get_offset_of_IsQuotedDomain_40() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90, ___IsQuotedDomain_40)); }
	inline bool get_IsQuotedDomain_40() const { return ___IsQuotedDomain_40; }
	inline bool* get_address_of_IsQuotedDomain_40() { return &___IsQuotedDomain_40; }
	inline void set_IsQuotedDomain_40(bool value)
	{
		___IsQuotedDomain_40 = value;
	}
};

struct Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields
{
public:
	// System.Char[] System.Net.Cookie::PortSplitDelimiters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___PortSplitDelimiters_16;
	// System.Char[] System.Net.Cookie::Reserved2Name
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___Reserved2Name_17;
	// System.Char[] System.Net.Cookie::Reserved2Value
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___Reserved2Value_18;
	// System.Net.Comparer System.Net.Cookie::staticComparer
	Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB * ___staticComparer_19;

public:
	inline static int32_t get_offset_of_PortSplitDelimiters_16() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___PortSplitDelimiters_16)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_PortSplitDelimiters_16() const { return ___PortSplitDelimiters_16; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_PortSplitDelimiters_16() { return &___PortSplitDelimiters_16; }
	inline void set_PortSplitDelimiters_16(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___PortSplitDelimiters_16 = value;
		Il2CppCodeGenWriteBarrier((&___PortSplitDelimiters_16), value);
	}

	inline static int32_t get_offset_of_Reserved2Name_17() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___Reserved2Name_17)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_Reserved2Name_17() const { return ___Reserved2Name_17; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_Reserved2Name_17() { return &___Reserved2Name_17; }
	inline void set_Reserved2Name_17(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___Reserved2Name_17 = value;
		Il2CppCodeGenWriteBarrier((&___Reserved2Name_17), value);
	}

	inline static int32_t get_offset_of_Reserved2Value_18() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___Reserved2Value_18)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_Reserved2Value_18() const { return ___Reserved2Value_18; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_Reserved2Value_18() { return &___Reserved2Value_18; }
	inline void set_Reserved2Value_18(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___Reserved2Value_18 = value;
		Il2CppCodeGenWriteBarrier((&___Reserved2Value_18), value);
	}

	inline static int32_t get_offset_of_staticComparer_19() { return static_cast<int32_t>(offsetof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields, ___staticComparer_19)); }
	inline Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB * get_staticComparer_19() const { return ___staticComparer_19; }
	inline Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB ** get_address_of_staticComparer_19() { return &___staticComparer_19; }
	inline void set_staticComparer_19(Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB * value)
	{
		___staticComparer_19 = value;
		Il2CppCodeGenWriteBarrier((&___staticComparer_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIE_T595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_H
#ifndef COOKIEEXCEPTION_T1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4_H
#define COOKIEEXCEPTION_T1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieException
struct  CookieException_t1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4  : public FormatException_t2808E076CDE4650AF89F55FD78F49290D0EC5BDC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEEXCEPTION_T1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4_H
#ifndef COOKIETOKENIZER_T5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_H
#define COOKIETOKENIZER_T5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieTokenizer
struct  CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD  : public RuntimeObject
{
public:
	// System.Boolean System.Net.CookieTokenizer::m_eofCookie
	bool ___m_eofCookie_0;
	// System.Int32 System.Net.CookieTokenizer::m_index
	int32_t ___m_index_1;
	// System.Int32 System.Net.CookieTokenizer::m_length
	int32_t ___m_length_2;
	// System.String System.Net.CookieTokenizer::m_name
	String_t* ___m_name_3;
	// System.Boolean System.Net.CookieTokenizer::m_quoted
	bool ___m_quoted_4;
	// System.Int32 System.Net.CookieTokenizer::m_start
	int32_t ___m_start_5;
	// System.Net.CookieToken System.Net.CookieTokenizer::m_token
	int32_t ___m_token_6;
	// System.Int32 System.Net.CookieTokenizer::m_tokenLength
	int32_t ___m_tokenLength_7;
	// System.String System.Net.CookieTokenizer::m_tokenStream
	String_t* ___m_tokenStream_8;
	// System.String System.Net.CookieTokenizer::m_value
	String_t* ___m_value_9;

public:
	inline static int32_t get_offset_of_m_eofCookie_0() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_eofCookie_0)); }
	inline bool get_m_eofCookie_0() const { return ___m_eofCookie_0; }
	inline bool* get_address_of_m_eofCookie_0() { return &___m_eofCookie_0; }
	inline void set_m_eofCookie_0(bool value)
	{
		___m_eofCookie_0 = value;
	}

	inline static int32_t get_offset_of_m_index_1() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_index_1)); }
	inline int32_t get_m_index_1() const { return ___m_index_1; }
	inline int32_t* get_address_of_m_index_1() { return &___m_index_1; }
	inline void set_m_index_1(int32_t value)
	{
		___m_index_1 = value;
	}

	inline static int32_t get_offset_of_m_length_2() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_length_2)); }
	inline int32_t get_m_length_2() const { return ___m_length_2; }
	inline int32_t* get_address_of_m_length_2() { return &___m_length_2; }
	inline void set_m_length_2(int32_t value)
	{
		___m_length_2 = value;
	}

	inline static int32_t get_offset_of_m_name_3() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_name_3)); }
	inline String_t* get_m_name_3() const { return ___m_name_3; }
	inline String_t** get_address_of_m_name_3() { return &___m_name_3; }
	inline void set_m_name_3(String_t* value)
	{
		___m_name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_3), value);
	}

	inline static int32_t get_offset_of_m_quoted_4() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_quoted_4)); }
	inline bool get_m_quoted_4() const { return ___m_quoted_4; }
	inline bool* get_address_of_m_quoted_4() { return &___m_quoted_4; }
	inline void set_m_quoted_4(bool value)
	{
		___m_quoted_4 = value;
	}

	inline static int32_t get_offset_of_m_start_5() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_start_5)); }
	inline int32_t get_m_start_5() const { return ___m_start_5; }
	inline int32_t* get_address_of_m_start_5() { return &___m_start_5; }
	inline void set_m_start_5(int32_t value)
	{
		___m_start_5 = value;
	}

	inline static int32_t get_offset_of_m_token_6() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_token_6)); }
	inline int32_t get_m_token_6() const { return ___m_token_6; }
	inline int32_t* get_address_of_m_token_6() { return &___m_token_6; }
	inline void set_m_token_6(int32_t value)
	{
		___m_token_6 = value;
	}

	inline static int32_t get_offset_of_m_tokenLength_7() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_tokenLength_7)); }
	inline int32_t get_m_tokenLength_7() const { return ___m_tokenLength_7; }
	inline int32_t* get_address_of_m_tokenLength_7() { return &___m_tokenLength_7; }
	inline void set_m_tokenLength_7(int32_t value)
	{
		___m_tokenLength_7 = value;
	}

	inline static int32_t get_offset_of_m_tokenStream_8() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_tokenStream_8)); }
	inline String_t* get_m_tokenStream_8() const { return ___m_tokenStream_8; }
	inline String_t** get_address_of_m_tokenStream_8() { return &___m_tokenStream_8; }
	inline void set_m_tokenStream_8(String_t* value)
	{
		___m_tokenStream_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_tokenStream_8), value);
	}

	inline static int32_t get_offset_of_m_value_9() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD, ___m_value_9)); }
	inline String_t* get_m_value_9() const { return ___m_value_9; }
	inline String_t** get_address_of_m_value_9() { return &___m_value_9; }
	inline void set_m_value_9(String_t* value)
	{
		___m_value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_value_9), value);
	}
};

struct CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields
{
public:
	// System.Net.CookieTokenizer_RecognizedAttribute[] System.Net.CookieTokenizer::RecognizedAttributes
	RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* ___RecognizedAttributes_10;
	// System.Net.CookieTokenizer_RecognizedAttribute[] System.Net.CookieTokenizer::RecognizedServerAttributes
	RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* ___RecognizedServerAttributes_11;

public:
	inline static int32_t get_offset_of_RecognizedAttributes_10() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields, ___RecognizedAttributes_10)); }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* get_RecognizedAttributes_10() const { return ___RecognizedAttributes_10; }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55** get_address_of_RecognizedAttributes_10() { return &___RecognizedAttributes_10; }
	inline void set_RecognizedAttributes_10(RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* value)
	{
		___RecognizedAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___RecognizedAttributes_10), value);
	}

	inline static int32_t get_offset_of_RecognizedServerAttributes_11() { return static_cast<int32_t>(offsetof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields, ___RecognizedServerAttributes_11)); }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* get_RecognizedServerAttributes_11() const { return ___RecognizedServerAttributes_11; }
	inline RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55** get_address_of_RecognizedServerAttributes_11() { return &___RecognizedServerAttributes_11; }
	inline void set_RecognizedServerAttributes_11(RecognizedAttributeU5BU5D_t7397ED532D4301EECF9C9119CFF1F732D3F75A55* value)
	{
		___RecognizedServerAttributes_11 = value;
		Il2CppCodeGenWriteBarrier((&___RecognizedServerAttributes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIETOKENIZER_T5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_H
#ifndef RECOGNIZEDATTRIBUTE_T300D9F628CDAED6F665BFE996936B9CE0FA0D95B_H
#define RECOGNIZEDATTRIBUTE_T300D9F628CDAED6F665BFE996936B9CE0FA0D95B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieTokenizer_RecognizedAttribute
struct  RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B 
{
public:
	// System.String System.Net.CookieTokenizer_RecognizedAttribute::m_name
	String_t* ___m_name_0;
	// System.Net.CookieToken System.Net.CookieTokenizer_RecognizedAttribute::m_token
	int32_t ___m_token_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_token_1() { return static_cast<int32_t>(offsetof(RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B, ___m_token_1)); }
	inline int32_t get_m_token_1() const { return ___m_token_1; }
	inline int32_t* get_address_of_m_token_1() { return &___m_token_1; }
	inline void set_m_token_1(int32_t value)
	{
		___m_token_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_token_1;
};
// Native definition for COM marshalling of System.Net.CookieTokenizer/RecognizedAttribute
struct RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_token_1;
};
#endif // RECOGNIZEDATTRIBUTE_T300D9F628CDAED6F665BFE996936B9CE0FA0D95B_H
#ifndef FILEWEBRESPONSE_T0F58570D82C33733C9D899AB113B862803A5C325_H
#define FILEWEBRESPONSE_T0F58570D82C33733C9D899AB113B862803A5C325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebResponse
struct  FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325  : public WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD
{
public:
	// System.Boolean System.Net.FileWebResponse::m_closed
	bool ___m_closed_5;
	// System.Int64 System.Net.FileWebResponse::m_contentLength
	int64_t ___m_contentLength_6;
	// System.IO.FileAccess System.Net.FileWebResponse::m_fileAccess
	int32_t ___m_fileAccess_7;
	// System.Net.WebHeaderCollection System.Net.FileWebResponse::m_headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___m_headers_8;
	// System.IO.Stream System.Net.FileWebResponse::m_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_stream_9;
	// System.Uri System.Net.FileWebResponse::m_uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_uri_10;

public:
	inline static int32_t get_offset_of_m_closed_5() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_closed_5)); }
	inline bool get_m_closed_5() const { return ___m_closed_5; }
	inline bool* get_address_of_m_closed_5() { return &___m_closed_5; }
	inline void set_m_closed_5(bool value)
	{
		___m_closed_5 = value;
	}

	inline static int32_t get_offset_of_m_contentLength_6() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_contentLength_6)); }
	inline int64_t get_m_contentLength_6() const { return ___m_contentLength_6; }
	inline int64_t* get_address_of_m_contentLength_6() { return &___m_contentLength_6; }
	inline void set_m_contentLength_6(int64_t value)
	{
		___m_contentLength_6 = value;
	}

	inline static int32_t get_offset_of_m_fileAccess_7() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_fileAccess_7)); }
	inline int32_t get_m_fileAccess_7() const { return ___m_fileAccess_7; }
	inline int32_t* get_address_of_m_fileAccess_7() { return &___m_fileAccess_7; }
	inline void set_m_fileAccess_7(int32_t value)
	{
		___m_fileAccess_7 = value;
	}

	inline static int32_t get_offset_of_m_headers_8() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_headers_8)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_m_headers_8() const { return ___m_headers_8; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_m_headers_8() { return &___m_headers_8; }
	inline void set_m_headers_8(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___m_headers_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_headers_8), value);
	}

	inline static int32_t get_offset_of_m_stream_9() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_stream_9)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_stream_9() const { return ___m_stream_9; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_stream_9() { return &___m_stream_9; }
	inline void set_m_stream_9(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream_9), value);
	}

	inline static int32_t get_offset_of_m_uri_10() { return static_cast<int32_t>(offsetof(FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325, ___m_uri_10)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_uri_10() const { return ___m_uri_10; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_uri_10() { return &___m_uri_10; }
	inline void set_m_uri_10(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_uri_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_uri_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBRESPONSE_T0F58570D82C33733C9D899AB113B862803A5C325_H
#ifndef HEADERVARIANTINFO_TFF12EDB71F2B9508779B160689F99BA209DA9E64_H
#define HEADERVARIANTINFO_TFF12EDB71F2B9508779B160689F99BA209DA9E64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HeaderVariantInfo
struct  HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64 
{
public:
	// System.String System.Net.HeaderVariantInfo::m_name
	String_t* ___m_name_0;
	// System.Net.CookieVariant System.Net.HeaderVariantInfo::m_variant
	int32_t ___m_variant_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_variant_1() { return static_cast<int32_t>(offsetof(HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64, ___m_variant_1)); }
	inline int32_t get_m_variant_1() const { return ___m_variant_1; }
	inline int32_t* get_address_of_m_variant_1() { return &___m_variant_1; }
	inline void set_m_variant_1(int32_t value)
	{
		___m_variant_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64_marshaled_pinvoke
{
	char* ___m_name_0;
	int32_t ___m_variant_1;
};
// Native definition for COM marshalling of System.Net.HeaderVariantInfo
struct HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64_marshaled_com
{
	Il2CppChar* ___m_name_0;
	int32_t ___m_variant_1;
};
#endif // HEADERVARIANTINFO_TFF12EDB71F2B9508779B160689F99BA209DA9E64_H
#ifndef CALLBACKCONTEXT_T1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8_H
#define CALLBACKCONTEXT_T1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ServerCertValidationCallback_CallbackContext
struct  CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8  : public RuntimeObject
{
public:
	// System.Object System.Net.ServerCertValidationCallback_CallbackContext::request
	RuntimeObject * ___request_0;
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Net.ServerCertValidationCallback_CallbackContext::certificate
	X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * ___certificate_1;
	// System.Security.Cryptography.X509Certificates.X509Chain System.Net.ServerCertValidationCallback_CallbackContext::chain
	X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * ___chain_2;
	// System.Net.Security.SslPolicyErrors System.Net.ServerCertValidationCallback_CallbackContext::sslPolicyErrors
	int32_t ___sslPolicyErrors_3;
	// System.Boolean System.Net.ServerCertValidationCallback_CallbackContext::result
	bool ___result_4;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___request_0)); }
	inline RuntimeObject * get_request_0() const { return ___request_0; }
	inline RuntimeObject ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(RuntimeObject * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_certificate_1() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___certificate_1)); }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * get_certificate_1() const { return ___certificate_1; }
	inline X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 ** get_address_of_certificate_1() { return &___certificate_1; }
	inline void set_certificate_1(X509Certificate_t6859B8914E252B6831D6F59A2A720CD23F7FA7B2 * value)
	{
		___certificate_1 = value;
		Il2CppCodeGenWriteBarrier((&___certificate_1), value);
	}

	inline static int32_t get_offset_of_chain_2() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___chain_2)); }
	inline X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * get_chain_2() const { return ___chain_2; }
	inline X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 ** get_address_of_chain_2() { return &___chain_2; }
	inline void set_chain_2(X509Chain_t4A28E9A30CBB331C9B68AE4AFCB30625C6C8B538 * value)
	{
		___chain_2 = value;
		Il2CppCodeGenWriteBarrier((&___chain_2), value);
	}

	inline static int32_t get_offset_of_sslPolicyErrors_3() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___sslPolicyErrors_3)); }
	inline int32_t get_sslPolicyErrors_3() const { return ___sslPolicyErrors_3; }
	inline int32_t* get_address_of_sslPolicyErrors_3() { return &___sslPolicyErrors_3; }
	inline void set_sslPolicyErrors_3(int32_t value)
	{
		___sslPolicyErrors_3 = value;
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8, ___result_4)); }
	inline bool get_result_4() const { return ___result_4; }
	inline bool* get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(bool value)
	{
		___result_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKCONTEXT_T1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8_H
#ifndef WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#define WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebRequest
struct  WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.Net.Security.AuthenticationLevel System.Net.WebRequest::m_AuthenticationLevel
	int32_t ___m_AuthenticationLevel_5;
	// System.Security.Principal.TokenImpersonationLevel System.Net.WebRequest::m_ImpersonationLevel
	int32_t ___m_ImpersonationLevel_6;
	// System.Net.Cache.RequestCachePolicy System.Net.WebRequest::m_CachePolicy
	RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * ___m_CachePolicy_7;
	// System.Net.Cache.RequestCacheProtocol System.Net.WebRequest::m_CacheProtocol
	RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * ___m_CacheProtocol_8;
	// System.Net.Cache.RequestCacheBinding System.Net.WebRequest::m_CacheBinding
	RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * ___m_CacheBinding_9;

public:
	inline static int32_t get_offset_of_m_AuthenticationLevel_5() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_AuthenticationLevel_5)); }
	inline int32_t get_m_AuthenticationLevel_5() const { return ___m_AuthenticationLevel_5; }
	inline int32_t* get_address_of_m_AuthenticationLevel_5() { return &___m_AuthenticationLevel_5; }
	inline void set_m_AuthenticationLevel_5(int32_t value)
	{
		___m_AuthenticationLevel_5 = value;
	}

	inline static int32_t get_offset_of_m_ImpersonationLevel_6() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_ImpersonationLevel_6)); }
	inline int32_t get_m_ImpersonationLevel_6() const { return ___m_ImpersonationLevel_6; }
	inline int32_t* get_address_of_m_ImpersonationLevel_6() { return &___m_ImpersonationLevel_6; }
	inline void set_m_ImpersonationLevel_6(int32_t value)
	{
		___m_ImpersonationLevel_6 = value;
	}

	inline static int32_t get_offset_of_m_CachePolicy_7() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CachePolicy_7)); }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * get_m_CachePolicy_7() const { return ___m_CachePolicy_7; }
	inline RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 ** get_address_of_m_CachePolicy_7() { return &___m_CachePolicy_7; }
	inline void set_m_CachePolicy_7(RequestCachePolicy_t30D7352C7E9D49EEADD492A70EC92C118D90CD61 * value)
	{
		___m_CachePolicy_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePolicy_7), value);
	}

	inline static int32_t get_offset_of_m_CacheProtocol_8() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheProtocol_8)); }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * get_m_CacheProtocol_8() const { return ___m_CacheProtocol_8; }
	inline RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D ** get_address_of_m_CacheProtocol_8() { return &___m_CacheProtocol_8; }
	inline void set_m_CacheProtocol_8(RequestCacheProtocol_t51DE21412EAD66CAD600D3A6940942920340D35D * value)
	{
		___m_CacheProtocol_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheProtocol_8), value);
	}

	inline static int32_t get_offset_of_m_CacheBinding_9() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8, ___m_CacheBinding_9)); }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * get_m_CacheBinding_9() const { return ___m_CacheBinding_9; }
	inline RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 ** get_address_of_m_CacheBinding_9() { return &___m_CacheBinding_9; }
	inline void set_m_CacheBinding_9(RequestCacheBinding_tB84D71781C4BCEF43DEBC72165283C4543BA4724 * value)
	{
		___m_CacheBinding_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheBinding_9), value);
	}
};

struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields
{
public:
	// System.Collections.ArrayList modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_PrefixList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___s_PrefixList_2;
	// System.Object System.Net.WebRequest::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_3;
	// System.Net.TimerThread_Queue System.Net.WebRequest::s_DefaultTimerQueue
	Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * ___s_DefaultTimerQueue_4;
	// System.Net.WebRequest_DesignerWebRequestCreate System.Net.WebRequest::webRequestCreate
	DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * ___webRequestCreate_10;
	// System.Net.IWebProxy modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxy
	RuntimeObject* ___s_DefaultWebProxy_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Net.WebRequest::s_DefaultWebProxyInitialized
	bool ___s_DefaultWebProxyInitialized_12;

public:
	inline static int32_t get_offset_of_s_PrefixList_2() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_PrefixList_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_s_PrefixList_2() const { return ___s_PrefixList_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_s_PrefixList_2() { return &___s_PrefixList_2; }
	inline void set_s_PrefixList_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___s_PrefixList_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PrefixList_2), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_3() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_InternalSyncObject_3)); }
	inline RuntimeObject * get_s_InternalSyncObject_3() const { return ___s_InternalSyncObject_3; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_3() { return &___s_InternalSyncObject_3; }
	inline void set_s_InternalSyncObject_3(RuntimeObject * value)
	{
		___s_InternalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_3), value);
	}

	inline static int32_t get_offset_of_s_DefaultTimerQueue_4() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultTimerQueue_4)); }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * get_s_DefaultTimerQueue_4() const { return ___s_DefaultTimerQueue_4; }
	inline Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 ** get_address_of_s_DefaultTimerQueue_4() { return &___s_DefaultTimerQueue_4; }
	inline void set_s_DefaultTimerQueue_4(Queue_tCCFF6A2FCF584216AEDA04A483FB808E2D493643 * value)
	{
		___s_DefaultTimerQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultTimerQueue_4), value);
	}

	inline static int32_t get_offset_of_webRequestCreate_10() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___webRequestCreate_10)); }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * get_webRequestCreate_10() const { return ___webRequestCreate_10; }
	inline DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 ** get_address_of_webRequestCreate_10() { return &___webRequestCreate_10; }
	inline void set_webRequestCreate_10(DesignerWebRequestCreate_t613DD91D4F07703DC65E847B367F4DCD5710E2A3 * value)
	{
		___webRequestCreate_10 = value;
		Il2CppCodeGenWriteBarrier((&___webRequestCreate_10), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxy_11() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxy_11)); }
	inline RuntimeObject* get_s_DefaultWebProxy_11() const { return ___s_DefaultWebProxy_11; }
	inline RuntimeObject** get_address_of_s_DefaultWebProxy_11() { return &___s_DefaultWebProxy_11; }
	inline void set_s_DefaultWebProxy_11(RuntimeObject* value)
	{
		___s_DefaultWebProxy_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultWebProxy_11), value);
	}

	inline static int32_t get_offset_of_s_DefaultWebProxyInitialized_12() { return static_cast<int32_t>(offsetof(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8_StaticFields, ___s_DefaultWebProxyInitialized_12)); }
	inline bool get_s_DefaultWebProxyInitialized_12() const { return ___s_DefaultWebProxyInitialized_12; }
	inline bool* get_address_of_s_DefaultWebProxyInitialized_12() { return &___s_DefaultWebProxyInitialized_12; }
	inline void set_s_DefaultWebProxyInitialized_12(bool value)
	{
		___s_DefaultWebProxyInitialized_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T5668DA48802E9FE2F1DE5F5A770B218608B918C8_H
#ifndef BINDIPENDPOINT_T6B179B1AD32AF233C8C8E6440DFEF78153A851B9_H
#define BINDIPENDPOINT_T6B179B1AD32AF233C8C8E6440DFEF78153A851B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BindIPEndPoint
struct  BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDIPENDPOINT_T6B179B1AD32AF233C8C8E6440DFEF78153A851B9_H
#ifndef COMPLETIONDELEGATE_T69E611769216705025C6437A9D18BAC6D3F0E674_H
#define COMPLETIONDELEGATE_T69E611769216705025C6437A9D18BAC6D3F0E674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CompletionDelegate
struct  CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETIONDELEGATE_T69E611769216705025C6437A9D18BAC6D3F0E674_H
#ifndef CREATECONNECTIONDELEGATE_TAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B_H
#define CREATECONNECTIONDELEGATE_TAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CreateConnectionDelegate
struct  CreateConnectionDelegate_tAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECONNECTIONDELEGATE_TAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B_H
#ifndef GETHOSTADDRESSESCALLBACK_T72B322C7C0A14B31965D567E9B1FDDB666EEEEE2_H
#define GETHOSTADDRESSESCALLBACK_T72B322C7C0A14B31965D567E9B1FDDB666EEEEE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns_GetHostAddressesCallback
struct  GetHostAddressesCallback_t72B322C7C0A14B31965D567E9B1FDDB666EEEEE2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTADDRESSESCALLBACK_T72B322C7C0A14B31965D567E9B1FDDB666EEEEE2_H
#ifndef GETHOSTBYNAMECALLBACK_T0D6FFE1F93BEAF0CE00ECD4696ADB7BCD005D256_H
#define GETHOSTBYNAMECALLBACK_T0D6FFE1F93BEAF0CE00ECD4696ADB7BCD005D256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns_GetHostByNameCallback
struct  GetHostByNameCallback_t0D6FFE1F93BEAF0CE00ECD4696ADB7BCD005D256  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTBYNAMECALLBACK_T0D6FFE1F93BEAF0CE00ECD4696ADB7BCD005D256_H
#ifndef GETHOSTENTRYIPCALLBACK_TFEDE5E53D72791FF6B55AAD2A1888B9430C67BF3_H
#define GETHOSTENTRYIPCALLBACK_TFEDE5E53D72791FF6B55AAD2A1888B9430C67BF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns_GetHostEntryIPCallback
struct  GetHostEntryIPCallback_tFEDE5E53D72791FF6B55AAD2A1888B9430C67BF3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTENTRYIPCALLBACK_TFEDE5E53D72791FF6B55AAD2A1888B9430C67BF3_H
#ifndef GETHOSTENTRYNAMECALLBACK_TF05C32DA3F038F628D9296E376871A094C83A88C_H
#define GETHOSTENTRYNAMECALLBACK_TF05C32DA3F038F628D9296E376871A094C83A88C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns_GetHostEntryNameCallback
struct  GetHostEntryNameCallback_tF05C32DA3F038F628D9296E376871A094C83A88C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHOSTENTRYNAMECALLBACK_TF05C32DA3F038F628D9296E376871A094C83A88C_H
#ifndef RESOLVECALLBACK_T5D8D8FC669C4D76ED94AB8AFFFAEB54059A55360_H
#define RESOLVECALLBACK_T5D8D8FC669C4D76ED94AB8AFFFAEB54059A55360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Dns_ResolveCallback
struct  ResolveCallback_t5D8D8FC669C4D76ED94AB8AFFFAEB54059A55360  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVECALLBACK_T5D8D8FC669C4D76ED94AB8AFFFAEB54059A55360_H
#ifndef DOWNLOADDATACOMPLETEDEVENTHANDLER_T8FD5DD56F481C0141EB2B89F8668770ED51EFA61_H
#define DOWNLOADDATACOMPLETEDEVENTHANDLER_T8FD5DD56F481C0141EB2B89F8668770ED51EFA61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadDataCompletedEventHandler
struct  DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADDATACOMPLETEDEVENTHANDLER_T8FD5DD56F481C0141EB2B89F8668770ED51EFA61_H
#ifndef DOWNLOADPROGRESSCHANGEDEVENTHANDLER_T7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F_H
#define DOWNLOADPROGRESSCHANGEDEVENTHANDLER_T7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadProgressChangedEventHandler
struct  DownloadProgressChangedEventHandler_t7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADPROGRESSCHANGEDEVENTHANDLER_T7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F_H
#ifndef DOWNLOADSTRINGCOMPLETEDEVENTHANDLER_T41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C_H
#define DOWNLOADSTRINGCOMPLETEDEVENTHANDLER_T41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DownloadStringCompletedEventHandler
struct  DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADSTRINGCOMPLETEDEVENTHANDLER_T41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C_H
#ifndef FILEWEBREQUEST_T198368018F8EBC18AD1A835D585A4B8E235C6E4F_H
#define FILEWEBREQUEST_T198368018F8EBC18AD1A835D585A4B8E235C6E4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebRequest
struct  FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F  : public WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8
{
public:
	// System.String System.Net.FileWebRequest::m_connectionGroupName
	String_t* ___m_connectionGroupName_15;
	// System.Int64 System.Net.FileWebRequest::m_contentLength
	int64_t ___m_contentLength_16;
	// System.Net.ICredentials System.Net.FileWebRequest::m_credentials
	RuntimeObject* ___m_credentials_17;
	// System.IO.FileAccess System.Net.FileWebRequest::m_fileAccess
	int32_t ___m_fileAccess_18;
	// System.Net.WebHeaderCollection System.Net.FileWebRequest::m_headers
	WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * ___m_headers_19;
	// System.String System.Net.FileWebRequest::m_method
	String_t* ___m_method_20;
	// System.Boolean System.Net.FileWebRequest::m_preauthenticate
	bool ___m_preauthenticate_21;
	// System.Net.IWebProxy System.Net.FileWebRequest::m_proxy
	RuntimeObject* ___m_proxy_22;
	// System.Threading.ManualResetEvent System.Net.FileWebRequest::m_readerEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___m_readerEvent_23;
	// System.Boolean System.Net.FileWebRequest::m_readPending
	bool ___m_readPending_24;
	// System.Net.WebResponse System.Net.FileWebRequest::m_response
	WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * ___m_response_25;
	// System.IO.Stream System.Net.FileWebRequest::m_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_stream_26;
	// System.Boolean System.Net.FileWebRequest::m_syncHint
	bool ___m_syncHint_27;
	// System.Int32 System.Net.FileWebRequest::m_timeout
	int32_t ___m_timeout_28;
	// System.Uri System.Net.FileWebRequest::m_uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_uri_29;
	// System.Boolean System.Net.FileWebRequest::m_writePending
	bool ___m_writePending_30;
	// System.Boolean System.Net.FileWebRequest::m_writing
	bool ___m_writing_31;
	// System.Net.LazyAsyncResult System.Net.FileWebRequest::m_WriteAResult
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * ___m_WriteAResult_32;
	// System.Net.LazyAsyncResult System.Net.FileWebRequest::m_ReadAResult
	LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * ___m_ReadAResult_33;
	// System.Int32 System.Net.FileWebRequest::m_Aborted
	int32_t ___m_Aborted_34;

public:
	inline static int32_t get_offset_of_m_connectionGroupName_15() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_connectionGroupName_15)); }
	inline String_t* get_m_connectionGroupName_15() const { return ___m_connectionGroupName_15; }
	inline String_t** get_address_of_m_connectionGroupName_15() { return &___m_connectionGroupName_15; }
	inline void set_m_connectionGroupName_15(String_t* value)
	{
		___m_connectionGroupName_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectionGroupName_15), value);
	}

	inline static int32_t get_offset_of_m_contentLength_16() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_contentLength_16)); }
	inline int64_t get_m_contentLength_16() const { return ___m_contentLength_16; }
	inline int64_t* get_address_of_m_contentLength_16() { return &___m_contentLength_16; }
	inline void set_m_contentLength_16(int64_t value)
	{
		___m_contentLength_16 = value;
	}

	inline static int32_t get_offset_of_m_credentials_17() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_credentials_17)); }
	inline RuntimeObject* get_m_credentials_17() const { return ___m_credentials_17; }
	inline RuntimeObject** get_address_of_m_credentials_17() { return &___m_credentials_17; }
	inline void set_m_credentials_17(RuntimeObject* value)
	{
		___m_credentials_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_credentials_17), value);
	}

	inline static int32_t get_offset_of_m_fileAccess_18() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_fileAccess_18)); }
	inline int32_t get_m_fileAccess_18() const { return ___m_fileAccess_18; }
	inline int32_t* get_address_of_m_fileAccess_18() { return &___m_fileAccess_18; }
	inline void set_m_fileAccess_18(int32_t value)
	{
		___m_fileAccess_18 = value;
	}

	inline static int32_t get_offset_of_m_headers_19() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_headers_19)); }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * get_m_headers_19() const { return ___m_headers_19; }
	inline WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 ** get_address_of_m_headers_19() { return &___m_headers_19; }
	inline void set_m_headers_19(WebHeaderCollection_tB57EC4CD795CACE87271D6887BBED385DC37B304 * value)
	{
		___m_headers_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_headers_19), value);
	}

	inline static int32_t get_offset_of_m_method_20() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_method_20)); }
	inline String_t* get_m_method_20() const { return ___m_method_20; }
	inline String_t** get_address_of_m_method_20() { return &___m_method_20; }
	inline void set_m_method_20(String_t* value)
	{
		___m_method_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_method_20), value);
	}

	inline static int32_t get_offset_of_m_preauthenticate_21() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_preauthenticate_21)); }
	inline bool get_m_preauthenticate_21() const { return ___m_preauthenticate_21; }
	inline bool* get_address_of_m_preauthenticate_21() { return &___m_preauthenticate_21; }
	inline void set_m_preauthenticate_21(bool value)
	{
		___m_preauthenticate_21 = value;
	}

	inline static int32_t get_offset_of_m_proxy_22() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_proxy_22)); }
	inline RuntimeObject* get_m_proxy_22() const { return ___m_proxy_22; }
	inline RuntimeObject** get_address_of_m_proxy_22() { return &___m_proxy_22; }
	inline void set_m_proxy_22(RuntimeObject* value)
	{
		___m_proxy_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_proxy_22), value);
	}

	inline static int32_t get_offset_of_m_readerEvent_23() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_readerEvent_23)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_m_readerEvent_23() const { return ___m_readerEvent_23; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_m_readerEvent_23() { return &___m_readerEvent_23; }
	inline void set_m_readerEvent_23(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___m_readerEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_readerEvent_23), value);
	}

	inline static int32_t get_offset_of_m_readPending_24() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_readPending_24)); }
	inline bool get_m_readPending_24() const { return ___m_readPending_24; }
	inline bool* get_address_of_m_readPending_24() { return &___m_readPending_24; }
	inline void set_m_readPending_24(bool value)
	{
		___m_readPending_24 = value;
	}

	inline static int32_t get_offset_of_m_response_25() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_response_25)); }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * get_m_response_25() const { return ___m_response_25; }
	inline WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD ** get_address_of_m_response_25() { return &___m_response_25; }
	inline void set_m_response_25(WebResponse_t5C91B5B83E2FBA2EABC6FDF2A70E9AFD9BB059BD * value)
	{
		___m_response_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_response_25), value);
	}

	inline static int32_t get_offset_of_m_stream_26() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_stream_26)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_stream_26() const { return ___m_stream_26; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_stream_26() { return &___m_stream_26; }
	inline void set_m_stream_26(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_stream_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream_26), value);
	}

	inline static int32_t get_offset_of_m_syncHint_27() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_syncHint_27)); }
	inline bool get_m_syncHint_27() const { return ___m_syncHint_27; }
	inline bool* get_address_of_m_syncHint_27() { return &___m_syncHint_27; }
	inline void set_m_syncHint_27(bool value)
	{
		___m_syncHint_27 = value;
	}

	inline static int32_t get_offset_of_m_timeout_28() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_timeout_28)); }
	inline int32_t get_m_timeout_28() const { return ___m_timeout_28; }
	inline int32_t* get_address_of_m_timeout_28() { return &___m_timeout_28; }
	inline void set_m_timeout_28(int32_t value)
	{
		___m_timeout_28 = value;
	}

	inline static int32_t get_offset_of_m_uri_29() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_uri_29)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_uri_29() const { return ___m_uri_29; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_uri_29() { return &___m_uri_29; }
	inline void set_m_uri_29(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_uri_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_uri_29), value);
	}

	inline static int32_t get_offset_of_m_writePending_30() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_writePending_30)); }
	inline bool get_m_writePending_30() const { return ___m_writePending_30; }
	inline bool* get_address_of_m_writePending_30() { return &___m_writePending_30; }
	inline void set_m_writePending_30(bool value)
	{
		___m_writePending_30 = value;
	}

	inline static int32_t get_offset_of_m_writing_31() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_writing_31)); }
	inline bool get_m_writing_31() const { return ___m_writing_31; }
	inline bool* get_address_of_m_writing_31() { return &___m_writing_31; }
	inline void set_m_writing_31(bool value)
	{
		___m_writing_31 = value;
	}

	inline static int32_t get_offset_of_m_WriteAResult_32() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_WriteAResult_32)); }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * get_m_WriteAResult_32() const { return ___m_WriteAResult_32; }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 ** get_address_of_m_WriteAResult_32() { return &___m_WriteAResult_32; }
	inline void set_m_WriteAResult_32(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * value)
	{
		___m_WriteAResult_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_WriteAResult_32), value);
	}

	inline static int32_t get_offset_of_m_ReadAResult_33() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_ReadAResult_33)); }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * get_m_ReadAResult_33() const { return ___m_ReadAResult_33; }
	inline LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 ** get_address_of_m_ReadAResult_33() { return &___m_ReadAResult_33; }
	inline void set_m_ReadAResult_33(LazyAsyncResult_t6D867D275402699126BB3DC89093BD94CFFDA5D3 * value)
	{
		___m_ReadAResult_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReadAResult_33), value);
	}

	inline static int32_t get_offset_of_m_Aborted_34() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F, ___m_Aborted_34)); }
	inline int32_t get_m_Aborted_34() const { return ___m_Aborted_34; }
	inline int32_t* get_address_of_m_Aborted_34() { return &___m_Aborted_34; }
	inline void set_m_Aborted_34(int32_t value)
	{
		___m_Aborted_34 = value;
	}
};

struct FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields
{
public:
	// System.Threading.WaitCallback System.Net.FileWebRequest::s_GetRequestStreamCallback
	WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * ___s_GetRequestStreamCallback_13;
	// System.Threading.WaitCallback System.Net.FileWebRequest::s_GetResponseCallback
	WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * ___s_GetResponseCallback_14;

public:
	inline static int32_t get_offset_of_s_GetRequestStreamCallback_13() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields, ___s_GetRequestStreamCallback_13)); }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * get_s_GetRequestStreamCallback_13() const { return ___s_GetRequestStreamCallback_13; }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC ** get_address_of_s_GetRequestStreamCallback_13() { return &___s_GetRequestStreamCallback_13; }
	inline void set_s_GetRequestStreamCallback_13(WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * value)
	{
		___s_GetRequestStreamCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetRequestStreamCallback_13), value);
	}

	inline static int32_t get_offset_of_s_GetResponseCallback_14() { return static_cast<int32_t>(offsetof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields, ___s_GetResponseCallback_14)); }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * get_s_GetResponseCallback_14() const { return ___s_GetResponseCallback_14; }
	inline WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC ** get_address_of_s_GetResponseCallback_14() { return &___s_GetResponseCallback_14; }
	inline void set_s_GetResponseCallback_14(WaitCallback_t61C5F053CAC7A7FE923208EFA060693D7997B4EC * value)
	{
		___s_GetResponseCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_GetResponseCallback_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBREQUEST_T198368018F8EBC18AD1A835D585A4B8E235C6E4F_H
#ifndef FILEWEBSTREAM_T67DDC539EC81FFB9F3615EBE17649E53E1CCA766_H
#define FILEWEBSTREAM_T67DDC539EC81FFB9F3615EBE17649E53E1CCA766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.FileWebStream
struct  FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766  : public FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418
{
public:
	// System.Net.FileWebRequest System.Net.FileWebStream::m_request
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F * ___m_request_22;

public:
	inline static int32_t get_offset_of_m_request_22() { return static_cast<int32_t>(offsetof(FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766, ___m_request_22)); }
	inline FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F * get_m_request_22() const { return ___m_request_22; }
	inline FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F ** get_address_of_m_request_22() { return &___m_request_22; }
	inline void set_m_request_22(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F * value)
	{
		___m_request_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_request_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEWEBSTREAM_T67DDC539EC81FFB9F3615EBE17649E53E1CCA766_H
#ifndef OPENREADCOMPLETEDEVENTHANDLER_TE06E67729CDFBEEDBBE9B75B1601DF6B3740170A_H
#define OPENREADCOMPLETEDEVENTHANDLER_TE06E67729CDFBEEDBBE9B75B1601DF6B3740170A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.OpenReadCompletedEventHandler
struct  OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENREADCOMPLETEDEVENTHANDLER_TE06E67729CDFBEEDBBE9B75B1601DF6B3740170A_H
#ifndef OPENWRITECOMPLETEDEVENTHANDLER_T2013FD9ADB32A901FF622FC3A33D079D7811ABCD_H
#define OPENWRITECOMPLETEDEVENTHANDLER_T2013FD9ADB32A901FF622FC3A33D079D7811ABCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.OpenWriteCompletedEventHandler
struct  OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENWRITECOMPLETEDEVENTHANDLER_T2013FD9ADB32A901FF622FC3A33D079D7811ABCD_H
#ifndef VALIDATEANDPARSEVALUE_T9EA93A8163C8E03CCD9B2A889BCD50CC93EF5D78_H
#define VALIDATEANDPARSEVALUE_T9EA93A8163C8E03CCD9B2A889BCD50CC93EF5D78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.TrackingValidationObjectDictionary_ValidateAndParseValue
struct  ValidateAndParseValue_t9EA93A8163C8E03CCD9B2A889BCD50CC93EF5D78  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATEANDPARSEVALUE_T9EA93A8163C8E03CCD9B2A889BCD50CC93EF5D78_H
#ifndef UPLOADDATACOMPLETEDEVENTHANDLER_TDBFD1A72E580A3D1A40538A326C2611718C78F33_H
#define UPLOADDATACOMPLETEDEVENTHANDLER_TDBFD1A72E580A3D1A40538A326C2611718C78F33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadDataCompletedEventHandler
struct  UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADDATACOMPLETEDEVENTHANDLER_TDBFD1A72E580A3D1A40538A326C2611718C78F33_H
#ifndef UPLOADFILECOMPLETEDEVENTHANDLER_T89A2B681EA334C51505116786C5B5BCFC77A0F86_H
#define UPLOADFILECOMPLETEDEVENTHANDLER_T89A2B681EA334C51505116786C5B5BCFC77A0F86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadFileCompletedEventHandler
struct  UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADFILECOMPLETEDEVENTHANDLER_T89A2B681EA334C51505116786C5B5BCFC77A0F86_H
#ifndef UPLOADPROGRESSCHANGEDEVENTHANDLER_TD3F687D5C0C93A87BA843B34B11570B2B0530B54_H
#define UPLOADPROGRESSCHANGEDEVENTHANDLER_TD3F687D5C0C93A87BA843B34B11570B2B0530B54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadProgressChangedEventHandler
struct  UploadProgressChangedEventHandler_tD3F687D5C0C93A87BA843B34B11570B2B0530B54  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADPROGRESSCHANGEDEVENTHANDLER_TD3F687D5C0C93A87BA843B34B11570B2B0530B54_H
#ifndef UPLOADSTRINGCOMPLETEDEVENTHANDLER_TA3952EC426244567DC7472E6C0B2FA5336743EA6_H
#define UPLOADSTRINGCOMPLETEDEVENTHANDLER_TA3952EC426244567DC7472E6C0B2FA5336743EA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadStringCompletedEventHandler
struct  UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSTRINGCOMPLETEDEVENTHANDLER_TA3952EC426244567DC7472E6C0B2FA5336743EA6_H
#ifndef UPLOADVALUESCOMPLETEDEVENTHANDLER_TE00EE8D708EFADC7C25E1785050C86D5A43395F6_H
#define UPLOADVALUESCOMPLETEDEVENTHANDLER_TE00EE8D708EFADC7C25E1785050C86D5A43395F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.UploadValuesCompletedEventHandler
struct  UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADVALUESCOMPLETEDEVENTHANDLER_TE00EE8D708EFADC7C25E1785050C86D5A43395F6_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (CreateConnectionDelegate_tAD0B51CBFFC28814B798CDB7FC2AFD9427FF038B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C), -1, sizeof(ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3001[27] = 
{
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields::get_offset_of_s_CleanupCallback_0(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields::get_offset_of_s_CancelErrorCallback_1(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C_StaticFields::get_offset_of_s_CancelErrorQueue_2(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_CleanupQueue_10(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_State_11(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_StackOld_12(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_StackNew_13(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_WaitCount_14(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_WaitHandles_15(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_ResError_16(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_ErrorOccured_17(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_ErrorTimer_18(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_ObjectList_19(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_TotalObjects_20(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_QueuedRequests_21(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_AsyncThread_22(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_MaxPoolSize_23(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_MinPoolSize_24(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_ServicePoint_25(),
	ConnectionPool_t7BEC38A8953B38D2C1262D373089B8915ECE020C::get_offset_of_m_CreateConnectionCallback_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (State_t91B9B70A7FEC6854AA0B62E466EEC54C6F7983E1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3002[4] = 
{
	State_t91B9B70A7FEC6854AA0B62E466EEC54C6F7983E1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[4] = 
{
	AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA::get_offset_of_OwningObject_0(),
	AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA::get_offset_of_AsyncCallback_1(),
	AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA::get_offset_of_Pool_2(),
	AsyncConnectionPoolRequest_tC39BE0320275AE9734B38A518415794109CEF1AA::get_offset_of_CreationTimeout_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[1] = 
{
	InterlockedStack_tFCF31BB75AAF94EE95CC970515F20CD84745CB2C::get_offset_of__stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3005[6] = 
{
	CookieVariant_t896D38AC6FBE01ADFB532B04C5E0E19842256CFC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90), -1, sizeof(Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3006[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_PortSplitDelimiters_16(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_Reserved2Name_17(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_Reserved2Value_18(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90_StaticFields::get_offset_of_staticComparer_19(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_comment_20(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_commentUri_21(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_cookieVariant_22(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_discard_23(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_domain_24(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_domain_implicit_25(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_expires_26(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_name_27(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_path_28(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_path_implicit_29(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_port_30(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_port_implicit_31(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_port_list_32(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_secure_33(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_httpOnly_34(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_timeStamp_35(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_value_36(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_version_37(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_m_domainKey_38(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_IsQuotedVersion_39(),
	Cookie_t595E2DCD94CB04B2C07875D5D7C14976F7B1EF90::get_offset_of_IsQuotedDomain_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3007[21] = 
{
	CookieToken_tB2F88831DE62615EAABB9CBF6CCFDFCD0A0D88B8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD), -1, sizeof(CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3008[12] = 
{
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_eofCookie_0(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_index_1(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_length_2(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_name_3(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_quoted_4(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_start_5(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_token_6(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_tokenLength_7(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_tokenStream_8(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD::get_offset_of_m_value_9(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields::get_offset_of_RecognizedAttributes_10(),
	CookieTokenizer_t5B1D0FF62FB109116954C5BE6EB6AA7C71AEC5AD_StaticFields::get_offset_of_RecognizedServerAttributes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B)+ sizeof (RuntimeObject), sizeof(RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3009[2] = 
{
	RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B::get_offset_of_m_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RecognizedAttribute_t300D9F628CDAED6F665BFE996936B9CE0FA0D95B::get_offset_of_m_token_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[2] = 
{
	CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396::get_offset_of_m_tokenizer_0(),
	CookieParser_t6034725CF7B5A3842FEC753620D331478F74B396::get_offset_of_m_savedCookie_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (Comparer_tFC5265AD65740F9DB39C75F1E3EF8032982F45AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[5] = 
{
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_version_0(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_list_1(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_TimeStamp_2(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_has_other_versions_3(),
	CookieCollection_t69ADF0ABD99419E54AB4740B341D94F443D995A3::get_offset_of_m_IsReadOnly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3013[5] = 
{
	Stamp_t06B0F70FA36D78E86543007609E79740E8BB87BE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[4] = 
{
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_cookies_0(),
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_count_1(),
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_index_2(),
	CookieCollectionEnumerator_tDADB2721F8B45D4F815C846DCE2EF92E3760A48D::get_offset_of_m_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64)+ sizeof (RuntimeObject), sizeof(HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3015[2] = 
{
	HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64::get_offset_of_m_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeaderVariantInfo_tFF12EDB71F2B9508779B160689F99BA209DA9E64::get_offset_of_m_variant_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73), -1, sizeof(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3016[10] = 
{
	0,
	0,
	0,
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73_StaticFields::get_offset_of_HeaderInfo_3(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_domainTable_4(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_maxCookieSize_5(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_maxCookies_6(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_maxCookiesPerDomain_7(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_count_8(),
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73::get_offset_of_m_fqdnMyDomain_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[1] = 
{
	PathList_tE89F0E044B0D96268DB20C9B0FC852C690C2DC8A::get_offset_of_m_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF), -1, sizeof(PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3018[1] = 
{
	PathListComparer_tDBE17A93599D72911D22973B739F500E8C26ADCF_StaticFields::get_offset_of_StaticInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (CookieException_t1366ADFB475F67C6BAD72CE2EC1AB504861C2FA4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F), -1, sizeof(FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3020[22] = 
{
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields::get_offset_of_s_GetRequestStreamCallback_13(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F_StaticFields::get_offset_of_s_GetResponseCallback_14(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_connectionGroupName_15(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_contentLength_16(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_credentials_17(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_fileAccess_18(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_headers_19(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_method_20(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_preauthenticate_21(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_proxy_22(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_readerEvent_23(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_readPending_24(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_response_25(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_stream_26(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_syncHint_27(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_timeout_28(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_uri_29(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_writePending_30(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_writing_31(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_WriteAResult_32(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_ReadAResult_33(),
	FileWebRequest_t198368018F8EBC18AD1A835D585A4B8E235C6E4F::get_offset_of_m_Aborted_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (FileWebRequestCreator_tC02A1A70722C45B078D759F22AE10256A6900C6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[1] = 
{
	FileWebStream_t67DDC539EC81FFB9F3615EBE17649E53E1CCA766::get_offset_of_m_request_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[8] = 
{
	0,
	0,
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_closed_5(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_contentLength_6(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_fileAccess_7(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_headers_8(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_stream_9(),
	FileWebResponse_t0F58570D82C33733C9D899AB113B862803A5C325::get_offset_of_m_uri_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0), -1, sizeof(Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3025[7] = 
{
	Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_StaticFields::get_offset_of_base64DecodeMap_7(),
	Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0_StaticFields::get_offset_of_base64EncodeMap_8(),
	Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0::get_offset_of_lineLength_9(),
	Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0::get_offset_of_readState_10(),
	Base64Stream_tF09D7E33A7A8D4252164DAB03351AF2C3F7A30E0::get_offset_of_writeState_11(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463), -1, sizeof(ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3026[6] = 
{
	ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463::get_offset_of_parent_12(),
	ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463::get_offset_of_buffer_13(),
	ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463::get_offset_of_offset_14(),
	ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463::get_offset_of_count_15(),
	ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463::get_offset_of_read_16(),
	ReadAsyncResult_t7E8F5D3534825385A0F0386E23DCCB4F81D16463_StaticFields::get_offset_of_onRead_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB), -1, sizeof(WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3027[6] = 
{
	WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB::get_offset_of_parent_12(),
	WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB::get_offset_of_buffer_13(),
	WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB::get_offset_of_offset_14(),
	WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB::get_offset_of_count_15(),
	WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB_StaticFields::get_offset_of_onWrite_16(),
	WriteAsyncResult_t797D3F470103C5836D4BE6B14D6CD4EA6797E6CB::get_offset_of_written_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[2] = 
{
	ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9::get_offset_of_val_0(),
	ReadStateInfo_t565A55D472ED85BBB4A76B7544030B84B8912DB9::get_offset_of_pos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (ClosableStream_tC64EAEC07968A4B6D2AAABD81A9AB59517F383F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[2] = 
{
	ClosableStream_tC64EAEC07968A4B6D2AAABD81A9AB59517F383F3::get_offset_of_onClose_7(),
	ClosableStream_tC64EAEC07968A4B6D2AAABD81A9AB59517F383F3::get_offset_of_closed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[2] = 
{
	DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC::get_offset_of_stream_5(),
	DelegatedStream_tA1FDA59D05B04F64B575B9D3AC6639D91917BBBC::get_offset_of_netStream_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (TrackingStringDictionary_t053716745947C37C70895827847C4F194773910E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[2] = 
{
	TrackingStringDictionary_t053716745947C37C70895827847C4F194773910E::get_offset_of_isChanged_1(),
	TrackingStringDictionary_t053716745947C37C70895827847C4F194773910E::get_offset_of_isReadOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[3] = 
{
	TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C::get_offset_of_internalObjects_1(),
	TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C::get_offset_of_validators_2(),
	TrackingValidationObjectDictionary_t8B007338BA6B5743538A5FA2F9A187DD52FA9E1C::get_offset_of_U3CIsChangedU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (ValidateAndParseValue_t9EA93A8163C8E03CCD9B2A889BCD50CC93EF5D78), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[46] = 
{
	0,
	0,
	0,
	0,
	0,
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_baseAddress_9(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_credentials_10(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_headers_11(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_requestParameters_12(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_WebResponse_13(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_WebRequest_14(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Encoding_15(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Method_16(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_ContentLength_17(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_InitWebClientAsync_18(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Cancelled_19(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Progress_20(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_Proxy_21(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_ProxySet_22(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_CachePolicy_23(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_U3CAllowReadStreamBufferingU3Ek__BackingField_24(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_U3CAllowWriteStreamBufferingU3Ek__BackingField_25(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_CallNesting_26(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_m_AsyncOp_27(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_OpenReadCompleted_28(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_openReadOperationCompleted_29(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_OpenWriteCompleted_30(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_openWriteOperationCompleted_31(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_DownloadStringCompleted_32(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_downloadStringOperationCompleted_33(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_DownloadDataCompleted_34(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_downloadDataOperationCompleted_35(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_DownloadFileCompleted_36(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_downloadFileOperationCompleted_37(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_UploadStringCompleted_38(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_uploadStringOperationCompleted_39(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_UploadDataCompleted_40(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_uploadDataOperationCompleted_41(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_UploadFileCompleted_42(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_uploadFileOperationCompleted_43(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_UploadValuesCompleted_44(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_uploadValuesOperationCompleted_45(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_DownloadProgressChanged_46(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_UploadProgressChanged_47(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_reportDownloadProgressChanged_48(),
	WebClient_t0BFD938623FF98FA7EEE0031AB9E8293DD295F6A::get_offset_of_reportUploadProgressChanged_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[5] = 
{
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_BytesSent_0(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_TotalBytesToSend_1(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_BytesReceived_2(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_TotalBytesToReceive_3(),
	ProgressData_tE1BDFEEC6B492E4A260BBEF1107683C28BB8AC20::get_offset_of_HasUploadPhase_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[12] = 
{
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_WebClient_0(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_WriteStream_1(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_InnerBuffer_2(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_AsyncOp_3(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_Request_4(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_CompletionDelegate_5(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_ReadStream_6(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_SgBuffers_7(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_ContentLength_8(),
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_Length_9(),
	0,
	DownloadBitsState_t294B3B4DB78D93D45120CA51692E089D96B9DB44::get_offset_of_Progress_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[13] = 
{
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_m_ChunkSize_0(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_m_BufferWritePosition_1(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_WebClient_2(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_WriteStream_3(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_InnerBuffer_4(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_Header_5(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_Footer_6(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_AsyncOp_7(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_Request_8(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_UploadCompletionDelegate_9(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_DownloadCompletionDelegate_10(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_ReadStream_11(),
	UploadBitsState_tA1CEEBE3707A217FE2E122B4CFD0FDAF020EFC48::get_offset_of_Progress_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[3] = 
{
	WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397::get_offset_of_m_request_5(),
	WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397::get_offset_of_m_stream_6(),
	WebClientWriteStream_t7E060E6D900CCC3256E5BFF30537376635158397::get_offset_of_m_WebClient_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[3] = 
{
	U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass219_0_t7B73AAA6A5D162D4621884B5989B86427FC9F326::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703), -1, sizeof(U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3040[19] = 
{
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__219_1_1(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__219_2_2(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__221_1_3(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__221_2_4(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__225_1_5(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__225_2_6(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__229_1_7(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__229_2_8(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__231_1_9(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__231_2_10(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__233_1_11(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__233_2_12(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__237_1_13(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__237_2_14(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__241_1_15(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__241_2_16(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__245_1_17(),
	U3CU3Ec_tA25D420E0876D9D904FC49A6C21CA7C82EB06703_StaticFields::get_offset_of_U3CU3E9__245_2_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[3] = 
{
	U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass221_0_t531C185B30D0EA7CDD8FBDD715CA5E0465F0AAAA::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[3] = 
{
	U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass225_0_t822CCE40F09D735E6EB7DC8C900BFD468673FEB7::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[3] = 
{
	U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass229_0_t4C0623F6A2EE8CB43AF4BF45BCC26AD0E3D820C2::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[3] = 
{
	U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass231_0_t58CD377CEBFBB0F9E2581D2AA33C3D756E512BAC::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[3] = 
{
	U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass233_0_tC1EE9A5459D05903741819AB7D146C9FDA0CA7A0::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[3] = 
{
	U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass237_0_tE92157534B53E3C62BED67AE88D68D30FF73C196::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[3] = 
{
	U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass241_0_tA756424A2D82104ABD6894645FEE056AC0C54341::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3048[3] = 
{
	U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED::get_offset_of_tcs_1(),
	U3CU3Ec__DisplayClass245_0_tA211240DB9A36C6DA5FD4DB6146D59D7DEB037ED::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (CompletionDelegate_t69E611769216705025C6437A9D18BAC6D3F0E674), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (OpenReadCompletedEventHandler_tE06E67729CDFBEEDBBE9B75B1601DF6B3740170A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (OpenReadCompletedEventArgs_tBC33F1F08441BE06D37C9F2DBE7BDA702DB86393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[1] = 
{
	OpenReadCompletedEventArgs_tBC33F1F08441BE06D37C9F2DBE7BDA702DB86393::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (OpenWriteCompletedEventHandler_t2013FD9ADB32A901FF622FC3A33D079D7811ABCD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (OpenWriteCompletedEventArgs_t46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[1] = 
{
	OpenWriteCompletedEventArgs_t46966DD04E4CF693B59AF3A7BEB55CF8FF225CA3::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (DownloadStringCompletedEventHandler_t41D55AA305C624FC26FC72DF0B53A2BC8CAEB98C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (DownloadStringCompletedEventArgs_t9517D2DD80B7233622CBD41E60D06F02E5F327AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[1] = 
{
	DownloadStringCompletedEventArgs_t9517D2DD80B7233622CBD41E60D06F02E5F327AE::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (DownloadDataCompletedEventHandler_t8FD5DD56F481C0141EB2B89F8668770ED51EFA61), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (DownloadDataCompletedEventArgs_t5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[1] = 
{
	DownloadDataCompletedEventArgs_t5DEAA9356BAA533DC0BCBB7135BC2A2353AA5A3C::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (UploadStringCompletedEventHandler_tA3952EC426244567DC7472E6C0B2FA5336743EA6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (UploadStringCompletedEventArgs_t5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[1] = 
{
	UploadStringCompletedEventArgs_t5C48267F6AD6DA863ED1A70E8979A6D0D8C9F8CB::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (UploadDataCompletedEventHandler_tDBFD1A72E580A3D1A40538A326C2611718C78F33), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (UploadDataCompletedEventArgs_t17D573D2463C12EF95555B0CA0135B4FD71A920D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[1] = 
{
	UploadDataCompletedEventArgs_t17D573D2463C12EF95555B0CA0135B4FD71A920D::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (UploadFileCompletedEventHandler_t89A2B681EA334C51505116786C5B5BCFC77A0F86), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (UploadFileCompletedEventArgs_t85C3133630FCC3D7DD1803046F61F5C3F21F6D0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[1] = 
{
	UploadFileCompletedEventArgs_t85C3133630FCC3D7DD1803046F61F5C3F21F6D0A::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (UploadValuesCompletedEventHandler_tE00EE8D708EFADC7C25E1785050C86D5A43395F6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (UploadValuesCompletedEventArgs_t5BC91E51C2B464B1912C651AAAEEE890F7AD3A74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[1] = 
{
	UploadValuesCompletedEventArgs_t5BC91E51C2B464B1912C651AAAEEE890F7AD3A74::get_offset_of_m_Result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (DownloadProgressChangedEventHandler_t7C05FA4ACF3C397B17CD5DD34EF6B4F0EDC5EC3F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[2] = 
{
	DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522::get_offset_of_m_BytesReceived_3(),
	DownloadProgressChangedEventArgs_tACD8549B8D84C4FBA17C2D19FA87E966CCCD4522::get_offset_of_m_TotalBytesToReceive_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (UploadProgressChangedEventHandler_tD3F687D5C0C93A87BA843B34B11570B2B0530B54), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[4] = 
{
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_BytesReceived_3(),
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_TotalBytesToReceive_4(),
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_BytesSent_5(),
	UploadProgressChangedEventArgs_tE606124E632B46CA14E4ED59011DBE3203BE975E::get_offset_of_m_TotalBytesToSend_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[6] = 
{
	WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922::get_offset_of_bypassOnLocal_0(),
	WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922::get_offset_of_automaticallyDetectSettings_1(),
	WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922::get_offset_of_proxyAddress_2(),
	WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922::get_offset_of_proxyHostAddresses_3(),
	WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922::get_offset_of_scriptLocation_4(),
	WebProxyData_t12D22F3302B5ED2B04EA88CE36650BB097E46922::get_offset_of_bypassList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[9] = 
{
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__UseRegistry_0(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__BypassOnLocal_1(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of_m_EnableAutoproxy_2(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__ProxyAddress_3(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__BypassList_4(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__Credentials_5(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__RegExBypassList_6(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of__ProxyHostAddresses_7(),
	WebProxy_t075305900B1D4D8BC8FAB08852842E42BC000417::get_offset_of_m_ScriptEngine_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[2] = 
{
	AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455::get_offset_of_U3CAutomaticConfigurationScriptU3Ek__BackingField_0(),
	AutoWebProxyScriptEngine_tA3B7EF6B73AD21A750868072B07936408AB3B455::get_offset_of_U3CAutomaticallyDetectSettingsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (UnsafeNclNativeMethods_t6130A140E270DE50A48DAA13D71A71F5BD9F1537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F), -1, sizeof(HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3074[3] = 
{
	0,
	0,
	HttpApi_tDD21E6C468ACB472A3D1CE51865417E65113B12F_StaticFields::get_offset_of_m_Strings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE), -1, sizeof(HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3075[1] = 
{
	HTTP_REQUEST_HEADER_ID_tB60B4713336EB0EA6D9963F588F4C9F9BAD3D9CE_StaticFields::get_offset_of_m_Strings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (HTTP_RESPONSE_HEADER_ID_tA74859FBE9C85DA67FD3342E8540DE020868BA79), -1, sizeof(HTTP_RESPONSE_HEADER_ID_tA74859FBE9C85DA67FD3342E8540DE020868BA79_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3076[1] = 
{
	HTTP_RESPONSE_HEADER_ID_tA74859FBE9C85DA67FD3342E8540DE020868BA79_StaticFields::get_offset_of_m_Hashtable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (Enum_t0563A3CEB7BF8F17212F91D1AF0DF4EB1BE13918)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3077[33] = 
{
	Enum_t0563A3CEB7BF8F17212F91D1AF0DF4EB1BE13918::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (SecureStringHelper_t9F5A5E822AB08545A97B612C217CC6C760FF78F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (HttpSysSettings_tD4022F248385DE2D6DC0E56ED065C427791663F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70), -1, sizeof(Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3080[1] = 
{
	Logging_t16C0516C5EFDB044614BF6CBD7044C9AD9C7FF70_StaticFields::get_offset_of_On_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (TraceSource_tB542C6FBC9B31C22CB10F33E872951BADCFA2633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[2] = 
{
	ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB::get_offset_of_m_ValidationCallback_0(),
	ServerCertValidationCallback_t431E949AECAE20901007813737F5B26311F5F9FB::get_offset_of_m_Context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[5] = 
{
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_request_0(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_certificate_1(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_chain_2(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_sslPolicyErrors_3(),
	CallbackContext_t1125F91F146CD802F05C4B2BDD323CBEAE4D4DF8::get_offset_of_result_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012), -1, sizeof(AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3084[3] = 
{
	AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t0C973C7282FB47EAA7E2A239421304D47571B012_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (BasicClient_t691369603F87465F4B5A78CD356545B56ABCA18C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (BindIPEndPoint_t6B179B1AD32AF233C8C8E6440DFEF78153A851B9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[4] = 
{
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_disposed_11(),
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_decoder_12(),
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_context_13(),
	ChunkedInputStream_t10E9B9112B748C89333D9C33D519EDAF17181930::get_offset_of_no_more_data_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[5] = 
{
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Buffer_0(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Offset_1(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Count_2(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_InitialCount_3(),
	ReadBufferState_tBD41DF9062E5FA86A5BFC35E08C1B9224E00585B::get_offset_of_Ares_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3089[4] = 
{
	DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (DefaultCertificatePolicy_t2B28BF921CE86F4956EF998580E48C314B14A674), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9), -1, sizeof(DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3091[5] = 
{
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_header_0(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_length_1(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_pos_2(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9_StaticFields::get_offset_of_keywords_3(),
	DigestHeaderParser_t86D1DE6D1DFE9926C6479D54A3FF221DAEBF0AF9::get_offset_of_values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627), -1, sizeof(DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3092[6] = 
{
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627_StaticFields::get_offset_of_rng_0(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of_lastUse_1(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of__nc_2(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of_hash_3(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of_parser_4(),
	DigestSession_tCFF843AD732355E50F57213B29E91E9016A17627::get_offset_of__cnonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C), -1, sizeof(DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3093[1] = 
{
	DigestClient_t2BDC81F623A5A62E8D1DBC26078CEF3D98CFB32C_StaticFields::get_offset_of_cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (Dns_t0E6B5B77C654107F106B577875FE899BAF8ADCF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (GetHostByNameCallback_t0D6FFE1F93BEAF0CE00ECD4696ADB7BCD005D256), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (ResolveCallback_t5D8D8FC669C4D76ED94AB8AFFFAEB54059A55360), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (GetHostEntryNameCallback_tF05C32DA3F038F628D9296E376871A094C83A88C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (GetHostEntryIPCallback_tFEDE5E53D72791FF6B55AAD2A1888B9430C67BF3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (GetHostAddressesCallback_t72B322C7C0A14B31965D567E9B1FDDB666EEEEE2), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
