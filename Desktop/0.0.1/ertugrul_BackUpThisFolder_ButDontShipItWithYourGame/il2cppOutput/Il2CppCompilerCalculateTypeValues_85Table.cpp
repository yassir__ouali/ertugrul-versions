﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AggroDisabledComponent
struct AggroDisabledComponent_t7D0F3FF57D659CAF0C81F2D14851E44388B01278;
// BattleReviveComponent
struct BattleReviveComponent_t274D262B5A3AA78EBC2831B67D816ED2737C8CDA;
// BlockingSpell
struct BlockingSpell_tEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007;
// ChapterModel
struct ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494;
// ChaptersSO
struct ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1;
// ChatListItemData
struct ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B;
// CoinChangeEvent
struct CoinChangeEvent_t6C548FCB73453B99DB2C3676CE6D004563A80B82;
// CollectableComponent
struct CollectableComponent_t0BD7C71F3589B884C11505B8D6D784632FCF1A5A;
// Contexts
struct Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D;
// CustomMessageData
struct CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD;
// DestroyableComponent
struct DestroyableComponent_t0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC;
// DisposeComponent
struct DisposeComponent_t9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E;
// EffectStartedComponent
struct EffectStartedComponent_tA442FC7E8A7696782A04C2786BEBE9EF91669E4A;
// Entitas.ContextInfo
struct ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED;
// Entitas.EntityComponentChanged
struct EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A;
// Entitas.EntityComponentReplaced
struct EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57;
// Entitas.EntityEvent
struct EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6;
// Entitas.IAERC
struct IAERC_t5B01A73F4F13C01268E7BFAEA2D6397AAA38DDA0;
// Entitas.ICollector`1<GameEntity>
struct ICollector_1_tDA9721194F1438993CA7EC5D39C69B898FBBB26A;
// Entitas.IComponent[]
struct IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B;
// Entitas.IMatcher`1<GameEntity>
struct IMatcher_1_t80816F5068976504A3BE2B63D2354428679B6B77;
// FXSoundComponent
struct FXSoundComponent_t67B072AC87CD6EDDE23219727D01789C036D6600;
// GameContext
struct GameContext_t7DA531FCF38581666B3642E3E737E5B057494978;
// GameSettingsSO
struct GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C;
// GameSparkPatcherTask
struct GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356;
// GameSparkTaskResult
struct GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D;
// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GetLeaderboardTask
struct GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F;
// GlobalSO
struct GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273;
// HeartChangeEvent
struct HeartChangeEvent_tA26E38B4515190BCC8CDA8D960DFFF4F480B4682;
// HomeViewComponent
struct HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818;
// InstantEffectComponent
struct InstantEffectComponent_t208614FDD7E97EAFE28016B6B16193058AFFFFCD;
// InventorySystem
struct InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2;
// InventoryVO
struct InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9;
// ItemsSO
struct ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A;
// KickerManager
struct KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA;
// KickerNodeData
struct KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE;
// KickerWithIconData
struct KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E;
// LeaderboardEntryVO
struct LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737;
// LeaderboardsVO
struct LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6;
// LevelChestComponent
struct LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6;
// LevelVariable
struct LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279;
// LoadCurrentChapterConfigTask
struct LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5;
// LoadMainConfigsTask
struct LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF;
// MainUINode
struct MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4;
// MusicSoundComponent
struct MusicSoundComponent_tB975365C7BD5C382B6D1685AA770668ED79096C4;
// ObjectiveCompleteComponent
struct ObjectiveCompleteComponent_t1AA886C7D93C680C37D82B7A0514801EA0F04852;
// OnQuestEndedEvent
struct OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F;
// OnQuestStartedEvent
struct OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948;
// OnQuestUpdatedEvent
struct OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB;
// PatchingLoadingTask
struct PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC;
// PatchingVO
struct PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4;
// Quest
struct Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021;
// QuestsVO
struct QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429;
// RemoveViewOnDisposeComponent
struct RemoveViewOnDisposeComponent_tE54A9A8E3DB4158A7D2E649412FEACD20965E449;
// SoundLoaderTask
struct SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94;
// SoundSO
struct SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73;
// SpellChangeEvent
struct SpellChangeEvent_tDE9387410E957AFF62D95542A2D2AE619A3A6D9D;
// StarChestComponent
struct StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508;
// StarTournamentSystem
struct StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<CurrencyType,BasicCurrency>
struct Dictionary_2_t1A4877ECBD56B243D0B1BEC96033345054EDD972;
// System.Collections.Generic.Dictionary`2<System.String,Quest>
struct Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793;
// System.Collections.Generic.List`1<Entitas.ICleanupSystem>
struct List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC;
// System.Collections.Generic.List`1<Entitas.IComponent>
struct List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE;
// System.Collections.Generic.List`1<Entitas.IExecuteSystem>
struct List_1_tE232269578E5E48549D25DD0C9823B612D968293;
// System.Collections.Generic.List`1<Entitas.IInitializeSystem>
struct List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C;
// System.Collections.Generic.List`1<Entitas.ITearDownSystem>
struct List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913;
// System.Collections.Generic.List`1<GameEntity>
struct List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29;
// System.Collections.Generic.List`1<GameSparks.Api.Responses.LeaderboardDataResponse/_LeaderboardData>
struct List_1_t0BF8EF0317CCCD765223A954137BAF568F8473F9;
// System.Collections.Generic.List`1<IDisposeListener>
struct List_1_t03D595E408D83DBD7951492238B61E93BABC7A03;
// System.Collections.Generic.List`1<LeaderboardEntryVO>
struct List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<Tayr.Trigger>
struct List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0;
// System.Collections.Generic.Stack`1<Entitas.IComponent>[]
struct Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438;
// System.Func`2<TribeTournamentContributionListItemData,System.Int32>
struct Func_2_t7FB2D586428E9D3F01BCA67C0B8FDB6C041C26BC;
// System.Func`3<GameEntity,Entitas.IComponent,AggroType>
struct Func_3_t9B9377BBD120A3DE9A9967DD7F46BD5497D06E8A;
// System.Func`3<GameEntity,Entitas.IComponent,System.String>
struct Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TMP_InputField
struct TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// Tayr.AssetBundleLoaderTask
struct AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD;
// Tayr.GameSparksPlatform
struct GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348;
// Tayr.ILibrary
struct ILibrary_tCBD3DE1F66AD92DAB1112B8018EBA4A4BD23767C;
// Tayr.INode
struct INode_tD44544594CD9341F4835D560B81E4A956C1D1DC7;
// Tayr.INodeAnimationHandler
struct INodeAnimationHandler_t7A50F84EDDDD8CA150788E8B2A8D0F06E5ABF200;
// Tayr.ITaskContainer
struct ITaskContainer_t2FD3049836F2CE1D1D36D26BE4E990C439E97B3E;
// Tayr.ITaskRunner
struct ITaskRunner_tB1CAE8EDCFC7F57D1F9FCE9C61BE7F3B8BB1389B;
// Tayr.NodeAnimator
struct NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F;
// Tayr.OnTaskCompleted`1<GameSparkTaskResult>
struct OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744;
// Tayr.OnTaskCompleted`1<System.Collections.Generic.List`1<GameSparks.Api.Responses.LeaderboardDataResponse/_LeaderboardData>>
struct OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47;
// Tayr.OnTaskCompleted`1<System.Int32>
struct OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE;
// Tayr.OnTaskCompleted`1<System.String>
struct OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74;
// Tayr.TDynamicList
struct TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19;
// Tayr.TList
struct TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292;
// Tayr.TSoundSystem
struct TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511;
// Tayr.TSystemsManager
struct TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462;
// Tayr.Trigger
struct Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09;
// Tayr.VOSaver
struct VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F;
// TowerHealingComponent
struct TowerHealingComponent_tBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UserEvents
struct UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.DisposableManager
struct DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BASICCURRENCY_TFFB51F40C3056DE62E56D9C4FFB0714758FAD359_H
#define BASICCURRENCY_TFFB51F40C3056DE62E56D9C4FFB0714758FAD359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicCurrency
struct  BasicCurrency_tFFB51F40C3056DE62E56D9C4FFB0714758FAD359  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCURRENCY_TFFB51F40C3056DE62E56D9C4FFB0714758FAD359_H
#ifndef CONTEXTS_T313FEE68C5FB0568E30785C33C0F6209F5F7B87D_H
#define CONTEXTS_T313FEE68C5FB0568E30785C33C0F6209F5F7B87D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Contexts
struct  Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D  : public RuntimeObject
{
public:
	// GameContext Contexts::<game>k__BackingField
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ___U3CgameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CgameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D, ___U3CgameU3Ek__BackingField_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get_U3CgameU3Ek__BackingField_1() const { return ___U3CgameU3Ek__BackingField_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of_U3CgameU3Ek__BackingField_1() { return &___U3CgameU3Ek__BackingField_1; }
	inline void set_U3CgameU3Ek__BackingField_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		___U3CgameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameU3Ek__BackingField_1), value);
	}
};

struct Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D_StaticFields
{
public:
	// Contexts Contexts::_sharedInstance
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____sharedInstance_0;

public:
	inline static int32_t get_offset_of__sharedInstance_0() { return static_cast<int32_t>(offsetof(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D_StaticFields, ____sharedInstance_0)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__sharedInstance_0() const { return ____sharedInstance_0; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__sharedInstance_0() { return &____sharedInstance_0; }
	inline void set__sharedInstance_0(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____sharedInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&____sharedInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTS_T313FEE68C5FB0568E30785C33C0F6209F5F7B87D_H
#ifndef U3CU3EC_TF545A53621D4FF47253EEA6E88AA26FA98B68931_H
#define U3CU3EC_TF545A53621D4FF47253EEA6E88AA26FA98B68931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Contexts_<>c
struct  U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields
{
public:
	// Contexts_<>c Contexts_<>c::<>9
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Contexts_<>c::<>9__10_0
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__10_0_1;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> Contexts_<>c::<>9__24_0
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__24_0_2;
	// System.Func`3<GameEntity,Entitas.IComponent,AggroType> Contexts_<>c::<>9__24_1
	Func_3_t9B9377BBD120A3DE9A9967DD7F46BD5497D06E8A * ___U3CU3E9__24_1_3;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> Contexts_<>c::<>9__24_2
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__24_2_4;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> Contexts_<>c::<>9__24_3
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__24_3_5;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> Contexts_<>c::<>9__24_4
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__24_4_6;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> Contexts_<>c::<>9__24_5
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__24_5_7;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> Contexts_<>c::<>9__24_6
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__24_6_8;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> Contexts_<>c::<>9__24_7
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__24_7_9;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_0_2)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__24_0_2() const { return ___U3CU3E9__24_0_2; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__24_0_2() { return &___U3CU3E9__24_0_2; }
	inline void set_U3CU3E9__24_0_2(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__24_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_1_3)); }
	inline Func_3_t9B9377BBD120A3DE9A9967DD7F46BD5497D06E8A * get_U3CU3E9__24_1_3() const { return ___U3CU3E9__24_1_3; }
	inline Func_3_t9B9377BBD120A3DE9A9967DD7F46BD5497D06E8A ** get_address_of_U3CU3E9__24_1_3() { return &___U3CU3E9__24_1_3; }
	inline void set_U3CU3E9__24_1_3(Func_3_t9B9377BBD120A3DE9A9967DD7F46BD5497D06E8A * value)
	{
		___U3CU3E9__24_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_2_4)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__24_2_4() const { return ___U3CU3E9__24_2_4; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__24_2_4() { return &___U3CU3E9__24_2_4; }
	inline void set_U3CU3E9__24_2_4(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__24_2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_3_5)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__24_3_5() const { return ___U3CU3E9__24_3_5; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__24_3_5() { return &___U3CU3E9__24_3_5; }
	inline void set_U3CU3E9__24_3_5(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__24_3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_4_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_4_6)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__24_4_6() const { return ___U3CU3E9__24_4_6; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__24_4_6() { return &___U3CU3E9__24_4_6; }
	inline void set_U3CU3E9__24_4_6(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__24_4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_5_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_5_7)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__24_5_7() const { return ___U3CU3E9__24_5_7; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__24_5_7() { return &___U3CU3E9__24_5_7; }
	inline void set_U3CU3E9__24_5_7(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__24_5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_6_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_6_8)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__24_6_8() const { return ___U3CU3E9__24_6_8; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__24_6_8() { return &___U3CU3E9__24_6_8; }
	inline void set_U3CU3E9__24_6_8(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__24_6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_6_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_7_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields, ___U3CU3E9__24_7_9)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__24_7_9() const { return ___U3CU3E9__24_7_9; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__24_7_9() { return &___U3CU3E9__24_7_9; }
	inline void set_U3CU3E9__24_7_9(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__24_7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_7_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TF545A53621D4FF47253EEA6E88AA26FA98B68931_H
#ifndef CONTEXTSEXTENSIONS_T2C3E30EF154BB8994481BB2F2B4249468FC6E123_H
#define CONTEXTSEXTENSIONS_T2C3E30EF154BB8994481BB2F2B4249468FC6E123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContextsExtensions
struct  ContextsExtensions_t2C3E30EF154BB8994481BB2F2B4249468FC6E123  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTSEXTENSIONS_T2C3E30EF154BB8994481BB2F2B4249468FC6E123_H
#ifndef CUSTOMMESSAGEDATA_T2BA087DE2A16E4C6D102083A45431AA2E422A6DD_H
#define CUSTOMMESSAGEDATA_T2BA087DE2A16E4C6D102083A45431AA2E422A6DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomMessageData
struct  CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD  : public RuntimeObject
{
public:
	// System.String CustomMessageData::Action
	String_t* ___Action_0;
	// System.String CustomMessageData::TargetId
	String_t* ___TargetId_1;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD, ___Action_0)); }
	inline String_t* get_Action_0() const { return ___Action_0; }
	inline String_t** get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(String_t* value)
	{
		___Action_0 = value;
		Il2CppCodeGenWriteBarrier((&___Action_0), value);
	}

	inline static int32_t get_offset_of_TargetId_1() { return static_cast<int32_t>(offsetof(CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD, ___TargetId_1)); }
	inline String_t* get_TargetId_1() const { return ___TargetId_1; }
	inline String_t** get_address_of_TargetId_1() { return &___TargetId_1; }
	inline void set_TargetId_1(String_t* value)
	{
		___TargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___TargetId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMMESSAGEDATA_T2BA087DE2A16E4C6D102083A45431AA2E422A6DD_H
#ifndef DISPOSELISTENERCOMPONENT_TDB470FB5BF8A06F03BE869C1E3C361691EF33A6E_H
#define DISPOSELISTENERCOMPONENT_TDB470FB5BF8A06F03BE869C1E3C361691EF33A6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisposeListenerComponent
struct  DisposeListenerComponent_tDB470FB5BF8A06F03BE869C1E3C361691EF33A6E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<IDisposeListener> DisposeListenerComponent::value
	List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(DisposeListenerComponent_tDB470FB5BF8A06F03BE869C1E3C361691EF33A6E, ___value_0)); }
	inline List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 * get_value_0() const { return ___value_0; }
	inline List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSELISTENERCOMPONENT_TDB470FB5BF8A06F03BE869C1E3C361691EF33A6E_H
#ifndef ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#define ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Entity
struct  Entity_tB86FED06A87B5FEA836FF73B89D5168789557783  : public RuntimeObject
{
public:
	// Entitas.EntityComponentChanged Entitas.Entity::OnComponentAdded
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___OnComponentAdded_0;
	// Entitas.EntityComponentChanged Entitas.Entity::OnComponentRemoved
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ___OnComponentRemoved_1;
	// Entitas.EntityComponentReplaced Entitas.Entity::OnComponentReplaced
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * ___OnComponentReplaced_2;
	// Entitas.EntityEvent Entitas.Entity::OnEntityReleased
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___OnEntityReleased_3;
	// Entitas.EntityEvent Entitas.Entity::OnDestroyEntity
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ___OnDestroyEntity_4;
	// System.Collections.Generic.List`1<Entitas.IComponent> Entitas.Entity::_componentBuffer
	List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * ____componentBuffer_5;
	// System.Collections.Generic.List`1<System.Int32> Entitas.Entity::_indexBuffer
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____indexBuffer_6;
	// System.Int32 Entitas.Entity::_creationIndex
	int32_t ____creationIndex_7;
	// System.Boolean Entitas.Entity::_isEnabled
	bool ____isEnabled_8;
	// System.Int32 Entitas.Entity::_totalComponents
	int32_t ____totalComponents_9;
	// Entitas.IComponent[] Entitas.Entity::_components
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____components_10;
	// System.Collections.Generic.Stack`1<Entitas.IComponent>[] Entitas.Entity::_componentPools
	Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* ____componentPools_11;
	// Entitas.ContextInfo Entitas.Entity::_contextInfo
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * ____contextInfo_12;
	// Entitas.IAERC Entitas.Entity::_aerc
	RuntimeObject* ____aerc_13;
	// Entitas.IComponent[] Entitas.Entity::_componentsCache
	IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* ____componentsCache_14;
	// System.Int32[] Entitas.Entity::_componentIndicesCache
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____componentIndicesCache_15;
	// System.String Entitas.Entity::_toStringCache
	String_t* ____toStringCache_16;
	// System.Text.StringBuilder Entitas.Entity::_toStringBuilder
	StringBuilder_t * ____toStringBuilder_17;

public:
	inline static int32_t get_offset_of_OnComponentAdded_0() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentAdded_0)); }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * get_OnComponentAdded_0() const { return ___OnComponentAdded_0; }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** get_address_of_OnComponentAdded_0() { return &___OnComponentAdded_0; }
	inline void set_OnComponentAdded_0(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * value)
	{
		___OnComponentAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentAdded_0), value);
	}

	inline static int32_t get_offset_of_OnComponentRemoved_1() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentRemoved_1)); }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * get_OnComponentRemoved_1() const { return ___OnComponentRemoved_1; }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** get_address_of_OnComponentRemoved_1() { return &___OnComponentRemoved_1; }
	inline void set_OnComponentRemoved_1(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * value)
	{
		___OnComponentRemoved_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentRemoved_1), value);
	}

	inline static int32_t get_offset_of_OnComponentReplaced_2() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnComponentReplaced_2)); }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * get_OnComponentReplaced_2() const { return ___OnComponentReplaced_2; }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 ** get_address_of_OnComponentReplaced_2() { return &___OnComponentReplaced_2; }
	inline void set_OnComponentReplaced_2(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * value)
	{
		___OnComponentReplaced_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnComponentReplaced_2), value);
	}

	inline static int32_t get_offset_of_OnEntityReleased_3() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnEntityReleased_3)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get_OnEntityReleased_3() const { return ___OnEntityReleased_3; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of_OnEntityReleased_3() { return &___OnEntityReleased_3; }
	inline void set_OnEntityReleased_3(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		___OnEntityReleased_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnEntityReleased_3), value);
	}

	inline static int32_t get_offset_of_OnDestroyEntity_4() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ___OnDestroyEntity_4)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get_OnDestroyEntity_4() const { return ___OnDestroyEntity_4; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of_OnDestroyEntity_4() { return &___OnDestroyEntity_4; }
	inline void set_OnDestroyEntity_4(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		___OnDestroyEntity_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEntity_4), value);
	}

	inline static int32_t get_offset_of__componentBuffer_5() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentBuffer_5)); }
	inline List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * get__componentBuffer_5() const { return ____componentBuffer_5; }
	inline List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE ** get_address_of__componentBuffer_5() { return &____componentBuffer_5; }
	inline void set__componentBuffer_5(List_1_tD91F3E0C71F5B0B17D56C4CD48174E1BC2FFB9AE * value)
	{
		____componentBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____componentBuffer_5), value);
	}

	inline static int32_t get_offset_of__indexBuffer_6() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____indexBuffer_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__indexBuffer_6() const { return ____indexBuffer_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__indexBuffer_6() { return &____indexBuffer_6; }
	inline void set__indexBuffer_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____indexBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&____indexBuffer_6), value);
	}

	inline static int32_t get_offset_of__creationIndex_7() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____creationIndex_7)); }
	inline int32_t get__creationIndex_7() const { return ____creationIndex_7; }
	inline int32_t* get_address_of__creationIndex_7() { return &____creationIndex_7; }
	inline void set__creationIndex_7(int32_t value)
	{
		____creationIndex_7 = value;
	}

	inline static int32_t get_offset_of__isEnabled_8() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____isEnabled_8)); }
	inline bool get__isEnabled_8() const { return ____isEnabled_8; }
	inline bool* get_address_of__isEnabled_8() { return &____isEnabled_8; }
	inline void set__isEnabled_8(bool value)
	{
		____isEnabled_8 = value;
	}

	inline static int32_t get_offset_of__totalComponents_9() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____totalComponents_9)); }
	inline int32_t get__totalComponents_9() const { return ____totalComponents_9; }
	inline int32_t* get_address_of__totalComponents_9() { return &____totalComponents_9; }
	inline void set__totalComponents_9(int32_t value)
	{
		____totalComponents_9 = value;
	}

	inline static int32_t get_offset_of__components_10() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____components_10)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__components_10() const { return ____components_10; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__components_10() { return &____components_10; }
	inline void set__components_10(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____components_10 = value;
		Il2CppCodeGenWriteBarrier((&____components_10), value);
	}

	inline static int32_t get_offset_of__componentPools_11() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentPools_11)); }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* get__componentPools_11() const { return ____componentPools_11; }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272** get_address_of__componentPools_11() { return &____componentPools_11; }
	inline void set__componentPools_11(Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* value)
	{
		____componentPools_11 = value;
		Il2CppCodeGenWriteBarrier((&____componentPools_11), value);
	}

	inline static int32_t get_offset_of__contextInfo_12() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____contextInfo_12)); }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * get__contextInfo_12() const { return ____contextInfo_12; }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED ** get_address_of__contextInfo_12() { return &____contextInfo_12; }
	inline void set__contextInfo_12(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * value)
	{
		____contextInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&____contextInfo_12), value);
	}

	inline static int32_t get_offset_of__aerc_13() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____aerc_13)); }
	inline RuntimeObject* get__aerc_13() const { return ____aerc_13; }
	inline RuntimeObject** get_address_of__aerc_13() { return &____aerc_13; }
	inline void set__aerc_13(RuntimeObject* value)
	{
		____aerc_13 = value;
		Il2CppCodeGenWriteBarrier((&____aerc_13), value);
	}

	inline static int32_t get_offset_of__componentsCache_14() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentsCache_14)); }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* get__componentsCache_14() const { return ____componentsCache_14; }
	inline IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B** get_address_of__componentsCache_14() { return &____componentsCache_14; }
	inline void set__componentsCache_14(IComponentU5BU5D_tFF2E27ADA201A9FEE088EAD3BA6A3B92F5FF9B0B* value)
	{
		____componentsCache_14 = value;
		Il2CppCodeGenWriteBarrier((&____componentsCache_14), value);
	}

	inline static int32_t get_offset_of__componentIndicesCache_15() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____componentIndicesCache_15)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__componentIndicesCache_15() const { return ____componentIndicesCache_15; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__componentIndicesCache_15() { return &____componentIndicesCache_15; }
	inline void set__componentIndicesCache_15(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____componentIndicesCache_15 = value;
		Il2CppCodeGenWriteBarrier((&____componentIndicesCache_15), value);
	}

	inline static int32_t get_offset_of__toStringCache_16() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____toStringCache_16)); }
	inline String_t* get__toStringCache_16() const { return ____toStringCache_16; }
	inline String_t** get_address_of__toStringCache_16() { return &____toStringCache_16; }
	inline void set__toStringCache_16(String_t* value)
	{
		____toStringCache_16 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_16), value);
	}

	inline static int32_t get_offset_of__toStringBuilder_17() { return static_cast<int32_t>(offsetof(Entity_tB86FED06A87B5FEA836FF73B89D5168789557783, ____toStringBuilder_17)); }
	inline StringBuilder_t * get__toStringBuilder_17() const { return ____toStringBuilder_17; }
	inline StringBuilder_t ** get_address_of__toStringBuilder_17() { return &____toStringBuilder_17; }
	inline void set__toStringBuilder_17(StringBuilder_t * value)
	{
		____toStringBuilder_17 = value;
		Il2CppCodeGenWriteBarrier((&____toStringBuilder_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITY_TB86FED06A87B5FEA836FF73B89D5168789557783_H
#ifndef REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#define REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ReactiveSystem`1<GameEntity>
struct  ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9  : public RuntimeObject
{
public:
	// Entitas.ICollector`1<TEntity> Entitas.ReactiveSystem`1::_collector
	RuntimeObject* ____collector_0;
	// System.Collections.Generic.List`1<TEntity> Entitas.ReactiveSystem`1::_buffer
	List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * ____buffer_1;
	// System.String Entitas.ReactiveSystem`1::_toStringCache
	String_t* ____toStringCache_2;

public:
	inline static int32_t get_offset_of__collector_0() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____collector_0)); }
	inline RuntimeObject* get__collector_0() const { return ____collector_0; }
	inline RuntimeObject** get_address_of__collector_0() { return &____collector_0; }
	inline void set__collector_0(RuntimeObject* value)
	{
		____collector_0 = value;
		Il2CppCodeGenWriteBarrier((&____collector_0), value);
	}

	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____buffer_1)); }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * get__buffer_1() const { return ____buffer_1; }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 ** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_1), value);
	}

	inline static int32_t get_offset_of__toStringCache_2() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____toStringCache_2)); }
	inline String_t* get__toStringCache_2() const { return ____toStringCache_2; }
	inline String_t** get_address_of__toStringCache_2() { return &____toStringCache_2; }
	inline void set__toStringCache_2(String_t* value)
	{
		____toStringCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifndef SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#define SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Systems
struct  Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Entitas.IInitializeSystem> Entitas.Systems::_initializeSystems
	List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * ____initializeSystems_0;
	// System.Collections.Generic.List`1<Entitas.IExecuteSystem> Entitas.Systems::_executeSystems
	List_1_tE232269578E5E48549D25DD0C9823B612D968293 * ____executeSystems_1;
	// System.Collections.Generic.List`1<Entitas.ICleanupSystem> Entitas.Systems::_cleanupSystems
	List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * ____cleanupSystems_2;
	// System.Collections.Generic.List`1<Entitas.ITearDownSystem> Entitas.Systems::_tearDownSystems
	List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * ____tearDownSystems_3;

public:
	inline static int32_t get_offset_of__initializeSystems_0() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____initializeSystems_0)); }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * get__initializeSystems_0() const { return ____initializeSystems_0; }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C ** get_address_of__initializeSystems_0() { return &____initializeSystems_0; }
	inline void set__initializeSystems_0(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * value)
	{
		____initializeSystems_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializeSystems_0), value);
	}

	inline static int32_t get_offset_of__executeSystems_1() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____executeSystems_1)); }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 * get__executeSystems_1() const { return ____executeSystems_1; }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 ** get_address_of__executeSystems_1() { return &____executeSystems_1; }
	inline void set__executeSystems_1(List_1_tE232269578E5E48549D25DD0C9823B612D968293 * value)
	{
		____executeSystems_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeSystems_1), value);
	}

	inline static int32_t get_offset_of__cleanupSystems_2() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____cleanupSystems_2)); }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * get__cleanupSystems_2() const { return ____cleanupSystems_2; }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC ** get_address_of__cleanupSystems_2() { return &____cleanupSystems_2; }
	inline void set__cleanupSystems_2(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * value)
	{
		____cleanupSystems_2 = value;
		Il2CppCodeGenWriteBarrier((&____cleanupSystems_2), value);
	}

	inline static int32_t get_offset_of__tearDownSystems_3() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____tearDownSystems_3)); }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * get__tearDownSystems_3() const { return ____tearDownSystems_3; }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 ** get_address_of__tearDownSystems_3() { return &____tearDownSystems_3; }
	inline void set__tearDownSystems_3(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * value)
	{
		____tearDownSystems_3 = value;
		Il2CppCodeGenWriteBarrier((&____tearDownSystems_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifndef GAMECOMPONENTSLOOKUP_T50E480B87F8B2739A0D03BE1DF2404509E7A44AA_H
#define GAMECOMPONENTSLOOKUP_T50E480B87F8B2739A0D03BE1DF2404509E7A44AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameComponentsLookup
struct  GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA  : public RuntimeObject
{
public:

public:
};

struct GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA_StaticFields
{
public:
	// System.String[] GameComponentsLookup::componentNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___componentNames_116;
	// System.Type[] GameComponentsLookup::componentTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___componentTypes_117;

public:
	inline static int32_t get_offset_of_componentNames_116() { return static_cast<int32_t>(offsetof(GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA_StaticFields, ___componentNames_116)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_componentNames_116() const { return ___componentNames_116; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_componentNames_116() { return &___componentNames_116; }
	inline void set_componentNames_116(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___componentNames_116 = value;
		Il2CppCodeGenWriteBarrier((&___componentNames_116), value);
	}

	inline static int32_t get_offset_of_componentTypes_117() { return static_cast<int32_t>(offsetof(GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA_StaticFields, ___componentTypes_117)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_componentTypes_117() const { return ___componentTypes_117; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_componentTypes_117() { return &___componentTypes_117; }
	inline void set_componentTypes_117(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___componentTypes_117 = value;
		Il2CppCodeGenWriteBarrier((&___componentTypes_117), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECOMPONENTSLOOKUP_T50E480B87F8B2739A0D03BE1DF2404509E7A44AA_H
#ifndef GAMEMATCHER_T84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_H
#define GAMEMATCHER_T84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMatcher
struct  GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5  : public RuntimeObject
{
public:

public:
};

struct GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields
{
public:
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAddGameObjectEffectRender
	RuntimeObject* ____matcherAddGameObjectEffectRender_0;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAggro
	RuntimeObject* ____matcherAggro_1;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAggroDisabled
	RuntimeObject* ____matcherAggroDisabled_2;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAggroHeight
	RuntimeObject* ____matcherAggroHeight_3;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAggroRange
	RuntimeObject* ____matcherAggroRange_4;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAggroStatus
	RuntimeObject* ____matcherAggroStatus_5;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAggroType
	RuntimeObject* ____matcherAggroType_6;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAggroWidth
	RuntimeObject* ____matcherAggroWidth_7;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAnimator
	RuntimeObject* ____matcherAnimator_8;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAnimatorTrigger
	RuntimeObject* ____matcherAnimatorTrigger_9;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAoe
	RuntimeObject* ____matcherAoe_10;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAoeEffectAfterTime
	RuntimeObject* ____matcherAoeEffectAfterTime_11;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAttackBoost
	RuntimeObject* ____matcherAttackBoost_12;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherAttackFrequency
	RuntimeObject* ____matcherAttackFrequency_13;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherBattleDuration
	RuntimeObject* ____matcherBattleDuration_14;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherBattleExtraDuration
	RuntimeObject* ____matcherBattleExtraDuration_15;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherBattleKickerWithIcon
	RuntimeObject* ____matcherBattleKickerWithIcon_16;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherBattleRevive
	RuntimeObject* ____matcherBattleRevive_17;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherBattleTime
	RuntimeObject* ____matcherBattleTime_18;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherBlockingSpell
	RuntimeObject* ____matcherBlockingSpell_19;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherButtonTap
	RuntimeObject* ____matcherButtonTap_20;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherCallouts
	RuntimeObject* ____matcherCallouts_21;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherCameraShake
	RuntimeObject* ____matcherCameraShake_22;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherCart
	RuntimeObject* ____matcherCart_23;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherCollectable
	RuntimeObject* ____matcherCollectable_24;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherCollected
	RuntimeObject* ____matcherCollected_25;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherCompanion
	RuntimeObject* ____matcherCompanion_26;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherCreateTrapAfterTime
	RuntimeObject* ____matcherCreateTrapAfterTime_27;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDamage
	RuntimeObject* ____matcherDamage_28;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDecoy
	RuntimeObject* ____matcherDecoy_29;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDefenseId
	RuntimeObject* ____matcherDefenseId_30;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDefenseSpell
	RuntimeObject* ____matcherDefenseSpell_31;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDefenseSpellController
	RuntimeObject* ____matcherDefenseSpellController_32;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDestroyable
	RuntimeObject* ____matcherDestroyable_33;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDisposeAfterTime
	RuntimeObject* ____matcherDisposeAfterTime_34;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDispose
	RuntimeObject* ____matcherDispose_35;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDisposeListener
	RuntimeObject* ____matcherDisposeListener_36;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDropCollectable
	RuntimeObject* ____matcherDropCollectable_37;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherDuration
	RuntimeObject* ____matcherDuration_38;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherEffect
	RuntimeObject* ____matcherEffect_39;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherEffectRendrer
	RuntimeObject* ____matcherEffectRendrer_40;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherEffectSource
	RuntimeObject* ____matcherEffectSource_41;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherEffectStarted
	RuntimeObject* ____matcherEffectStarted_42;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherEffectStartTime
	RuntimeObject* ____matcherEffectStartTime_43;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherEffectVisual
	RuntimeObject* ____matcherEffectVisual_44;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherFXSound
	RuntimeObject* ____matcherFXSound_45;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherGoal
	RuntimeObject* ____matcherGoal_46;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherGroup
	RuntimeObject* ____matcherGroup_47;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherHero
	RuntimeObject* ____matcherHero_48;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherHeroState
	RuntimeObject* ____matcherHeroState_49;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherHPBoost
	RuntimeObject* ____matcherHPBoost_50;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherHP
	RuntimeObject* ____matcherHP_51;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherId
	RuntimeObject* ____matcherId_52;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherILibrary
	RuntimeObject* ____matcherILibrary_53;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherInitPosition
	RuntimeObject* ____matcherInitPosition_54;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherInstantEffect
	RuntimeObject* ____matcherInstantEffect_55;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherInventory
	RuntimeObject* ____matcherInventory_56;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherInventoryIndex
	RuntimeObject* ____matcherInventoryIndex_57;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherInventoryItem
	RuntimeObject* ____matcherInventoryItem_58;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherInventoryItems
	RuntimeObject* ____matcherInventoryItems_59;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherItem
	RuntimeObject* ____matcherItem_60;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherItemGenerator
	RuntimeObject* ____matcherItemGenerator_61;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherItemsSpawnPoints
	RuntimeObject* ____matcherItemsSpawnPoints_62;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherJoystick
	RuntimeObject* ____matcherJoystick_63;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherLastAttackTime
	RuntimeObject* ____matcherLastAttackTime_64;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherLeftHand
	RuntimeObject* ____matcherLeftHand_65;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherLevel
	RuntimeObject* ____matcherLevel_66;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherLifetimeEffect
	RuntimeObject* ____matcherLifetimeEffect_67;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherLoopingAI
	RuntimeObject* ____matcherLoopingAI_68;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherMapItem
	RuntimeObject* ____matcherMapItem_69;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherMaxHP
	RuntimeObject* ____matcherMaxHP_70;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherMusicSound
	RuntimeObject* ____matcherMusicSound_71;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherObjectiveComplete
	RuntimeObject* ____matcherObjectiveComplete_72;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherObjectiveCounter
	RuntimeObject* ____matcherObjectiveCounter_73;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherObjectives
	RuntimeObject* ____matcherObjectives_74;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherObjectiveStep
	RuntimeObject* ____matcherObjectiveStep_75;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherObjectiveType
	RuntimeObject* ____matcherObjectiveType_76;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherParent
	RuntimeObject* ____matcherParent_77;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherPosition
	RuntimeObject* ____matcherPosition_78;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherProjectile
	RuntimeObject* ____matcherProjectile_79;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherProjectileSource
	RuntimeObject* ____matcherProjectileSource_80;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherProjectileSpell
	RuntimeObject* ____matcherProjectileSpell_81;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherProjectileStartPosition
	RuntimeObject* ____matcherProjectileStartPosition_82;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherProjectileTargetEntity
	RuntimeObject* ____matcherProjectileTargetEntity_83;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherPulseAI
	RuntimeObject* ____matcherPulseAI_84;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherRemoveViewOnDispose
	RuntimeObject* ____matcherRemoveViewOnDispose_85;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherResist
	RuntimeObject* ____matcherResist_86;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherRotation
	RuntimeObject* ____matcherRotation_87;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherScreenTap
	RuntimeObject* ____matcherScreenTap_88;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSlowSpeed
	RuntimeObject* ____matcherSlowSpeed_89;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSound
	RuntimeObject* ____matcherSound_90;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSpeedBoost
	RuntimeObject* ____matcherSpeedBoost_91;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSpeed
	RuntimeObject* ____matcherSpeed_92;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSpell
	RuntimeObject* ____matcherSpell_93;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSpellController
	RuntimeObject* ____matcherSpellController_94;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSpellIndex
	RuntimeObject* ____matcherSpellIndex_95;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSpellMeta
	RuntimeObject* ____matcherSpellMeta_96;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSpellStatus
	RuntimeObject* ____matcherSpellStatus_97;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherSphereCollider
	RuntimeObject* ____matcherSphereCollider_98;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherStartTime
	RuntimeObject* ____matcherStartTime_99;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTapTapGroundRecognizer
	RuntimeObject* ____matcherTapTapGroundRecognizer_100;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTapTapTowerRecognizer
	RuntimeObject* ____matcherTapTapTowerRecognizer_101;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTower
	RuntimeObject* ____matcherTower_102;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTowerHealing
	RuntimeObject* ____matcherTowerHealing_103;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTowerHealingDuration
	RuntimeObject* ____matcherTowerHealingDuration_104;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTowerHealingStartTime
	RuntimeObject* ____matcherTowerHealingStartTime_105;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTowerHPBar
	RuntimeObject* ____matcherTowerHPBar_106;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTowerRender
	RuntimeObject* ____matcherTowerRender_107;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTowerTap
	RuntimeObject* ____matcherTowerTap_108;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTowerTop
	RuntimeObject* ____matcherTowerTop_109;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherTrap
	RuntimeObject* ____matcherTrap_110;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherUIHolder
	RuntimeObject* ____matcherUIHolder_111;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherView
	RuntimeObject* ____matcherView_112;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherVisual
	RuntimeObject* ____matcherVisual_113;
	// Entitas.IMatcher`1<GameEntity> GameMatcher::_matcherWall
	RuntimeObject* ____matcherWall_114;

public:
	inline static int32_t get_offset_of__matcherAddGameObjectEffectRender_0() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAddGameObjectEffectRender_0)); }
	inline RuntimeObject* get__matcherAddGameObjectEffectRender_0() const { return ____matcherAddGameObjectEffectRender_0; }
	inline RuntimeObject** get_address_of__matcherAddGameObjectEffectRender_0() { return &____matcherAddGameObjectEffectRender_0; }
	inline void set__matcherAddGameObjectEffectRender_0(RuntimeObject* value)
	{
		____matcherAddGameObjectEffectRender_0 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAddGameObjectEffectRender_0), value);
	}

	inline static int32_t get_offset_of__matcherAggro_1() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAggro_1)); }
	inline RuntimeObject* get__matcherAggro_1() const { return ____matcherAggro_1; }
	inline RuntimeObject** get_address_of__matcherAggro_1() { return &____matcherAggro_1; }
	inline void set__matcherAggro_1(RuntimeObject* value)
	{
		____matcherAggro_1 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAggro_1), value);
	}

	inline static int32_t get_offset_of__matcherAggroDisabled_2() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAggroDisabled_2)); }
	inline RuntimeObject* get__matcherAggroDisabled_2() const { return ____matcherAggroDisabled_2; }
	inline RuntimeObject** get_address_of__matcherAggroDisabled_2() { return &____matcherAggroDisabled_2; }
	inline void set__matcherAggroDisabled_2(RuntimeObject* value)
	{
		____matcherAggroDisabled_2 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAggroDisabled_2), value);
	}

	inline static int32_t get_offset_of__matcherAggroHeight_3() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAggroHeight_3)); }
	inline RuntimeObject* get__matcherAggroHeight_3() const { return ____matcherAggroHeight_3; }
	inline RuntimeObject** get_address_of__matcherAggroHeight_3() { return &____matcherAggroHeight_3; }
	inline void set__matcherAggroHeight_3(RuntimeObject* value)
	{
		____matcherAggroHeight_3 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAggroHeight_3), value);
	}

	inline static int32_t get_offset_of__matcherAggroRange_4() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAggroRange_4)); }
	inline RuntimeObject* get__matcherAggroRange_4() const { return ____matcherAggroRange_4; }
	inline RuntimeObject** get_address_of__matcherAggroRange_4() { return &____matcherAggroRange_4; }
	inline void set__matcherAggroRange_4(RuntimeObject* value)
	{
		____matcherAggroRange_4 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAggroRange_4), value);
	}

	inline static int32_t get_offset_of__matcherAggroStatus_5() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAggroStatus_5)); }
	inline RuntimeObject* get__matcherAggroStatus_5() const { return ____matcherAggroStatus_5; }
	inline RuntimeObject** get_address_of__matcherAggroStatus_5() { return &____matcherAggroStatus_5; }
	inline void set__matcherAggroStatus_5(RuntimeObject* value)
	{
		____matcherAggroStatus_5 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAggroStatus_5), value);
	}

	inline static int32_t get_offset_of__matcherAggroType_6() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAggroType_6)); }
	inline RuntimeObject* get__matcherAggroType_6() const { return ____matcherAggroType_6; }
	inline RuntimeObject** get_address_of__matcherAggroType_6() { return &____matcherAggroType_6; }
	inline void set__matcherAggroType_6(RuntimeObject* value)
	{
		____matcherAggroType_6 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAggroType_6), value);
	}

	inline static int32_t get_offset_of__matcherAggroWidth_7() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAggroWidth_7)); }
	inline RuntimeObject* get__matcherAggroWidth_7() const { return ____matcherAggroWidth_7; }
	inline RuntimeObject** get_address_of__matcherAggroWidth_7() { return &____matcherAggroWidth_7; }
	inline void set__matcherAggroWidth_7(RuntimeObject* value)
	{
		____matcherAggroWidth_7 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAggroWidth_7), value);
	}

	inline static int32_t get_offset_of__matcherAnimator_8() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAnimator_8)); }
	inline RuntimeObject* get__matcherAnimator_8() const { return ____matcherAnimator_8; }
	inline RuntimeObject** get_address_of__matcherAnimator_8() { return &____matcherAnimator_8; }
	inline void set__matcherAnimator_8(RuntimeObject* value)
	{
		____matcherAnimator_8 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAnimator_8), value);
	}

	inline static int32_t get_offset_of__matcherAnimatorTrigger_9() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAnimatorTrigger_9)); }
	inline RuntimeObject* get__matcherAnimatorTrigger_9() const { return ____matcherAnimatorTrigger_9; }
	inline RuntimeObject** get_address_of__matcherAnimatorTrigger_9() { return &____matcherAnimatorTrigger_9; }
	inline void set__matcherAnimatorTrigger_9(RuntimeObject* value)
	{
		____matcherAnimatorTrigger_9 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAnimatorTrigger_9), value);
	}

	inline static int32_t get_offset_of__matcherAoe_10() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAoe_10)); }
	inline RuntimeObject* get__matcherAoe_10() const { return ____matcherAoe_10; }
	inline RuntimeObject** get_address_of__matcherAoe_10() { return &____matcherAoe_10; }
	inline void set__matcherAoe_10(RuntimeObject* value)
	{
		____matcherAoe_10 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAoe_10), value);
	}

	inline static int32_t get_offset_of__matcherAoeEffectAfterTime_11() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAoeEffectAfterTime_11)); }
	inline RuntimeObject* get__matcherAoeEffectAfterTime_11() const { return ____matcherAoeEffectAfterTime_11; }
	inline RuntimeObject** get_address_of__matcherAoeEffectAfterTime_11() { return &____matcherAoeEffectAfterTime_11; }
	inline void set__matcherAoeEffectAfterTime_11(RuntimeObject* value)
	{
		____matcherAoeEffectAfterTime_11 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAoeEffectAfterTime_11), value);
	}

	inline static int32_t get_offset_of__matcherAttackBoost_12() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAttackBoost_12)); }
	inline RuntimeObject* get__matcherAttackBoost_12() const { return ____matcherAttackBoost_12; }
	inline RuntimeObject** get_address_of__matcherAttackBoost_12() { return &____matcherAttackBoost_12; }
	inline void set__matcherAttackBoost_12(RuntimeObject* value)
	{
		____matcherAttackBoost_12 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAttackBoost_12), value);
	}

	inline static int32_t get_offset_of__matcherAttackFrequency_13() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherAttackFrequency_13)); }
	inline RuntimeObject* get__matcherAttackFrequency_13() const { return ____matcherAttackFrequency_13; }
	inline RuntimeObject** get_address_of__matcherAttackFrequency_13() { return &____matcherAttackFrequency_13; }
	inline void set__matcherAttackFrequency_13(RuntimeObject* value)
	{
		____matcherAttackFrequency_13 = value;
		Il2CppCodeGenWriteBarrier((&____matcherAttackFrequency_13), value);
	}

	inline static int32_t get_offset_of__matcherBattleDuration_14() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherBattleDuration_14)); }
	inline RuntimeObject* get__matcherBattleDuration_14() const { return ____matcherBattleDuration_14; }
	inline RuntimeObject** get_address_of__matcherBattleDuration_14() { return &____matcherBattleDuration_14; }
	inline void set__matcherBattleDuration_14(RuntimeObject* value)
	{
		____matcherBattleDuration_14 = value;
		Il2CppCodeGenWriteBarrier((&____matcherBattleDuration_14), value);
	}

	inline static int32_t get_offset_of__matcherBattleExtraDuration_15() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherBattleExtraDuration_15)); }
	inline RuntimeObject* get__matcherBattleExtraDuration_15() const { return ____matcherBattleExtraDuration_15; }
	inline RuntimeObject** get_address_of__matcherBattleExtraDuration_15() { return &____matcherBattleExtraDuration_15; }
	inline void set__matcherBattleExtraDuration_15(RuntimeObject* value)
	{
		____matcherBattleExtraDuration_15 = value;
		Il2CppCodeGenWriteBarrier((&____matcherBattleExtraDuration_15), value);
	}

	inline static int32_t get_offset_of__matcherBattleKickerWithIcon_16() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherBattleKickerWithIcon_16)); }
	inline RuntimeObject* get__matcherBattleKickerWithIcon_16() const { return ____matcherBattleKickerWithIcon_16; }
	inline RuntimeObject** get_address_of__matcherBattleKickerWithIcon_16() { return &____matcherBattleKickerWithIcon_16; }
	inline void set__matcherBattleKickerWithIcon_16(RuntimeObject* value)
	{
		____matcherBattleKickerWithIcon_16 = value;
		Il2CppCodeGenWriteBarrier((&____matcherBattleKickerWithIcon_16), value);
	}

	inline static int32_t get_offset_of__matcherBattleRevive_17() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherBattleRevive_17)); }
	inline RuntimeObject* get__matcherBattleRevive_17() const { return ____matcherBattleRevive_17; }
	inline RuntimeObject** get_address_of__matcherBattleRevive_17() { return &____matcherBattleRevive_17; }
	inline void set__matcherBattleRevive_17(RuntimeObject* value)
	{
		____matcherBattleRevive_17 = value;
		Il2CppCodeGenWriteBarrier((&____matcherBattleRevive_17), value);
	}

	inline static int32_t get_offset_of__matcherBattleTime_18() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherBattleTime_18)); }
	inline RuntimeObject* get__matcherBattleTime_18() const { return ____matcherBattleTime_18; }
	inline RuntimeObject** get_address_of__matcherBattleTime_18() { return &____matcherBattleTime_18; }
	inline void set__matcherBattleTime_18(RuntimeObject* value)
	{
		____matcherBattleTime_18 = value;
		Il2CppCodeGenWriteBarrier((&____matcherBattleTime_18), value);
	}

	inline static int32_t get_offset_of__matcherBlockingSpell_19() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherBlockingSpell_19)); }
	inline RuntimeObject* get__matcherBlockingSpell_19() const { return ____matcherBlockingSpell_19; }
	inline RuntimeObject** get_address_of__matcherBlockingSpell_19() { return &____matcherBlockingSpell_19; }
	inline void set__matcherBlockingSpell_19(RuntimeObject* value)
	{
		____matcherBlockingSpell_19 = value;
		Il2CppCodeGenWriteBarrier((&____matcherBlockingSpell_19), value);
	}

	inline static int32_t get_offset_of__matcherButtonTap_20() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherButtonTap_20)); }
	inline RuntimeObject* get__matcherButtonTap_20() const { return ____matcherButtonTap_20; }
	inline RuntimeObject** get_address_of__matcherButtonTap_20() { return &____matcherButtonTap_20; }
	inline void set__matcherButtonTap_20(RuntimeObject* value)
	{
		____matcherButtonTap_20 = value;
		Il2CppCodeGenWriteBarrier((&____matcherButtonTap_20), value);
	}

	inline static int32_t get_offset_of__matcherCallouts_21() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherCallouts_21)); }
	inline RuntimeObject* get__matcherCallouts_21() const { return ____matcherCallouts_21; }
	inline RuntimeObject** get_address_of__matcherCallouts_21() { return &____matcherCallouts_21; }
	inline void set__matcherCallouts_21(RuntimeObject* value)
	{
		____matcherCallouts_21 = value;
		Il2CppCodeGenWriteBarrier((&____matcherCallouts_21), value);
	}

	inline static int32_t get_offset_of__matcherCameraShake_22() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherCameraShake_22)); }
	inline RuntimeObject* get__matcherCameraShake_22() const { return ____matcherCameraShake_22; }
	inline RuntimeObject** get_address_of__matcherCameraShake_22() { return &____matcherCameraShake_22; }
	inline void set__matcherCameraShake_22(RuntimeObject* value)
	{
		____matcherCameraShake_22 = value;
		Il2CppCodeGenWriteBarrier((&____matcherCameraShake_22), value);
	}

	inline static int32_t get_offset_of__matcherCart_23() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherCart_23)); }
	inline RuntimeObject* get__matcherCart_23() const { return ____matcherCart_23; }
	inline RuntimeObject** get_address_of__matcherCart_23() { return &____matcherCart_23; }
	inline void set__matcherCart_23(RuntimeObject* value)
	{
		____matcherCart_23 = value;
		Il2CppCodeGenWriteBarrier((&____matcherCart_23), value);
	}

	inline static int32_t get_offset_of__matcherCollectable_24() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherCollectable_24)); }
	inline RuntimeObject* get__matcherCollectable_24() const { return ____matcherCollectable_24; }
	inline RuntimeObject** get_address_of__matcherCollectable_24() { return &____matcherCollectable_24; }
	inline void set__matcherCollectable_24(RuntimeObject* value)
	{
		____matcherCollectable_24 = value;
		Il2CppCodeGenWriteBarrier((&____matcherCollectable_24), value);
	}

	inline static int32_t get_offset_of__matcherCollected_25() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherCollected_25)); }
	inline RuntimeObject* get__matcherCollected_25() const { return ____matcherCollected_25; }
	inline RuntimeObject** get_address_of__matcherCollected_25() { return &____matcherCollected_25; }
	inline void set__matcherCollected_25(RuntimeObject* value)
	{
		____matcherCollected_25 = value;
		Il2CppCodeGenWriteBarrier((&____matcherCollected_25), value);
	}

	inline static int32_t get_offset_of__matcherCompanion_26() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherCompanion_26)); }
	inline RuntimeObject* get__matcherCompanion_26() const { return ____matcherCompanion_26; }
	inline RuntimeObject** get_address_of__matcherCompanion_26() { return &____matcherCompanion_26; }
	inline void set__matcherCompanion_26(RuntimeObject* value)
	{
		____matcherCompanion_26 = value;
		Il2CppCodeGenWriteBarrier((&____matcherCompanion_26), value);
	}

	inline static int32_t get_offset_of__matcherCreateTrapAfterTime_27() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherCreateTrapAfterTime_27)); }
	inline RuntimeObject* get__matcherCreateTrapAfterTime_27() const { return ____matcherCreateTrapAfterTime_27; }
	inline RuntimeObject** get_address_of__matcherCreateTrapAfterTime_27() { return &____matcherCreateTrapAfterTime_27; }
	inline void set__matcherCreateTrapAfterTime_27(RuntimeObject* value)
	{
		____matcherCreateTrapAfterTime_27 = value;
		Il2CppCodeGenWriteBarrier((&____matcherCreateTrapAfterTime_27), value);
	}

	inline static int32_t get_offset_of__matcherDamage_28() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDamage_28)); }
	inline RuntimeObject* get__matcherDamage_28() const { return ____matcherDamage_28; }
	inline RuntimeObject** get_address_of__matcherDamage_28() { return &____matcherDamage_28; }
	inline void set__matcherDamage_28(RuntimeObject* value)
	{
		____matcherDamage_28 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDamage_28), value);
	}

	inline static int32_t get_offset_of__matcherDecoy_29() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDecoy_29)); }
	inline RuntimeObject* get__matcherDecoy_29() const { return ____matcherDecoy_29; }
	inline RuntimeObject** get_address_of__matcherDecoy_29() { return &____matcherDecoy_29; }
	inline void set__matcherDecoy_29(RuntimeObject* value)
	{
		____matcherDecoy_29 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDecoy_29), value);
	}

	inline static int32_t get_offset_of__matcherDefenseId_30() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDefenseId_30)); }
	inline RuntimeObject* get__matcherDefenseId_30() const { return ____matcherDefenseId_30; }
	inline RuntimeObject** get_address_of__matcherDefenseId_30() { return &____matcherDefenseId_30; }
	inline void set__matcherDefenseId_30(RuntimeObject* value)
	{
		____matcherDefenseId_30 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDefenseId_30), value);
	}

	inline static int32_t get_offset_of__matcherDefenseSpell_31() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDefenseSpell_31)); }
	inline RuntimeObject* get__matcherDefenseSpell_31() const { return ____matcherDefenseSpell_31; }
	inline RuntimeObject** get_address_of__matcherDefenseSpell_31() { return &____matcherDefenseSpell_31; }
	inline void set__matcherDefenseSpell_31(RuntimeObject* value)
	{
		____matcherDefenseSpell_31 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDefenseSpell_31), value);
	}

	inline static int32_t get_offset_of__matcherDefenseSpellController_32() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDefenseSpellController_32)); }
	inline RuntimeObject* get__matcherDefenseSpellController_32() const { return ____matcherDefenseSpellController_32; }
	inline RuntimeObject** get_address_of__matcherDefenseSpellController_32() { return &____matcherDefenseSpellController_32; }
	inline void set__matcherDefenseSpellController_32(RuntimeObject* value)
	{
		____matcherDefenseSpellController_32 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDefenseSpellController_32), value);
	}

	inline static int32_t get_offset_of__matcherDestroyable_33() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDestroyable_33)); }
	inline RuntimeObject* get__matcherDestroyable_33() const { return ____matcherDestroyable_33; }
	inline RuntimeObject** get_address_of__matcherDestroyable_33() { return &____matcherDestroyable_33; }
	inline void set__matcherDestroyable_33(RuntimeObject* value)
	{
		____matcherDestroyable_33 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDestroyable_33), value);
	}

	inline static int32_t get_offset_of__matcherDisposeAfterTime_34() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDisposeAfterTime_34)); }
	inline RuntimeObject* get__matcherDisposeAfterTime_34() const { return ____matcherDisposeAfterTime_34; }
	inline RuntimeObject** get_address_of__matcherDisposeAfterTime_34() { return &____matcherDisposeAfterTime_34; }
	inline void set__matcherDisposeAfterTime_34(RuntimeObject* value)
	{
		____matcherDisposeAfterTime_34 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDisposeAfterTime_34), value);
	}

	inline static int32_t get_offset_of__matcherDispose_35() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDispose_35)); }
	inline RuntimeObject* get__matcherDispose_35() const { return ____matcherDispose_35; }
	inline RuntimeObject** get_address_of__matcherDispose_35() { return &____matcherDispose_35; }
	inline void set__matcherDispose_35(RuntimeObject* value)
	{
		____matcherDispose_35 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDispose_35), value);
	}

	inline static int32_t get_offset_of__matcherDisposeListener_36() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDisposeListener_36)); }
	inline RuntimeObject* get__matcherDisposeListener_36() const { return ____matcherDisposeListener_36; }
	inline RuntimeObject** get_address_of__matcherDisposeListener_36() { return &____matcherDisposeListener_36; }
	inline void set__matcherDisposeListener_36(RuntimeObject* value)
	{
		____matcherDisposeListener_36 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDisposeListener_36), value);
	}

	inline static int32_t get_offset_of__matcherDropCollectable_37() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDropCollectable_37)); }
	inline RuntimeObject* get__matcherDropCollectable_37() const { return ____matcherDropCollectable_37; }
	inline RuntimeObject** get_address_of__matcherDropCollectable_37() { return &____matcherDropCollectable_37; }
	inline void set__matcherDropCollectable_37(RuntimeObject* value)
	{
		____matcherDropCollectable_37 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDropCollectable_37), value);
	}

	inline static int32_t get_offset_of__matcherDuration_38() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherDuration_38)); }
	inline RuntimeObject* get__matcherDuration_38() const { return ____matcherDuration_38; }
	inline RuntimeObject** get_address_of__matcherDuration_38() { return &____matcherDuration_38; }
	inline void set__matcherDuration_38(RuntimeObject* value)
	{
		____matcherDuration_38 = value;
		Il2CppCodeGenWriteBarrier((&____matcherDuration_38), value);
	}

	inline static int32_t get_offset_of__matcherEffect_39() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherEffect_39)); }
	inline RuntimeObject* get__matcherEffect_39() const { return ____matcherEffect_39; }
	inline RuntimeObject** get_address_of__matcherEffect_39() { return &____matcherEffect_39; }
	inline void set__matcherEffect_39(RuntimeObject* value)
	{
		____matcherEffect_39 = value;
		Il2CppCodeGenWriteBarrier((&____matcherEffect_39), value);
	}

	inline static int32_t get_offset_of__matcherEffectRendrer_40() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherEffectRendrer_40)); }
	inline RuntimeObject* get__matcherEffectRendrer_40() const { return ____matcherEffectRendrer_40; }
	inline RuntimeObject** get_address_of__matcherEffectRendrer_40() { return &____matcherEffectRendrer_40; }
	inline void set__matcherEffectRendrer_40(RuntimeObject* value)
	{
		____matcherEffectRendrer_40 = value;
		Il2CppCodeGenWriteBarrier((&____matcherEffectRendrer_40), value);
	}

	inline static int32_t get_offset_of__matcherEffectSource_41() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherEffectSource_41)); }
	inline RuntimeObject* get__matcherEffectSource_41() const { return ____matcherEffectSource_41; }
	inline RuntimeObject** get_address_of__matcherEffectSource_41() { return &____matcherEffectSource_41; }
	inline void set__matcherEffectSource_41(RuntimeObject* value)
	{
		____matcherEffectSource_41 = value;
		Il2CppCodeGenWriteBarrier((&____matcherEffectSource_41), value);
	}

	inline static int32_t get_offset_of__matcherEffectStarted_42() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherEffectStarted_42)); }
	inline RuntimeObject* get__matcherEffectStarted_42() const { return ____matcherEffectStarted_42; }
	inline RuntimeObject** get_address_of__matcherEffectStarted_42() { return &____matcherEffectStarted_42; }
	inline void set__matcherEffectStarted_42(RuntimeObject* value)
	{
		____matcherEffectStarted_42 = value;
		Il2CppCodeGenWriteBarrier((&____matcherEffectStarted_42), value);
	}

	inline static int32_t get_offset_of__matcherEffectStartTime_43() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherEffectStartTime_43)); }
	inline RuntimeObject* get__matcherEffectStartTime_43() const { return ____matcherEffectStartTime_43; }
	inline RuntimeObject** get_address_of__matcherEffectStartTime_43() { return &____matcherEffectStartTime_43; }
	inline void set__matcherEffectStartTime_43(RuntimeObject* value)
	{
		____matcherEffectStartTime_43 = value;
		Il2CppCodeGenWriteBarrier((&____matcherEffectStartTime_43), value);
	}

	inline static int32_t get_offset_of__matcherEffectVisual_44() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherEffectVisual_44)); }
	inline RuntimeObject* get__matcherEffectVisual_44() const { return ____matcherEffectVisual_44; }
	inline RuntimeObject** get_address_of__matcherEffectVisual_44() { return &____matcherEffectVisual_44; }
	inline void set__matcherEffectVisual_44(RuntimeObject* value)
	{
		____matcherEffectVisual_44 = value;
		Il2CppCodeGenWriteBarrier((&____matcherEffectVisual_44), value);
	}

	inline static int32_t get_offset_of__matcherFXSound_45() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherFXSound_45)); }
	inline RuntimeObject* get__matcherFXSound_45() const { return ____matcherFXSound_45; }
	inline RuntimeObject** get_address_of__matcherFXSound_45() { return &____matcherFXSound_45; }
	inline void set__matcherFXSound_45(RuntimeObject* value)
	{
		____matcherFXSound_45 = value;
		Il2CppCodeGenWriteBarrier((&____matcherFXSound_45), value);
	}

	inline static int32_t get_offset_of__matcherGoal_46() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherGoal_46)); }
	inline RuntimeObject* get__matcherGoal_46() const { return ____matcherGoal_46; }
	inline RuntimeObject** get_address_of__matcherGoal_46() { return &____matcherGoal_46; }
	inline void set__matcherGoal_46(RuntimeObject* value)
	{
		____matcherGoal_46 = value;
		Il2CppCodeGenWriteBarrier((&____matcherGoal_46), value);
	}

	inline static int32_t get_offset_of__matcherGroup_47() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherGroup_47)); }
	inline RuntimeObject* get__matcherGroup_47() const { return ____matcherGroup_47; }
	inline RuntimeObject** get_address_of__matcherGroup_47() { return &____matcherGroup_47; }
	inline void set__matcherGroup_47(RuntimeObject* value)
	{
		____matcherGroup_47 = value;
		Il2CppCodeGenWriteBarrier((&____matcherGroup_47), value);
	}

	inline static int32_t get_offset_of__matcherHero_48() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherHero_48)); }
	inline RuntimeObject* get__matcherHero_48() const { return ____matcherHero_48; }
	inline RuntimeObject** get_address_of__matcherHero_48() { return &____matcherHero_48; }
	inline void set__matcherHero_48(RuntimeObject* value)
	{
		____matcherHero_48 = value;
		Il2CppCodeGenWriteBarrier((&____matcherHero_48), value);
	}

	inline static int32_t get_offset_of__matcherHeroState_49() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherHeroState_49)); }
	inline RuntimeObject* get__matcherHeroState_49() const { return ____matcherHeroState_49; }
	inline RuntimeObject** get_address_of__matcherHeroState_49() { return &____matcherHeroState_49; }
	inline void set__matcherHeroState_49(RuntimeObject* value)
	{
		____matcherHeroState_49 = value;
		Il2CppCodeGenWriteBarrier((&____matcherHeroState_49), value);
	}

	inline static int32_t get_offset_of__matcherHPBoost_50() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherHPBoost_50)); }
	inline RuntimeObject* get__matcherHPBoost_50() const { return ____matcherHPBoost_50; }
	inline RuntimeObject** get_address_of__matcherHPBoost_50() { return &____matcherHPBoost_50; }
	inline void set__matcherHPBoost_50(RuntimeObject* value)
	{
		____matcherHPBoost_50 = value;
		Il2CppCodeGenWriteBarrier((&____matcherHPBoost_50), value);
	}

	inline static int32_t get_offset_of__matcherHP_51() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherHP_51)); }
	inline RuntimeObject* get__matcherHP_51() const { return ____matcherHP_51; }
	inline RuntimeObject** get_address_of__matcherHP_51() { return &____matcherHP_51; }
	inline void set__matcherHP_51(RuntimeObject* value)
	{
		____matcherHP_51 = value;
		Il2CppCodeGenWriteBarrier((&____matcherHP_51), value);
	}

	inline static int32_t get_offset_of__matcherId_52() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherId_52)); }
	inline RuntimeObject* get__matcherId_52() const { return ____matcherId_52; }
	inline RuntimeObject** get_address_of__matcherId_52() { return &____matcherId_52; }
	inline void set__matcherId_52(RuntimeObject* value)
	{
		____matcherId_52 = value;
		Il2CppCodeGenWriteBarrier((&____matcherId_52), value);
	}

	inline static int32_t get_offset_of__matcherILibrary_53() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherILibrary_53)); }
	inline RuntimeObject* get__matcherILibrary_53() const { return ____matcherILibrary_53; }
	inline RuntimeObject** get_address_of__matcherILibrary_53() { return &____matcherILibrary_53; }
	inline void set__matcherILibrary_53(RuntimeObject* value)
	{
		____matcherILibrary_53 = value;
		Il2CppCodeGenWriteBarrier((&____matcherILibrary_53), value);
	}

	inline static int32_t get_offset_of__matcherInitPosition_54() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherInitPosition_54)); }
	inline RuntimeObject* get__matcherInitPosition_54() const { return ____matcherInitPosition_54; }
	inline RuntimeObject** get_address_of__matcherInitPosition_54() { return &____matcherInitPosition_54; }
	inline void set__matcherInitPosition_54(RuntimeObject* value)
	{
		____matcherInitPosition_54 = value;
		Il2CppCodeGenWriteBarrier((&____matcherInitPosition_54), value);
	}

	inline static int32_t get_offset_of__matcherInstantEffect_55() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherInstantEffect_55)); }
	inline RuntimeObject* get__matcherInstantEffect_55() const { return ____matcherInstantEffect_55; }
	inline RuntimeObject** get_address_of__matcherInstantEffect_55() { return &____matcherInstantEffect_55; }
	inline void set__matcherInstantEffect_55(RuntimeObject* value)
	{
		____matcherInstantEffect_55 = value;
		Il2CppCodeGenWriteBarrier((&____matcherInstantEffect_55), value);
	}

	inline static int32_t get_offset_of__matcherInventory_56() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherInventory_56)); }
	inline RuntimeObject* get__matcherInventory_56() const { return ____matcherInventory_56; }
	inline RuntimeObject** get_address_of__matcherInventory_56() { return &____matcherInventory_56; }
	inline void set__matcherInventory_56(RuntimeObject* value)
	{
		____matcherInventory_56 = value;
		Il2CppCodeGenWriteBarrier((&____matcherInventory_56), value);
	}

	inline static int32_t get_offset_of__matcherInventoryIndex_57() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherInventoryIndex_57)); }
	inline RuntimeObject* get__matcherInventoryIndex_57() const { return ____matcherInventoryIndex_57; }
	inline RuntimeObject** get_address_of__matcherInventoryIndex_57() { return &____matcherInventoryIndex_57; }
	inline void set__matcherInventoryIndex_57(RuntimeObject* value)
	{
		____matcherInventoryIndex_57 = value;
		Il2CppCodeGenWriteBarrier((&____matcherInventoryIndex_57), value);
	}

	inline static int32_t get_offset_of__matcherInventoryItem_58() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherInventoryItem_58)); }
	inline RuntimeObject* get__matcherInventoryItem_58() const { return ____matcherInventoryItem_58; }
	inline RuntimeObject** get_address_of__matcherInventoryItem_58() { return &____matcherInventoryItem_58; }
	inline void set__matcherInventoryItem_58(RuntimeObject* value)
	{
		____matcherInventoryItem_58 = value;
		Il2CppCodeGenWriteBarrier((&____matcherInventoryItem_58), value);
	}

	inline static int32_t get_offset_of__matcherInventoryItems_59() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherInventoryItems_59)); }
	inline RuntimeObject* get__matcherInventoryItems_59() const { return ____matcherInventoryItems_59; }
	inline RuntimeObject** get_address_of__matcherInventoryItems_59() { return &____matcherInventoryItems_59; }
	inline void set__matcherInventoryItems_59(RuntimeObject* value)
	{
		____matcherInventoryItems_59 = value;
		Il2CppCodeGenWriteBarrier((&____matcherInventoryItems_59), value);
	}

	inline static int32_t get_offset_of__matcherItem_60() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherItem_60)); }
	inline RuntimeObject* get__matcherItem_60() const { return ____matcherItem_60; }
	inline RuntimeObject** get_address_of__matcherItem_60() { return &____matcherItem_60; }
	inline void set__matcherItem_60(RuntimeObject* value)
	{
		____matcherItem_60 = value;
		Il2CppCodeGenWriteBarrier((&____matcherItem_60), value);
	}

	inline static int32_t get_offset_of__matcherItemGenerator_61() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherItemGenerator_61)); }
	inline RuntimeObject* get__matcherItemGenerator_61() const { return ____matcherItemGenerator_61; }
	inline RuntimeObject** get_address_of__matcherItemGenerator_61() { return &____matcherItemGenerator_61; }
	inline void set__matcherItemGenerator_61(RuntimeObject* value)
	{
		____matcherItemGenerator_61 = value;
		Il2CppCodeGenWriteBarrier((&____matcherItemGenerator_61), value);
	}

	inline static int32_t get_offset_of__matcherItemsSpawnPoints_62() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherItemsSpawnPoints_62)); }
	inline RuntimeObject* get__matcherItemsSpawnPoints_62() const { return ____matcherItemsSpawnPoints_62; }
	inline RuntimeObject** get_address_of__matcherItemsSpawnPoints_62() { return &____matcherItemsSpawnPoints_62; }
	inline void set__matcherItemsSpawnPoints_62(RuntimeObject* value)
	{
		____matcherItemsSpawnPoints_62 = value;
		Il2CppCodeGenWriteBarrier((&____matcherItemsSpawnPoints_62), value);
	}

	inline static int32_t get_offset_of__matcherJoystick_63() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherJoystick_63)); }
	inline RuntimeObject* get__matcherJoystick_63() const { return ____matcherJoystick_63; }
	inline RuntimeObject** get_address_of__matcherJoystick_63() { return &____matcherJoystick_63; }
	inline void set__matcherJoystick_63(RuntimeObject* value)
	{
		____matcherJoystick_63 = value;
		Il2CppCodeGenWriteBarrier((&____matcherJoystick_63), value);
	}

	inline static int32_t get_offset_of__matcherLastAttackTime_64() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherLastAttackTime_64)); }
	inline RuntimeObject* get__matcherLastAttackTime_64() const { return ____matcherLastAttackTime_64; }
	inline RuntimeObject** get_address_of__matcherLastAttackTime_64() { return &____matcherLastAttackTime_64; }
	inline void set__matcherLastAttackTime_64(RuntimeObject* value)
	{
		____matcherLastAttackTime_64 = value;
		Il2CppCodeGenWriteBarrier((&____matcherLastAttackTime_64), value);
	}

	inline static int32_t get_offset_of__matcherLeftHand_65() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherLeftHand_65)); }
	inline RuntimeObject* get__matcherLeftHand_65() const { return ____matcherLeftHand_65; }
	inline RuntimeObject** get_address_of__matcherLeftHand_65() { return &____matcherLeftHand_65; }
	inline void set__matcherLeftHand_65(RuntimeObject* value)
	{
		____matcherLeftHand_65 = value;
		Il2CppCodeGenWriteBarrier((&____matcherLeftHand_65), value);
	}

	inline static int32_t get_offset_of__matcherLevel_66() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherLevel_66)); }
	inline RuntimeObject* get__matcherLevel_66() const { return ____matcherLevel_66; }
	inline RuntimeObject** get_address_of__matcherLevel_66() { return &____matcherLevel_66; }
	inline void set__matcherLevel_66(RuntimeObject* value)
	{
		____matcherLevel_66 = value;
		Il2CppCodeGenWriteBarrier((&____matcherLevel_66), value);
	}

	inline static int32_t get_offset_of__matcherLifetimeEffect_67() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherLifetimeEffect_67)); }
	inline RuntimeObject* get__matcherLifetimeEffect_67() const { return ____matcherLifetimeEffect_67; }
	inline RuntimeObject** get_address_of__matcherLifetimeEffect_67() { return &____matcherLifetimeEffect_67; }
	inline void set__matcherLifetimeEffect_67(RuntimeObject* value)
	{
		____matcherLifetimeEffect_67 = value;
		Il2CppCodeGenWriteBarrier((&____matcherLifetimeEffect_67), value);
	}

	inline static int32_t get_offset_of__matcherLoopingAI_68() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherLoopingAI_68)); }
	inline RuntimeObject* get__matcherLoopingAI_68() const { return ____matcherLoopingAI_68; }
	inline RuntimeObject** get_address_of__matcherLoopingAI_68() { return &____matcherLoopingAI_68; }
	inline void set__matcherLoopingAI_68(RuntimeObject* value)
	{
		____matcherLoopingAI_68 = value;
		Il2CppCodeGenWriteBarrier((&____matcherLoopingAI_68), value);
	}

	inline static int32_t get_offset_of__matcherMapItem_69() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherMapItem_69)); }
	inline RuntimeObject* get__matcherMapItem_69() const { return ____matcherMapItem_69; }
	inline RuntimeObject** get_address_of__matcherMapItem_69() { return &____matcherMapItem_69; }
	inline void set__matcherMapItem_69(RuntimeObject* value)
	{
		____matcherMapItem_69 = value;
		Il2CppCodeGenWriteBarrier((&____matcherMapItem_69), value);
	}

	inline static int32_t get_offset_of__matcherMaxHP_70() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherMaxHP_70)); }
	inline RuntimeObject* get__matcherMaxHP_70() const { return ____matcherMaxHP_70; }
	inline RuntimeObject** get_address_of__matcherMaxHP_70() { return &____matcherMaxHP_70; }
	inline void set__matcherMaxHP_70(RuntimeObject* value)
	{
		____matcherMaxHP_70 = value;
		Il2CppCodeGenWriteBarrier((&____matcherMaxHP_70), value);
	}

	inline static int32_t get_offset_of__matcherMusicSound_71() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherMusicSound_71)); }
	inline RuntimeObject* get__matcherMusicSound_71() const { return ____matcherMusicSound_71; }
	inline RuntimeObject** get_address_of__matcherMusicSound_71() { return &____matcherMusicSound_71; }
	inline void set__matcherMusicSound_71(RuntimeObject* value)
	{
		____matcherMusicSound_71 = value;
		Il2CppCodeGenWriteBarrier((&____matcherMusicSound_71), value);
	}

	inline static int32_t get_offset_of__matcherObjectiveComplete_72() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherObjectiveComplete_72)); }
	inline RuntimeObject* get__matcherObjectiveComplete_72() const { return ____matcherObjectiveComplete_72; }
	inline RuntimeObject** get_address_of__matcherObjectiveComplete_72() { return &____matcherObjectiveComplete_72; }
	inline void set__matcherObjectiveComplete_72(RuntimeObject* value)
	{
		____matcherObjectiveComplete_72 = value;
		Il2CppCodeGenWriteBarrier((&____matcherObjectiveComplete_72), value);
	}

	inline static int32_t get_offset_of__matcherObjectiveCounter_73() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherObjectiveCounter_73)); }
	inline RuntimeObject* get__matcherObjectiveCounter_73() const { return ____matcherObjectiveCounter_73; }
	inline RuntimeObject** get_address_of__matcherObjectiveCounter_73() { return &____matcherObjectiveCounter_73; }
	inline void set__matcherObjectiveCounter_73(RuntimeObject* value)
	{
		____matcherObjectiveCounter_73 = value;
		Il2CppCodeGenWriteBarrier((&____matcherObjectiveCounter_73), value);
	}

	inline static int32_t get_offset_of__matcherObjectives_74() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherObjectives_74)); }
	inline RuntimeObject* get__matcherObjectives_74() const { return ____matcherObjectives_74; }
	inline RuntimeObject** get_address_of__matcherObjectives_74() { return &____matcherObjectives_74; }
	inline void set__matcherObjectives_74(RuntimeObject* value)
	{
		____matcherObjectives_74 = value;
		Il2CppCodeGenWriteBarrier((&____matcherObjectives_74), value);
	}

	inline static int32_t get_offset_of__matcherObjectiveStep_75() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherObjectiveStep_75)); }
	inline RuntimeObject* get__matcherObjectiveStep_75() const { return ____matcherObjectiveStep_75; }
	inline RuntimeObject** get_address_of__matcherObjectiveStep_75() { return &____matcherObjectiveStep_75; }
	inline void set__matcherObjectiveStep_75(RuntimeObject* value)
	{
		____matcherObjectiveStep_75 = value;
		Il2CppCodeGenWriteBarrier((&____matcherObjectiveStep_75), value);
	}

	inline static int32_t get_offset_of__matcherObjectiveType_76() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherObjectiveType_76)); }
	inline RuntimeObject* get__matcherObjectiveType_76() const { return ____matcherObjectiveType_76; }
	inline RuntimeObject** get_address_of__matcherObjectiveType_76() { return &____matcherObjectiveType_76; }
	inline void set__matcherObjectiveType_76(RuntimeObject* value)
	{
		____matcherObjectiveType_76 = value;
		Il2CppCodeGenWriteBarrier((&____matcherObjectiveType_76), value);
	}

	inline static int32_t get_offset_of__matcherParent_77() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherParent_77)); }
	inline RuntimeObject* get__matcherParent_77() const { return ____matcherParent_77; }
	inline RuntimeObject** get_address_of__matcherParent_77() { return &____matcherParent_77; }
	inline void set__matcherParent_77(RuntimeObject* value)
	{
		____matcherParent_77 = value;
		Il2CppCodeGenWriteBarrier((&____matcherParent_77), value);
	}

	inline static int32_t get_offset_of__matcherPosition_78() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherPosition_78)); }
	inline RuntimeObject* get__matcherPosition_78() const { return ____matcherPosition_78; }
	inline RuntimeObject** get_address_of__matcherPosition_78() { return &____matcherPosition_78; }
	inline void set__matcherPosition_78(RuntimeObject* value)
	{
		____matcherPosition_78 = value;
		Il2CppCodeGenWriteBarrier((&____matcherPosition_78), value);
	}

	inline static int32_t get_offset_of__matcherProjectile_79() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherProjectile_79)); }
	inline RuntimeObject* get__matcherProjectile_79() const { return ____matcherProjectile_79; }
	inline RuntimeObject** get_address_of__matcherProjectile_79() { return &____matcherProjectile_79; }
	inline void set__matcherProjectile_79(RuntimeObject* value)
	{
		____matcherProjectile_79 = value;
		Il2CppCodeGenWriteBarrier((&____matcherProjectile_79), value);
	}

	inline static int32_t get_offset_of__matcherProjectileSource_80() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherProjectileSource_80)); }
	inline RuntimeObject* get__matcherProjectileSource_80() const { return ____matcherProjectileSource_80; }
	inline RuntimeObject** get_address_of__matcherProjectileSource_80() { return &____matcherProjectileSource_80; }
	inline void set__matcherProjectileSource_80(RuntimeObject* value)
	{
		____matcherProjectileSource_80 = value;
		Il2CppCodeGenWriteBarrier((&____matcherProjectileSource_80), value);
	}

	inline static int32_t get_offset_of__matcherProjectileSpell_81() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherProjectileSpell_81)); }
	inline RuntimeObject* get__matcherProjectileSpell_81() const { return ____matcherProjectileSpell_81; }
	inline RuntimeObject** get_address_of__matcherProjectileSpell_81() { return &____matcherProjectileSpell_81; }
	inline void set__matcherProjectileSpell_81(RuntimeObject* value)
	{
		____matcherProjectileSpell_81 = value;
		Il2CppCodeGenWriteBarrier((&____matcherProjectileSpell_81), value);
	}

	inline static int32_t get_offset_of__matcherProjectileStartPosition_82() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherProjectileStartPosition_82)); }
	inline RuntimeObject* get__matcherProjectileStartPosition_82() const { return ____matcherProjectileStartPosition_82; }
	inline RuntimeObject** get_address_of__matcherProjectileStartPosition_82() { return &____matcherProjectileStartPosition_82; }
	inline void set__matcherProjectileStartPosition_82(RuntimeObject* value)
	{
		____matcherProjectileStartPosition_82 = value;
		Il2CppCodeGenWriteBarrier((&____matcherProjectileStartPosition_82), value);
	}

	inline static int32_t get_offset_of__matcherProjectileTargetEntity_83() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherProjectileTargetEntity_83)); }
	inline RuntimeObject* get__matcherProjectileTargetEntity_83() const { return ____matcherProjectileTargetEntity_83; }
	inline RuntimeObject** get_address_of__matcherProjectileTargetEntity_83() { return &____matcherProjectileTargetEntity_83; }
	inline void set__matcherProjectileTargetEntity_83(RuntimeObject* value)
	{
		____matcherProjectileTargetEntity_83 = value;
		Il2CppCodeGenWriteBarrier((&____matcherProjectileTargetEntity_83), value);
	}

	inline static int32_t get_offset_of__matcherPulseAI_84() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherPulseAI_84)); }
	inline RuntimeObject* get__matcherPulseAI_84() const { return ____matcherPulseAI_84; }
	inline RuntimeObject** get_address_of__matcherPulseAI_84() { return &____matcherPulseAI_84; }
	inline void set__matcherPulseAI_84(RuntimeObject* value)
	{
		____matcherPulseAI_84 = value;
		Il2CppCodeGenWriteBarrier((&____matcherPulseAI_84), value);
	}

	inline static int32_t get_offset_of__matcherRemoveViewOnDispose_85() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherRemoveViewOnDispose_85)); }
	inline RuntimeObject* get__matcherRemoveViewOnDispose_85() const { return ____matcherRemoveViewOnDispose_85; }
	inline RuntimeObject** get_address_of__matcherRemoveViewOnDispose_85() { return &____matcherRemoveViewOnDispose_85; }
	inline void set__matcherRemoveViewOnDispose_85(RuntimeObject* value)
	{
		____matcherRemoveViewOnDispose_85 = value;
		Il2CppCodeGenWriteBarrier((&____matcherRemoveViewOnDispose_85), value);
	}

	inline static int32_t get_offset_of__matcherResist_86() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherResist_86)); }
	inline RuntimeObject* get__matcherResist_86() const { return ____matcherResist_86; }
	inline RuntimeObject** get_address_of__matcherResist_86() { return &____matcherResist_86; }
	inline void set__matcherResist_86(RuntimeObject* value)
	{
		____matcherResist_86 = value;
		Il2CppCodeGenWriteBarrier((&____matcherResist_86), value);
	}

	inline static int32_t get_offset_of__matcherRotation_87() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherRotation_87)); }
	inline RuntimeObject* get__matcherRotation_87() const { return ____matcherRotation_87; }
	inline RuntimeObject** get_address_of__matcherRotation_87() { return &____matcherRotation_87; }
	inline void set__matcherRotation_87(RuntimeObject* value)
	{
		____matcherRotation_87 = value;
		Il2CppCodeGenWriteBarrier((&____matcherRotation_87), value);
	}

	inline static int32_t get_offset_of__matcherScreenTap_88() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherScreenTap_88)); }
	inline RuntimeObject* get__matcherScreenTap_88() const { return ____matcherScreenTap_88; }
	inline RuntimeObject** get_address_of__matcherScreenTap_88() { return &____matcherScreenTap_88; }
	inline void set__matcherScreenTap_88(RuntimeObject* value)
	{
		____matcherScreenTap_88 = value;
		Il2CppCodeGenWriteBarrier((&____matcherScreenTap_88), value);
	}

	inline static int32_t get_offset_of__matcherSlowSpeed_89() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSlowSpeed_89)); }
	inline RuntimeObject* get__matcherSlowSpeed_89() const { return ____matcherSlowSpeed_89; }
	inline RuntimeObject** get_address_of__matcherSlowSpeed_89() { return &____matcherSlowSpeed_89; }
	inline void set__matcherSlowSpeed_89(RuntimeObject* value)
	{
		____matcherSlowSpeed_89 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSlowSpeed_89), value);
	}

	inline static int32_t get_offset_of__matcherSound_90() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSound_90)); }
	inline RuntimeObject* get__matcherSound_90() const { return ____matcherSound_90; }
	inline RuntimeObject** get_address_of__matcherSound_90() { return &____matcherSound_90; }
	inline void set__matcherSound_90(RuntimeObject* value)
	{
		____matcherSound_90 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSound_90), value);
	}

	inline static int32_t get_offset_of__matcherSpeedBoost_91() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSpeedBoost_91)); }
	inline RuntimeObject* get__matcherSpeedBoost_91() const { return ____matcherSpeedBoost_91; }
	inline RuntimeObject** get_address_of__matcherSpeedBoost_91() { return &____matcherSpeedBoost_91; }
	inline void set__matcherSpeedBoost_91(RuntimeObject* value)
	{
		____matcherSpeedBoost_91 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSpeedBoost_91), value);
	}

	inline static int32_t get_offset_of__matcherSpeed_92() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSpeed_92)); }
	inline RuntimeObject* get__matcherSpeed_92() const { return ____matcherSpeed_92; }
	inline RuntimeObject** get_address_of__matcherSpeed_92() { return &____matcherSpeed_92; }
	inline void set__matcherSpeed_92(RuntimeObject* value)
	{
		____matcherSpeed_92 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSpeed_92), value);
	}

	inline static int32_t get_offset_of__matcherSpell_93() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSpell_93)); }
	inline RuntimeObject* get__matcherSpell_93() const { return ____matcherSpell_93; }
	inline RuntimeObject** get_address_of__matcherSpell_93() { return &____matcherSpell_93; }
	inline void set__matcherSpell_93(RuntimeObject* value)
	{
		____matcherSpell_93 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSpell_93), value);
	}

	inline static int32_t get_offset_of__matcherSpellController_94() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSpellController_94)); }
	inline RuntimeObject* get__matcherSpellController_94() const { return ____matcherSpellController_94; }
	inline RuntimeObject** get_address_of__matcherSpellController_94() { return &____matcherSpellController_94; }
	inline void set__matcherSpellController_94(RuntimeObject* value)
	{
		____matcherSpellController_94 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSpellController_94), value);
	}

	inline static int32_t get_offset_of__matcherSpellIndex_95() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSpellIndex_95)); }
	inline RuntimeObject* get__matcherSpellIndex_95() const { return ____matcherSpellIndex_95; }
	inline RuntimeObject** get_address_of__matcherSpellIndex_95() { return &____matcherSpellIndex_95; }
	inline void set__matcherSpellIndex_95(RuntimeObject* value)
	{
		____matcherSpellIndex_95 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSpellIndex_95), value);
	}

	inline static int32_t get_offset_of__matcherSpellMeta_96() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSpellMeta_96)); }
	inline RuntimeObject* get__matcherSpellMeta_96() const { return ____matcherSpellMeta_96; }
	inline RuntimeObject** get_address_of__matcherSpellMeta_96() { return &____matcherSpellMeta_96; }
	inline void set__matcherSpellMeta_96(RuntimeObject* value)
	{
		____matcherSpellMeta_96 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSpellMeta_96), value);
	}

	inline static int32_t get_offset_of__matcherSpellStatus_97() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSpellStatus_97)); }
	inline RuntimeObject* get__matcherSpellStatus_97() const { return ____matcherSpellStatus_97; }
	inline RuntimeObject** get_address_of__matcherSpellStatus_97() { return &____matcherSpellStatus_97; }
	inline void set__matcherSpellStatus_97(RuntimeObject* value)
	{
		____matcherSpellStatus_97 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSpellStatus_97), value);
	}

	inline static int32_t get_offset_of__matcherSphereCollider_98() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherSphereCollider_98)); }
	inline RuntimeObject* get__matcherSphereCollider_98() const { return ____matcherSphereCollider_98; }
	inline RuntimeObject** get_address_of__matcherSphereCollider_98() { return &____matcherSphereCollider_98; }
	inline void set__matcherSphereCollider_98(RuntimeObject* value)
	{
		____matcherSphereCollider_98 = value;
		Il2CppCodeGenWriteBarrier((&____matcherSphereCollider_98), value);
	}

	inline static int32_t get_offset_of__matcherStartTime_99() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherStartTime_99)); }
	inline RuntimeObject* get__matcherStartTime_99() const { return ____matcherStartTime_99; }
	inline RuntimeObject** get_address_of__matcherStartTime_99() { return &____matcherStartTime_99; }
	inline void set__matcherStartTime_99(RuntimeObject* value)
	{
		____matcherStartTime_99 = value;
		Il2CppCodeGenWriteBarrier((&____matcherStartTime_99), value);
	}

	inline static int32_t get_offset_of__matcherTapTapGroundRecognizer_100() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTapTapGroundRecognizer_100)); }
	inline RuntimeObject* get__matcherTapTapGroundRecognizer_100() const { return ____matcherTapTapGroundRecognizer_100; }
	inline RuntimeObject** get_address_of__matcherTapTapGroundRecognizer_100() { return &____matcherTapTapGroundRecognizer_100; }
	inline void set__matcherTapTapGroundRecognizer_100(RuntimeObject* value)
	{
		____matcherTapTapGroundRecognizer_100 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTapTapGroundRecognizer_100), value);
	}

	inline static int32_t get_offset_of__matcherTapTapTowerRecognizer_101() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTapTapTowerRecognizer_101)); }
	inline RuntimeObject* get__matcherTapTapTowerRecognizer_101() const { return ____matcherTapTapTowerRecognizer_101; }
	inline RuntimeObject** get_address_of__matcherTapTapTowerRecognizer_101() { return &____matcherTapTapTowerRecognizer_101; }
	inline void set__matcherTapTapTowerRecognizer_101(RuntimeObject* value)
	{
		____matcherTapTapTowerRecognizer_101 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTapTapTowerRecognizer_101), value);
	}

	inline static int32_t get_offset_of__matcherTower_102() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTower_102)); }
	inline RuntimeObject* get__matcherTower_102() const { return ____matcherTower_102; }
	inline RuntimeObject** get_address_of__matcherTower_102() { return &____matcherTower_102; }
	inline void set__matcherTower_102(RuntimeObject* value)
	{
		____matcherTower_102 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTower_102), value);
	}

	inline static int32_t get_offset_of__matcherTowerHealing_103() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTowerHealing_103)); }
	inline RuntimeObject* get__matcherTowerHealing_103() const { return ____matcherTowerHealing_103; }
	inline RuntimeObject** get_address_of__matcherTowerHealing_103() { return &____matcherTowerHealing_103; }
	inline void set__matcherTowerHealing_103(RuntimeObject* value)
	{
		____matcherTowerHealing_103 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTowerHealing_103), value);
	}

	inline static int32_t get_offset_of__matcherTowerHealingDuration_104() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTowerHealingDuration_104)); }
	inline RuntimeObject* get__matcherTowerHealingDuration_104() const { return ____matcherTowerHealingDuration_104; }
	inline RuntimeObject** get_address_of__matcherTowerHealingDuration_104() { return &____matcherTowerHealingDuration_104; }
	inline void set__matcherTowerHealingDuration_104(RuntimeObject* value)
	{
		____matcherTowerHealingDuration_104 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTowerHealingDuration_104), value);
	}

	inline static int32_t get_offset_of__matcherTowerHealingStartTime_105() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTowerHealingStartTime_105)); }
	inline RuntimeObject* get__matcherTowerHealingStartTime_105() const { return ____matcherTowerHealingStartTime_105; }
	inline RuntimeObject** get_address_of__matcherTowerHealingStartTime_105() { return &____matcherTowerHealingStartTime_105; }
	inline void set__matcherTowerHealingStartTime_105(RuntimeObject* value)
	{
		____matcherTowerHealingStartTime_105 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTowerHealingStartTime_105), value);
	}

	inline static int32_t get_offset_of__matcherTowerHPBar_106() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTowerHPBar_106)); }
	inline RuntimeObject* get__matcherTowerHPBar_106() const { return ____matcherTowerHPBar_106; }
	inline RuntimeObject** get_address_of__matcherTowerHPBar_106() { return &____matcherTowerHPBar_106; }
	inline void set__matcherTowerHPBar_106(RuntimeObject* value)
	{
		____matcherTowerHPBar_106 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTowerHPBar_106), value);
	}

	inline static int32_t get_offset_of__matcherTowerRender_107() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTowerRender_107)); }
	inline RuntimeObject* get__matcherTowerRender_107() const { return ____matcherTowerRender_107; }
	inline RuntimeObject** get_address_of__matcherTowerRender_107() { return &____matcherTowerRender_107; }
	inline void set__matcherTowerRender_107(RuntimeObject* value)
	{
		____matcherTowerRender_107 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTowerRender_107), value);
	}

	inline static int32_t get_offset_of__matcherTowerTap_108() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTowerTap_108)); }
	inline RuntimeObject* get__matcherTowerTap_108() const { return ____matcherTowerTap_108; }
	inline RuntimeObject** get_address_of__matcherTowerTap_108() { return &____matcherTowerTap_108; }
	inline void set__matcherTowerTap_108(RuntimeObject* value)
	{
		____matcherTowerTap_108 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTowerTap_108), value);
	}

	inline static int32_t get_offset_of__matcherTowerTop_109() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTowerTop_109)); }
	inline RuntimeObject* get__matcherTowerTop_109() const { return ____matcherTowerTop_109; }
	inline RuntimeObject** get_address_of__matcherTowerTop_109() { return &____matcherTowerTop_109; }
	inline void set__matcherTowerTop_109(RuntimeObject* value)
	{
		____matcherTowerTop_109 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTowerTop_109), value);
	}

	inline static int32_t get_offset_of__matcherTrap_110() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherTrap_110)); }
	inline RuntimeObject* get__matcherTrap_110() const { return ____matcherTrap_110; }
	inline RuntimeObject** get_address_of__matcherTrap_110() { return &____matcherTrap_110; }
	inline void set__matcherTrap_110(RuntimeObject* value)
	{
		____matcherTrap_110 = value;
		Il2CppCodeGenWriteBarrier((&____matcherTrap_110), value);
	}

	inline static int32_t get_offset_of__matcherUIHolder_111() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherUIHolder_111)); }
	inline RuntimeObject* get__matcherUIHolder_111() const { return ____matcherUIHolder_111; }
	inline RuntimeObject** get_address_of__matcherUIHolder_111() { return &____matcherUIHolder_111; }
	inline void set__matcherUIHolder_111(RuntimeObject* value)
	{
		____matcherUIHolder_111 = value;
		Il2CppCodeGenWriteBarrier((&____matcherUIHolder_111), value);
	}

	inline static int32_t get_offset_of__matcherView_112() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherView_112)); }
	inline RuntimeObject* get__matcherView_112() const { return ____matcherView_112; }
	inline RuntimeObject** get_address_of__matcherView_112() { return &____matcherView_112; }
	inline void set__matcherView_112(RuntimeObject* value)
	{
		____matcherView_112 = value;
		Il2CppCodeGenWriteBarrier((&____matcherView_112), value);
	}

	inline static int32_t get_offset_of__matcherVisual_113() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherVisual_113)); }
	inline RuntimeObject* get__matcherVisual_113() const { return ____matcherVisual_113; }
	inline RuntimeObject** get_address_of__matcherVisual_113() { return &____matcherVisual_113; }
	inline void set__matcherVisual_113(RuntimeObject* value)
	{
		____matcherVisual_113 = value;
		Il2CppCodeGenWriteBarrier((&____matcherVisual_113), value);
	}

	inline static int32_t get_offset_of__matcherWall_114() { return static_cast<int32_t>(offsetof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields, ____matcherWall_114)); }
	inline RuntimeObject* get__matcherWall_114() const { return ____matcherWall_114; }
	inline RuntimeObject** get_address_of__matcherWall_114() { return &____matcherWall_114; }
	inline void set__matcherWall_114(RuntimeObject* value)
	{
		____matcherWall_114 = value;
		Il2CppCodeGenWriteBarrier((&____matcherWall_114), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMATCHER_T84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_H
#ifndef U3CTASKU3ED__2_T5707BA5537CFD728B20C7BC0EF542494BA061CDB_H
#define U3CTASKU3ED__2_T5707BA5537CFD728B20C7BC0EF542494BA061CDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkChapterPatcherTask_<Task>d__2
struct  U3CTaskU3Ed__2_t5707BA5537CFD728B20C7BC0EF542494BA061CDB  : public RuntimeObject
{
public:
	// System.Int32 GameSparkChapterPatcherTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GameSparkChapterPatcherTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t5707BA5537CFD728B20C7BC0EF542494BA061CDB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t5707BA5537CFD728B20C7BC0EF542494BA061CDB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T5707BA5537CFD728B20C7BC0EF542494BA061CDB_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T3310E910B0C69A0FBECC365551D8BF0E9CA80BC4_H
#define U3CU3EC__DISPLAYCLASS11_0_T3310E910B0C69A0FBECC365551D8BF0E9CA80BC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkPatcherTask_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4  : public RuntimeObject
{
public:
	// GameSparkPatcherTask GameSparkPatcherTask_<>c__DisplayClass11_0::<>4__this
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * ___U3CU3E4__this_0;
	// System.String GameSparkPatcherTask_<>c__DisplayClass11_0::asset
	String_t* ___asset_1;
	// System.String GameSparkPatcherTask_<>c__DisplayClass11_0::fileNameGS
	String_t* ___fileNameGS_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4, ___U3CU3E4__this_0)); }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_asset_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4, ___asset_1)); }
	inline String_t* get_asset_1() const { return ___asset_1; }
	inline String_t** get_address_of_asset_1() { return &___asset_1; }
	inline void set_asset_1(String_t* value)
	{
		___asset_1 = value;
		Il2CppCodeGenWriteBarrier((&___asset_1), value);
	}

	inline static int32_t get_offset_of_fileNameGS_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4, ___fileNameGS_2)); }
	inline String_t* get_fileNameGS_2() const { return ___fileNameGS_2; }
	inline String_t** get_address_of_fileNameGS_2() { return &___fileNameGS_2; }
	inline void set_fileNameGS_2(String_t* value)
	{
		___fileNameGS_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileNameGS_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T3310E910B0C69A0FBECC365551D8BF0E9CA80BC4_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T539AB4894B46C7A3ED82A559BC7242BCF086B1E5_H
#define U3CU3EC__DISPLAYCLASS12_0_T539AB4894B46C7A3ED82A559BC7242BCF086B1E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkPatcherTask_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t539AB4894B46C7A3ED82A559BC7242BCF086B1E5  : public RuntimeObject
{
public:
	// GameSparkPatcherTask GameSparkPatcherTask_<>c__DisplayClass12_0::<>4__this
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * ___U3CU3E4__this_0;
	// System.String GameSparkPatcherTask_<>c__DisplayClass12_0::path
	String_t* ___path_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t539AB4894B46C7A3ED82A559BC7242BCF086B1E5, ___U3CU3E4__this_0)); }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t539AB4894B46C7A3ED82A559BC7242BCF086B1E5, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T539AB4894B46C7A3ED82A559BC7242BCF086B1E5_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T93D61961BFB9FD85994672A961ADF6A89315C6B0_H
#define U3CU3EC__DISPLAYCLASS7_0_T93D61961BFB9FD85994672A961ADF6A89315C6B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkPatcherTask_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t93D61961BFB9FD85994672A961ADF6A89315C6B0  : public RuntimeObject
{
public:
	// System.String GameSparkPatcherTask_<>c__DisplayClass7_0::fileNameGS
	String_t* ___fileNameGS_0;
	// GameSparkPatcherTask GameSparkPatcherTask_<>c__DisplayClass7_0::<>4__this
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_fileNameGS_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t93D61961BFB9FD85994672A961ADF6A89315C6B0, ___fileNameGS_0)); }
	inline String_t* get_fileNameGS_0() const { return ___fileNameGS_0; }
	inline String_t** get_address_of_fileNameGS_0() { return &___fileNameGS_0; }
	inline void set_fileNameGS_0(String_t* value)
	{
		___fileNameGS_0 = value;
		Il2CppCodeGenWriteBarrier((&___fileNameGS_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t93D61961BFB9FD85994672A961ADF6A89315C6B0, ___U3CU3E4__this_1)); }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T93D61961BFB9FD85994672A961ADF6A89315C6B0_H
#ifndef U3CTASKU3ED__7_T366290A4947CE5D103F6A6125F8664AC64EED18A_H
#define U3CTASKU3ED__7_T366290A4947CE5D103F6A6125F8664AC64EED18A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkPatcherTask_<Task>d__7
struct  U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A  : public RuntimeObject
{
public:
	// System.Int32 GameSparkPatcherTask_<Task>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GameSparkPatcherTask_<Task>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GameSparkPatcherTask GameSparkPatcherTask_<Task>d__7::<>4__this
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A, ___U3CU3E4__this_2)); }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__7_T366290A4947CE5D103F6A6125F8664AC64EED18A_H
#ifndef GAMESPARKTASKRESULT_T4BD72A1DBD60CBE3D6EE8496698180E59A72D65D_H
#define GAMESPARKTASKRESULT_T4BD72A1DBD60CBE3D6EE8496698180E59A72D65D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkTaskResult
struct  GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D  : public RuntimeObject
{
public:
	// System.String GameSparkTaskResult::Version
	String_t* ___Version_0;
	// System.String GameSparkTaskResult::Hash
	String_t* ___Hash_1;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version_0), value);
	}

	inline static int32_t get_offset_of_Hash_1() { return static_cast<int32_t>(offsetof(GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D, ___Hash_1)); }
	inline String_t* get_Hash_1() const { return ___Hash_1; }
	inline String_t** get_address_of_Hash_1() { return &___Hash_1; }
	inline void set_Hash_1(String_t* value)
	{
		___Hash_1 = value;
		Il2CppCodeGenWriteBarrier((&___Hash_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKTASKRESULT_T4BD72A1DBD60CBE3D6EE8496698180E59A72D65D_H
#ifndef U3CTASKU3ED__4_T8C151BD8EBC09519433B6B94953858DD97BCBF2D_H
#define U3CTASKU3ED__4_T8C151BD8EBC09519433B6B94953858DD97BCBF2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetLeaderboardTask_<Task>d__4
struct  U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D  : public RuntimeObject
{
public:
	// System.Int32 GetLeaderboardTask_<Task>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GetLeaderboardTask_<Task>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GetLeaderboardTask GetLeaderboardTask_<Task>d__4::<>4__this
	GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D, ___U3CU3E4__this_2)); }
	inline GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__4_T8C151BD8EBC09519433B6B94953858DD97BCBF2D_H
#ifndef GROUPMODEL_TE75857CD63B052D0127D746D6D58CD8A90655399_H
#define GROUPMODEL_TE75857CD63B052D0127D746D6D58CD8A90655399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupModel
struct  GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399  : public RuntimeObject
{
public:
	// System.String GroupModel::Name
	String_t* ___Name_0;
	// System.String GroupModel::Badge
	String_t* ___Badge_1;
	// System.String GroupModel::Description
	String_t* ___Description_2;
	// System.Boolean GroupModel::Private
	bool ___Private_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Badge_1() { return static_cast<int32_t>(offsetof(GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399, ___Badge_1)); }
	inline String_t* get_Badge_1() const { return ___Badge_1; }
	inline String_t** get_address_of_Badge_1() { return &___Badge_1; }
	inline void set_Badge_1(String_t* value)
	{
		___Badge_1 = value;
		Il2CppCodeGenWriteBarrier((&___Badge_1), value);
	}

	inline static int32_t get_offset_of_Description_2() { return static_cast<int32_t>(offsetof(GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399, ___Description_2)); }
	inline String_t* get_Description_2() const { return ___Description_2; }
	inline String_t** get_address_of_Description_2() { return &___Description_2; }
	inline void set_Description_2(String_t* value)
	{
		___Description_2 = value;
		Il2CppCodeGenWriteBarrier((&___Description_2), value);
	}

	inline static int32_t get_offset_of_Private_3() { return static_cast<int32_t>(offsetof(GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399, ___Private_3)); }
	inline bool get_Private_3() const { return ___Private_3; }
	inline bool* get_address_of_Private_3() { return &___Private_3; }
	inline void set_Private_3(bool value)
	{
		___Private_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPMODEL_TE75857CD63B052D0127D746D6D58CD8A90655399_H
#ifndef ICONSUTILS_TDB62243081F19841AF984250B871723B52E629A3_H
#define ICONSUTILS_TDB62243081F19841AF984250B871723B52E629A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconsUtils
struct  IconsUtils_tDB62243081F19841AF984250B871723B52E629A3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONSUTILS_TDB62243081F19841AF984250B871723B52E629A3_H
#ifndef KICKERMANAGER_T6B713EC4ED206DF81133170EB7545C76EEDEBDCA_H
#define KICKERMANAGER_T6B713EC4ED206DF81133170EB7545C76EEDEBDCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickerManager
struct  KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA  : public RuntimeObject
{
public:
	// Tayr.ILibrary KickerManager::_uiMain
	RuntimeObject* ____uiMain_0;
	// Tayr.INode KickerManager::_node
	RuntimeObject* ____node_1;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__node_1() { return static_cast<int32_t>(offsetof(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA, ____node_1)); }
	inline RuntimeObject* get__node_1() const { return ____node_1; }
	inline RuntimeObject** get_address_of__node_1() { return &____node_1; }
	inline void set__node_1(RuntimeObject* value)
	{
		____node_1 = value;
		Il2CppCodeGenWriteBarrier((&____node_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKERMANAGER_T6B713EC4ED206DF81133170EB7545C76EEDEBDCA_H
#ifndef KICKERNODEDATA_T9707F38C332456DFF9E4FBA0729EFC45866778AE_H
#define KICKERNODEDATA_T9707F38C332456DFF9E4FBA0729EFC45866778AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickerNodeData
struct  KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE  : public RuntimeObject
{
public:
	// System.Single KickerNodeData::Duration
	float ___Duration_0;
	// System.String KickerNodeData::LocalizationKey
	String_t* ___LocalizationKey_1;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE, ___Duration_0)); }
	inline float get_Duration_0() const { return ___Duration_0; }
	inline float* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(float value)
	{
		___Duration_0 = value;
	}

	inline static int32_t get_offset_of_LocalizationKey_1() { return static_cast<int32_t>(offsetof(KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE, ___LocalizationKey_1)); }
	inline String_t* get_LocalizationKey_1() const { return ___LocalizationKey_1; }
	inline String_t** get_address_of_LocalizationKey_1() { return &___LocalizationKey_1; }
	inline void set_LocalizationKey_1(String_t* value)
	{
		___LocalizationKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalizationKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKERNODEDATA_T9707F38C332456DFF9E4FBA0729EFC45866778AE_H
#ifndef LEADERBOARDDATAPARSER_T87581A0FEF1C1A4B0A48942220FC7FA8B21602FB_H
#define LEADERBOARDDATAPARSER_T87581A0FEF1C1A4B0A48942220FC7FA8B21602FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardDataParser
struct  LeaderboardDataParser_t87581A0FEF1C1A4B0A48942220FC7FA8B21602FB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDDATAPARSER_T87581A0FEF1C1A4B0A48942220FC7FA8B21602FB_H
#ifndef U3CU3EC_T1F36BE254EA38A92288ED01B3475412E3639B569_H
#define U3CU3EC_T1F36BE254EA38A92288ED01B3475412E3639B569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardDataParser_<>c
struct  U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569_StaticFields
{
public:
	// LeaderboardDataParser_<>c LeaderboardDataParser_<>c::<>9
	U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569 * ___U3CU3E9_0;
	// System.Func`2<TribeTournamentContributionListItemData,System.Int32> LeaderboardDataParser_<>c::<>9__4_0
	Func_2_t7FB2D586428E9D3F01BCA67C0B8FDB6C041C26BC * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t7FB2D586428E9D3F01BCA67C0B8FDB6C041C26BC * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t7FB2D586428E9D3F01BCA67C0B8FDB6C041C26BC ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t7FB2D586428E9D3F01BCA67C0B8FDB6C041C26BC * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1F36BE254EA38A92288ED01B3475412E3639B569_H
#ifndef LEADERBOARDITEM_TBDF8B18DB646A74FD083543E4F0F1C9D72734D2B_H
#define LEADERBOARDITEM_TBDF8B18DB646A74FD083543E4F0F1C9D72734D2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardItem
struct  LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B  : public RuntimeObject
{
public:
	// System.Int32 LeaderboardItem::Position
	int32_t ___Position_0;
	// System.String LeaderboardItem::Name
	String_t* ___Name_1;
	// System.String LeaderboardItem::Value
	String_t* ___Value_2;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B, ___Position_0)); }
	inline int32_t get_Position_0() const { return ___Position_0; }
	inline int32_t* get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(int32_t value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B, ___Value_2)); }
	inline String_t* get_Value_2() const { return ___Value_2; }
	inline String_t** get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(String_t* value)
	{
		___Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___Value_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDITEM_TBDF8B18DB646A74FD083543E4F0F1C9D72734D2B_H
#ifndef LEADERBOARDRESULT_TF0787FB00F09160F7DFA5473557E5B5004C887D7_H
#define LEADERBOARDRESULT_TF0787FB00F09160F7DFA5473557E5B5004C887D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardResult
struct  LeaderboardResult_tF0787FB00F09160F7DFA5473557E5B5004C887D7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDRESULT_TF0787FB00F09160F7DFA5473557E5B5004C887D7_H
#ifndef LEADERBOARDSVO_TB97F14B95031799573A5FBA28C02AE297BDE86C6_H
#define LEADERBOARDSVO_TB97F14B95031799573A5FBA28C02AE297BDE86C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardsVO
struct  LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<LeaderboardEntryVO> LeaderboardsVO::PlayersWorld
	List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * ___PlayersWorld_0;
	// System.Collections.Generic.List`1<LeaderboardEntryVO> LeaderboardsVO::PlayersCountry
	List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * ___PlayersCountry_1;
	// System.Collections.Generic.List`1<LeaderboardEntryVO> LeaderboardsVO::TeamsWorld
	List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * ___TeamsWorld_2;
	// System.Collections.Generic.List`1<LeaderboardEntryVO> LeaderboardsVO::TeamsCountry
	List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * ___TeamsCountry_3;
	// System.Collections.Generic.List`1<LeaderboardEntryVO> LeaderboardsVO::Friends
	List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * ___Friends_4;

public:
	inline static int32_t get_offset_of_PlayersWorld_0() { return static_cast<int32_t>(offsetof(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6, ___PlayersWorld_0)); }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * get_PlayersWorld_0() const { return ___PlayersWorld_0; }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 ** get_address_of_PlayersWorld_0() { return &___PlayersWorld_0; }
	inline void set_PlayersWorld_0(List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * value)
	{
		___PlayersWorld_0 = value;
		Il2CppCodeGenWriteBarrier((&___PlayersWorld_0), value);
	}

	inline static int32_t get_offset_of_PlayersCountry_1() { return static_cast<int32_t>(offsetof(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6, ___PlayersCountry_1)); }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * get_PlayersCountry_1() const { return ___PlayersCountry_1; }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 ** get_address_of_PlayersCountry_1() { return &___PlayersCountry_1; }
	inline void set_PlayersCountry_1(List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * value)
	{
		___PlayersCountry_1 = value;
		Il2CppCodeGenWriteBarrier((&___PlayersCountry_1), value);
	}

	inline static int32_t get_offset_of_TeamsWorld_2() { return static_cast<int32_t>(offsetof(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6, ___TeamsWorld_2)); }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * get_TeamsWorld_2() const { return ___TeamsWorld_2; }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 ** get_address_of_TeamsWorld_2() { return &___TeamsWorld_2; }
	inline void set_TeamsWorld_2(List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * value)
	{
		___TeamsWorld_2 = value;
		Il2CppCodeGenWriteBarrier((&___TeamsWorld_2), value);
	}

	inline static int32_t get_offset_of_TeamsCountry_3() { return static_cast<int32_t>(offsetof(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6, ___TeamsCountry_3)); }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * get_TeamsCountry_3() const { return ___TeamsCountry_3; }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 ** get_address_of_TeamsCountry_3() { return &___TeamsCountry_3; }
	inline void set_TeamsCountry_3(List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * value)
	{
		___TeamsCountry_3 = value;
		Il2CppCodeGenWriteBarrier((&___TeamsCountry_3), value);
	}

	inline static int32_t get_offset_of_Friends_4() { return static_cast<int32_t>(offsetof(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6, ___Friends_4)); }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * get_Friends_4() const { return ___Friends_4; }
	inline List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 ** get_address_of_Friends_4() { return &___Friends_4; }
	inline void set_Friends_4(List_1_tA8AEB5A9D40442686FDDDE9B6769BCEE30583379 * value)
	{
		___Friends_4 = value;
		Il2CppCodeGenWriteBarrier((&___Friends_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDSVO_TB97F14B95031799573A5FBA28C02AE297BDE86C6_H
#ifndef U3CTASKU3ED__2_TFBEA4B80789C3F3A2DD3E970156E579693DACD9D_H
#define U3CTASKU3ED__2_TFBEA4B80789C3F3A2DD3E970156E579693DACD9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadCurrentChapterConfigTask_<Task>d__2
struct  U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D  : public RuntimeObject
{
public:
	// System.Int32 LoadCurrentChapterConfigTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadCurrentChapterConfigTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadCurrentChapterConfigTask LoadCurrentChapterConfigTask_<Task>d__2::<>4__this
	LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 * ___U3CU3E4__this_2;
	// ChapterModel LoadCurrentChapterConfigTask_<Task>d__2::<chapterModel>5__2
	ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494 * ___U3CchapterModelU3E5__2_3;
	// Tayr.AssetBundleLoaderTask LoadCurrentChapterConfigTask_<Task>d__2::<loadConfTask>5__3
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * ___U3CloadConfTaskU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D, ___U3CU3E4__this_2)); }
	inline LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CchapterModelU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D, ___U3CchapterModelU3E5__2_3)); }
	inline ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494 * get_U3CchapterModelU3E5__2_3() const { return ___U3CchapterModelU3E5__2_3; }
	inline ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494 ** get_address_of_U3CchapterModelU3E5__2_3() { return &___U3CchapterModelU3E5__2_3; }
	inline void set_U3CchapterModelU3E5__2_3(ChapterModel_t128F63AB2A81208183CD6AC72466B96980DA0494 * value)
	{
		___U3CchapterModelU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchapterModelU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CloadConfTaskU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D, ___U3CloadConfTaskU3E5__3_4)); }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * get_U3CloadConfTaskU3E5__3_4() const { return ___U3CloadConfTaskU3E5__3_4; }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD ** get_address_of_U3CloadConfTaskU3E5__3_4() { return &___U3CloadConfTaskU3E5__3_4; }
	inline void set_U3CloadConfTaskU3E5__3_4(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * value)
	{
		___U3CloadConfTaskU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadConfTaskU3E5__3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_TFBEA4B80789C3F3A2DD3E970156E579693DACD9D_H
#ifndef U3CTASKU3ED__2_T8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35_H
#define U3CTASKU3ED__2_T8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadMainConfigsTask_<Task>d__2
struct  U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35  : public RuntimeObject
{
public:
	// System.Int32 LoadMainConfigsTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoadMainConfigsTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoadMainConfigsTask LoadMainConfigsTask_<Task>d__2::<>4__this
	LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF * ___U3CU3E4__this_2;
	// Tayr.AssetBundleLoaderTask LoadMainConfigsTask_<Task>d__2::<loadConfTask>5__2
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * ___U3CloadConfTaskU3E5__2_3;
	// LoadCurrentChapterConfigTask LoadMainConfigsTask_<Task>d__2::<loadCurrentChapterConfigTask>5__3
	LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 * ___U3CloadCurrentChapterConfigTaskU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35, ___U3CU3E4__this_2)); }
	inline LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CloadConfTaskU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35, ___U3CloadConfTaskU3E5__2_3)); }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * get_U3CloadConfTaskU3E5__2_3() const { return ___U3CloadConfTaskU3E5__2_3; }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD ** get_address_of_U3CloadConfTaskU3E5__2_3() { return &___U3CloadConfTaskU3E5__2_3; }
	inline void set_U3CloadConfTaskU3E5__2_3(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * value)
	{
		___U3CloadConfTaskU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadConfTaskU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CloadCurrentChapterConfigTaskU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35, ___U3CloadCurrentChapterConfigTaskU3E5__3_4)); }
	inline LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 * get_U3CloadCurrentChapterConfigTaskU3E5__3_4() const { return ___U3CloadCurrentChapterConfigTaskU3E5__3_4; }
	inline LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 ** get_address_of_U3CloadCurrentChapterConfigTaskU3E5__3_4() { return &___U3CloadCurrentChapterConfigTaskU3E5__3_4; }
	inline void set_U3CloadCurrentChapterConfigTaskU3E5__3_4(LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5 * value)
	{
		___U3CloadCurrentChapterConfigTaskU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadCurrentChapterConfigTaskU3E5__3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35_H
#ifndef NOTIFICATIONSUTILS_T5C24829892F954D60BC0B145F54F03700C46D50F_H
#define NOTIFICATIONSUTILS_T5C24829892F954D60BC0B145F54F03700C46D50F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationsUtils
struct  NotificationsUtils_t5C24829892F954D60BC0B145F54F03700C46D50F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSUTILS_T5C24829892F954D60BC0B145F54F03700C46D50F_H
#ifndef U3CTASKU3ED__2_TAF7989670F96DAAF2C2062141D75550BE6EB2710_H
#define U3CTASKU3ED__2_TAF7989670F96DAAF2C2062141D75550BE6EB2710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingLoadingTask_<Task>d__2
struct  U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710  : public RuntimeObject
{
public:
	// System.Int32 PatchingLoadingTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PatchingLoadingTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PatchingLoadingTask PatchingLoadingTask_<Task>d__2::<>4__this
	PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710, ___U3CU3E4__this_2)); }
	inline PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_TAF7989670F96DAAF2C2062141D75550BE6EB2710_H
#ifndef QUEST_TBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021_H
#define QUEST_TBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest
struct  Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021  : public RuntimeObject
{
public:
	// OnQuestStartedEvent Quest::_onQuestStarted
	OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 * ____onQuestStarted_0;
	// OnQuestUpdatedEvent Quest::_onQuestUpdated
	OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB * ____onQuestUpdated_1;
	// OnQuestEndedEvent Quest::_onQuestEnded
	OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F * ____onQuestEnded_2;
	// System.Collections.Generic.List`1<Tayr.Trigger> Quest::_startTriggers
	List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * ____startTriggers_3;
	// System.Collections.Generic.List`1<Tayr.Trigger> Quest::_endTriggers
	List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * ____endTriggers_4;
	// System.Boolean Quest::_isStarted
	bool ____isStarted_5;
	// System.Boolean Quest::_isUpdated
	bool ____isUpdated_6;
	// System.Boolean Quest::_isEnded
	bool ____isEnded_7;

public:
	inline static int32_t get_offset_of__onQuestStarted_0() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____onQuestStarted_0)); }
	inline OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 * get__onQuestStarted_0() const { return ____onQuestStarted_0; }
	inline OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 ** get_address_of__onQuestStarted_0() { return &____onQuestStarted_0; }
	inline void set__onQuestStarted_0(OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948 * value)
	{
		____onQuestStarted_0 = value;
		Il2CppCodeGenWriteBarrier((&____onQuestStarted_0), value);
	}

	inline static int32_t get_offset_of__onQuestUpdated_1() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____onQuestUpdated_1)); }
	inline OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB * get__onQuestUpdated_1() const { return ____onQuestUpdated_1; }
	inline OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB ** get_address_of__onQuestUpdated_1() { return &____onQuestUpdated_1; }
	inline void set__onQuestUpdated_1(OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB * value)
	{
		____onQuestUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&____onQuestUpdated_1), value);
	}

	inline static int32_t get_offset_of__onQuestEnded_2() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____onQuestEnded_2)); }
	inline OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F * get__onQuestEnded_2() const { return ____onQuestEnded_2; }
	inline OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F ** get_address_of__onQuestEnded_2() { return &____onQuestEnded_2; }
	inline void set__onQuestEnded_2(OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F * value)
	{
		____onQuestEnded_2 = value;
		Il2CppCodeGenWriteBarrier((&____onQuestEnded_2), value);
	}

	inline static int32_t get_offset_of__startTriggers_3() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____startTriggers_3)); }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * get__startTriggers_3() const { return ____startTriggers_3; }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 ** get_address_of__startTriggers_3() { return &____startTriggers_3; }
	inline void set__startTriggers_3(List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * value)
	{
		____startTriggers_3 = value;
		Il2CppCodeGenWriteBarrier((&____startTriggers_3), value);
	}

	inline static int32_t get_offset_of__endTriggers_4() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____endTriggers_4)); }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * get__endTriggers_4() const { return ____endTriggers_4; }
	inline List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 ** get_address_of__endTriggers_4() { return &____endTriggers_4; }
	inline void set__endTriggers_4(List_1_tD769F165CB73AC301E135982F0DB33265BCF7EB0 * value)
	{
		____endTriggers_4 = value;
		Il2CppCodeGenWriteBarrier((&____endTriggers_4), value);
	}

	inline static int32_t get_offset_of__isStarted_5() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____isStarted_5)); }
	inline bool get__isStarted_5() const { return ____isStarted_5; }
	inline bool* get_address_of__isStarted_5() { return &____isStarted_5; }
	inline void set__isStarted_5(bool value)
	{
		____isStarted_5 = value;
	}

	inline static int32_t get_offset_of__isUpdated_6() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____isUpdated_6)); }
	inline bool get__isUpdated_6() const { return ____isUpdated_6; }
	inline bool* get_address_of__isUpdated_6() { return &____isUpdated_6; }
	inline void set__isUpdated_6(bool value)
	{
		____isUpdated_6 = value;
	}

	inline static int32_t get_offset_of__isEnded_7() { return static_cast<int32_t>(offsetof(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021, ____isEnded_7)); }
	inline bool get__isEnded_7() const { return ____isEnded_7; }
	inline bool* get_address_of__isEnded_7() { return &____isEnded_7; }
	inline void set__isEnded_7(bool value)
	{
		____isEnded_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEST_TBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T54616E3EFBF3ACD267B52391C2028B56E60D9328_H
#define U3CU3EC__DISPLAYCLASS11_0_T54616E3EFBF3ACD267B52391C2028B56E60D9328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328  : public RuntimeObject
{
public:
	// Tayr.Trigger Quest_<>c__DisplayClass11_0::t
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ___t_0;
	// Quest Quest_<>c__DisplayClass11_0::<>4__this
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328, ___t_0)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get_t_0() const { return ___t_0; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328, ___U3CU3E4__this_1)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T54616E3EFBF3ACD267B52391C2028B56E60D9328_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T7EED697364D448A257B04EB82E68193380EE4802_H
#define U3CU3EC__DISPLAYCLASS12_0_T7EED697364D448A257B04EB82E68193380EE4802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802  : public RuntimeObject
{
public:
	// Quest Quest_<>c__DisplayClass12_0::<>4__this
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ___U3CU3E4__this_0;
	// Tayr.Trigger Quest_<>c__DisplayClass12_0::t
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ___t_1;
	// UnityEngine.Events.UnityAction Quest_<>c__DisplayClass12_0::<>9__0
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802, ___U3CU3E4__this_0)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802, ___t_1)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get_t_1() const { return ___t_1; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		___t_1 = value;
		Il2CppCodeGenWriteBarrier((&___t_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802, ___U3CU3E9__0_2)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T7EED697364D448A257B04EB82E68193380EE4802_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T273D4CBDC29F9DB87ACF1356FE964106CF9E21D9_H
#define U3CU3EC__DISPLAYCLASS8_0_T273D4CBDC29F9DB87ACF1356FE964106CF9E21D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Quest_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9  : public RuntimeObject
{
public:
	// Tayr.Trigger Quest_<>c__DisplayClass8_0::t
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ___t_0;
	// Quest Quest_<>c__DisplayClass8_0::<>4__this
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9, ___t_0)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get_t_0() const { return ___t_0; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9, ___U3CU3E4__this_1)); }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T273D4CBDC29F9DB87ACF1356FE964106CF9E21D9_H
#ifndef QUESTSYSTEM_TDFB5A64D6154EACAD855C582FF3E40C7A14C83D1_H
#define QUESTSYSTEM_TDFB5A64D6154EACAD855C582FF3E40C7A14C83D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestSystem
struct  QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1  : public RuntimeObject
{
public:
	// QuestsVO QuestSystem::_questsVO
	QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 * ____questsVO_0;
	// System.Collections.Generic.Dictionary`2<System.String,Quest> QuestSystem::_quests
	Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 * ____quests_1;

public:
	inline static int32_t get_offset_of__questsVO_0() { return static_cast<int32_t>(offsetof(QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1, ____questsVO_0)); }
	inline QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 * get__questsVO_0() const { return ____questsVO_0; }
	inline QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 ** get_address_of__questsVO_0() { return &____questsVO_0; }
	inline void set__questsVO_0(QuestsVO_t585A9D6E6540A29D33D1DE147EAFF40417B85429 * value)
	{
		____questsVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____questsVO_0), value);
	}

	inline static int32_t get_offset_of__quests_1() { return static_cast<int32_t>(offsetof(QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1, ____quests_1)); }
	inline Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 * get__quests_1() const { return ____quests_1; }
	inline Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 ** get_address_of__quests_1() { return &____quests_1; }
	inline void set__quests_1(Dictionary_2_t3AA935A331F98DF15B2004585C1645B647B60793 * value)
	{
		____quests_1 = value;
		Il2CppCodeGenWriteBarrier((&____quests_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTSYSTEM_TDFB5A64D6154EACAD855C582FF3E40C7A14C83D1_H
#ifndef U3CTASKU3ED__2_T97406B9E700FC87C8C33A6ABB7569928F6574E63_H
#define U3CTASKU3ED__2_T97406B9E700FC87C8C33A6ABB7569928F6574E63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundLoaderTask_<Task>d__2
struct  U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63  : public RuntimeObject
{
public:
	// System.Int32 SoundLoaderTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SoundLoaderTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SoundLoaderTask SoundLoaderTask_<Task>d__2::<>4__this
	SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94 * ___U3CU3E4__this_2;
	// Tayr.AssetBundleLoaderTask SoundLoaderTask_<Task>d__2::<loadSoundsTask>5__2
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * ___U3CloadSoundsTaskU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63, ___U3CU3E4__this_2)); }
	inline SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CloadSoundsTaskU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63, ___U3CloadSoundsTaskU3E5__2_3)); }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * get_U3CloadSoundsTaskU3E5__2_3() const { return ___U3CloadSoundsTaskU3E5__2_3; }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD ** get_address_of_U3CloadSoundsTaskU3E5__2_3() { return &___U3CloadSoundsTaskU3E5__2_3; }
	inline void set_U3CloadSoundsTaskU3E5__2_3(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * value)
	{
		___U3CloadSoundsTaskU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadSoundsTaskU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T97406B9E700FC87C8C33A6ABB7569928F6574E63_H
#ifndef SOUNDUTILS_T6727D4BB1D79FF716E16DB7DF6045389EF1978ED_H
#define SOUNDUTILS_T6727D4BB1D79FF716E16DB7DF6045389EF1978ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundUtils
struct  SoundUtils_t6727D4BB1D79FF716E16DB7DF6045389EF1978ED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDUTILS_T6727D4BB1D79FF716E16DB7DF6045389EF1978ED_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#define BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTSystem
struct  BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8  : public RuntimeObject
{
public:
	// Zenject.DisposableManager Tayr.BasicTSystem::_disposableManager
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * ____disposableManager_0;
	// Tayr.TSystemsManager Tayr.BasicTSystem::_systemsManager
	TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * ____systemsManager_1;
	// System.Boolean Tayr.BasicTSystem::<IsInitialized>k__BackingField
	bool ___U3CIsInitializedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__disposableManager_0() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____disposableManager_0)); }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * get__disposableManager_0() const { return ____disposableManager_0; }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC ** get_address_of__disposableManager_0() { return &____disposableManager_0; }
	inline void set__disposableManager_0(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * value)
	{
		____disposableManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposableManager_0), value);
	}

	inline static int32_t get_offset_of__systemsManager_1() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____systemsManager_1)); }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * get__systemsManager_1() const { return ____systemsManager_1; }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 ** get_address_of__systemsManager_1() { return &____systemsManager_1; }
	inline void set__systemsManager_1(TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * value)
	{
		____systemsManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____systemsManager_1), value);
	}

	inline static int32_t get_offset_of_U3CIsInitializedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ___U3CIsInitializedU3Ek__BackingField_2)); }
	inline bool get_U3CIsInitializedU3Ek__BackingField_2() const { return ___U3CIsInitializedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsInitializedU3Ek__BackingField_2() { return &___U3CIsInitializedU3Ek__BackingField_2; }
	inline void set_U3CIsInitializedU3Ek__BackingField_2(bool value)
	{
		___U3CIsInitializedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifndef BASICTASK_1_T3D37C4CEAD254AC581775BEC8CD68C5995932272_H
#define BASICTASK_1_T3D37C4CEAD254AC581775BEC8CD68C5995932272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<GameSparkTaskResult>
struct  BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D * ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_t43EF8AC2701BB8F3562A810C81A2F12AB7816744 * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272, ___Result_2)); }
	inline GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D * get_Result_2() const { return ___Result_2; }
	inline GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D ** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D * value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_T3D37C4CEAD254AC581775BEC8CD68C5995932272_H
#ifndef BASICTASK_1_T72628B9661223E9AC81F764A0CC87506D9220190_H
#define BASICTASK_1_T72628B9661223E9AC81F764A0CC87506D9220190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.Collections.Generic.List`1<GameSparks.Api.Responses.LeaderboardDataResponse__LeaderboardData>>
struct  BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	List_1_t0BF8EF0317CCCD765223A954137BAF568F8473F9 * ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_tFBF23A05774991651CA8A8353F52D0996F128F47 * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190, ___Result_2)); }
	inline List_1_t0BF8EF0317CCCD765223A954137BAF568F8473F9 * get_Result_2() const { return ___Result_2; }
	inline List_1_t0BF8EF0317CCCD765223A954137BAF568F8473F9 ** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(List_1_t0BF8EF0317CCCD765223A954137BAF568F8473F9 * value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_T72628B9661223E9AC81F764A0CC87506D9220190_H
#ifndef BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#define BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.Int32>
struct  BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	int32_t ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ___Result_2)); }
	inline int32_t get_Result_2() const { return ___Result_2; }
	inline int32_t* get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(int32_t value)
	{
		___Result_2 = value;
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#ifndef BASICTASK_1_TE70B7733E99882370633F0BDBE9D6208EB03B77E_H
#define BASICTASK_1_TE70B7733E99882370633F0BDBE9D6208EB03B77E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.String>
struct  BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	String_t* ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ___Result_2)); }
	inline String_t* get_Result_2() const { return ___Result_2; }
	inline String_t** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(String_t* value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_TE70B7733E99882370633F0BDBE9D6208EB03B77E_H
#ifndef TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#define TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TDynamicListData
struct  TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Tayr.TDynamicListData::Prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_0;

public:
	inline static int32_t get_offset_of_Prefab_0() { return static_cast<int32_t>(offsetof(TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369, ___Prefab_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Prefab_0() const { return ___Prefab_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Prefab_0() { return &___Prefab_0; }
	inline void set_Prefab_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef USERINITIALIZER_TB34747F20CCB084A0CB56CDD4D0F2E70A7904B56_H
#define USERINITIALIZER_TB34747F20CCB084A0CB56CDD4D0F2E70A7904B56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserInitializer
struct  UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56  : public RuntimeObject
{
public:
	// Zenject.DiContainer UserInitializer::_gameContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____gameContainer_0;
	// InventorySystem UserInitializer::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_1;
	// System.Diagnostics.Stopwatch UserInitializer::_stopwatch
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ____stopwatch_2;

public:
	inline static int32_t get_offset_of__gameContainer_0() { return static_cast<int32_t>(offsetof(UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56, ____gameContainer_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__gameContainer_0() const { return ____gameContainer_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__gameContainer_0() { return &____gameContainer_0; }
	inline void set__gameContainer_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____gameContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameContainer_0), value);
	}

	inline static int32_t get_offset_of__inventorySystem_1() { return static_cast<int32_t>(offsetof(UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56, ____inventorySystem_1)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_1() const { return ____inventorySystem_1; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_1() { return &____inventorySystem_1; }
	inline void set__inventorySystem_1(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_1 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_1), value);
	}

	inline static int32_t get_offset_of__stopwatch_2() { return static_cast<int32_t>(offsetof(UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56, ____stopwatch_2)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get__stopwatch_2() const { return ____stopwatch_2; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of__stopwatch_2() { return &____stopwatch_2; }
	inline void set__stopwatch_2(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		____stopwatch_2 = value;
		Il2CppCodeGenWriteBarrier((&____stopwatch_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERINITIALIZER_TB34747F20CCB084A0CB56CDD4D0F2E70A7904B56_H
#ifndef CHATLISTITEMDATA_TB039C181C297B802B7B35327AA170A2106CDD53B_H
#define CHATLISTITEMDATA_TB039C181C297B802B7B35327AA170A2106CDD53B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatListItemData
struct  ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B  : public TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369
{
public:
	// System.String ChatListItemData::MessageId
	String_t* ___MessageId_1;
	// System.String ChatListItemData::SenderId
	String_t* ___SenderId_2;
	// System.String ChatListItemData::SenderDisplayName
	String_t* ___SenderDisplayName_3;
	// System.String ChatListItemData::Message
	String_t* ___Message_4;
	// CustomMessageData ChatListItemData::CustomMessageData
	CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD * ___CustomMessageData_5;
	// System.Int64 ChatListItemData::EpochTime
	int64_t ___EpochTime_6;
	// Tayr.TDynamicList ChatListItemData::List
	TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * ___List_7;

public:
	inline static int32_t get_offset_of_MessageId_1() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___MessageId_1)); }
	inline String_t* get_MessageId_1() const { return ___MessageId_1; }
	inline String_t** get_address_of_MessageId_1() { return &___MessageId_1; }
	inline void set_MessageId_1(String_t* value)
	{
		___MessageId_1 = value;
		Il2CppCodeGenWriteBarrier((&___MessageId_1), value);
	}

	inline static int32_t get_offset_of_SenderId_2() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___SenderId_2)); }
	inline String_t* get_SenderId_2() const { return ___SenderId_2; }
	inline String_t** get_address_of_SenderId_2() { return &___SenderId_2; }
	inline void set_SenderId_2(String_t* value)
	{
		___SenderId_2 = value;
		Il2CppCodeGenWriteBarrier((&___SenderId_2), value);
	}

	inline static int32_t get_offset_of_SenderDisplayName_3() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___SenderDisplayName_3)); }
	inline String_t* get_SenderDisplayName_3() const { return ___SenderDisplayName_3; }
	inline String_t** get_address_of_SenderDisplayName_3() { return &___SenderDisplayName_3; }
	inline void set_SenderDisplayName_3(String_t* value)
	{
		___SenderDisplayName_3 = value;
		Il2CppCodeGenWriteBarrier((&___SenderDisplayName_3), value);
	}

	inline static int32_t get_offset_of_Message_4() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___Message_4)); }
	inline String_t* get_Message_4() const { return ___Message_4; }
	inline String_t** get_address_of_Message_4() { return &___Message_4; }
	inline void set_Message_4(String_t* value)
	{
		___Message_4 = value;
		Il2CppCodeGenWriteBarrier((&___Message_4), value);
	}

	inline static int32_t get_offset_of_CustomMessageData_5() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___CustomMessageData_5)); }
	inline CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD * get_CustomMessageData_5() const { return ___CustomMessageData_5; }
	inline CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD ** get_address_of_CustomMessageData_5() { return &___CustomMessageData_5; }
	inline void set_CustomMessageData_5(CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD * value)
	{
		___CustomMessageData_5 = value;
		Il2CppCodeGenWriteBarrier((&___CustomMessageData_5), value);
	}

	inline static int32_t get_offset_of_EpochTime_6() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___EpochTime_6)); }
	inline int64_t get_EpochTime_6() const { return ___EpochTime_6; }
	inline int64_t* get_address_of_EpochTime_6() { return &___EpochTime_6; }
	inline void set_EpochTime_6(int64_t value)
	{
		___EpochTime_6 = value;
	}

	inline static int32_t get_offset_of_List_7() { return static_cast<int32_t>(offsetof(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B, ___List_7)); }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * get_List_7() const { return ___List_7; }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 ** get_address_of_List_7() { return &___List_7; }
	inline void set_List_7(TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * value)
	{
		___List_7 = value;
		Il2CppCodeGenWriteBarrier((&___List_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATLISTITEMDATA_TB039C181C297B802B7B35327AA170A2106CDD53B_H
#ifndef COINCURRENCY_T70769E5BB795869C1DED24C416885D24B3CE856F_H
#define COINCURRENCY_T70769E5BB795869C1DED24C416885D24B3CE856F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinCurrency
struct  CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F  : public BasicCurrency_tFFB51F40C3056DE62E56D9C4FFB0714758FAD359
{
public:
	// InventoryVO CoinCurrency::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_1;

public:
	inline static int32_t get_offset_of__inventoryVO_1() { return static_cast<int32_t>(offsetof(CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F, ____inventoryVO_1)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_1() const { return ____inventoryVO_1; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_1() { return &____inventoryVO_1; }
	inline void set__inventoryVO_1(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_1), value);
	}
};

struct CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F_StaticFields
{
public:
	// CoinChangeEvent CoinCurrency::OnCoinChanged
	CoinChangeEvent_t6C548FCB73453B99DB2C3676CE6D004563A80B82 * ___OnCoinChanged_0;

public:
	inline static int32_t get_offset_of_OnCoinChanged_0() { return static_cast<int32_t>(offsetof(CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F_StaticFields, ___OnCoinChanged_0)); }
	inline CoinChangeEvent_t6C548FCB73453B99DB2C3676CE6D004563A80B82 * get_OnCoinChanged_0() const { return ___OnCoinChanged_0; }
	inline CoinChangeEvent_t6C548FCB73453B99DB2C3676CE6D004563A80B82 ** get_address_of_OnCoinChanged_0() { return &___OnCoinChanged_0; }
	inline void set_OnCoinChanged_0(CoinChangeEvent_t6C548FCB73453B99DB2C3676CE6D004563A80B82 * value)
	{
		___OnCoinChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnCoinChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COINCURRENCY_T70769E5BB795869C1DED24C416885D24B3CE856F_H
#ifndef DISPOSEEVENTSYSTEM_TF862CD99C56BD5FF05B9AD75ECEDDD0D5862D18C_H
#define DISPOSEEVENTSYSTEM_TF862CD99C56BD5FF05B9AD75ECEDDD0D5862D18C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisposeEventSystem
struct  DisposeEventSystem_tF862CD99C56BD5FF05B9AD75ECEDDD0D5862D18C  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// System.Collections.Generic.List`1<IDisposeListener> DisposeEventSystem::_listenerBuffer
	List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 * ____listenerBuffer_3;

public:
	inline static int32_t get_offset_of__listenerBuffer_3() { return static_cast<int32_t>(offsetof(DisposeEventSystem_tF862CD99C56BD5FF05B9AD75ECEDDD0D5862D18C, ____listenerBuffer_3)); }
	inline List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 * get__listenerBuffer_3() const { return ____listenerBuffer_3; }
	inline List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 ** get_address_of__listenerBuffer_3() { return &____listenerBuffer_3; }
	inline void set__listenerBuffer_3(List_1_t03D595E408D83DBD7951492238B61E93BABC7A03 * value)
	{
		____listenerBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&____listenerBuffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSEEVENTSYSTEM_TF862CD99C56BD5FF05B9AD75ECEDDD0D5862D18C_H
#ifndef CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#define CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.CodeGeneration.Attributes.ContextAttribute
struct  ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Entitas.CodeGeneration.Attributes.ContextAttribute::contextName
	String_t* ___contextName_0;

public:
	inline static int32_t get_offset_of_contextName_0() { return static_cast<int32_t>(offsetof(ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817, ___contextName_0)); }
	inline String_t* get_contextName_0() const { return ___contextName_0; }
	inline String_t** get_address_of_contextName_0() { return &___contextName_0; }
	inline void set_contextName_0(String_t* value)
	{
		___contextName_0 = value;
		Il2CppCodeGenWriteBarrier((&___contextName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTATTRIBUTE_TCC46F2BA273A4F8FE981674FA22BA7D229AF6817_H
#ifndef FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#define FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Feature
struct  Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F  : public Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifndef GAMEENTITY_T995A5498A539905D7F1F8284D59BD48F077D78FF_H
#define GAMEENTITY_T995A5498A539905D7F1F8284D59BD48F077D78FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEntity
struct  GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF  : public Entity_tB86FED06A87B5FEA836FF73B89D5168789557783
{
public:

public:
};

struct GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields
{
public:
	// AggroDisabledComponent GameEntity::aggroDisabledComponent
	AggroDisabledComponent_t7D0F3FF57D659CAF0C81F2D14851E44388B01278 * ___aggroDisabledComponent_18;
	// BattleReviveComponent GameEntity::battleReviveComponent
	BattleReviveComponent_t274D262B5A3AA78EBC2831B67D816ED2737C8CDA * ___battleReviveComponent_19;
	// BlockingSpell GameEntity::blockingSpellComponent
	BlockingSpell_tEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007 * ___blockingSpellComponent_20;
	// CollectableComponent GameEntity::collectableComponent
	CollectableComponent_t0BD7C71F3589B884C11505B8D6D784632FCF1A5A * ___collectableComponent_21;
	// DestroyableComponent GameEntity::destroyableComponent
	DestroyableComponent_t0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC * ___destroyableComponent_22;
	// DisposeComponent GameEntity::disposeComponent
	DisposeComponent_t9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E * ___disposeComponent_23;
	// EffectStartedComponent GameEntity::effectStartedComponent
	EffectStartedComponent_tA442FC7E8A7696782A04C2786BEBE9EF91669E4A * ___effectStartedComponent_24;
	// FXSoundComponent GameEntity::fXSoundComponent
	FXSoundComponent_t67B072AC87CD6EDDE23219727D01789C036D6600 * ___fXSoundComponent_25;
	// InstantEffectComponent GameEntity::instantEffectComponent
	InstantEffectComponent_t208614FDD7E97EAFE28016B6B16193058AFFFFCD * ___instantEffectComponent_26;
	// MusicSoundComponent GameEntity::musicSoundComponent
	MusicSoundComponent_tB975365C7BD5C382B6D1685AA770668ED79096C4 * ___musicSoundComponent_27;
	// ObjectiveCompleteComponent GameEntity::objectiveCompleteComponent
	ObjectiveCompleteComponent_t1AA886C7D93C680C37D82B7A0514801EA0F04852 * ___objectiveCompleteComponent_28;
	// RemoveViewOnDisposeComponent GameEntity::removeViewOnDisposeComponent
	RemoveViewOnDisposeComponent_tE54A9A8E3DB4158A7D2E649412FEACD20965E449 * ___removeViewOnDisposeComponent_29;
	// TowerHealingComponent GameEntity::towerHealingComponent
	TowerHealingComponent_tBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4 * ___towerHealingComponent_30;

public:
	inline static int32_t get_offset_of_aggroDisabledComponent_18() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___aggroDisabledComponent_18)); }
	inline AggroDisabledComponent_t7D0F3FF57D659CAF0C81F2D14851E44388B01278 * get_aggroDisabledComponent_18() const { return ___aggroDisabledComponent_18; }
	inline AggroDisabledComponent_t7D0F3FF57D659CAF0C81F2D14851E44388B01278 ** get_address_of_aggroDisabledComponent_18() { return &___aggroDisabledComponent_18; }
	inline void set_aggroDisabledComponent_18(AggroDisabledComponent_t7D0F3FF57D659CAF0C81F2D14851E44388B01278 * value)
	{
		___aggroDisabledComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___aggroDisabledComponent_18), value);
	}

	inline static int32_t get_offset_of_battleReviveComponent_19() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___battleReviveComponent_19)); }
	inline BattleReviveComponent_t274D262B5A3AA78EBC2831B67D816ED2737C8CDA * get_battleReviveComponent_19() const { return ___battleReviveComponent_19; }
	inline BattleReviveComponent_t274D262B5A3AA78EBC2831B67D816ED2737C8CDA ** get_address_of_battleReviveComponent_19() { return &___battleReviveComponent_19; }
	inline void set_battleReviveComponent_19(BattleReviveComponent_t274D262B5A3AA78EBC2831B67D816ED2737C8CDA * value)
	{
		___battleReviveComponent_19 = value;
		Il2CppCodeGenWriteBarrier((&___battleReviveComponent_19), value);
	}

	inline static int32_t get_offset_of_blockingSpellComponent_20() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___blockingSpellComponent_20)); }
	inline BlockingSpell_tEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007 * get_blockingSpellComponent_20() const { return ___blockingSpellComponent_20; }
	inline BlockingSpell_tEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007 ** get_address_of_blockingSpellComponent_20() { return &___blockingSpellComponent_20; }
	inline void set_blockingSpellComponent_20(BlockingSpell_tEC8ACE0D193BB3B7997CFE67A30BB0F454CB6007 * value)
	{
		___blockingSpellComponent_20 = value;
		Il2CppCodeGenWriteBarrier((&___blockingSpellComponent_20), value);
	}

	inline static int32_t get_offset_of_collectableComponent_21() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___collectableComponent_21)); }
	inline CollectableComponent_t0BD7C71F3589B884C11505B8D6D784632FCF1A5A * get_collectableComponent_21() const { return ___collectableComponent_21; }
	inline CollectableComponent_t0BD7C71F3589B884C11505B8D6D784632FCF1A5A ** get_address_of_collectableComponent_21() { return &___collectableComponent_21; }
	inline void set_collectableComponent_21(CollectableComponent_t0BD7C71F3589B884C11505B8D6D784632FCF1A5A * value)
	{
		___collectableComponent_21 = value;
		Il2CppCodeGenWriteBarrier((&___collectableComponent_21), value);
	}

	inline static int32_t get_offset_of_destroyableComponent_22() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___destroyableComponent_22)); }
	inline DestroyableComponent_t0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC * get_destroyableComponent_22() const { return ___destroyableComponent_22; }
	inline DestroyableComponent_t0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC ** get_address_of_destroyableComponent_22() { return &___destroyableComponent_22; }
	inline void set_destroyableComponent_22(DestroyableComponent_t0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC * value)
	{
		___destroyableComponent_22 = value;
		Il2CppCodeGenWriteBarrier((&___destroyableComponent_22), value);
	}

	inline static int32_t get_offset_of_disposeComponent_23() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___disposeComponent_23)); }
	inline DisposeComponent_t9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E * get_disposeComponent_23() const { return ___disposeComponent_23; }
	inline DisposeComponent_t9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E ** get_address_of_disposeComponent_23() { return &___disposeComponent_23; }
	inline void set_disposeComponent_23(DisposeComponent_t9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E * value)
	{
		___disposeComponent_23 = value;
		Il2CppCodeGenWriteBarrier((&___disposeComponent_23), value);
	}

	inline static int32_t get_offset_of_effectStartedComponent_24() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___effectStartedComponent_24)); }
	inline EffectStartedComponent_tA442FC7E8A7696782A04C2786BEBE9EF91669E4A * get_effectStartedComponent_24() const { return ___effectStartedComponent_24; }
	inline EffectStartedComponent_tA442FC7E8A7696782A04C2786BEBE9EF91669E4A ** get_address_of_effectStartedComponent_24() { return &___effectStartedComponent_24; }
	inline void set_effectStartedComponent_24(EffectStartedComponent_tA442FC7E8A7696782A04C2786BEBE9EF91669E4A * value)
	{
		___effectStartedComponent_24 = value;
		Il2CppCodeGenWriteBarrier((&___effectStartedComponent_24), value);
	}

	inline static int32_t get_offset_of_fXSoundComponent_25() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___fXSoundComponent_25)); }
	inline FXSoundComponent_t67B072AC87CD6EDDE23219727D01789C036D6600 * get_fXSoundComponent_25() const { return ___fXSoundComponent_25; }
	inline FXSoundComponent_t67B072AC87CD6EDDE23219727D01789C036D6600 ** get_address_of_fXSoundComponent_25() { return &___fXSoundComponent_25; }
	inline void set_fXSoundComponent_25(FXSoundComponent_t67B072AC87CD6EDDE23219727D01789C036D6600 * value)
	{
		___fXSoundComponent_25 = value;
		Il2CppCodeGenWriteBarrier((&___fXSoundComponent_25), value);
	}

	inline static int32_t get_offset_of_instantEffectComponent_26() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___instantEffectComponent_26)); }
	inline InstantEffectComponent_t208614FDD7E97EAFE28016B6B16193058AFFFFCD * get_instantEffectComponent_26() const { return ___instantEffectComponent_26; }
	inline InstantEffectComponent_t208614FDD7E97EAFE28016B6B16193058AFFFFCD ** get_address_of_instantEffectComponent_26() { return &___instantEffectComponent_26; }
	inline void set_instantEffectComponent_26(InstantEffectComponent_t208614FDD7E97EAFE28016B6B16193058AFFFFCD * value)
	{
		___instantEffectComponent_26 = value;
		Il2CppCodeGenWriteBarrier((&___instantEffectComponent_26), value);
	}

	inline static int32_t get_offset_of_musicSoundComponent_27() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___musicSoundComponent_27)); }
	inline MusicSoundComponent_tB975365C7BD5C382B6D1685AA770668ED79096C4 * get_musicSoundComponent_27() const { return ___musicSoundComponent_27; }
	inline MusicSoundComponent_tB975365C7BD5C382B6D1685AA770668ED79096C4 ** get_address_of_musicSoundComponent_27() { return &___musicSoundComponent_27; }
	inline void set_musicSoundComponent_27(MusicSoundComponent_tB975365C7BD5C382B6D1685AA770668ED79096C4 * value)
	{
		___musicSoundComponent_27 = value;
		Il2CppCodeGenWriteBarrier((&___musicSoundComponent_27), value);
	}

	inline static int32_t get_offset_of_objectiveCompleteComponent_28() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___objectiveCompleteComponent_28)); }
	inline ObjectiveCompleteComponent_t1AA886C7D93C680C37D82B7A0514801EA0F04852 * get_objectiveCompleteComponent_28() const { return ___objectiveCompleteComponent_28; }
	inline ObjectiveCompleteComponent_t1AA886C7D93C680C37D82B7A0514801EA0F04852 ** get_address_of_objectiveCompleteComponent_28() { return &___objectiveCompleteComponent_28; }
	inline void set_objectiveCompleteComponent_28(ObjectiveCompleteComponent_t1AA886C7D93C680C37D82B7A0514801EA0F04852 * value)
	{
		___objectiveCompleteComponent_28 = value;
		Il2CppCodeGenWriteBarrier((&___objectiveCompleteComponent_28), value);
	}

	inline static int32_t get_offset_of_removeViewOnDisposeComponent_29() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___removeViewOnDisposeComponent_29)); }
	inline RemoveViewOnDisposeComponent_tE54A9A8E3DB4158A7D2E649412FEACD20965E449 * get_removeViewOnDisposeComponent_29() const { return ___removeViewOnDisposeComponent_29; }
	inline RemoveViewOnDisposeComponent_tE54A9A8E3DB4158A7D2E649412FEACD20965E449 ** get_address_of_removeViewOnDisposeComponent_29() { return &___removeViewOnDisposeComponent_29; }
	inline void set_removeViewOnDisposeComponent_29(RemoveViewOnDisposeComponent_tE54A9A8E3DB4158A7D2E649412FEACD20965E449 * value)
	{
		___removeViewOnDisposeComponent_29 = value;
		Il2CppCodeGenWriteBarrier((&___removeViewOnDisposeComponent_29), value);
	}

	inline static int32_t get_offset_of_towerHealingComponent_30() { return static_cast<int32_t>(offsetof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields, ___towerHealingComponent_30)); }
	inline TowerHealingComponent_tBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4 * get_towerHealingComponent_30() const { return ___towerHealingComponent_30; }
	inline TowerHealingComponent_tBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4 ** get_address_of_towerHealingComponent_30() { return &___towerHealingComponent_30; }
	inline void set_towerHealingComponent_30(TowerHealingComponent_tBF1C6AD3469D91F868A272D1ED2BD56CDB4D93D4 * value)
	{
		___towerHealingComponent_30 = value;
		Il2CppCodeGenWriteBarrier((&___towerHealingComponent_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEENTITY_T995A5498A539905D7F1F8284D59BD48F077D78FF_H
#ifndef GAMESPARKCHAPTERPATCHERTASK_TB58333A280C2A0E03D79E5038D0D0D5B6E6D1497_H
#define GAMESPARKCHAPTERPATCHERTASK_TB58333A280C2A0E03D79E5038D0D0D5B6E6D1497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkChapterPatcherTask
struct  GameSparkChapterPatcherTask_tB58333A280C2A0E03D79E5038D0D0D5B6E6D1497  : public BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E
{
public:
	// System.String GameSparkChapterPatcherTask::_chapter
	String_t* ____chapter_7;

public:
	inline static int32_t get_offset_of__chapter_7() { return static_cast<int32_t>(offsetof(GameSparkChapterPatcherTask_tB58333A280C2A0E03D79E5038D0D0D5B6E6D1497, ____chapter_7)); }
	inline String_t* get__chapter_7() const { return ____chapter_7; }
	inline String_t** get_address_of__chapter_7() { return &____chapter_7; }
	inline void set__chapter_7(String_t* value)
	{
		____chapter_7 = value;
		Il2CppCodeGenWriteBarrier((&____chapter_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKCHAPTERPATCHERTASK_TB58333A280C2A0E03D79E5038D0D0D5B6E6D1497_H
#ifndef GAMESPARKPATCHERTASK_T5BAF57CC343AF3D35224622137E6F71F96E3F356_H
#define GAMESPARKPATCHERTASK_T5BAF57CC343AF3D35224622137E6F71F96E3F356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparkPatcherTask
struct  GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356  : public BasicTask_1_t3D37C4CEAD254AC581775BEC8CD68C5995932272
{
public:
	// Tayr.GameSparksPlatform GameSparkPatcherTask::_platfrom
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____platfrom_7;
	// PatchingVO GameSparkPatcherTask::_patchingVO
	PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * ____patchingVO_8;
	// System.Collections.Generic.List`1<System.String> GameSparkPatcherTask::MissingAssets
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___MissingAssets_9;
	// System.String GameSparkPatcherTask::_file
	String_t* ____file_10;
	// System.String GameSparkPatcherTask::_filePath
	String_t* ____filePath_11;
	// System.String GameSparkPatcherTask::_version
	String_t* ____version_12;

public:
	inline static int32_t get_offset_of__platfrom_7() { return static_cast<int32_t>(offsetof(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356, ____platfrom_7)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__platfrom_7() const { return ____platfrom_7; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__platfrom_7() { return &____platfrom_7; }
	inline void set__platfrom_7(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____platfrom_7 = value;
		Il2CppCodeGenWriteBarrier((&____platfrom_7), value);
	}

	inline static int32_t get_offset_of__patchingVO_8() { return static_cast<int32_t>(offsetof(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356, ____patchingVO_8)); }
	inline PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * get__patchingVO_8() const { return ____patchingVO_8; }
	inline PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 ** get_address_of__patchingVO_8() { return &____patchingVO_8; }
	inline void set__patchingVO_8(PatchingVO_tF6810B5A961785E0EB990B4C511101A0A75F3CB4 * value)
	{
		____patchingVO_8 = value;
		Il2CppCodeGenWriteBarrier((&____patchingVO_8), value);
	}

	inline static int32_t get_offset_of_MissingAssets_9() { return static_cast<int32_t>(offsetof(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356, ___MissingAssets_9)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_MissingAssets_9() const { return ___MissingAssets_9; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_MissingAssets_9() { return &___MissingAssets_9; }
	inline void set_MissingAssets_9(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___MissingAssets_9 = value;
		Il2CppCodeGenWriteBarrier((&___MissingAssets_9), value);
	}

	inline static int32_t get_offset_of__file_10() { return static_cast<int32_t>(offsetof(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356, ____file_10)); }
	inline String_t* get__file_10() const { return ____file_10; }
	inline String_t** get_address_of__file_10() { return &____file_10; }
	inline void set__file_10(String_t* value)
	{
		____file_10 = value;
		Il2CppCodeGenWriteBarrier((&____file_10), value);
	}

	inline static int32_t get_offset_of__filePath_11() { return static_cast<int32_t>(offsetof(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356, ____filePath_11)); }
	inline String_t* get__filePath_11() const { return ____filePath_11; }
	inline String_t** get_address_of__filePath_11() { return &____filePath_11; }
	inline void set__filePath_11(String_t* value)
	{
		____filePath_11 = value;
		Il2CppCodeGenWriteBarrier((&____filePath_11), value);
	}

	inline static int32_t get_offset_of__version_12() { return static_cast<int32_t>(offsetof(GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356, ____version_12)); }
	inline String_t* get__version_12() const { return ____version_12; }
	inline String_t** get_address_of__version_12() { return &____version_12; }
	inline void set__version_12(String_t* value)
	{
		____version_12 = value;
		Il2CppCodeGenWriteBarrier((&____version_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKPATCHERTASK_T5BAF57CC343AF3D35224622137E6F71F96E3F356_H
#ifndef GETLEADERBOARDTASK_TB21F2867CBA59C00C45CBC34AEFC21AFDED6595F_H
#define GETLEADERBOARDTASK_TB21F2867CBA59C00C45CBC34AEFC21AFDED6595F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetLeaderboardTask
struct  GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F  : public BasicTask_1_t72628B9661223E9AC81F764A0CC87506D9220190
{
public:
	// Tayr.GameSparksPlatform GetLeaderboardTask::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_7;
	// System.String GetLeaderboardTask::_leaderboard
	String_t* ____leaderboard_8;
	// System.Int32 GetLeaderboardTask::_entries
	int32_t ____entries_9;

public:
	inline static int32_t get_offset_of__gameSparks_7() { return static_cast<int32_t>(offsetof(GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F, ____gameSparks_7)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_7() const { return ____gameSparks_7; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_7() { return &____gameSparks_7; }
	inline void set__gameSparks_7(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_7 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_7), value);
	}

	inline static int32_t get_offset_of__leaderboard_8() { return static_cast<int32_t>(offsetof(GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F, ____leaderboard_8)); }
	inline String_t* get__leaderboard_8() const { return ____leaderboard_8; }
	inline String_t** get_address_of__leaderboard_8() { return &____leaderboard_8; }
	inline void set__leaderboard_8(String_t* value)
	{
		____leaderboard_8 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboard_8), value);
	}

	inline static int32_t get_offset_of__entries_9() { return static_cast<int32_t>(offsetof(GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F, ____entries_9)); }
	inline int32_t get__entries_9() const { return ____entries_9; }
	inline int32_t* get_address_of__entries_9() { return &____entries_9; }
	inline void set__entries_9(int32_t value)
	{
		____entries_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLEADERBOARDTASK_TB21F2867CBA59C00C45CBC34AEFC21AFDED6595F_H
#ifndef HEARTCURRENCY_T05BCFE0B1401C086F694BE623529CBCA8321D520_H
#define HEARTCURRENCY_T05BCFE0B1401C086F694BE623529CBCA8321D520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeartCurrency
struct  HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520  : public BasicCurrency_tFFB51F40C3056DE62E56D9C4FFB0714758FAD359
{
public:
	// GameSettingsSO HeartCurrency::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_0;
	// InventoryVO HeartCurrency::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_1;

public:
	inline static int32_t get_offset_of__gameSettingsSO_0() { return static_cast<int32_t>(offsetof(HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520, ____gameSettingsSO_0)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_0() const { return ____gameSettingsSO_0; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_0() { return &____gameSettingsSO_0; }
	inline void set__gameSettingsSO_0(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_0), value);
	}

	inline static int32_t get_offset_of__inventoryVO_1() { return static_cast<int32_t>(offsetof(HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520, ____inventoryVO_1)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_1() const { return ____inventoryVO_1; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_1() { return &____inventoryVO_1; }
	inline void set__inventoryVO_1(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_1), value);
	}
};

struct HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520_StaticFields
{
public:
	// HeartChangeEvent HeartCurrency::OnHeartChanged
	HeartChangeEvent_tA26E38B4515190BCC8CDA8D960DFFF4F480B4682 * ___OnHeartChanged_2;

public:
	inline static int32_t get_offset_of_OnHeartChanged_2() { return static_cast<int32_t>(offsetof(HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520_StaticFields, ___OnHeartChanged_2)); }
	inline HeartChangeEvent_tA26E38B4515190BCC8CDA8D960DFFF4F480B4682 * get_OnHeartChanged_2() const { return ___OnHeartChanged_2; }
	inline HeartChangeEvent_tA26E38B4515190BCC8CDA8D960DFFF4F480B4682 ** get_address_of_OnHeartChanged_2() { return &___OnHeartChanged_2; }
	inline void set_OnHeartChanged_2(HeartChangeEvent_tA26E38B4515190BCC8CDA8D960DFFF4F480B4682 * value)
	{
		___OnHeartChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnHeartChanged_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTCURRENCY_T05BCFE0B1401C086F694BE623529CBCA8321D520_H
#ifndef INVENTORYSYSTEM_T7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2_H
#define INVENTORYSYSTEM_T7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventorySystem
struct  InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// Zenject.DiContainer InventorySystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// System.Collections.Generic.Dictionary`2<CurrencyType,BasicCurrency> InventorySystem::Currencies
	Dictionary_2_t1A4877ECBD56B243D0B1BEC96033345054EDD972 * ___Currencies_4;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of_Currencies_4() { return static_cast<int32_t>(offsetof(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2, ___Currencies_4)); }
	inline Dictionary_2_t1A4877ECBD56B243D0B1BEC96033345054EDD972 * get_Currencies_4() const { return ___Currencies_4; }
	inline Dictionary_2_t1A4877ECBD56B243D0B1BEC96033345054EDD972 ** get_address_of_Currencies_4() { return &___Currencies_4; }
	inline void set_Currencies_4(Dictionary_2_t1A4877ECBD56B243D0B1BEC96033345054EDD972 * value)
	{
		___Currencies_4 = value;
		Il2CppCodeGenWriteBarrier((&___Currencies_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYSYSTEM_T7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2_H
#ifndef LOADCURRENTCHAPTERCONFIGTASK_T9C541A313EC69D380D57BEB368B1DDE8943562A5_H
#define LOADCURRENTCHAPTERCONFIGTASK_T9C541A313EC69D380D57BEB368B1DDE8943562A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadCurrentChapterConfigTask
struct  LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Zenject.DiContainer LoadCurrentChapterConfigTask::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_7;

public:
	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5, ____container_7)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_7() const { return ____container_7; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCURRENTCHAPTERCONFIGTASK_T9C541A313EC69D380D57BEB368B1DDE8943562A5_H
#ifndef LOADMAINCONFIGSTASK_TEFA8D762329C2D5DD9A4310C02729F9239559BCF_H
#define LOADMAINCONFIGSTASK_TEFA8D762329C2D5DD9A4310C02729F9239559BCF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadMainConfigsTask
struct  LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Zenject.DiContainer LoadMainConfigsTask::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_7;

public:
	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF, ____container_7)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_7() const { return ____container_7; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADMAINCONFIGSTASK_TEFA8D762329C2D5DD9A4310C02729F9239559BCF_H
#ifndef NOTIFICATIONSSYSTEM_T112A3E3163906777DF795E66F73BBA012B942BEC_H
#define NOTIFICATIONSSYSTEM_T112A3E3163906777DF795E66F73BBA012B942BEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationsSystem
struct  NotificationsSystem_t112A3E3163906777DF795E66F73BBA012B942BEC  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONSSYSTEM_T112A3E3163906777DF795E66F73BBA012B942BEC_H
#ifndef PATCHINGLOADINGTASK_TAD0100057A1341A5BEE5F2F8C5976D322882DFDC_H
#define PATCHINGLOADINGTASK_TAD0100057A1341A5BEE5F2F8C5976D322882DFDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatchingLoadingTask
struct  PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Zenject.DiContainer PatchingLoadingTask::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_7;

public:
	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC, ____container_7)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_7() const { return ____container_7; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGLOADINGTASK_TAD0100057A1341A5BEE5F2F8C5976D322882DFDC_H
#ifndef SOUNDLOADERTASK_T86E568DBD1682DBED4340EF4BDE820F1DB7F3B94_H
#define SOUNDLOADERTASK_T86E568DBD1682DBED4340EF4BDE820F1DB7F3B94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundLoaderTask
struct  SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Zenject.DiContainer SoundLoaderTask::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_7;

public:
	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94, ____container_7)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_7() const { return ____container_7; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDLOADERTASK_T86E568DBD1682DBED4340EF4BDE820F1DB7F3B94_H
#ifndef SOUNDSYSTEM_TAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D_H
#define SOUNDSYSTEM_TAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundSystem
struct  SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// UserVO SoundSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_3;
	// Tayr.TSoundSystem SoundSystem::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_4;

public:
	inline static int32_t get_offset_of__userVO_3() { return static_cast<int32_t>(offsetof(SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D, ____userVO_3)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_3() const { return ____userVO_3; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_3() { return &____userVO_3; }
	inline void set__userVO_3(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_3), value);
	}

	inline static int32_t get_offset_of__soundSystem_4() { return static_cast<int32_t>(offsetof(SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D, ____soundSystem_4)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_4() const { return ____soundSystem_4; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_4() { return &____soundSystem_4; }
	inline void set__soundSystem_4(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDSYSTEM_TAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D_H
#ifndef SPELLCURRENCY_T9223222B256A6443353853F6C36226D4751FF6F8_H
#define SPELLCURRENCY_T9223222B256A6443353853F6C36226D4751FF6F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellCurrency
struct  SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8  : public BasicCurrency_tFFB51F40C3056DE62E56D9C4FFB0714758FAD359
{
public:
	// InventoryVO SpellCurrency::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_0;

public:
	inline static int32_t get_offset_of__inventoryVO_0() { return static_cast<int32_t>(offsetof(SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8, ____inventoryVO_0)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_0() const { return ____inventoryVO_0; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_0() { return &____inventoryVO_0; }
	inline void set__inventoryVO_0(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_0 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_0), value);
	}
};

struct SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8_StaticFields
{
public:
	// SpellChangeEvent SpellCurrency::OnSpellChange
	SpellChangeEvent_tDE9387410E957AFF62D95542A2D2AE619A3A6D9D * ___OnSpellChange_1;

public:
	inline static int32_t get_offset_of_OnSpellChange_1() { return static_cast<int32_t>(offsetof(SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8_StaticFields, ___OnSpellChange_1)); }
	inline SpellChangeEvent_tDE9387410E957AFF62D95542A2D2AE619A3A6D9D * get_OnSpellChange_1() const { return ___OnSpellChange_1; }
	inline SpellChangeEvent_tDE9387410E957AFF62D95542A2D2AE619A3A6D9D ** get_address_of_OnSpellChange_1() { return &___OnSpellChange_1; }
	inline void set_OnSpellChange_1(SpellChangeEvent_tDE9387410E957AFF62D95542A2D2AE619A3A6D9D * value)
	{
		___OnSpellChange_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnSpellChange_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLCURRENCY_T9223222B256A6443353853F6C36226D4751FF6F8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#define NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#define UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifndef UNITYEVENT_2_TB6FBF368E0FCB69F38AD7BBF80F532B281E0A62D_H
#define UNITYEVENT_2_TB6FBF368E0FCB69F38AD7BBF80F532B281E0A62D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.String,System.Int32>
struct  UnityEvent_2_tB6FBF368E0FCB69F38AD7BBF80F532B281E0A62D  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_2_tB6FBF368E0FCB69F38AD7BBF80F532B281E0A62D, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_TB6FBF368E0FCB69F38AD7BBF80F532B281E0A62D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef COINCHANGEEVENT_T6C548FCB73453B99DB2C3676CE6D004563A80B82_H
#define COINCHANGEEVENT_T6C548FCB73453B99DB2C3676CE6D004563A80B82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinChangeEvent
struct  CoinChangeEvent_t6C548FCB73453B99DB2C3676CE6D004563A80B82  : public UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COINCHANGEEVENT_T6C548FCB73453B99DB2C3676CE6D004563A80B82_H
#ifndef CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#define CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurrencyType
struct  CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55 
{
public:
	// System.Int32 CurrencyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCYTYPE_T22F6D5B9845CBB5FA241F40F84F92C0900D51C55_H
#ifndef GAMEATTRIBUTE_TEAC4F9478367269006D9122C41F46A607105BE84_H
#define GAMEATTRIBUTE_TEAC4F9478367269006D9122C41F46A607105BE84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAttribute
struct  GameAttribute_tEAC4F9478367269006D9122C41F46A607105BE84  : public ContextAttribute_tCC46F2BA273A4F8FE981674FA22BA7D229AF6817
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEATTRIBUTE_TEAC4F9478367269006D9122C41F46A607105BE84_H
#ifndef GAMEEVENTSYSTEMS_T761FC34066CD15B909F184B97715937F42D6986C_H
#define GAMEEVENTSYSTEMS_T761FC34066CD15B909F184B97715937F42D6986C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEventSystems
struct  GameEventSystems_t761FC34066CD15B909F184B97715937F42D6986C  : public Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENTSYSTEMS_T761FC34066CD15B909F184B97715937F42D6986C_H
#ifndef GAMESERVERTYPE_TCBECD733E74A8E692D2BBBA2808C030954318020_H
#define GAMESERVERTYPE_TCBECD733E74A8E692D2BBBA2808C030954318020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameServerType
struct  GameServerType_tCBECD733E74A8E692D2BBBA2808C030954318020 
{
public:
	// System.Int32 GameServerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameServerType_tCBECD733E74A8E692D2BBBA2808C030954318020, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESERVERTYPE_TCBECD733E74A8E692D2BBBA2808C030954318020_H
#ifndef HEARTCHANGEEVENT_TA26E38B4515190BCC8CDA8D960DFFF4F480B4682_H
#define HEARTCHANGEEVENT_TA26E38B4515190BCC8CDA8D960DFFF4F480B4682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeartChangeEvent
struct  HeartChangeEvent_tA26E38B4515190BCC8CDA8D960DFFF4F480B4682  : public UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTCHANGEEVENT_TA26E38B4515190BCC8CDA8D960DFFF4F480B4682_H
#ifndef KICKERWITHICONDATA_T687808E0A968B2ACC743F8678E33BA19AB1EF54E_H
#define KICKERWITHICONDATA_T687808E0A968B2ACC743F8678E33BA19AB1EF54E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickerWithIconData
struct  KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E  : public RuntimeObject
{
public:
	// System.String KickerWithIconData::UnitId
	String_t* ___UnitId_0;
	// UnityEngine.Vector3 KickerWithIconData::UnitPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___UnitPosition_1;
	// UnityEngine.Vector3 KickerWithIconData::UIDestination
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___UIDestination_2;

public:
	inline static int32_t get_offset_of_UnitId_0() { return static_cast<int32_t>(offsetof(KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E, ___UnitId_0)); }
	inline String_t* get_UnitId_0() const { return ___UnitId_0; }
	inline String_t** get_address_of_UnitId_0() { return &___UnitId_0; }
	inline void set_UnitId_0(String_t* value)
	{
		___UnitId_0 = value;
		Il2CppCodeGenWriteBarrier((&___UnitId_0), value);
	}

	inline static int32_t get_offset_of_UnitPosition_1() { return static_cast<int32_t>(offsetof(KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E, ___UnitPosition_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_UnitPosition_1() const { return ___UnitPosition_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_UnitPosition_1() { return &___UnitPosition_1; }
	inline void set_UnitPosition_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___UnitPosition_1 = value;
	}

	inline static int32_t get_offset_of_UIDestination_2() { return static_cast<int32_t>(offsetof(KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E, ___UIDestination_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_UIDestination_2() const { return ___UIDestination_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_UIDestination_2() { return &___UIDestination_2; }
	inline void set_UIDestination_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___UIDestination_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKERWITHICONDATA_T687808E0A968B2ACC743F8678E33BA19AB1EF54E_H
#ifndef LEADERBOARDENTRYVO_TC88EE4E32C6B13EFB59FFB703619EE1115EA9737_H
#define LEADERBOARDENTRYVO_TC88EE4E32C6B13EFB59FFB703619EE1115EA9737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardEntryVO
struct  LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Int64> LeaderboardEntryVO::Rank
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___Rank_0;
	// System.String LeaderboardEntryVO::PlayerId
	String_t* ___PlayerId_1;
	// System.String LeaderboardEntryVO::PlayerName
	String_t* ___PlayerName_2;
	// System.String LeaderboardEntryVO::TeamName
	String_t* ___TeamName_3;
	// System.Int32 LeaderboardEntryVO::TeamIcon
	int32_t ___TeamIcon_4;
	// System.String LeaderboardEntryVO::TeamId
	String_t* ___TeamId_5;
	// System.Int32 LeaderboardEntryVO::Trophy
	int32_t ___Trophy_6;
	// System.Int32 LeaderboardEntryVO::Level
	int32_t ___Level_7;

public:
	inline static int32_t get_offset_of_Rank_0() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___Rank_0)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_Rank_0() const { return ___Rank_0; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_Rank_0() { return &___Rank_0; }
	inline void set_Rank_0(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___Rank_0 = value;
	}

	inline static int32_t get_offset_of_PlayerId_1() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___PlayerId_1)); }
	inline String_t* get_PlayerId_1() const { return ___PlayerId_1; }
	inline String_t** get_address_of_PlayerId_1() { return &___PlayerId_1; }
	inline void set_PlayerId_1(String_t* value)
	{
		___PlayerId_1 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerId_1), value);
	}

	inline static int32_t get_offset_of_PlayerName_2() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___PlayerName_2)); }
	inline String_t* get_PlayerName_2() const { return ___PlayerName_2; }
	inline String_t** get_address_of_PlayerName_2() { return &___PlayerName_2; }
	inline void set_PlayerName_2(String_t* value)
	{
		___PlayerName_2 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerName_2), value);
	}

	inline static int32_t get_offset_of_TeamName_3() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___TeamName_3)); }
	inline String_t* get_TeamName_3() const { return ___TeamName_3; }
	inline String_t** get_address_of_TeamName_3() { return &___TeamName_3; }
	inline void set_TeamName_3(String_t* value)
	{
		___TeamName_3 = value;
		Il2CppCodeGenWriteBarrier((&___TeamName_3), value);
	}

	inline static int32_t get_offset_of_TeamIcon_4() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___TeamIcon_4)); }
	inline int32_t get_TeamIcon_4() const { return ___TeamIcon_4; }
	inline int32_t* get_address_of_TeamIcon_4() { return &___TeamIcon_4; }
	inline void set_TeamIcon_4(int32_t value)
	{
		___TeamIcon_4 = value;
	}

	inline static int32_t get_offset_of_TeamId_5() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___TeamId_5)); }
	inline String_t* get_TeamId_5() const { return ___TeamId_5; }
	inline String_t** get_address_of_TeamId_5() { return &___TeamId_5; }
	inline void set_TeamId_5(String_t* value)
	{
		___TeamId_5 = value;
		Il2CppCodeGenWriteBarrier((&___TeamId_5), value);
	}

	inline static int32_t get_offset_of_Trophy_6() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___Trophy_6)); }
	inline int32_t get_Trophy_6() const { return ___Trophy_6; }
	inline int32_t* get_address_of_Trophy_6() { return &___Trophy_6; }
	inline void set_Trophy_6(int32_t value)
	{
		___Trophy_6 = value;
	}

	inline static int32_t get_offset_of_Level_7() { return static_cast<int32_t>(offsetof(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737, ___Level_7)); }
	inline int32_t get_Level_7() const { return ___Level_7; }
	inline int32_t* get_address_of_Level_7() { return &___Level_7; }
	inline void set_Level_7(int32_t value)
	{
		___Level_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDENTRYVO_TC88EE4E32C6B13EFB59FFB703619EE1115EA9737_H
#ifndef ONQUESTENDEDEVENT_TC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F_H
#define ONQUESTENDEDEVENT_TC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnQuestEndedEvent
struct  OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUESTENDEDEVENT_TC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F_H
#ifndef ONQUESTSTARTEDEVENT_T8E404B059FB2011AF0DC8299EBBF6A2DB9657948_H
#define ONQUESTSTARTEDEVENT_T8E404B059FB2011AF0DC8299EBBF6A2DB9657948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnQuestStartedEvent
struct  OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUESTSTARTEDEVENT_T8E404B059FB2011AF0DC8299EBBF6A2DB9657948_H
#ifndef ONQUESTUPDATEDEVENT_T8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB_H
#define ONQUESTUPDATEDEVENT_T8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnQuestUpdatedEvent
struct  OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUESTUPDATEDEVENT_T8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB_H
#ifndef SPELLCHANGEEVENT_TDE9387410E957AFF62D95542A2D2AE619A3A6D9D_H
#define SPELLCHANGEEVENT_TDE9387410E957AFF62D95542A2D2AE619A3A6D9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellChangeEvent
struct  SpellChangeEvent_tDE9387410E957AFF62D95542A2D2AE619A3A6D9D  : public UnityEvent_2_tB6FBF368E0FCB69F38AD7BBF80F532B281E0A62D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLCHANGEEVENT_TDE9387410E957AFF62D95542A2D2AE619A3A6D9D_H
#ifndef TEAM_TYPE_T3C178DBCDEB1B9A42045B6C497BEC33A8557E170_H
#define TEAM_TYPE_T3C178DBCDEB1B9A42045B6C497BEC33A8557E170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TEAM_TYPE
struct  TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170 
{
public:
	// System.Int32 TEAM_TYPE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAM_TYPE_T3C178DBCDEB1B9A42045B6C497BEC33A8557E170_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CURRENCYMODEL_TDE590F10FA55F9C8B7ABA73472C2536A14F37550_H
#define CURRENCYMODEL_TDE590F10FA55F9C8B7ABA73472C2536A14F37550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CurrencyModel
struct  CurrencyModel_tDE590F10FA55F9C8B7ABA73472C2536A14F37550  : public RuntimeObject
{
public:
	// CurrencyType CurrencyModel::Type
	int32_t ___Type_0;
	// System.Int32 CurrencyModel::Value
	int32_t ___Value_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(CurrencyModel_tDE590F10FA55F9C8B7ABA73472C2536A14F37550, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(CurrencyModel_tDE590F10FA55F9C8B7ABA73472C2536A14F37550, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENCYMODEL_TDE590F10FA55F9C8B7ABA73472C2536A14F37550_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef GAMEINITIALIZER_T87F1DF05038F161E7A997344511D38DEF08F6BBF_H
#define GAMEINITIALIZER_T87F1DF05038F161E7A997344511D38DEF08F6BBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameInitializer
struct  GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.DiContainer GameInitializer::_gameContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____gameContainer_4;
	// UserEvents GameInitializer::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_5;
	// System.Diagnostics.Stopwatch GameInitializer::_stopwatch
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ____stopwatch_6;
	// UnityEngine.TextAsset GameInitializer::_globalSO
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ____globalSO_7;

public:
	inline static int32_t get_offset_of__gameContainer_4() { return static_cast<int32_t>(offsetof(GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF, ____gameContainer_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__gameContainer_4() const { return ____gameContainer_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__gameContainer_4() { return &____gameContainer_4; }
	inline void set__gameContainer_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____gameContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameContainer_4), value);
	}

	inline static int32_t get_offset_of__userEvents_5() { return static_cast<int32_t>(offsetof(GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF, ____userEvents_5)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_5() const { return ____userEvents_5; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_5() { return &____userEvents_5; }
	inline void set__userEvents_5(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_5), value);
	}

	inline static int32_t get_offset_of__stopwatch_6() { return static_cast<int32_t>(offsetof(GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF, ____stopwatch_6)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get__stopwatch_6() const { return ____stopwatch_6; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of__stopwatch_6() { return &____stopwatch_6; }
	inline void set__stopwatch_6(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		____stopwatch_6 = value;
		Il2CppCodeGenWriteBarrier((&____stopwatch_6), value);
	}

	inline static int32_t get_offset_of__globalSO_7() { return static_cast<int32_t>(offsetof(GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF, ____globalSO_7)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get__globalSO_7() const { return ____globalSO_7; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of__globalSO_7() { return &____globalSO_7; }
	inline void set__globalSO_7(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		____globalSO_7 = value;
		Il2CppCodeGenWriteBarrier((&____globalSO_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEINITIALIZER_T87F1DF05038F161E7A997344511D38DEF08F6BBF_H
#ifndef TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#define TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TMonoBehaviour
struct  TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifndef COINUICOMPONENT_T6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3_H
#define COINUICOMPONENT_T6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinUIComponent
struct  CoinUIComponent_t6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// TMPro.TextMeshProUGUI CoinUIComponent::_coinValue
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____coinValue_4;
	// InventorySystem CoinUIComponent::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_5;

public:
	inline static int32_t get_offset_of__coinValue_4() { return static_cast<int32_t>(offsetof(CoinUIComponent_t6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3, ____coinValue_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__coinValue_4() const { return ____coinValue_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__coinValue_4() { return &____coinValue_4; }
	inline void set__coinValue_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____coinValue_4 = value;
		Il2CppCodeGenWriteBarrier((&____coinValue_4), value);
	}

	inline static int32_t get_offset_of__inventorySystem_5() { return static_cast<int32_t>(offsetof(CoinUIComponent_t6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3, ____inventorySystem_5)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_5() const { return ____inventorySystem_5; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_5() { return &____inventorySystem_5; }
	inline void set__inventorySystem_5(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_5 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COINUICOMPONENT_T6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3_H
#ifndef HEARTUICOMPONENT_T1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90_H
#define HEARTUICOMPONENT_T1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeartUIComponent
struct  HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// TMPro.TextMeshProUGUI HeartUIComponent::_heartValue
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____heartValue_4;
	// TMPro.TextMeshProUGUI HeartUIComponent::_heartCountDown
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____heartCountDown_5;
	// InventorySystem HeartUIComponent::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_6;
	// GameSettingsSO HeartUIComponent::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_7;
	// InventoryVO HeartUIComponent::_inventoryVO
	InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * ____inventoryVO_8;
	// System.String HeartUIComponent::_fullLocalized
	String_t* ____fullLocalized_9;

public:
	inline static int32_t get_offset_of__heartValue_4() { return static_cast<int32_t>(offsetof(HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90, ____heartValue_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__heartValue_4() const { return ____heartValue_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__heartValue_4() { return &____heartValue_4; }
	inline void set__heartValue_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____heartValue_4 = value;
		Il2CppCodeGenWriteBarrier((&____heartValue_4), value);
	}

	inline static int32_t get_offset_of__heartCountDown_5() { return static_cast<int32_t>(offsetof(HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90, ____heartCountDown_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__heartCountDown_5() const { return ____heartCountDown_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__heartCountDown_5() { return &____heartCountDown_5; }
	inline void set__heartCountDown_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____heartCountDown_5 = value;
		Il2CppCodeGenWriteBarrier((&____heartCountDown_5), value);
	}

	inline static int32_t get_offset_of__inventorySystem_6() { return static_cast<int32_t>(offsetof(HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90, ____inventorySystem_6)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_6() const { return ____inventorySystem_6; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_6() { return &____inventorySystem_6; }
	inline void set__inventorySystem_6(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_6 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_6), value);
	}

	inline static int32_t get_offset_of__gameSettingsSO_7() { return static_cast<int32_t>(offsetof(HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90, ____gameSettingsSO_7)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_7() const { return ____gameSettingsSO_7; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_7() { return &____gameSettingsSO_7; }
	inline void set__gameSettingsSO_7(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_7 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_7), value);
	}

	inline static int32_t get_offset_of__inventoryVO_8() { return static_cast<int32_t>(offsetof(HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90, ____inventoryVO_8)); }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * get__inventoryVO_8() const { return ____inventoryVO_8; }
	inline InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 ** get_address_of__inventoryVO_8() { return &____inventoryVO_8; }
	inline void set__inventoryVO_8(InventoryVO_tC7F0305FC930782BD2E4A47E3BD0504F0DCC6AD9 * value)
	{
		____inventoryVO_8 = value;
		Il2CppCodeGenWriteBarrier((&____inventoryVO_8), value);
	}

	inline static int32_t get_offset_of__fullLocalized_9() { return static_cast<int32_t>(offsetof(HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90, ____fullLocalized_9)); }
	inline String_t* get__fullLocalized_9() const { return ____fullLocalized_9; }
	inline String_t** get_address_of__fullLocalized_9() { return &____fullLocalized_9; }
	inline void set__fullLocalized_9(String_t* value)
	{
		____fullLocalized_9 = value;
		Il2CppCodeGenWriteBarrier((&____fullLocalized_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTUICOMPONENT_T1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90_H
#ifndef HOMEVIEWCOMPONENT_T542EECD1BE167E3BCC518711DDEA72C32D01D818_H
#define HOMEVIEWCOMPONENT_T542EECD1BE167E3BCC518711DDEA72C32D01D818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeViewComponent
struct  HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.UI.Button HomeViewComponent::_playBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____playBtn_4;
	// UnityEngine.UI.Button HomeViewComponent::_contributeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____contributeBtn_5;
	// TMPro.TextMeshProUGUI HomeViewComponent::_levelNumberTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____levelNumberTxt_6;
	// StarChestComponent HomeViewComponent::_starChest
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 * ____starChest_7;
	// Tayr.ILibrary HomeViewComponent::_uiMain
	RuntimeObject* ____uiMain_8;
	// StarTournamentSystem HomeViewComponent::_starTournamentSystem
	StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 * ____starTournamentSystem_9;
	// UserVO HomeViewComponent::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_10;
	// KickerManager HomeViewComponent::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_11;
	// InventorySystem HomeViewComponent::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_12;
	// Zenject.DiContainer HomeViewComponent::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_13;
	// Zenject.DiContainer HomeViewComponent::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_14;
	// Tayr.TSoundSystem HomeViewComponent::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_15;
	// SoundSO HomeViewComponent::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_16;
	// ChaptersSO HomeViewComponent::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_17;
	// LevelVariable HomeViewComponent::_levelVariable
	LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 * ____levelVariable_18;

public:
	inline static int32_t get_offset_of__playBtn_4() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____playBtn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__playBtn_4() const { return ____playBtn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__playBtn_4() { return &____playBtn_4; }
	inline void set__playBtn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____playBtn_4 = value;
		Il2CppCodeGenWriteBarrier((&____playBtn_4), value);
	}

	inline static int32_t get_offset_of__contributeBtn_5() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____contributeBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__contributeBtn_5() const { return ____contributeBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__contributeBtn_5() { return &____contributeBtn_5; }
	inline void set__contributeBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____contributeBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&____contributeBtn_5), value);
	}

	inline static int32_t get_offset_of__levelNumberTxt_6() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____levelNumberTxt_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__levelNumberTxt_6() const { return ____levelNumberTxt_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__levelNumberTxt_6() { return &____levelNumberTxt_6; }
	inline void set__levelNumberTxt_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____levelNumberTxt_6 = value;
		Il2CppCodeGenWriteBarrier((&____levelNumberTxt_6), value);
	}

	inline static int32_t get_offset_of__starChest_7() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____starChest_7)); }
	inline StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 * get__starChest_7() const { return ____starChest_7; }
	inline StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 ** get_address_of__starChest_7() { return &____starChest_7; }
	inline void set__starChest_7(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 * value)
	{
		____starChest_7 = value;
		Il2CppCodeGenWriteBarrier((&____starChest_7), value);
	}

	inline static int32_t get_offset_of__uiMain_8() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____uiMain_8)); }
	inline RuntimeObject* get__uiMain_8() const { return ____uiMain_8; }
	inline RuntimeObject** get_address_of__uiMain_8() { return &____uiMain_8; }
	inline void set__uiMain_8(RuntimeObject* value)
	{
		____uiMain_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_8), value);
	}

	inline static int32_t get_offset_of__starTournamentSystem_9() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____starTournamentSystem_9)); }
	inline StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 * get__starTournamentSystem_9() const { return ____starTournamentSystem_9; }
	inline StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 ** get_address_of__starTournamentSystem_9() { return &____starTournamentSystem_9; }
	inline void set__starTournamentSystem_9(StarTournamentSystem_tBBE982F84537BACA13B9A61826F04C1F5F726C88 * value)
	{
		____starTournamentSystem_9 = value;
		Il2CppCodeGenWriteBarrier((&____starTournamentSystem_9), value);
	}

	inline static int32_t get_offset_of__userVO_10() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____userVO_10)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_10() const { return ____userVO_10; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_10() { return &____userVO_10; }
	inline void set__userVO_10(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_10 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_10), value);
	}

	inline static int32_t get_offset_of__kickerManager_11() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____kickerManager_11)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_11() const { return ____kickerManager_11; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_11() { return &____kickerManager_11; }
	inline void set__kickerManager_11(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_11 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_11), value);
	}

	inline static int32_t get_offset_of__inventorySystem_12() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____inventorySystem_12)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_12() const { return ____inventorySystem_12; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_12() { return &____inventorySystem_12; }
	inline void set__inventorySystem_12(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_12 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_12), value);
	}

	inline static int32_t get_offset_of__diContainer_13() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____diContainer_13)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_13() const { return ____diContainer_13; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_13() { return &____diContainer_13; }
	inline void set__diContainer_13(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_13 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_13), value);
	}

	inline static int32_t get_offset_of__container_14() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____container_14)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_14() const { return ____container_14; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_14() { return &____container_14; }
	inline void set__container_14(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_14 = value;
		Il2CppCodeGenWriteBarrier((&____container_14), value);
	}

	inline static int32_t get_offset_of__soundSystem_15() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____soundSystem_15)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_15() const { return ____soundSystem_15; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_15() { return &____soundSystem_15; }
	inline void set__soundSystem_15(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_15 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_15), value);
	}

	inline static int32_t get_offset_of__soundSO_16() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____soundSO_16)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_16() const { return ____soundSO_16; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_16() { return &____soundSO_16; }
	inline void set__soundSO_16(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_16 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_16), value);
	}

	inline static int32_t get_offset_of__chaptersSO_17() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____chaptersSO_17)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_17() const { return ____chaptersSO_17; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_17() { return &____chaptersSO_17; }
	inline void set__chaptersSO_17(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_17 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_17), value);
	}

	inline static int32_t get_offset_of__levelVariable_18() { return static_cast<int32_t>(offsetof(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818, ____levelVariable_18)); }
	inline LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 * get__levelVariable_18() const { return ____levelVariable_18; }
	inline LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 ** get_address_of__levelVariable_18() { return &____levelVariable_18; }
	inline void set__levelVariable_18(LevelVariable_t8E3E27789809A8DFA12D41DB3927AC1DC90D4279 * value)
	{
		____levelVariable_18 = value;
		Il2CppCodeGenWriteBarrier((&____levelVariable_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOMEVIEWCOMPONENT_T542EECD1BE167E3BCC518711DDEA72C32D01D818_H
#ifndef LIVESVIEWCOMPONENT_T06302A373C9D185C357A2BE407DFF82D1D7E3C16_H
#define LIVESVIEWCOMPONENT_T06302A373C9D185C357A2BE407DFF82D1D7E3C16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LivesViewComponent
struct  LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.UI.Button LivesViewComponent::_joinBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____joinBtn_4;
	// TMPro.TextMeshProUGUI LivesViewComponent::_joinText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____joinText_5;
	// GameSettingsSO LivesViewComponent::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_6;
	// MainUINode LivesViewComponent::_node
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4 * ____node_7;

public:
	inline static int32_t get_offset_of__joinBtn_4() { return static_cast<int32_t>(offsetof(LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16, ____joinBtn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__joinBtn_4() const { return ____joinBtn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__joinBtn_4() { return &____joinBtn_4; }
	inline void set__joinBtn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____joinBtn_4 = value;
		Il2CppCodeGenWriteBarrier((&____joinBtn_4), value);
	}

	inline static int32_t get_offset_of__joinText_5() { return static_cast<int32_t>(offsetof(LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16, ____joinText_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__joinText_5() const { return ____joinText_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__joinText_5() { return &____joinText_5; }
	inline void set__joinText_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____joinText_5 = value;
		Il2CppCodeGenWriteBarrier((&____joinText_5), value);
	}

	inline static int32_t get_offset_of__gameSettingsSO_6() { return static_cast<int32_t>(offsetof(LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16, ____gameSettingsSO_6)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_6() const { return ____gameSettingsSO_6; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_6() { return &____gameSettingsSO_6; }
	inline void set__gameSettingsSO_6(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_6 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_6), value);
	}

	inline static int32_t get_offset_of__node_7() { return static_cast<int32_t>(offsetof(LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16, ____node_7)); }
	inline MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4 * get__node_7() const { return ____node_7; }
	inline MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4 ** get_address_of__node_7() { return &____node_7; }
	inline void set__node_7(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4 * value)
	{
		____node_7 = value;
		Il2CppCodeGenWriteBarrier((&____node_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIVESVIEWCOMPONENT_T06302A373C9D185C357A2BE407DFF82D1D7E3C16_H
#ifndef BASICNODE_1_T0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED_H
#define BASICNODE_1_T0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<GameSparks.Core.GSData>
struct  BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED, ___Data_4)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_Data_4() const { return ___Data_4; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED_H
#ifndef BASICNODE_1_T35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF_H
#define BASICNODE_1_T35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<KickerNodeData>
struct  BasicNode_1_t35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF, ___Data_4)); }
	inline KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE * get_Data_4() const { return ___Data_4; }
	inline KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF_H
#ifndef BASICNODE_1_T39BEC8934AF01F7BDF51F905745C8109F7316F75_H
#define BASICNODE_1_T39BEC8934AF01F7BDF51F905745C8109F7316F75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<KickerWithIconData>
struct  BasicNode_1_t39BEC8934AF01F7BDF51F905745C8109F7316F75  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E * ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t39BEC8934AF01F7BDF51F905745C8109F7316F75, ___Data_4)); }
	inline KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E * get_Data_4() const { return ___Data_4; }
	inline KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E ** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E * value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t39BEC8934AF01F7BDF51F905745C8109F7316F75, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t39BEC8934AF01F7BDF51F905745C8109F7316F75, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T39BEC8934AF01F7BDF51F905745C8109F7316F75_H
#ifndef BASICNODE_1_T1EE3FADC6580D788C109C53BD985DF163FDC806C_H
#define BASICNODE_1_T1EE3FADC6580D788C109C53BD985DF163FDC806C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.Boolean>
struct  BasicNode_1_t1EE3FADC6580D788C109C53BD985DF163FDC806C  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	bool ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t1EE3FADC6580D788C109C53BD985DF163FDC806C, ___Data_4)); }
	inline bool get_Data_4() const { return ___Data_4; }
	inline bool* get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(bool value)
	{
		___Data_4 = value;
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t1EE3FADC6580D788C109C53BD985DF163FDC806C, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t1EE3FADC6580D788C109C53BD985DF163FDC806C, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T1EE3FADC6580D788C109C53BD985DF163FDC806C_H
#ifndef BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#define BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.Int32>
struct  BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	int32_t ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ___Data_4)); }
	inline int32_t get_Data_4() const { return ___Data_4; }
	inline int32_t* get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(int32_t value)
	{
		___Data_4 = value;
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T80C898A8840F148EAC8A5FE47B08C3ABA79CE64D_H
#ifndef BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#define BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.String>
struct  BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	String_t* ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ___Data_4)); }
	inline String_t* get_Data_4() const { return ___Data_4; }
	inline String_t** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(String_t* value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifndef TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#define TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TListItem
struct  TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifndef CHATHEARTLISTITEM_TBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29_H
#define CHATHEARTLISTITEM_TBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatHeartListItem
struct  ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI ChatHeartListItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_4;
	// UnityEngine.UI.Button ChatHeartListItem::_helpBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____helpBtn_5;
	// TMPro.TextMeshProUGUI ChatHeartListItem::_lifeReceived
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____lifeReceived_6;
	// UserVO ChatHeartListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_7;
	// UserEvents ChatHeartListItem::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_8;
	// Tayr.GameSparksPlatform ChatHeartListItem::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_9;
	// ChatListItemData ChatHeartListItem::_item
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * ____item_10;
	// System.Int32 ChatHeartListItem::_lifeReceivedCount
	int32_t ____lifeReceivedCount_11;
	// System.Int32 ChatHeartListItem::_index
	int32_t ____index_12;

public:
	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____name_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_4() const { return ____name_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__helpBtn_5() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____helpBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__helpBtn_5() const { return ____helpBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__helpBtn_5() { return &____helpBtn_5; }
	inline void set__helpBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____helpBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpBtn_5), value);
	}

	inline static int32_t get_offset_of__lifeReceived_6() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____lifeReceived_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__lifeReceived_6() const { return ____lifeReceived_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__lifeReceived_6() { return &____lifeReceived_6; }
	inline void set__lifeReceived_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____lifeReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&____lifeReceived_6), value);
	}

	inline static int32_t get_offset_of__userVO_7() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____userVO_7)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_7() const { return ____userVO_7; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_7() { return &____userVO_7; }
	inline void set__userVO_7(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_7 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_7), value);
	}

	inline static int32_t get_offset_of__userEvents_8() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____userEvents_8)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_8() const { return ____userEvents_8; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_8() { return &____userEvents_8; }
	inline void set__userEvents_8(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_8 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_8), value);
	}

	inline static int32_t get_offset_of__gameSparks_9() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____gameSparks_9)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_9() const { return ____gameSparks_9; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_9() { return &____gameSparks_9; }
	inline void set__gameSparks_9(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_9 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_9), value);
	}

	inline static int32_t get_offset_of__item_10() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____item_10)); }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * get__item_10() const { return ____item_10; }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B ** get_address_of__item_10() { return &____item_10; }
	inline void set__item_10(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * value)
	{
		____item_10 = value;
		Il2CppCodeGenWriteBarrier((&____item_10), value);
	}

	inline static int32_t get_offset_of__lifeReceivedCount_11() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____lifeReceivedCount_11)); }
	inline int32_t get__lifeReceivedCount_11() const { return ____lifeReceivedCount_11; }
	inline int32_t* get_address_of__lifeReceivedCount_11() { return &____lifeReceivedCount_11; }
	inline void set__lifeReceivedCount_11(int32_t value)
	{
		____lifeReceivedCount_11 = value;
	}

	inline static int32_t get_offset_of__index_12() { return static_cast<int32_t>(offsetof(ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29, ____index_12)); }
	inline int32_t get__index_12() const { return ____index_12; }
	inline int32_t* get_address_of__index_12() { return &____index_12; }
	inline void set__index_12(int32_t value)
	{
		____index_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATHEARTLISTITEM_TBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29_H
#ifndef CHATJOINREQUESTITEM_T8DA07962248D92C46A048DD7AAA046D52226CDAC_H
#define CHATJOINREQUESTITEM_T8DA07962248D92C46A048DD7AAA046D52226CDAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatJoinRequestItem
struct  ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI ChatJoinRequestItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_4;
	// UnityEngine.UI.Button ChatJoinRequestItem::_yesBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____yesBtn_5;
	// UnityEngine.UI.Button ChatJoinRequestItem::_noBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____noBtn_6;
	// Tayr.GameSparksPlatform ChatJoinRequestItem::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_7;
	// KickerManager ChatJoinRequestItem::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_8;
	// ChatListItemData ChatJoinRequestItem::_data
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * ____data_9;
	// System.Int32 ChatJoinRequestItem::_index
	int32_t ____index_10;

public:
	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____name_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_4() const { return ____name_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__yesBtn_5() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____yesBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__yesBtn_5() const { return ____yesBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__yesBtn_5() { return &____yesBtn_5; }
	inline void set__yesBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____yesBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&____yesBtn_5), value);
	}

	inline static int32_t get_offset_of__noBtn_6() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____noBtn_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__noBtn_6() const { return ____noBtn_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__noBtn_6() { return &____noBtn_6; }
	inline void set__noBtn_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____noBtn_6 = value;
		Il2CppCodeGenWriteBarrier((&____noBtn_6), value);
	}

	inline static int32_t get_offset_of__gameSparks_7() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____gameSparks_7)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_7() const { return ____gameSparks_7; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_7() { return &____gameSparks_7; }
	inline void set__gameSparks_7(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_7 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_7), value);
	}

	inline static int32_t get_offset_of__kickerManager_8() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____kickerManager_8)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_8() const { return ____kickerManager_8; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_8() { return &____kickerManager_8; }
	inline void set__kickerManager_8(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_8 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_8), value);
	}

	inline static int32_t get_offset_of__data_9() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____data_9)); }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * get__data_9() const { return ____data_9; }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B ** get_address_of__data_9() { return &____data_9; }
	inline void set__data_9(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * value)
	{
		____data_9 = value;
		Il2CppCodeGenWriteBarrier((&____data_9), value);
	}

	inline static int32_t get_offset_of__index_10() { return static_cast<int32_t>(offsetof(ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC, ____index_10)); }
	inline int32_t get__index_10() const { return ____index_10; }
	inline int32_t* get_address_of__index_10() { return &____index_10; }
	inline void set__index_10(int32_t value)
	{
		____index_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATJOINREQUESTITEM_T8DA07962248D92C46A048DD7AAA046D52226CDAC_H
#ifndef CHATNODE_T7A741E0D933BAFC733CCBF17E9D553DA370552A0_H
#define CHATNODE_T7A741E0D933BAFC733CCBF17E9D553DA370552A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatNode
struct  ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:
	// Tayr.TDynamicList ChatNode::_chatList
	TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * ____chatList_7;
	// UnityEngine.UI.Button ChatNode::_chatBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____chatBtn_8;
	// UnityEngine.UI.Button ChatNode::_heartBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____heartBtn_9;
	// Tayr.GameSparksPlatform ChatNode::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_10;
	// Tayr.ILibrary ChatNode::_uiMain
	RuntimeObject* ____uiMain_11;
	// UserVO ChatNode::_userVo
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVo_12;
	// UserEvents ChatNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_13;
	// UnityEngine.TouchScreenKeyboard ChatNode::_keyboard
	TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * ____keyboard_14;

public:
	inline static int32_t get_offset_of__chatList_7() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____chatList_7)); }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * get__chatList_7() const { return ____chatList_7; }
	inline TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 ** get_address_of__chatList_7() { return &____chatList_7; }
	inline void set__chatList_7(TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19 * value)
	{
		____chatList_7 = value;
		Il2CppCodeGenWriteBarrier((&____chatList_7), value);
	}

	inline static int32_t get_offset_of__chatBtn_8() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____chatBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__chatBtn_8() const { return ____chatBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__chatBtn_8() { return &____chatBtn_8; }
	inline void set__chatBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____chatBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____chatBtn_8), value);
	}

	inline static int32_t get_offset_of__heartBtn_9() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____heartBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__heartBtn_9() const { return ____heartBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__heartBtn_9() { return &____heartBtn_9; }
	inline void set__heartBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____heartBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____heartBtn_9), value);
	}

	inline static int32_t get_offset_of__gameSparks_10() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____gameSparks_10)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_10() const { return ____gameSparks_10; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_10() { return &____gameSparks_10; }
	inline void set__gameSparks_10(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_10 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_10), value);
	}

	inline static int32_t get_offset_of__uiMain_11() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____uiMain_11)); }
	inline RuntimeObject* get__uiMain_11() const { return ____uiMain_11; }
	inline RuntimeObject** get_address_of__uiMain_11() { return &____uiMain_11; }
	inline void set__uiMain_11(RuntimeObject* value)
	{
		____uiMain_11 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_11), value);
	}

	inline static int32_t get_offset_of__userVo_12() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____userVo_12)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVo_12() const { return ____userVo_12; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVo_12() { return &____userVo_12; }
	inline void set__userVo_12(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVo_12 = value;
		Il2CppCodeGenWriteBarrier((&____userVo_12), value);
	}

	inline static int32_t get_offset_of__userEvents_13() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____userEvents_13)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_13() const { return ____userEvents_13; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_13() { return &____userEvents_13; }
	inline void set__userEvents_13(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_13 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_13), value);
	}

	inline static int32_t get_offset_of__keyboard_14() { return static_cast<int32_t>(offsetof(ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0, ____keyboard_14)); }
	inline TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * get__keyboard_14() const { return ____keyboard_14; }
	inline TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 ** get_address_of__keyboard_14() { return &____keyboard_14; }
	inline void set__keyboard_14(TouchScreenKeyboard_t2A69F85698E9780470181532D3F2BC903623FD90 * value)
	{
		____keyboard_14 = value;
		Il2CppCodeGenWriteBarrier((&____keyboard_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATNODE_T7A741E0D933BAFC733CCBF17E9D553DA370552A0_H
#ifndef CHATRECEIVEMSGLISTITEM_TD9C9B67291E9A1C2E30707832237FA6CC0082670_H
#define CHATRECEIVEMSGLISTITEM_TD9C9B67291E9A1C2E30707832237FA6CC0082670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatReceiveMsgListItem
struct  ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Button ChatReceiveMsgListItem::_btn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btn_4;
	// TMPro.TextMeshProUGUI ChatReceiveMsgListItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_5;
	// TMPro.TextMeshProUGUI ChatReceiveMsgListItem::_message
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____message_6;
	// TMPro.TextMeshProUGUI ChatReceiveMsgListItem::_time
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____time_7;
	// Tayr.ILibrary ChatReceiveMsgListItem::_uiMain
	RuntimeObject* ____uiMain_8;
	// ChatListItemData ChatReceiveMsgListItem::_item
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * ____item_9;

public:
	inline static int32_t get_offset_of__btn_4() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____btn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btn_4() const { return ____btn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btn_4() { return &____btn_4; }
	inline void set__btn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btn_4 = value;
		Il2CppCodeGenWriteBarrier((&____btn_4), value);
	}

	inline static int32_t get_offset_of__name_5() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____name_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_5() const { return ____name_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_5() { return &____name_5; }
	inline void set__name_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_5 = value;
		Il2CppCodeGenWriteBarrier((&____name_5), value);
	}

	inline static int32_t get_offset_of__message_6() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____message_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__message_6() const { return ____message_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__message_6() { return &____message_6; }
	inline void set__message_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____message_6 = value;
		Il2CppCodeGenWriteBarrier((&____message_6), value);
	}

	inline static int32_t get_offset_of__time_7() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____time_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__time_7() const { return ____time_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__time_7() { return &____time_7; }
	inline void set__time_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____time_7 = value;
		Il2CppCodeGenWriteBarrier((&____time_7), value);
	}

	inline static int32_t get_offset_of__uiMain_8() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____uiMain_8)); }
	inline RuntimeObject* get__uiMain_8() const { return ____uiMain_8; }
	inline RuntimeObject** get_address_of__uiMain_8() { return &____uiMain_8; }
	inline void set__uiMain_8(RuntimeObject* value)
	{
		____uiMain_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_8), value);
	}

	inline static int32_t get_offset_of__item_9() { return static_cast<int32_t>(offsetof(ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670, ____item_9)); }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * get__item_9() const { return ____item_9; }
	inline ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B ** get_address_of__item_9() { return &____item_9; }
	inline void set__item_9(ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B * value)
	{
		____item_9 = value;
		Il2CppCodeGenWriteBarrier((&____item_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATRECEIVEMSGLISTITEM_TD9C9B67291E9A1C2E30707832237FA6CC0082670_H
#ifndef CHATSENDMSGLISTITEM_TC821821D0C7D3E952A9848B38025C10F436A2884_H
#define CHATSENDMSGLISTITEM_TC821821D0C7D3E952A9848B38025C10F436A2884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatSendMsgListItem
struct  ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI ChatSendMsgListItem::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_4;
	// TMPro.TextMeshProUGUI ChatSendMsgListItem::_message
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____message_5;
	// TMPro.TextMeshProUGUI ChatSendMsgListItem::_time
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____time_6;

public:
	inline static int32_t get_offset_of__name_4() { return static_cast<int32_t>(offsetof(ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884, ____name_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_4() const { return ____name_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_4() { return &____name_4; }
	inline void set__name_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_4 = value;
		Il2CppCodeGenWriteBarrier((&____name_4), value);
	}

	inline static int32_t get_offset_of__message_5() { return static_cast<int32_t>(offsetof(ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884, ____message_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__message_5() const { return ____message_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__message_5() { return &____message_5; }
	inline void set__message_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____message_5 = value;
		Il2CppCodeGenWriteBarrier((&____message_5), value);
	}

	inline static int32_t get_offset_of__time_6() { return static_cast<int32_t>(offsetof(ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884, ____time_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__time_6() const { return ____time_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__time_6() { return &____time_6; }
	inline void set__time_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____time_6 = value;
		Il2CppCodeGenWriteBarrier((&____time_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATSENDMSGLISTITEM_TC821821D0C7D3E952A9848B38025C10F436A2884_H
#ifndef CONTRIBUTEPOPUPNODE_T7DFE5B01F50CEB3582A725E489CE5C3251189C3F_H
#define CONTRIBUTEPOPUPNODE_T7DFE5B01F50CEB3582A725E489CE5C3251189C3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContributePopupNode
struct  ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// UnityEngine.UI.Button ContributePopupNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_7;
	// UnityEngine.UI.Button ContributePopupNode::_contributeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____contributeBtn_8;
	// Zenject.DiContainer ContributePopupNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_9;
	// Tayr.TSoundSystem ContributePopupNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_10;
	// SoundSO ContributePopupNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_11;
	// GameSettingsSO ContributePopupNode::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_12;

public:
	inline static int32_t get_offset_of__closeBtn_7() { return static_cast<int32_t>(offsetof(ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F, ____closeBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_7() const { return ____closeBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_7() { return &____closeBtn_7; }
	inline void set__closeBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_7), value);
	}

	inline static int32_t get_offset_of__contributeBtn_8() { return static_cast<int32_t>(offsetof(ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F, ____contributeBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__contributeBtn_8() const { return ____contributeBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__contributeBtn_8() { return &____contributeBtn_8; }
	inline void set__contributeBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____contributeBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____contributeBtn_8), value);
	}

	inline static int32_t get_offset_of__container_9() { return static_cast<int32_t>(offsetof(ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F, ____container_9)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_9() const { return ____container_9; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_9() { return &____container_9; }
	inline void set__container_9(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_9 = value;
		Il2CppCodeGenWriteBarrier((&____container_9), value);
	}

	inline static int32_t get_offset_of__soundSystem_10() { return static_cast<int32_t>(offsetof(ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F, ____soundSystem_10)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_10() const { return ____soundSystem_10; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_10() { return &____soundSystem_10; }
	inline void set__soundSystem_10(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_10 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_10), value);
	}

	inline static int32_t get_offset_of__soundSO_11() { return static_cast<int32_t>(offsetof(ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F, ____soundSO_11)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_11() const { return ____soundSO_11; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_11() { return &____soundSO_11; }
	inline void set__soundSO_11(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_11 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_11), value);
	}

	inline static int32_t get_offset_of__gameSettingsSO_12() { return static_cast<int32_t>(offsetof(ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F, ____gameSettingsSO_12)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_12() const { return ____gameSettingsSO_12; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_12() { return &____gameSettingsSO_12; }
	inline void set__gameSettingsSO_12(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_12 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRIBUTEPOPUPNODE_T7DFE5B01F50CEB3582A725E489CE5C3251189C3F_H
#ifndef CREATETEAMNODE_T83E09D1D20B2ADF00639F2549FD9E6935F405689_H
#define CREATETEAMNODE_T83E09D1D20B2ADF00639F2549FD9E6935F405689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateTeamNode
struct  CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// TMPro.TMP_InputField CreateTeamNode::_nameField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____nameField_7;
	// UnityEngine.UI.Button CreateTeamNode::_browseBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____browseBtn_8;
	// UnityEngine.UI.Image CreateTeamNode::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_9;
	// TMPro.TMP_InputField CreateTeamNode::_descriptionField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____descriptionField_10;
	// UnityEngine.UI.Button CreateTeamNode::_typeLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeLeftBtn_11;
	// UnityEngine.UI.Button CreateTeamNode::_typeRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeRightBtn_12;
	// TMPro.TextMeshProUGUI CreateTeamNode::_type
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____type_13;
	// UnityEngine.UI.Button CreateTeamNode::_requiredLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredLeftBtn_14;
	// UnityEngine.UI.Button CreateTeamNode::_requiredRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredRightBtn_15;
	// TMPro.TextMeshProUGUI CreateTeamNode::_required
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____required_16;
	// UnityEngine.UI.Button CreateTeamNode::_createBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____createBtn_17;
	// Tayr.ILibrary CreateTeamNode::_iconLibrary
	RuntimeObject* ____iconLibrary_18;
	// Tayr.ILibrary CreateTeamNode::_uiMain
	RuntimeObject* ____uiMain_19;
	// Tayr.GameSparksPlatform CreateTeamNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_20;
	// UserEvents CreateTeamNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_21;
	// UserVO CreateTeamNode::_userVo
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVo_22;
	// KickerManager CreateTeamNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_23;
	// Tayr.VOSaver CreateTeamNode::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_24;
	// TEAM_TYPE CreateTeamNode::_currentType
	int32_t ____currentType_25;
	// System.Int32 CreateTeamNode::_currentRequirement
	int32_t ____currentRequirement_26;
	// System.Int32 CreateTeamNode::_currentIcon
	int32_t ____currentIcon_27;
	// Tayr.INode CreateTeamNode::_badgesNode
	RuntimeObject* ____badgesNode_28;

public:
	inline static int32_t get_offset_of__nameField_7() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____nameField_7)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__nameField_7() const { return ____nameField_7; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__nameField_7() { return &____nameField_7; }
	inline void set__nameField_7(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____nameField_7 = value;
		Il2CppCodeGenWriteBarrier((&____nameField_7), value);
	}

	inline static int32_t get_offset_of__browseBtn_8() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____browseBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__browseBtn_8() const { return ____browseBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__browseBtn_8() { return &____browseBtn_8; }
	inline void set__browseBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____browseBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____browseBtn_8), value);
	}

	inline static int32_t get_offset_of__teamIcon_9() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____teamIcon_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_9() const { return ____teamIcon_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_9() { return &____teamIcon_9; }
	inline void set__teamIcon_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_9), value);
	}

	inline static int32_t get_offset_of__descriptionField_10() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____descriptionField_10)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__descriptionField_10() const { return ____descriptionField_10; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__descriptionField_10() { return &____descriptionField_10; }
	inline void set__descriptionField_10(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____descriptionField_10 = value;
		Il2CppCodeGenWriteBarrier((&____descriptionField_10), value);
	}

	inline static int32_t get_offset_of__typeLeftBtn_11() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____typeLeftBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeLeftBtn_11() const { return ____typeLeftBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeLeftBtn_11() { return &____typeLeftBtn_11; }
	inline void set__typeLeftBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeLeftBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____typeLeftBtn_11), value);
	}

	inline static int32_t get_offset_of__typeRightBtn_12() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____typeRightBtn_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeRightBtn_12() const { return ____typeRightBtn_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeRightBtn_12() { return &____typeRightBtn_12; }
	inline void set__typeRightBtn_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeRightBtn_12 = value;
		Il2CppCodeGenWriteBarrier((&____typeRightBtn_12), value);
	}

	inline static int32_t get_offset_of__type_13() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____type_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__type_13() const { return ____type_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__type_13() { return &____type_13; }
	inline void set__type_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____type_13 = value;
		Il2CppCodeGenWriteBarrier((&____type_13), value);
	}

	inline static int32_t get_offset_of__requiredLeftBtn_14() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____requiredLeftBtn_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredLeftBtn_14() const { return ____requiredLeftBtn_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredLeftBtn_14() { return &____requiredLeftBtn_14; }
	inline void set__requiredLeftBtn_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredLeftBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____requiredLeftBtn_14), value);
	}

	inline static int32_t get_offset_of__requiredRightBtn_15() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____requiredRightBtn_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredRightBtn_15() const { return ____requiredRightBtn_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredRightBtn_15() { return &____requiredRightBtn_15; }
	inline void set__requiredRightBtn_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredRightBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&____requiredRightBtn_15), value);
	}

	inline static int32_t get_offset_of__required_16() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____required_16)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__required_16() const { return ____required_16; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__required_16() { return &____required_16; }
	inline void set__required_16(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____required_16 = value;
		Il2CppCodeGenWriteBarrier((&____required_16), value);
	}

	inline static int32_t get_offset_of__createBtn_17() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____createBtn_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__createBtn_17() const { return ____createBtn_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__createBtn_17() { return &____createBtn_17; }
	inline void set__createBtn_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____createBtn_17 = value;
		Il2CppCodeGenWriteBarrier((&____createBtn_17), value);
	}

	inline static int32_t get_offset_of__iconLibrary_18() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____iconLibrary_18)); }
	inline RuntimeObject* get__iconLibrary_18() const { return ____iconLibrary_18; }
	inline RuntimeObject** get_address_of__iconLibrary_18() { return &____iconLibrary_18; }
	inline void set__iconLibrary_18(RuntimeObject* value)
	{
		____iconLibrary_18 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_18), value);
	}

	inline static int32_t get_offset_of__uiMain_19() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____uiMain_19)); }
	inline RuntimeObject* get__uiMain_19() const { return ____uiMain_19; }
	inline RuntimeObject** get_address_of__uiMain_19() { return &____uiMain_19; }
	inline void set__uiMain_19(RuntimeObject* value)
	{
		____uiMain_19 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_19), value);
	}

	inline static int32_t get_offset_of__gameSpark_20() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____gameSpark_20)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_20() const { return ____gameSpark_20; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_20() { return &____gameSpark_20; }
	inline void set__gameSpark_20(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_20 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_20), value);
	}

	inline static int32_t get_offset_of__userEvents_21() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____userEvents_21)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_21() const { return ____userEvents_21; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_21() { return &____userEvents_21; }
	inline void set__userEvents_21(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_21 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_21), value);
	}

	inline static int32_t get_offset_of__userVo_22() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____userVo_22)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVo_22() const { return ____userVo_22; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVo_22() { return &____userVo_22; }
	inline void set__userVo_22(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVo_22 = value;
		Il2CppCodeGenWriteBarrier((&____userVo_22), value);
	}

	inline static int32_t get_offset_of__kickerManager_23() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____kickerManager_23)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_23() const { return ____kickerManager_23; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_23() { return &____kickerManager_23; }
	inline void set__kickerManager_23(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_23 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_23), value);
	}

	inline static int32_t get_offset_of__voSaver_24() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____voSaver_24)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_24() const { return ____voSaver_24; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_24() { return &____voSaver_24; }
	inline void set__voSaver_24(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_24 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_24), value);
	}

	inline static int32_t get_offset_of__currentType_25() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____currentType_25)); }
	inline int32_t get__currentType_25() const { return ____currentType_25; }
	inline int32_t* get_address_of__currentType_25() { return &____currentType_25; }
	inline void set__currentType_25(int32_t value)
	{
		____currentType_25 = value;
	}

	inline static int32_t get_offset_of__currentRequirement_26() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____currentRequirement_26)); }
	inline int32_t get__currentRequirement_26() const { return ____currentRequirement_26; }
	inline int32_t* get_address_of__currentRequirement_26() { return &____currentRequirement_26; }
	inline void set__currentRequirement_26(int32_t value)
	{
		____currentRequirement_26 = value;
	}

	inline static int32_t get_offset_of__currentIcon_27() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____currentIcon_27)); }
	inline int32_t get__currentIcon_27() const { return ____currentIcon_27; }
	inline int32_t* get_address_of__currentIcon_27() { return &____currentIcon_27; }
	inline void set__currentIcon_27(int32_t value)
	{
		____currentIcon_27 = value;
	}

	inline static int32_t get_offset_of__badgesNode_28() { return static_cast<int32_t>(offsetof(CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689, ____badgesNode_28)); }
	inline RuntimeObject* get__badgesNode_28() const { return ____badgesNode_28; }
	inline RuntimeObject** get_address_of__badgesNode_28() { return &____badgesNode_28; }
	inline void set__badgesNode_28(RuntimeObject* value)
	{
		____badgesNode_28 = value;
		Il2CppCodeGenWriteBarrier((&____badgesNode_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETEAMNODE_T83E09D1D20B2ADF00639F2549FD9E6935F405689_H
#ifndef FRIENDLEADERBOARDNODE_TE8BB1DA28030370D85299660C5827B999415C8AE_H
#define FRIENDLEADERBOARDNODE_TE8BB1DA28030370D85299660C5827B999415C8AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendLeaderboardNode
struct  FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// Tayr.TList FriendLeaderboardNode::_list
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____list_7;
	// UnityEngine.UI.Button FriendLeaderboardNode::_loginBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____loginBtn_8;
	// Tayr.GameSparksPlatform FriendLeaderboardNode::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_9;
	// LeaderboardsVO FriendLeaderboardNode::_leaderboardVO
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * ____leaderboardVO_10;
	// Tayr.VOSaver FriendLeaderboardNode::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_11;

public:
	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE, ____list_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__list_7() const { return ____list_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}

	inline static int32_t get_offset_of__loginBtn_8() { return static_cast<int32_t>(offsetof(FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE, ____loginBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__loginBtn_8() const { return ____loginBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__loginBtn_8() { return &____loginBtn_8; }
	inline void set__loginBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____loginBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____loginBtn_8), value);
	}

	inline static int32_t get_offset_of__gameSparks_9() { return static_cast<int32_t>(offsetof(FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE, ____gameSparks_9)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_9() const { return ____gameSparks_9; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_9() { return &____gameSparks_9; }
	inline void set__gameSparks_9(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_9 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_9), value);
	}

	inline static int32_t get_offset_of__leaderboardVO_10() { return static_cast<int32_t>(offsetof(FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE, ____leaderboardVO_10)); }
	inline LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * get__leaderboardVO_10() const { return ____leaderboardVO_10; }
	inline LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 ** get_address_of__leaderboardVO_10() { return &____leaderboardVO_10; }
	inline void set__leaderboardVO_10(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * value)
	{
		____leaderboardVO_10 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardVO_10), value);
	}

	inline static int32_t get_offset_of__voSaver_11() { return static_cast<int32_t>(offsetof(FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE, ____voSaver_11)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_11() const { return ____voSaver_11; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_11() { return &____voSaver_11; }
	inline void set__voSaver_11(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_11 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRIENDLEADERBOARDNODE_TE8BB1DA28030370D85299660C5827B999415C8AE_H
#ifndef ICONSLISTITEM_TFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7_H
#define ICONSLISTITEM_TFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconsListItem
struct  IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image IconsListItem::_icon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____icon_4;
	// UnityEngine.UI.Button IconsListItem::_selectIcon
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____selectIcon_5;
	// Tayr.ILibrary IconsListItem::_iconLibrary
	RuntimeObject* ____iconLibrary_6;
	// UserEvents IconsListItem::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_7;
	// System.Int32 IconsListItem::_index
	int32_t ____index_8;

public:
	inline static int32_t get_offset_of__icon_4() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____icon_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__icon_4() const { return ____icon_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__icon_4() { return &____icon_4; }
	inline void set__icon_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____icon_4 = value;
		Il2CppCodeGenWriteBarrier((&____icon_4), value);
	}

	inline static int32_t get_offset_of__selectIcon_5() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____selectIcon_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__selectIcon_5() const { return ____selectIcon_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__selectIcon_5() { return &____selectIcon_5; }
	inline void set__selectIcon_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____selectIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&____selectIcon_5), value);
	}

	inline static int32_t get_offset_of__iconLibrary_6() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____iconLibrary_6)); }
	inline RuntimeObject* get__iconLibrary_6() const { return ____iconLibrary_6; }
	inline RuntimeObject** get_address_of__iconLibrary_6() { return &____iconLibrary_6; }
	inline void set__iconLibrary_6(RuntimeObject* value)
	{
		____iconLibrary_6 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_6), value);
	}

	inline static int32_t get_offset_of__userEvents_7() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____userEvents_7)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_7() const { return ____userEvents_7; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_7() { return &____userEvents_7; }
	inline void set__userEvents_7(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_7 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_7), value);
	}

	inline static int32_t get_offset_of__index_8() { return static_cast<int32_t>(offsetof(IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7, ____index_8)); }
	inline int32_t get__index_8() const { return ____index_8; }
	inline int32_t* get_address_of__index_8() { return &____index_8; }
	inline void set__index_8(int32_t value)
	{
		____index_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ICONSLISTITEM_TFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7_H
#ifndef JOINTEAMLISTITEM_T44B45CC0825AB01016E454D9EC234EF33B392A6B_H
#define JOINTEAMLISTITEM_T44B45CC0825AB01016E454D9EC234EF33B392A6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoinTeamListItem
struct  JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// TMPro.TextMeshProUGUI JoinTeamListItem::_teamName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamName_4;
	// UnityEngine.UI.Image JoinTeamListItem::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_5;
	// TMPro.TextMeshProUGUI JoinTeamListItem::_membersCount
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____membersCount_6;
	// UnityEngine.UI.Button JoinTeamListItem::_joinBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____joinBtn_7;
	// TMPro.TextMeshProUGUI JoinTeamListItem::_joinBtnText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____joinBtnText_8;
	// Tayr.ILibrary JoinTeamListItem::_uiMain
	RuntimeObject* ____uiMain_9;
	// Tayr.ILibrary JoinTeamListItem::_iconLibrary
	RuntimeObject* ____iconLibrary_10;
	// Tayr.GameSparksPlatform JoinTeamListItem::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_11;
	// UserEvents JoinTeamListItem::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_12;
	// UserVO JoinTeamListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_13;
	// GameSparks.Core.GSData JoinTeamListItem::_item
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ____item_14;

public:
	inline static int32_t get_offset_of__teamName_4() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____teamName_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamName_4() const { return ____teamName_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamName_4() { return &____teamName_4; }
	inline void set__teamName_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamName_4 = value;
		Il2CppCodeGenWriteBarrier((&____teamName_4), value);
	}

	inline static int32_t get_offset_of__teamIcon_5() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____teamIcon_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_5() const { return ____teamIcon_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_5() { return &____teamIcon_5; }
	inline void set__teamIcon_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_5), value);
	}

	inline static int32_t get_offset_of__membersCount_6() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____membersCount_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__membersCount_6() const { return ____membersCount_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__membersCount_6() { return &____membersCount_6; }
	inline void set__membersCount_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____membersCount_6 = value;
		Il2CppCodeGenWriteBarrier((&____membersCount_6), value);
	}

	inline static int32_t get_offset_of__joinBtn_7() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____joinBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__joinBtn_7() const { return ____joinBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__joinBtn_7() { return &____joinBtn_7; }
	inline void set__joinBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____joinBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____joinBtn_7), value);
	}

	inline static int32_t get_offset_of__joinBtnText_8() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____joinBtnText_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__joinBtnText_8() const { return ____joinBtnText_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__joinBtnText_8() { return &____joinBtnText_8; }
	inline void set__joinBtnText_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____joinBtnText_8 = value;
		Il2CppCodeGenWriteBarrier((&____joinBtnText_8), value);
	}

	inline static int32_t get_offset_of__uiMain_9() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____uiMain_9)); }
	inline RuntimeObject* get__uiMain_9() const { return ____uiMain_9; }
	inline RuntimeObject** get_address_of__uiMain_9() { return &____uiMain_9; }
	inline void set__uiMain_9(RuntimeObject* value)
	{
		____uiMain_9 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_9), value);
	}

	inline static int32_t get_offset_of__iconLibrary_10() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____iconLibrary_10)); }
	inline RuntimeObject* get__iconLibrary_10() const { return ____iconLibrary_10; }
	inline RuntimeObject** get_address_of__iconLibrary_10() { return &____iconLibrary_10; }
	inline void set__iconLibrary_10(RuntimeObject* value)
	{
		____iconLibrary_10 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_10), value);
	}

	inline static int32_t get_offset_of__gameSpark_11() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____gameSpark_11)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_11() const { return ____gameSpark_11; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_11() { return &____gameSpark_11; }
	inline void set__gameSpark_11(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_11 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_11), value);
	}

	inline static int32_t get_offset_of__userEvents_12() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____userEvents_12)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_12() const { return ____userEvents_12; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_12() { return &____userEvents_12; }
	inline void set__userEvents_12(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_12 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_12), value);
	}

	inline static int32_t get_offset_of__userVO_13() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____userVO_13)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_13() const { return ____userVO_13; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_13() { return &____userVO_13; }
	inline void set__userVO_13(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_13 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_13), value);
	}

	inline static int32_t get_offset_of__item_14() { return static_cast<int32_t>(offsetof(JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B, ____item_14)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get__item_14() const { return ____item_14; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of__item_14() { return &____item_14; }
	inline void set__item_14(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		____item_14 = value;
		Il2CppCodeGenWriteBarrier((&____item_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTEAMLISTITEM_T44B45CC0825AB01016E454D9EC234EF33B392A6B_H
#ifndef KICKERNODE_TBA08241A855C464C10713FAEB8C827AF7B36877E_H
#define KICKERNODE_TBA08241A855C464C10713FAEB8C827AF7B36877E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickerNode
struct  KickerNode_tBA08241A855C464C10713FAEB8C827AF7B36877E  : public BasicNode_1_t35D8ED7DF8BCDA75AFAE9B97DC25632EC59D21AF
{
public:
	// TMPro.TextMeshProUGUI KickerNode::_text
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____text_7;

public:
	inline static int32_t get_offset_of__text_7() { return static_cast<int32_t>(offsetof(KickerNode_tBA08241A855C464C10713FAEB8C827AF7B36877E, ____text_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__text_7() const { return ____text_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__text_7() { return &____text_7; }
	inline void set__text_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____text_7 = value;
		Il2CppCodeGenWriteBarrier((&____text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKERNODE_TBA08241A855C464C10713FAEB8C827AF7B36877E_H
#ifndef KICKERWITHICONNODE_TC6AD175D504C7A645B06F8C020885470A1B37EEA_H
#define KICKERWITHICONNODE_TC6AD175D504C7A645B06F8C020885470A1B37EEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KickerWithIconNode
struct  KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA  : public BasicNode_1_t39BEC8934AF01F7BDF51F905745C8109F7316F75
{
public:
	// TMPro.TextMeshProUGUI KickerWithIconNode::_text
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____text_7;
	// UnityEngine.UI.Image KickerWithIconNode::_icon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____icon_8;
	// ItemsSO KickerWithIconNode::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_9;
	// Contexts KickerWithIconNode::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_10;
	// Zenject.DiContainer KickerWithIconNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_11;
	// GameSettingsSO KickerWithIconNode::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_12;

public:
	inline static int32_t get_offset_of__text_7() { return static_cast<int32_t>(offsetof(KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA, ____text_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__text_7() const { return ____text_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__text_7() { return &____text_7; }
	inline void set__text_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____text_7 = value;
		Il2CppCodeGenWriteBarrier((&____text_7), value);
	}

	inline static int32_t get_offset_of__icon_8() { return static_cast<int32_t>(offsetof(KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA, ____icon_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__icon_8() const { return ____icon_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__icon_8() { return &____icon_8; }
	inline void set__icon_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____icon_8 = value;
		Il2CppCodeGenWriteBarrier((&____icon_8), value);
	}

	inline static int32_t get_offset_of__itemsSO_9() { return static_cast<int32_t>(offsetof(KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA, ____itemsSO_9)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_9() const { return ____itemsSO_9; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_9() { return &____itemsSO_9; }
	inline void set__itemsSO_9(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_9 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_9), value);
	}

	inline static int32_t get_offset_of__contexts_10() { return static_cast<int32_t>(offsetof(KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA, ____contexts_10)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_10() const { return ____contexts_10; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_10() { return &____contexts_10; }
	inline void set__contexts_10(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_10 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_10), value);
	}

	inline static int32_t get_offset_of__container_11() { return static_cast<int32_t>(offsetof(KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA, ____container_11)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_11() const { return ____container_11; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_11() { return &____container_11; }
	inline void set__container_11(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_11 = value;
		Il2CppCodeGenWriteBarrier((&____container_11), value);
	}

	inline static int32_t get_offset_of__gameSettingsSO_12() { return static_cast<int32_t>(offsetof(KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA, ____gameSettingsSO_12)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_12() const { return ____gameSettingsSO_12; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_12() { return &____gameSettingsSO_12; }
	inline void set__gameSettingsSO_12(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_12 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KICKERWITHICONNODE_TC6AD175D504C7A645B06F8C020885470A1B37EEA_H
#ifndef LEADERBOARDNODE_1_TBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F_H
#define LEADERBOARDNODE_1_TBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardNode`1<System.Int32>
struct  LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// Tayr.TList LeaderboardNode`1::List
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ___List_7;
	// UnityEngine.UI.Button LeaderboardNode`1::World
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___World_8;
	// UnityEngine.Transform LeaderboardNode`1::WorldInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___WorldInactive_9;
	// UnityEngine.Transform LeaderboardNode`1::WorldParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___WorldParent_10;
	// UnityEngine.UI.Button LeaderboardNode`1::Country
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___Country_11;
	// UnityEngine.Transform LeaderboardNode`1::CountryInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___CountryInactive_12;
	// UnityEngine.Transform LeaderboardNode`1::CountryParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___CountryParent_13;
	// TMPro.TextMeshProUGUI LeaderboardNode`1::CountryNameActive
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___CountryNameActive_14;
	// TMPro.TextMeshProUGUI LeaderboardNode`1::CountryNameInactive
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___CountryNameInactive_15;
	// LeaderboardsVO LeaderboardNode`1::LeaderboardVO
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * ___LeaderboardVO_16;
	// UserEvents LeaderboardNode`1::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_17;
	// UserVO LeaderboardNode`1::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_18;
	// Zenject.DiContainer LeaderboardNode`1::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_19;
	// Tayr.TSoundSystem LeaderboardNode`1::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_20;
	// SoundSO LeaderboardNode`1::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_21;
	// System.Boolean LeaderboardNode`1::IsCountrySelected
	bool ___IsCountrySelected_22;

public:
	inline static int32_t get_offset_of_List_7() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___List_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get_List_7() const { return ___List_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of_List_7() { return &___List_7; }
	inline void set_List_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		___List_7 = value;
		Il2CppCodeGenWriteBarrier((&___List_7), value);
	}

	inline static int32_t get_offset_of_World_8() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___World_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_World_8() const { return ___World_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_World_8() { return &___World_8; }
	inline void set_World_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___World_8 = value;
		Il2CppCodeGenWriteBarrier((&___World_8), value);
	}

	inline static int32_t get_offset_of_WorldInactive_9() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___WorldInactive_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_WorldInactive_9() const { return ___WorldInactive_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_WorldInactive_9() { return &___WorldInactive_9; }
	inline void set_WorldInactive_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___WorldInactive_9 = value;
		Il2CppCodeGenWriteBarrier((&___WorldInactive_9), value);
	}

	inline static int32_t get_offset_of_WorldParent_10() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___WorldParent_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_WorldParent_10() const { return ___WorldParent_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_WorldParent_10() { return &___WorldParent_10; }
	inline void set_WorldParent_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___WorldParent_10 = value;
		Il2CppCodeGenWriteBarrier((&___WorldParent_10), value);
	}

	inline static int32_t get_offset_of_Country_11() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___Country_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_Country_11() const { return ___Country_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_Country_11() { return &___Country_11; }
	inline void set_Country_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___Country_11 = value;
		Il2CppCodeGenWriteBarrier((&___Country_11), value);
	}

	inline static int32_t get_offset_of_CountryInactive_12() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___CountryInactive_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_CountryInactive_12() const { return ___CountryInactive_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_CountryInactive_12() { return &___CountryInactive_12; }
	inline void set_CountryInactive_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___CountryInactive_12 = value;
		Il2CppCodeGenWriteBarrier((&___CountryInactive_12), value);
	}

	inline static int32_t get_offset_of_CountryParent_13() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___CountryParent_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_CountryParent_13() const { return ___CountryParent_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_CountryParent_13() { return &___CountryParent_13; }
	inline void set_CountryParent_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___CountryParent_13 = value;
		Il2CppCodeGenWriteBarrier((&___CountryParent_13), value);
	}

	inline static int32_t get_offset_of_CountryNameActive_14() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___CountryNameActive_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_CountryNameActive_14() const { return ___CountryNameActive_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_CountryNameActive_14() { return &___CountryNameActive_14; }
	inline void set_CountryNameActive_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___CountryNameActive_14 = value;
		Il2CppCodeGenWriteBarrier((&___CountryNameActive_14), value);
	}

	inline static int32_t get_offset_of_CountryNameInactive_15() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___CountryNameInactive_15)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_CountryNameInactive_15() const { return ___CountryNameInactive_15; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_CountryNameInactive_15() { return &___CountryNameInactive_15; }
	inline void set_CountryNameInactive_15(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___CountryNameInactive_15 = value;
		Il2CppCodeGenWriteBarrier((&___CountryNameInactive_15), value);
	}

	inline static int32_t get_offset_of_LeaderboardVO_16() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___LeaderboardVO_16)); }
	inline LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * get_LeaderboardVO_16() const { return ___LeaderboardVO_16; }
	inline LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 ** get_address_of_LeaderboardVO_16() { return &___LeaderboardVO_16; }
	inline void set_LeaderboardVO_16(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * value)
	{
		___LeaderboardVO_16 = value;
		Il2CppCodeGenWriteBarrier((&___LeaderboardVO_16), value);
	}

	inline static int32_t get_offset_of__userEvents_17() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ____userEvents_17)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_17() const { return ____userEvents_17; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_17() { return &____userEvents_17; }
	inline void set__userEvents_17(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_17 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_17), value);
	}

	inline static int32_t get_offset_of__userVO_18() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ____userVO_18)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_18() const { return ____userVO_18; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_18() { return &____userVO_18; }
	inline void set__userVO_18(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_18 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_18), value);
	}

	inline static int32_t get_offset_of__container_19() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ____container_19)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_19() const { return ____container_19; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_19() { return &____container_19; }
	inline void set__container_19(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_19 = value;
		Il2CppCodeGenWriteBarrier((&____container_19), value);
	}

	inline static int32_t get_offset_of__soundSystem_20() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ____soundSystem_20)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_20() const { return ____soundSystem_20; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_20() { return &____soundSystem_20; }
	inline void set__soundSystem_20(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_20 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_20), value);
	}

	inline static int32_t get_offset_of__soundSO_21() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ____soundSO_21)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_21() const { return ____soundSO_21; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_21() { return &____soundSO_21; }
	inline void set__soundSO_21(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_21 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_21), value);
	}

	inline static int32_t get_offset_of_IsCountrySelected_22() { return static_cast<int32_t>(offsetof(LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F, ___IsCountrySelected_22)); }
	inline bool get_IsCountrySelected_22() const { return ___IsCountrySelected_22; }
	inline bool* get_address_of_IsCountrySelected_22() { return &___IsCountrySelected_22; }
	inline void set_IsCountrySelected_22(bool value)
	{
		___IsCountrySelected_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDNODE_1_TBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F_H
#ifndef LEADERBOARDPLAYERITEM_T0DBC7C119FC10FC6140139F36D0DC8945A08114D_H
#define LEADERBOARDPLAYERITEM_T0DBC7C119FC10FC6140139F36D0DC8945A08114D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardPlayerItem
struct  LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image LeaderboardPlayerItem::_bgHighlight
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgHighlight_4;
	// UnityEngine.UI.Image LeaderboardPlayerItem::_bgNormal
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgNormal_5;
	// UnityEngine.UI.Image LeaderboardPlayerItem::_firstRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____firstRank_6;
	// UnityEngine.UI.Image LeaderboardPlayerItem::_secondRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____secondRank_7;
	// UnityEngine.UI.Image LeaderboardPlayerItem::_thirdRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____thirdRank_8;
	// TMPro.TextMeshProUGUI LeaderboardPlayerItem::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_9;
	// UnityEngine.UI.Image LeaderboardPlayerItem::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_10;
	// TMPro.TextMeshProUGUI LeaderboardPlayerItem::_playerName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____playerName_11;
	// TMPro.TextMeshProUGUI LeaderboardPlayerItem::_teamName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamName_12;
	// TMPro.TextMeshProUGUI LeaderboardPlayerItem::_levelScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____levelScore_13;
	// TMPro.TextMeshProUGUI LeaderboardPlayerItem::_trophyScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____trophyScore_14;
	// UnityEngine.UI.Button LeaderboardPlayerItem::_viewTeamBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____viewTeamBtn_15;
	// Tayr.ILibrary LeaderboardPlayerItem::_uiMain
	RuntimeObject* ____uiMain_16;
	// UserVO LeaderboardPlayerItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_17;
	// LeaderboardEntryVO LeaderboardPlayerItem::_entry
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737 * ____entry_18;

public:
	inline static int32_t get_offset_of__bgHighlight_4() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____bgHighlight_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgHighlight_4() const { return ____bgHighlight_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgHighlight_4() { return &____bgHighlight_4; }
	inline void set__bgHighlight_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgHighlight_4 = value;
		Il2CppCodeGenWriteBarrier((&____bgHighlight_4), value);
	}

	inline static int32_t get_offset_of__bgNormal_5() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____bgNormal_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgNormal_5() const { return ____bgNormal_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgNormal_5() { return &____bgNormal_5; }
	inline void set__bgNormal_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&____bgNormal_5), value);
	}

	inline static int32_t get_offset_of__firstRank_6() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____firstRank_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__firstRank_6() const { return ____firstRank_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__firstRank_6() { return &____firstRank_6; }
	inline void set__firstRank_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____firstRank_6 = value;
		Il2CppCodeGenWriteBarrier((&____firstRank_6), value);
	}

	inline static int32_t get_offset_of__secondRank_7() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____secondRank_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__secondRank_7() const { return ____secondRank_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__secondRank_7() { return &____secondRank_7; }
	inline void set__secondRank_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____secondRank_7 = value;
		Il2CppCodeGenWriteBarrier((&____secondRank_7), value);
	}

	inline static int32_t get_offset_of__thirdRank_8() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____thirdRank_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__thirdRank_8() const { return ____thirdRank_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__thirdRank_8() { return &____thirdRank_8; }
	inline void set__thirdRank_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____thirdRank_8 = value;
		Il2CppCodeGenWriteBarrier((&____thirdRank_8), value);
	}

	inline static int32_t get_offset_of__rankText_9() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____rankText_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_9() const { return ____rankText_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_9() { return &____rankText_9; }
	inline void set__rankText_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_9 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_9), value);
	}

	inline static int32_t get_offset_of__teamIcon_10() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____teamIcon_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_10() const { return ____teamIcon_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_10() { return &____teamIcon_10; }
	inline void set__teamIcon_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_10 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_10), value);
	}

	inline static int32_t get_offset_of__playerName_11() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____playerName_11)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__playerName_11() const { return ____playerName_11; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__playerName_11() { return &____playerName_11; }
	inline void set__playerName_11(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____playerName_11 = value;
		Il2CppCodeGenWriteBarrier((&____playerName_11), value);
	}

	inline static int32_t get_offset_of__teamName_12() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____teamName_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamName_12() const { return ____teamName_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamName_12() { return &____teamName_12; }
	inline void set__teamName_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamName_12 = value;
		Il2CppCodeGenWriteBarrier((&____teamName_12), value);
	}

	inline static int32_t get_offset_of__levelScore_13() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____levelScore_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__levelScore_13() const { return ____levelScore_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__levelScore_13() { return &____levelScore_13; }
	inline void set__levelScore_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____levelScore_13 = value;
		Il2CppCodeGenWriteBarrier((&____levelScore_13), value);
	}

	inline static int32_t get_offset_of__trophyScore_14() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____trophyScore_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__trophyScore_14() const { return ____trophyScore_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__trophyScore_14() { return &____trophyScore_14; }
	inline void set__trophyScore_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____trophyScore_14 = value;
		Il2CppCodeGenWriteBarrier((&____trophyScore_14), value);
	}

	inline static int32_t get_offset_of__viewTeamBtn_15() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____viewTeamBtn_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__viewTeamBtn_15() const { return ____viewTeamBtn_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__viewTeamBtn_15() { return &____viewTeamBtn_15; }
	inline void set__viewTeamBtn_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____viewTeamBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&____viewTeamBtn_15), value);
	}

	inline static int32_t get_offset_of__uiMain_16() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____uiMain_16)); }
	inline RuntimeObject* get__uiMain_16() const { return ____uiMain_16; }
	inline RuntimeObject** get_address_of__uiMain_16() { return &____uiMain_16; }
	inline void set__uiMain_16(RuntimeObject* value)
	{
		____uiMain_16 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_16), value);
	}

	inline static int32_t get_offset_of__userVO_17() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____userVO_17)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_17() const { return ____userVO_17; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_17() { return &____userVO_17; }
	inline void set__userVO_17(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_17 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_17), value);
	}

	inline static int32_t get_offset_of__entry_18() { return static_cast<int32_t>(offsetof(LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D, ____entry_18)); }
	inline LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737 * get__entry_18() const { return ____entry_18; }
	inline LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737 ** get_address_of__entry_18() { return &____entry_18; }
	inline void set__entry_18(LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737 * value)
	{
		____entry_18 = value;
		Il2CppCodeGenWriteBarrier((&____entry_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDPLAYERITEM_T0DBC7C119FC10FC6140139F36D0DC8945A08114D_H
#ifndef LEADERBOARDTEAMITEM_T335432750597076BCEC059E5F0A20659CE2552A1_H
#define LEADERBOARDTEAMITEM_T335432750597076BCEC059E5F0A20659CE2552A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardTeamItem
struct  LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image LeaderboardTeamItem::_firstRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____firstRank_4;
	// UnityEngine.UI.Image LeaderboardTeamItem::_secondRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____secondRank_5;
	// UnityEngine.UI.Image LeaderboardTeamItem::_thirdRank
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____thirdRank_6;
	// TMPro.TextMeshProUGUI LeaderboardTeamItem::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_7;
	// UnityEngine.UI.Image LeaderboardTeamItem::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_8;
	// TMPro.TextMeshProUGUI LeaderboardTeamItem::_teamName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____teamName_9;
	// TMPro.TextMeshProUGUI LeaderboardTeamItem::_levelScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____levelScore_10;
	// TMPro.TextMeshProUGUI LeaderboardTeamItem::_stageScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____stageScore_11;

public:
	inline static int32_t get_offset_of__firstRank_4() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____firstRank_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__firstRank_4() const { return ____firstRank_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__firstRank_4() { return &____firstRank_4; }
	inline void set__firstRank_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____firstRank_4 = value;
		Il2CppCodeGenWriteBarrier((&____firstRank_4), value);
	}

	inline static int32_t get_offset_of__secondRank_5() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____secondRank_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__secondRank_5() const { return ____secondRank_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__secondRank_5() { return &____secondRank_5; }
	inline void set__secondRank_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____secondRank_5 = value;
		Il2CppCodeGenWriteBarrier((&____secondRank_5), value);
	}

	inline static int32_t get_offset_of__thirdRank_6() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____thirdRank_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__thirdRank_6() const { return ____thirdRank_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__thirdRank_6() { return &____thirdRank_6; }
	inline void set__thirdRank_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____thirdRank_6 = value;
		Il2CppCodeGenWriteBarrier((&____thirdRank_6), value);
	}

	inline static int32_t get_offset_of__rankText_7() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____rankText_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_7() const { return ____rankText_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_7() { return &____rankText_7; }
	inline void set__rankText_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_7 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_7), value);
	}

	inline static int32_t get_offset_of__teamIcon_8() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____teamIcon_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_8() const { return ____teamIcon_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_8() { return &____teamIcon_8; }
	inline void set__teamIcon_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_8), value);
	}

	inline static int32_t get_offset_of__teamName_9() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____teamName_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__teamName_9() const { return ____teamName_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__teamName_9() { return &____teamName_9; }
	inline void set__teamName_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____teamName_9 = value;
		Il2CppCodeGenWriteBarrier((&____teamName_9), value);
	}

	inline static int32_t get_offset_of__levelScore_10() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____levelScore_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__levelScore_10() const { return ____levelScore_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__levelScore_10() { return &____levelScore_10; }
	inline void set__levelScore_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____levelScore_10 = value;
		Il2CppCodeGenWriteBarrier((&____levelScore_10), value);
	}

	inline static int32_t get_offset_of__stageScore_11() { return static_cast<int32_t>(offsetof(LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1, ____stageScore_11)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__stageScore_11() const { return ____stageScore_11; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__stageScore_11() { return &____stageScore_11; }
	inline void set__stageScore_11(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____stageScore_11 = value;
		Il2CppCodeGenWriteBarrier((&____stageScore_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDTEAMITEM_T335432750597076BCEC059E5F0A20659CE2552A1_H
#ifndef MAINLOADINGNODE_TAD03E2D98EBDEFDBB6C2BA4373A016C7A594F5D4_H
#define MAINLOADINGNODE_TAD03E2D98EBDEFDBB6C2BA4373A016C7A594F5D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainLoadingNode
struct  MainLoadingNode_tAD03E2D98EBDEFDBB6C2BA4373A016C7A594F5D4  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINLOADINGNODE_TAD03E2D98EBDEFDBB6C2BA4373A016C7A594F5D4_H
#ifndef MAINUINODE_TF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4_H
#define MAINUINODE_TF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainUINode
struct  MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4  : public BasicNode_1_t1EE3FADC6580D788C109C53BD985DF163FDC806C
{
public:
	// UnityEngine.UI.Button MainUINode::_settingsBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____settingsBtn_7;
	// UnityEngine.Transform MainUINode::_teamView
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____teamView_8;
	// UnityEngine.UI.Button MainUINode::_teamBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____teamBtn_9;
	// UnityEngine.Transform MainUINode::_leaderboardView
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____leaderboardView_10;
	// UnityEngine.UI.Button MainUINode::_leaderboardBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____leaderboardBtn_11;
	// UnityEngine.Transform MainUINode::_homeView
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____homeView_12;
	// UnityEngine.UI.Button MainUINode::_homeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____homeBtn_13;
	// UnityEngine.Transform MainUINode::_lifesView
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____lifesView_14;
	// UnityEngine.UI.Button MainUINode::_lifesBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____lifesBtn_15;
	// UnityEngine.Transform MainUINode::_shopView
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____shopView_16;
	// UnityEngine.UI.Button MainUINode::_shopBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____shopBtn_17;
	// LevelChestComponent MainUINode::_levelChestComponent
	LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6 * ____levelChestComponent_18;
	// StarChestComponent MainUINode::_starsChestComponent
	StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 * ____starsChestComponent_19;
	// HomeViewComponent MainUINode::_homwViewComponent
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818 * ____homwViewComponent_20;
	// Zenject.DiContainer MainUINode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_21;
	// Tayr.ILibrary MainUINode::_uiMain
	RuntimeObject* ____uiMain_22;
	// Tayr.TSoundSystem MainUINode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_23;
	// SoundSO MainUINode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_24;
	// GlobalSO MainUINode::_globalSO
	GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * ____globalSO_25;
	// KickerManager MainUINode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_26;
	// UserVO MainUINode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_27;
	// System.Int32 MainUINode::_currentView
	int32_t ____currentView_28;

public:
	inline static int32_t get_offset_of__settingsBtn_7() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____settingsBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__settingsBtn_7() const { return ____settingsBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__settingsBtn_7() { return &____settingsBtn_7; }
	inline void set__settingsBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____settingsBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____settingsBtn_7), value);
	}

	inline static int32_t get_offset_of__teamView_8() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____teamView_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__teamView_8() const { return ____teamView_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__teamView_8() { return &____teamView_8; }
	inline void set__teamView_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____teamView_8 = value;
		Il2CppCodeGenWriteBarrier((&____teamView_8), value);
	}

	inline static int32_t get_offset_of__teamBtn_9() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____teamBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__teamBtn_9() const { return ____teamBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__teamBtn_9() { return &____teamBtn_9; }
	inline void set__teamBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____teamBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____teamBtn_9), value);
	}

	inline static int32_t get_offset_of__leaderboardView_10() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____leaderboardView_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__leaderboardView_10() const { return ____leaderboardView_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__leaderboardView_10() { return &____leaderboardView_10; }
	inline void set__leaderboardView_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____leaderboardView_10 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardView_10), value);
	}

	inline static int32_t get_offset_of__leaderboardBtn_11() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____leaderboardBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__leaderboardBtn_11() const { return ____leaderboardBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__leaderboardBtn_11() { return &____leaderboardBtn_11; }
	inline void set__leaderboardBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____leaderboardBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardBtn_11), value);
	}

	inline static int32_t get_offset_of__homeView_12() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____homeView_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__homeView_12() const { return ____homeView_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__homeView_12() { return &____homeView_12; }
	inline void set__homeView_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____homeView_12 = value;
		Il2CppCodeGenWriteBarrier((&____homeView_12), value);
	}

	inline static int32_t get_offset_of__homeBtn_13() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____homeBtn_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__homeBtn_13() const { return ____homeBtn_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__homeBtn_13() { return &____homeBtn_13; }
	inline void set__homeBtn_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____homeBtn_13 = value;
		Il2CppCodeGenWriteBarrier((&____homeBtn_13), value);
	}

	inline static int32_t get_offset_of__lifesView_14() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____lifesView_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__lifesView_14() const { return ____lifesView_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__lifesView_14() { return &____lifesView_14; }
	inline void set__lifesView_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____lifesView_14 = value;
		Il2CppCodeGenWriteBarrier((&____lifesView_14), value);
	}

	inline static int32_t get_offset_of__lifesBtn_15() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____lifesBtn_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__lifesBtn_15() const { return ____lifesBtn_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__lifesBtn_15() { return &____lifesBtn_15; }
	inline void set__lifesBtn_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____lifesBtn_15 = value;
		Il2CppCodeGenWriteBarrier((&____lifesBtn_15), value);
	}

	inline static int32_t get_offset_of__shopView_16() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____shopView_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__shopView_16() const { return ____shopView_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__shopView_16() { return &____shopView_16; }
	inline void set__shopView_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____shopView_16 = value;
		Il2CppCodeGenWriteBarrier((&____shopView_16), value);
	}

	inline static int32_t get_offset_of__shopBtn_17() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____shopBtn_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__shopBtn_17() const { return ____shopBtn_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__shopBtn_17() { return &____shopBtn_17; }
	inline void set__shopBtn_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____shopBtn_17 = value;
		Il2CppCodeGenWriteBarrier((&____shopBtn_17), value);
	}

	inline static int32_t get_offset_of__levelChestComponent_18() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____levelChestComponent_18)); }
	inline LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6 * get__levelChestComponent_18() const { return ____levelChestComponent_18; }
	inline LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6 ** get_address_of__levelChestComponent_18() { return &____levelChestComponent_18; }
	inline void set__levelChestComponent_18(LevelChestComponent_t72D74F7CC3B050957B6074881BD7D1E8B0ABB2E6 * value)
	{
		____levelChestComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&____levelChestComponent_18), value);
	}

	inline static int32_t get_offset_of__starsChestComponent_19() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____starsChestComponent_19)); }
	inline StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 * get__starsChestComponent_19() const { return ____starsChestComponent_19; }
	inline StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 ** get_address_of__starsChestComponent_19() { return &____starsChestComponent_19; }
	inline void set__starsChestComponent_19(StarChestComponent_t8ADEE7CB2C641D2EB6AD4D08E19B94D627A99508 * value)
	{
		____starsChestComponent_19 = value;
		Il2CppCodeGenWriteBarrier((&____starsChestComponent_19), value);
	}

	inline static int32_t get_offset_of__homwViewComponent_20() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____homwViewComponent_20)); }
	inline HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818 * get__homwViewComponent_20() const { return ____homwViewComponent_20; }
	inline HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818 ** get_address_of__homwViewComponent_20() { return &____homwViewComponent_20; }
	inline void set__homwViewComponent_20(HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818 * value)
	{
		____homwViewComponent_20 = value;
		Il2CppCodeGenWriteBarrier((&____homwViewComponent_20), value);
	}

	inline static int32_t get_offset_of__container_21() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____container_21)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_21() const { return ____container_21; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_21() { return &____container_21; }
	inline void set__container_21(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_21 = value;
		Il2CppCodeGenWriteBarrier((&____container_21), value);
	}

	inline static int32_t get_offset_of__uiMain_22() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____uiMain_22)); }
	inline RuntimeObject* get__uiMain_22() const { return ____uiMain_22; }
	inline RuntimeObject** get_address_of__uiMain_22() { return &____uiMain_22; }
	inline void set__uiMain_22(RuntimeObject* value)
	{
		____uiMain_22 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_22), value);
	}

	inline static int32_t get_offset_of__soundSystem_23() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____soundSystem_23)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_23() const { return ____soundSystem_23; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_23() { return &____soundSystem_23; }
	inline void set__soundSystem_23(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_23 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_23), value);
	}

	inline static int32_t get_offset_of__soundSO_24() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____soundSO_24)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_24() const { return ____soundSO_24; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_24() { return &____soundSO_24; }
	inline void set__soundSO_24(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_24 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_24), value);
	}

	inline static int32_t get_offset_of__globalSO_25() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____globalSO_25)); }
	inline GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * get__globalSO_25() const { return ____globalSO_25; }
	inline GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 ** get_address_of__globalSO_25() { return &____globalSO_25; }
	inline void set__globalSO_25(GlobalSO_tACFCA803E1D70E0299E50028D7799CE31F402273 * value)
	{
		____globalSO_25 = value;
		Il2CppCodeGenWriteBarrier((&____globalSO_25), value);
	}

	inline static int32_t get_offset_of__kickerManager_26() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____kickerManager_26)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_26() const { return ____kickerManager_26; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_26() { return &____kickerManager_26; }
	inline void set__kickerManager_26(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_26 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_26), value);
	}

	inline static int32_t get_offset_of__userVO_27() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____userVO_27)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_27() const { return ____userVO_27; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_27() { return &____userVO_27; }
	inline void set__userVO_27(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_27 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_27), value);
	}

	inline static int32_t get_offset_of__currentView_28() { return static_cast<int32_t>(offsetof(MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4, ____currentView_28)); }
	inline int32_t get__currentView_28() const { return ____currentView_28; }
	inline int32_t* get_address_of__currentView_28() { return &____currentView_28; }
	inline void set__currentView_28(int32_t value)
	{
		____currentView_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINUINODE_TF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4_H
#ifndef MYTEAMLISTITEM_TB264E6A70F6FB1A5968128CBF86FF0987023FFBC_H
#define MYTEAMLISTITEM_TB264E6A70F6FB1A5968128CBF86FF0987023FFBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyTeamListItem
struct  MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC  : public TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C
{
public:
	// UnityEngine.UI.Image MyTeamListItem::_bgHighlight
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgHighlight_4;
	// UnityEngine.UI.Image MyTeamListItem::_bgNormal
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bgNormal_5;
	// TMPro.TextMeshProUGUI MyTeamListItem::_rankText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____rankText_6;
	// UnityEngine.UI.Image MyTeamListItem::_rank1
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____rank1_7;
	// UnityEngine.UI.Image MyTeamListItem::_rank2
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____rank2_8;
	// UnityEngine.UI.Image MyTeamListItem::_rank3
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____rank3_9;
	// TMPro.TextMeshProUGUI MyTeamListItem::_normaleName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____normaleName_10;
	// UnityEngine.Transform MyTeamListItem::_leaderParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____leaderParent_11;
	// TMPro.TextMeshProUGUI MyTeamListItem::_leaderName
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____leaderName_12;
	// TMPro.TextMeshProUGUI MyTeamListItem::_help
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____help_13;
	// TMPro.TextMeshProUGUI MyTeamListItem::_level
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____level_14;
	// UnityEngine.UI.Button MyTeamListItem::_kick
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____kick_15;
	// UserVO MyTeamListItem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_16;
	// Tayr.ILibrary MyTeamListItem::_uiMain
	RuntimeObject* ____uiMain_17;
	// GameSparks.Core.GSData MyTeamListItem::_data
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ____data_18;

public:
	inline static int32_t get_offset_of__bgHighlight_4() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____bgHighlight_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgHighlight_4() const { return ____bgHighlight_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgHighlight_4() { return &____bgHighlight_4; }
	inline void set__bgHighlight_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgHighlight_4 = value;
		Il2CppCodeGenWriteBarrier((&____bgHighlight_4), value);
	}

	inline static int32_t get_offset_of__bgNormal_5() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____bgNormal_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bgNormal_5() const { return ____bgNormal_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bgNormal_5() { return &____bgNormal_5; }
	inline void set__bgNormal_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bgNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&____bgNormal_5), value);
	}

	inline static int32_t get_offset_of__rankText_6() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rankText_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__rankText_6() const { return ____rankText_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__rankText_6() { return &____rankText_6; }
	inline void set__rankText_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____rankText_6 = value;
		Il2CppCodeGenWriteBarrier((&____rankText_6), value);
	}

	inline static int32_t get_offset_of__rank1_7() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rank1_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__rank1_7() const { return ____rank1_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__rank1_7() { return &____rank1_7; }
	inline void set__rank1_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____rank1_7 = value;
		Il2CppCodeGenWriteBarrier((&____rank1_7), value);
	}

	inline static int32_t get_offset_of__rank2_8() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rank2_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__rank2_8() const { return ____rank2_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__rank2_8() { return &____rank2_8; }
	inline void set__rank2_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____rank2_8 = value;
		Il2CppCodeGenWriteBarrier((&____rank2_8), value);
	}

	inline static int32_t get_offset_of__rank3_9() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____rank3_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__rank3_9() const { return ____rank3_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__rank3_9() { return &____rank3_9; }
	inline void set__rank3_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____rank3_9 = value;
		Il2CppCodeGenWriteBarrier((&____rank3_9), value);
	}

	inline static int32_t get_offset_of__normaleName_10() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____normaleName_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__normaleName_10() const { return ____normaleName_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__normaleName_10() { return &____normaleName_10; }
	inline void set__normaleName_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____normaleName_10 = value;
		Il2CppCodeGenWriteBarrier((&____normaleName_10), value);
	}

	inline static int32_t get_offset_of__leaderParent_11() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____leaderParent_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__leaderParent_11() const { return ____leaderParent_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__leaderParent_11() { return &____leaderParent_11; }
	inline void set__leaderParent_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____leaderParent_11 = value;
		Il2CppCodeGenWriteBarrier((&____leaderParent_11), value);
	}

	inline static int32_t get_offset_of__leaderName_12() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____leaderName_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__leaderName_12() const { return ____leaderName_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__leaderName_12() { return &____leaderName_12; }
	inline void set__leaderName_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____leaderName_12 = value;
		Il2CppCodeGenWriteBarrier((&____leaderName_12), value);
	}

	inline static int32_t get_offset_of__help_13() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____help_13)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__help_13() const { return ____help_13; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__help_13() { return &____help_13; }
	inline void set__help_13(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____help_13 = value;
		Il2CppCodeGenWriteBarrier((&____help_13), value);
	}

	inline static int32_t get_offset_of__level_14() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____level_14)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__level_14() const { return ____level_14; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__level_14() { return &____level_14; }
	inline void set__level_14(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____level_14 = value;
		Il2CppCodeGenWriteBarrier((&____level_14), value);
	}

	inline static int32_t get_offset_of__kick_15() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____kick_15)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__kick_15() const { return ____kick_15; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__kick_15() { return &____kick_15; }
	inline void set__kick_15(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____kick_15 = value;
		Il2CppCodeGenWriteBarrier((&____kick_15), value);
	}

	inline static int32_t get_offset_of__userVO_16() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____userVO_16)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_16() const { return ____userVO_16; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_16() { return &____userVO_16; }
	inline void set__userVO_16(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_16 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_16), value);
	}

	inline static int32_t get_offset_of__uiMain_17() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____uiMain_17)); }
	inline RuntimeObject* get__uiMain_17() const { return ____uiMain_17; }
	inline RuntimeObject** get_address_of__uiMain_17() { return &____uiMain_17; }
	inline void set__uiMain_17(RuntimeObject* value)
	{
		____uiMain_17 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_17), value);
	}

	inline static int32_t get_offset_of__data_18() { return static_cast<int32_t>(offsetof(MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC, ____data_18)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get__data_18() const { return ____data_18; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of__data_18() { return &____data_18; }
	inline void set__data_18(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		____data_18 = value;
		Il2CppCodeGenWriteBarrier((&____data_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYTEAMLISTITEM_TB264E6A70F6FB1A5968128CBF86FF0987023FFBC_H
#ifndef SETTINGSPOPUPNODE_TD66DF9E2BD66A8D5E5039F02445C385C79EED064_H
#define SETTINGSPOPUPNODE_TD66DF9E2BD66A8D5E5039F02445C385C79EED064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsPopupNode
struct  SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// UnityEngine.UI.Button SettingsPopupNode::_closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____closeBtn_7;
	// UnityEngine.UI.Button SettingsPopupNode::_fxSoundBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____fxSoundBtn_8;
	// UnityEngine.UI.Button SettingsPopupNode::_musicSoundBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____musicSoundBtn_9;
	// UnityEngine.UI.Image SettingsPopupNode::_fxSoundOn
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____fxSoundOn_10;
	// UnityEngine.UI.Image SettingsPopupNode::_fxSoundOff
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____fxSoundOff_11;
	// UnityEngine.UI.Image SettingsPopupNode::_musicSoundOn
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____musicSoundOn_12;
	// UnityEngine.UI.Image SettingsPopupNode::_musicSoundOff
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____musicSoundOff_13;
	// Zenject.DiContainer SettingsPopupNode::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_14;
	// UserVO SettingsPopupNode::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_15;
	// Tayr.TSoundSystem SettingsPopupNode::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_16;
	// SoundSO SettingsPopupNode::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_17;
	// System.Boolean SettingsPopupNode::_fx
	bool ____fx_18;
	// System.Boolean SettingsPopupNode::_music
	bool ____music_19;

public:
	inline static int32_t get_offset_of__closeBtn_7() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____closeBtn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__closeBtn_7() const { return ____closeBtn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__closeBtn_7() { return &____closeBtn_7; }
	inline void set__closeBtn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____closeBtn_7 = value;
		Il2CppCodeGenWriteBarrier((&____closeBtn_7), value);
	}

	inline static int32_t get_offset_of__fxSoundBtn_8() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____fxSoundBtn_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__fxSoundBtn_8() const { return ____fxSoundBtn_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__fxSoundBtn_8() { return &____fxSoundBtn_8; }
	inline void set__fxSoundBtn_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____fxSoundBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&____fxSoundBtn_8), value);
	}

	inline static int32_t get_offset_of__musicSoundBtn_9() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____musicSoundBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__musicSoundBtn_9() const { return ____musicSoundBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__musicSoundBtn_9() { return &____musicSoundBtn_9; }
	inline void set__musicSoundBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____musicSoundBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____musicSoundBtn_9), value);
	}

	inline static int32_t get_offset_of__fxSoundOn_10() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____fxSoundOn_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__fxSoundOn_10() const { return ____fxSoundOn_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__fxSoundOn_10() { return &____fxSoundOn_10; }
	inline void set__fxSoundOn_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____fxSoundOn_10 = value;
		Il2CppCodeGenWriteBarrier((&____fxSoundOn_10), value);
	}

	inline static int32_t get_offset_of__fxSoundOff_11() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____fxSoundOff_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__fxSoundOff_11() const { return ____fxSoundOff_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__fxSoundOff_11() { return &____fxSoundOff_11; }
	inline void set__fxSoundOff_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____fxSoundOff_11 = value;
		Il2CppCodeGenWriteBarrier((&____fxSoundOff_11), value);
	}

	inline static int32_t get_offset_of__musicSoundOn_12() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____musicSoundOn_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__musicSoundOn_12() const { return ____musicSoundOn_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__musicSoundOn_12() { return &____musicSoundOn_12; }
	inline void set__musicSoundOn_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____musicSoundOn_12 = value;
		Il2CppCodeGenWriteBarrier((&____musicSoundOn_12), value);
	}

	inline static int32_t get_offset_of__musicSoundOff_13() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____musicSoundOff_13)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__musicSoundOff_13() const { return ____musicSoundOff_13; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__musicSoundOff_13() { return &____musicSoundOff_13; }
	inline void set__musicSoundOff_13(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____musicSoundOff_13 = value;
		Il2CppCodeGenWriteBarrier((&____musicSoundOff_13), value);
	}

	inline static int32_t get_offset_of__container_14() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____container_14)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_14() const { return ____container_14; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_14() { return &____container_14; }
	inline void set__container_14(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_14 = value;
		Il2CppCodeGenWriteBarrier((&____container_14), value);
	}

	inline static int32_t get_offset_of__userVO_15() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____userVO_15)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_15() const { return ____userVO_15; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_15() { return &____userVO_15; }
	inline void set__userVO_15(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_15 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_15), value);
	}

	inline static int32_t get_offset_of__soundSystem_16() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____soundSystem_16)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_16() const { return ____soundSystem_16; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_16() { return &____soundSystem_16; }
	inline void set__soundSystem_16(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_16 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_16), value);
	}

	inline static int32_t get_offset_of__soundSO_17() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____soundSO_17)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_17() const { return ____soundSO_17; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_17() { return &____soundSO_17; }
	inline void set__soundSO_17(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_17 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_17), value);
	}

	inline static int32_t get_offset_of__fx_18() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____fx_18)); }
	inline bool get__fx_18() const { return ____fx_18; }
	inline bool* get_address_of__fx_18() { return &____fx_18; }
	inline void set__fx_18(bool value)
	{
		____fx_18 = value;
	}

	inline static int32_t get_offset_of__music_19() { return static_cast<int32_t>(offsetof(SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064, ____music_19)); }
	inline bool get__music_19() const { return ____music_19; }
	inline bool* get_address_of__music_19() { return &____music_19; }
	inline void set__music_19(bool value)
	{
		____music_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSPOPUPNODE_TD66DF9E2BD66A8D5E5039F02445C385C79EED064_H
#ifndef POPUPNODE_1_T0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4_H
#define POPUPNODE_1_T0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PopupNode`1<GameSparks.Core.GSData>
struct  PopupNode_1_t0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4  : public BasicNode_1_t0CD6D84C6B86DCAC0B15FA6B29832B5A10179CED
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPNODE_1_T0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4_H
#ifndef POPUPNODE_1_T8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4_H
#define POPUPNODE_1_T8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PopupNode`1<System.Int32>
struct  PopupNode_1_t8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPNODE_1_T8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4_H
#ifndef TABSNODE_1_T49E3604644CDBB4D36A232C96C46E05ECF9B7E18_H
#define TABSNODE_1_T49E3604644CDBB4D36A232C96C46E05ECF9B7E18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TabsNode`1<System.Int32>
struct  TabsNode_1_t49E3604644CDBB4D36A232C96C46E05ECF9B7E18  : public BasicNode_1_t80C898A8840F148EAC8A5FE47B08C3ABA79CE64D
{
public:
	// Tayr.INode Tayr.TabsNode`1::_currentTab
	RuntimeObject* ____currentTab_7;

public:
	inline static int32_t get_offset_of__currentTab_7() { return static_cast<int32_t>(offsetof(TabsNode_1_t49E3604644CDBB4D36A232C96C46E05ECF9B7E18, ____currentTab_7)); }
	inline RuntimeObject* get__currentTab_7() const { return ____currentTab_7; }
	inline RuntimeObject** get_address_of__currentTab_7() { return &____currentTab_7; }
	inline void set__currentTab_7(RuntimeObject* value)
	{
		____currentTab_7 = value;
		Il2CppCodeGenWriteBarrier((&____currentTab_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABSNODE_1_T49E3604644CDBB4D36A232C96C46E05ECF9B7E18_H
#ifndef VIEWTEAMTOOLTIPNODE_TA7F80A6EE2B92BB22816992440073A816176A769_H
#define VIEWTEAMTOOLTIPNODE_TA7F80A6EE2B92BB22816992440073A816176A769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewTeamTooltipNode
struct  ViewTeamTooltipNode_tA7F80A6EE2B92BB22816992440073A816176A769  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:
	// UnityEngine.UI.Button ViewTeamTooltipNode::_btn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btn_7;
	// Tayr.ILibrary ViewTeamTooltipNode::_uiMain
	RuntimeObject* ____uiMain_8;

public:
	inline static int32_t get_offset_of__btn_7() { return static_cast<int32_t>(offsetof(ViewTeamTooltipNode_tA7F80A6EE2B92BB22816992440073A816176A769, ____btn_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btn_7() const { return ____btn_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btn_7() { return &____btn_7; }
	inline void set__btn_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btn_7 = value;
		Il2CppCodeGenWriteBarrier((&____btn_7), value);
	}

	inline static int32_t get_offset_of__uiMain_8() { return static_cast<int32_t>(offsetof(ViewTeamTooltipNode_tA7F80A6EE2B92BB22816992440073A816176A769, ____uiMain_8)); }
	inline RuntimeObject* get__uiMain_8() const { return ____uiMain_8; }
	inline RuntimeObject** get_address_of__uiMain_8() { return &____uiMain_8; }
	inline void set__uiMain_8(RuntimeObject* value)
	{
		____uiMain_8 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWTEAMTOOLTIPNODE_TA7F80A6EE2B92BB22816992440073A816176A769_H
#ifndef BADGESPOPUPNODE_TB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A_H
#define BADGESPOPUPNODE_TB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BadgesPopupNode
struct  BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A  : public PopupNode_1_t8943E6F3C0A3C9A8E7B4BD8A0BE5D9CFAAA59EF4
{
public:
	// Tayr.TList BadgesPopupNode::_list
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____list_7;
	// GameSettingsSO BadgesPopupNode::_globalSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____globalSO_8;

public:
	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A, ____list_7)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__list_7() const { return ____list_7; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}

	inline static int32_t get_offset_of__globalSO_8() { return static_cast<int32_t>(offsetof(BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A, ____globalSO_8)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__globalSO_8() const { return ____globalSO_8; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__globalSO_8() { return &____globalSO_8; }
	inline void set__globalSO_8(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____globalSO_8 = value;
		Il2CppCodeGenWriteBarrier((&____globalSO_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BADGESPOPUPNODE_TB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A_H
#ifndef EDITTEAMPOPUPNODE_TCC67D85F6C59BDB715909ED1A0E53D9A660491D5_H
#define EDITTEAMPOPUPNODE_TCC67D85F6C59BDB715909ED1A0E53D9A660491D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditTeamPopupNode
struct  EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5  : public PopupNode_1_t0CE6B02DBF7AA10094AC095DCCDFAB5C11B256D4
{
public:
	// TMPro.TextMeshProUGUI EditTeamPopupNode::_name
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____name_7;
	// UnityEngine.UI.Image EditTeamPopupNode::_teamIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____teamIcon_8;
	// TMPro.TMP_InputField EditTeamPopupNode::_descriptionField
	TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * ____descriptionField_9;
	// UnityEngine.UI.Button EditTeamPopupNode::_typeLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeLeftBtn_10;
	// UnityEngine.UI.Button EditTeamPopupNode::_typeRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____typeRightBtn_11;
	// TMPro.TextMeshProUGUI EditTeamPopupNode::_type
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____type_12;
	// UnityEngine.UI.Button EditTeamPopupNode::_requiredLeftBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredLeftBtn_13;
	// UnityEngine.UI.Button EditTeamPopupNode::_requiredRightBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____requiredRightBtn_14;
	// TMPro.TextMeshProUGUI EditTeamPopupNode::_required
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____required_15;
	// UnityEngine.UI.Button EditTeamPopupNode::_saveBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____saveBtn_16;
	// Tayr.ILibrary EditTeamPopupNode::_iconLibrary
	RuntimeObject* ____iconLibrary_17;
	// Tayr.ILibrary EditTeamPopupNode::_uiMain
	RuntimeObject* ____uiMain_18;
	// Tayr.GameSparksPlatform EditTeamPopupNode::_gameSpark
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSpark_19;
	// UserEvents EditTeamPopupNode::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_20;
	// UserVO EditTeamPopupNode::_userVo
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVo_21;
	// KickerManager EditTeamPopupNode::_kickerManager
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * ____kickerManager_22;
	// System.Int32 EditTeamPopupNode::_currentRequirement
	int32_t ____currentRequirement_23;
	// TEAM_TYPE EditTeamPopupNode::_currentType
	int32_t ____currentType_24;

public:
	inline static int32_t get_offset_of__name_7() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____name_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__name_7() const { return ____name_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__name_7() { return &____name_7; }
	inline void set__name_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____name_7 = value;
		Il2CppCodeGenWriteBarrier((&____name_7), value);
	}

	inline static int32_t get_offset_of__teamIcon_8() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____teamIcon_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__teamIcon_8() const { return ____teamIcon_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__teamIcon_8() { return &____teamIcon_8; }
	inline void set__teamIcon_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____teamIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&____teamIcon_8), value);
	}

	inline static int32_t get_offset_of__descriptionField_9() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____descriptionField_9)); }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * get__descriptionField_9() const { return ____descriptionField_9; }
	inline TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB ** get_address_of__descriptionField_9() { return &____descriptionField_9; }
	inline void set__descriptionField_9(TMP_InputField_tC3C57E697A57232E8A855D39600CF06CFDA8F6CB * value)
	{
		____descriptionField_9 = value;
		Il2CppCodeGenWriteBarrier((&____descriptionField_9), value);
	}

	inline static int32_t get_offset_of__typeLeftBtn_10() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____typeLeftBtn_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeLeftBtn_10() const { return ____typeLeftBtn_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeLeftBtn_10() { return &____typeLeftBtn_10; }
	inline void set__typeLeftBtn_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeLeftBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&____typeLeftBtn_10), value);
	}

	inline static int32_t get_offset_of__typeRightBtn_11() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____typeRightBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__typeRightBtn_11() const { return ____typeRightBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__typeRightBtn_11() { return &____typeRightBtn_11; }
	inline void set__typeRightBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____typeRightBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____typeRightBtn_11), value);
	}

	inline static int32_t get_offset_of__type_12() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____type_12)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__type_12() const { return ____type_12; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__type_12() { return &____type_12; }
	inline void set__type_12(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____type_12 = value;
		Il2CppCodeGenWriteBarrier((&____type_12), value);
	}

	inline static int32_t get_offset_of__requiredLeftBtn_13() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____requiredLeftBtn_13)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredLeftBtn_13() const { return ____requiredLeftBtn_13; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredLeftBtn_13() { return &____requiredLeftBtn_13; }
	inline void set__requiredLeftBtn_13(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredLeftBtn_13 = value;
		Il2CppCodeGenWriteBarrier((&____requiredLeftBtn_13), value);
	}

	inline static int32_t get_offset_of__requiredRightBtn_14() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____requiredRightBtn_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__requiredRightBtn_14() const { return ____requiredRightBtn_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__requiredRightBtn_14() { return &____requiredRightBtn_14; }
	inline void set__requiredRightBtn_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____requiredRightBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____requiredRightBtn_14), value);
	}

	inline static int32_t get_offset_of__required_15() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____required_15)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__required_15() const { return ____required_15; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__required_15() { return &____required_15; }
	inline void set__required_15(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____required_15 = value;
		Il2CppCodeGenWriteBarrier((&____required_15), value);
	}

	inline static int32_t get_offset_of__saveBtn_16() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____saveBtn_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__saveBtn_16() const { return ____saveBtn_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__saveBtn_16() { return &____saveBtn_16; }
	inline void set__saveBtn_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____saveBtn_16 = value;
		Il2CppCodeGenWriteBarrier((&____saveBtn_16), value);
	}

	inline static int32_t get_offset_of__iconLibrary_17() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____iconLibrary_17)); }
	inline RuntimeObject* get__iconLibrary_17() const { return ____iconLibrary_17; }
	inline RuntimeObject** get_address_of__iconLibrary_17() { return &____iconLibrary_17; }
	inline void set__iconLibrary_17(RuntimeObject* value)
	{
		____iconLibrary_17 = value;
		Il2CppCodeGenWriteBarrier((&____iconLibrary_17), value);
	}

	inline static int32_t get_offset_of__uiMain_18() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____uiMain_18)); }
	inline RuntimeObject* get__uiMain_18() const { return ____uiMain_18; }
	inline RuntimeObject** get_address_of__uiMain_18() { return &____uiMain_18; }
	inline void set__uiMain_18(RuntimeObject* value)
	{
		____uiMain_18 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_18), value);
	}

	inline static int32_t get_offset_of__gameSpark_19() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____gameSpark_19)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSpark_19() const { return ____gameSpark_19; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSpark_19() { return &____gameSpark_19; }
	inline void set__gameSpark_19(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSpark_19 = value;
		Il2CppCodeGenWriteBarrier((&____gameSpark_19), value);
	}

	inline static int32_t get_offset_of__userEvents_20() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____userEvents_20)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_20() const { return ____userEvents_20; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_20() { return &____userEvents_20; }
	inline void set__userEvents_20(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_20 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_20), value);
	}

	inline static int32_t get_offset_of__userVo_21() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____userVo_21)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVo_21() const { return ____userVo_21; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVo_21() { return &____userVo_21; }
	inline void set__userVo_21(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVo_21 = value;
		Il2CppCodeGenWriteBarrier((&____userVo_21), value);
	}

	inline static int32_t get_offset_of__kickerManager_22() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____kickerManager_22)); }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * get__kickerManager_22() const { return ____kickerManager_22; }
	inline KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA ** get_address_of__kickerManager_22() { return &____kickerManager_22; }
	inline void set__kickerManager_22(KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA * value)
	{
		____kickerManager_22 = value;
		Il2CppCodeGenWriteBarrier((&____kickerManager_22), value);
	}

	inline static int32_t get_offset_of__currentRequirement_23() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____currentRequirement_23)); }
	inline int32_t get__currentRequirement_23() const { return ____currentRequirement_23; }
	inline int32_t* get_address_of__currentRequirement_23() { return &____currentRequirement_23; }
	inline void set__currentRequirement_23(int32_t value)
	{
		____currentRequirement_23 = value;
	}

	inline static int32_t get_offset_of__currentType_24() { return static_cast<int32_t>(offsetof(EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5, ____currentType_24)); }
	inline int32_t get__currentType_24() const { return ____currentType_24; }
	inline int32_t* get_address_of__currentType_24() { return &____currentType_24; }
	inline void set__currentType_24(int32_t value)
	{
		____currentType_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITTEAMPOPUPNODE_TCC67D85F6C59BDB715909ED1A0E53D9A660491D5_H
#ifndef LEADERBOARDVIEWCOMPONENT_TFC4F3A2D563962E22E4EE544E8724F4625E77A60_H
#define LEADERBOARDVIEWCOMPONENT_TFC4F3A2D563962E22E4EE544E8724F4625E77A60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardViewComponent
struct  LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60  : public TabsNode_1_t49E3604644CDBB4D36A232C96C46E05ECF9B7E18
{
public:
	// UnityEngine.UI.Button LeaderboardViewComponent::PlayersTab
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___PlayersTab_8;
	// UnityEngine.Transform LeaderboardViewComponent::PlayersTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___PlayersTabActive_9;
	// UnityEngine.Transform LeaderboardViewComponent::PlayersTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___PlayersTabInactive_10;
	// UnityEngine.UI.Button LeaderboardViewComponent::TeamsTab
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___TeamsTab_11;
	// UnityEngine.Transform LeaderboardViewComponent::TeamsTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___TeamsTabActive_12;
	// UnityEngine.Transform LeaderboardViewComponent::TeamsTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___TeamsTabInactive_13;
	// UnityEngine.UI.Button LeaderboardViewComponent::FriendsTab
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___FriendsTab_14;
	// UnityEngine.Transform LeaderboardViewComponent::FriendsTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___FriendsTabActive_15;
	// UnityEngine.Transform LeaderboardViewComponent::FriendsTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___FriendsTabInactive_16;
	// Tayr.GameSparksPlatform LeaderboardViewComponent::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_17;
	// UserVO LeaderboardViewComponent::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_18;
	// LeaderboardsVO LeaderboardViewComponent::_leaderboardVO
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * ____leaderboardVO_19;
	// UserEvents LeaderboardViewComponent::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_20;
	// Tayr.ILibrary LeaderboardViewComponent::_uiMain
	RuntimeObject* ____uiMain_21;
	// Tayr.VOSaver LeaderboardViewComponent::_voSaver
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * ____voSaver_22;
	// Zenject.DiContainer LeaderboardViewComponent::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_23;
	// Tayr.TSoundSystem LeaderboardViewComponent::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_24;
	// SoundSO LeaderboardViewComponent::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_25;
	// System.Single LeaderboardViewComponent::_lastTimeLeaderboardUpdated
	float ____lastTimeLeaderboardUpdated_26;
	// Tayr.TList LeaderboardViewComponent::_activeList
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * ____activeList_27;

public:
	inline static int32_t get_offset_of_PlayersTab_8() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___PlayersTab_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_PlayersTab_8() const { return ___PlayersTab_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_PlayersTab_8() { return &___PlayersTab_8; }
	inline void set_PlayersTab_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___PlayersTab_8 = value;
		Il2CppCodeGenWriteBarrier((&___PlayersTab_8), value);
	}

	inline static int32_t get_offset_of_PlayersTabActive_9() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___PlayersTabActive_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_PlayersTabActive_9() const { return ___PlayersTabActive_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_PlayersTabActive_9() { return &___PlayersTabActive_9; }
	inline void set_PlayersTabActive_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___PlayersTabActive_9 = value;
		Il2CppCodeGenWriteBarrier((&___PlayersTabActive_9), value);
	}

	inline static int32_t get_offset_of_PlayersTabInactive_10() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___PlayersTabInactive_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_PlayersTabInactive_10() const { return ___PlayersTabInactive_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_PlayersTabInactive_10() { return &___PlayersTabInactive_10; }
	inline void set_PlayersTabInactive_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___PlayersTabInactive_10 = value;
		Il2CppCodeGenWriteBarrier((&___PlayersTabInactive_10), value);
	}

	inline static int32_t get_offset_of_TeamsTab_11() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___TeamsTab_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_TeamsTab_11() const { return ___TeamsTab_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_TeamsTab_11() { return &___TeamsTab_11; }
	inline void set_TeamsTab_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___TeamsTab_11 = value;
		Il2CppCodeGenWriteBarrier((&___TeamsTab_11), value);
	}

	inline static int32_t get_offset_of_TeamsTabActive_12() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___TeamsTabActive_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_TeamsTabActive_12() const { return ___TeamsTabActive_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_TeamsTabActive_12() { return &___TeamsTabActive_12; }
	inline void set_TeamsTabActive_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___TeamsTabActive_12 = value;
		Il2CppCodeGenWriteBarrier((&___TeamsTabActive_12), value);
	}

	inline static int32_t get_offset_of_TeamsTabInactive_13() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___TeamsTabInactive_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_TeamsTabInactive_13() const { return ___TeamsTabInactive_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_TeamsTabInactive_13() { return &___TeamsTabInactive_13; }
	inline void set_TeamsTabInactive_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___TeamsTabInactive_13 = value;
		Il2CppCodeGenWriteBarrier((&___TeamsTabInactive_13), value);
	}

	inline static int32_t get_offset_of_FriendsTab_14() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___FriendsTab_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_FriendsTab_14() const { return ___FriendsTab_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_FriendsTab_14() { return &___FriendsTab_14; }
	inline void set_FriendsTab_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___FriendsTab_14 = value;
		Il2CppCodeGenWriteBarrier((&___FriendsTab_14), value);
	}

	inline static int32_t get_offset_of_FriendsTabActive_15() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___FriendsTabActive_15)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_FriendsTabActive_15() const { return ___FriendsTabActive_15; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_FriendsTabActive_15() { return &___FriendsTabActive_15; }
	inline void set_FriendsTabActive_15(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___FriendsTabActive_15 = value;
		Il2CppCodeGenWriteBarrier((&___FriendsTabActive_15), value);
	}

	inline static int32_t get_offset_of_FriendsTabInactive_16() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ___FriendsTabInactive_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_FriendsTabInactive_16() const { return ___FriendsTabInactive_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_FriendsTabInactive_16() { return &___FriendsTabInactive_16; }
	inline void set_FriendsTabInactive_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___FriendsTabInactive_16 = value;
		Il2CppCodeGenWriteBarrier((&___FriendsTabInactive_16), value);
	}

	inline static int32_t get_offset_of__gameSparks_17() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____gameSparks_17)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_17() const { return ____gameSparks_17; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_17() { return &____gameSparks_17; }
	inline void set__gameSparks_17(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_17 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_17), value);
	}

	inline static int32_t get_offset_of__userVO_18() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____userVO_18)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_18() const { return ____userVO_18; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_18() { return &____userVO_18; }
	inline void set__userVO_18(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_18 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_18), value);
	}

	inline static int32_t get_offset_of__leaderboardVO_19() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____leaderboardVO_19)); }
	inline LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * get__leaderboardVO_19() const { return ____leaderboardVO_19; }
	inline LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 ** get_address_of__leaderboardVO_19() { return &____leaderboardVO_19; }
	inline void set__leaderboardVO_19(LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6 * value)
	{
		____leaderboardVO_19 = value;
		Il2CppCodeGenWriteBarrier((&____leaderboardVO_19), value);
	}

	inline static int32_t get_offset_of__userEvents_20() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____userEvents_20)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_20() const { return ____userEvents_20; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_20() { return &____userEvents_20; }
	inline void set__userEvents_20(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_20 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_20), value);
	}

	inline static int32_t get_offset_of__uiMain_21() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____uiMain_21)); }
	inline RuntimeObject* get__uiMain_21() const { return ____uiMain_21; }
	inline RuntimeObject** get_address_of__uiMain_21() { return &____uiMain_21; }
	inline void set__uiMain_21(RuntimeObject* value)
	{
		____uiMain_21 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_21), value);
	}

	inline static int32_t get_offset_of__voSaver_22() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____voSaver_22)); }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * get__voSaver_22() const { return ____voSaver_22; }
	inline VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F ** get_address_of__voSaver_22() { return &____voSaver_22; }
	inline void set__voSaver_22(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F * value)
	{
		____voSaver_22 = value;
		Il2CppCodeGenWriteBarrier((&____voSaver_22), value);
	}

	inline static int32_t get_offset_of__container_23() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____container_23)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_23() const { return ____container_23; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_23() { return &____container_23; }
	inline void set__container_23(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_23 = value;
		Il2CppCodeGenWriteBarrier((&____container_23), value);
	}

	inline static int32_t get_offset_of__soundSystem_24() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____soundSystem_24)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_24() const { return ____soundSystem_24; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_24() { return &____soundSystem_24; }
	inline void set__soundSystem_24(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_24 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_24), value);
	}

	inline static int32_t get_offset_of__soundSO_25() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____soundSO_25)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_25() const { return ____soundSO_25; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_25() { return &____soundSO_25; }
	inline void set__soundSO_25(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_25 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_25), value);
	}

	inline static int32_t get_offset_of__lastTimeLeaderboardUpdated_26() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____lastTimeLeaderboardUpdated_26)); }
	inline float get__lastTimeLeaderboardUpdated_26() const { return ____lastTimeLeaderboardUpdated_26; }
	inline float* get_address_of__lastTimeLeaderboardUpdated_26() { return &____lastTimeLeaderboardUpdated_26; }
	inline void set__lastTimeLeaderboardUpdated_26(float value)
	{
		____lastTimeLeaderboardUpdated_26 = value;
	}

	inline static int32_t get_offset_of__activeList_27() { return static_cast<int32_t>(offsetof(LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60, ____activeList_27)); }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * get__activeList_27() const { return ____activeList_27; }
	inline TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 ** get_address_of__activeList_27() { return &____activeList_27; }
	inline void set__activeList_27(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292 * value)
	{
		____activeList_27 = value;
		Il2CppCodeGenWriteBarrier((&____activeList_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEADERBOARDVIEWCOMPONENT_TFC4F3A2D563962E22E4EE544E8724F4625E77A60_H
#ifndef PLAYERLEADERBOARDNODE_T639686FA813812A7BBD784C6DEB03606988DD5BA_H
#define PLAYERLEADERBOARDNODE_T639686FA813812A7BBD784C6DEB03606988DD5BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerLeaderboardNode
struct  PlayerLeaderboardNode_t639686FA813812A7BBD784C6DEB03606988DD5BA  : public LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERLEADERBOARDNODE_T639686FA813812A7BBD784C6DEB03606988DD5BA_H
#ifndef TEAMLEADERBOARDNODE_TA4967F25251D87815AEE613128701F01D6D5E068_H
#define TEAMLEADERBOARDNODE_TA4967F25251D87815AEE613128701F01D6D5E068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamLeaderboardNode
struct  TeamLeaderboardNode_tA4967F25251D87815AEE613128701F01D6D5E068  : public LeaderboardNode_1_tBAB95057065A1C3CD1E31776EAAB8BDDDA50DA1F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMLEADERBOARDNODE_TA4967F25251D87815AEE613128701F01D6D5E068_H
#ifndef TEAMVIEWCOMPONENT_TC26F5EF388A1F94F841B0FFD54B1C6204453020D_H
#define TEAMVIEWCOMPONENT_TC26F5EF388A1F94F841B0FFD54B1C6204453020D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeamViewComponent
struct  TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D  : public TabsNode_1_t49E3604644CDBB4D36A232C96C46E05ECF9B7E18
{
public:
	// UnityEngine.Transform TeamViewComponent::_notConnected
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____notConnected_8;
	// UnityEngine.Transform TeamViewComponent::_joinTabs
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____joinTabs_9;
	// UnityEngine.Transform TeamViewComponent::_joinedTabs
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____joinedTabs_10;
	// UnityEngine.UI.Button TeamViewComponent::_teamsTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____teamsTabBtn_11;
	// UnityEngine.Transform TeamViewComponent::_teamsTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____teamsTabActive_12;
	// UnityEngine.Transform TeamViewComponent::_teamsTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____teamsTabInactive_13;
	// UnityEngine.UI.Button TeamViewComponent::_createTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____createTabBtn_14;
	// UnityEngine.Transform TeamViewComponent::_createTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____createTabActive_15;
	// UnityEngine.Transform TeamViewComponent::_createTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____createTabInactive_16;
	// UnityEngine.UI.Button TeamViewComponent::_chatTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____chatTabBtn_17;
	// UnityEngine.Transform TeamViewComponent::_chatTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____chatTabActive_18;
	// UnityEngine.Transform TeamViewComponent::_chatTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____chatTabInactive_19;
	// UnityEngine.UI.Button TeamViewComponent::_myteamTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____myteamTabBtn_20;
	// UnityEngine.Transform TeamViewComponent::_myteamTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____myteamTabActive_21;
	// UnityEngine.Transform TeamViewComponent::_myteamTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____myteamTabInactive_22;
	// UnityEngine.UI.Button TeamViewComponent::_searchTabBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____searchTabBtn_23;
	// UnityEngine.Transform TeamViewComponent::_searchTabActive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____searchTabActive_24;
	// UnityEngine.Transform TeamViewComponent::_searchTabInactive
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____searchTabInactive_25;
	// TMPro.TextMeshProUGUI TeamViewComponent::_requirementText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____requirementText_26;
	// Tayr.ILibrary TeamViewComponent::_uiMain
	RuntimeObject* ____uiMain_27;
	// UserEvents TeamViewComponent::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_28;
	// UserVO TeamViewComponent::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_29;
	// GameSettingsSO TeamViewComponent::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_30;

public:
	inline static int32_t get_offset_of__notConnected_8() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____notConnected_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__notConnected_8() const { return ____notConnected_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__notConnected_8() { return &____notConnected_8; }
	inline void set__notConnected_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____notConnected_8 = value;
		Il2CppCodeGenWriteBarrier((&____notConnected_8), value);
	}

	inline static int32_t get_offset_of__joinTabs_9() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____joinTabs_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__joinTabs_9() const { return ____joinTabs_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__joinTabs_9() { return &____joinTabs_9; }
	inline void set__joinTabs_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____joinTabs_9 = value;
		Il2CppCodeGenWriteBarrier((&____joinTabs_9), value);
	}

	inline static int32_t get_offset_of__joinedTabs_10() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____joinedTabs_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__joinedTabs_10() const { return ____joinedTabs_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__joinedTabs_10() { return &____joinedTabs_10; }
	inline void set__joinedTabs_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____joinedTabs_10 = value;
		Il2CppCodeGenWriteBarrier((&____joinedTabs_10), value);
	}

	inline static int32_t get_offset_of__teamsTabBtn_11() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____teamsTabBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__teamsTabBtn_11() const { return ____teamsTabBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__teamsTabBtn_11() { return &____teamsTabBtn_11; }
	inline void set__teamsTabBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____teamsTabBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____teamsTabBtn_11), value);
	}

	inline static int32_t get_offset_of__teamsTabActive_12() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____teamsTabActive_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__teamsTabActive_12() const { return ____teamsTabActive_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__teamsTabActive_12() { return &____teamsTabActive_12; }
	inline void set__teamsTabActive_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____teamsTabActive_12 = value;
		Il2CppCodeGenWriteBarrier((&____teamsTabActive_12), value);
	}

	inline static int32_t get_offset_of__teamsTabInactive_13() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____teamsTabInactive_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__teamsTabInactive_13() const { return ____teamsTabInactive_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__teamsTabInactive_13() { return &____teamsTabInactive_13; }
	inline void set__teamsTabInactive_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____teamsTabInactive_13 = value;
		Il2CppCodeGenWriteBarrier((&____teamsTabInactive_13), value);
	}

	inline static int32_t get_offset_of__createTabBtn_14() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____createTabBtn_14)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__createTabBtn_14() const { return ____createTabBtn_14; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__createTabBtn_14() { return &____createTabBtn_14; }
	inline void set__createTabBtn_14(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____createTabBtn_14 = value;
		Il2CppCodeGenWriteBarrier((&____createTabBtn_14), value);
	}

	inline static int32_t get_offset_of__createTabActive_15() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____createTabActive_15)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__createTabActive_15() const { return ____createTabActive_15; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__createTabActive_15() { return &____createTabActive_15; }
	inline void set__createTabActive_15(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____createTabActive_15 = value;
		Il2CppCodeGenWriteBarrier((&____createTabActive_15), value);
	}

	inline static int32_t get_offset_of__createTabInactive_16() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____createTabInactive_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__createTabInactive_16() const { return ____createTabInactive_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__createTabInactive_16() { return &____createTabInactive_16; }
	inline void set__createTabInactive_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____createTabInactive_16 = value;
		Il2CppCodeGenWriteBarrier((&____createTabInactive_16), value);
	}

	inline static int32_t get_offset_of__chatTabBtn_17() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____chatTabBtn_17)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__chatTabBtn_17() const { return ____chatTabBtn_17; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__chatTabBtn_17() { return &____chatTabBtn_17; }
	inline void set__chatTabBtn_17(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____chatTabBtn_17 = value;
		Il2CppCodeGenWriteBarrier((&____chatTabBtn_17), value);
	}

	inline static int32_t get_offset_of__chatTabActive_18() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____chatTabActive_18)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__chatTabActive_18() const { return ____chatTabActive_18; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__chatTabActive_18() { return &____chatTabActive_18; }
	inline void set__chatTabActive_18(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____chatTabActive_18 = value;
		Il2CppCodeGenWriteBarrier((&____chatTabActive_18), value);
	}

	inline static int32_t get_offset_of__chatTabInactive_19() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____chatTabInactive_19)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__chatTabInactive_19() const { return ____chatTabInactive_19; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__chatTabInactive_19() { return &____chatTabInactive_19; }
	inline void set__chatTabInactive_19(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____chatTabInactive_19 = value;
		Il2CppCodeGenWriteBarrier((&____chatTabInactive_19), value);
	}

	inline static int32_t get_offset_of__myteamTabBtn_20() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____myteamTabBtn_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__myteamTabBtn_20() const { return ____myteamTabBtn_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__myteamTabBtn_20() { return &____myteamTabBtn_20; }
	inline void set__myteamTabBtn_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____myteamTabBtn_20 = value;
		Il2CppCodeGenWriteBarrier((&____myteamTabBtn_20), value);
	}

	inline static int32_t get_offset_of__myteamTabActive_21() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____myteamTabActive_21)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__myteamTabActive_21() const { return ____myteamTabActive_21; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__myteamTabActive_21() { return &____myteamTabActive_21; }
	inline void set__myteamTabActive_21(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____myteamTabActive_21 = value;
		Il2CppCodeGenWriteBarrier((&____myteamTabActive_21), value);
	}

	inline static int32_t get_offset_of__myteamTabInactive_22() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____myteamTabInactive_22)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__myteamTabInactive_22() const { return ____myteamTabInactive_22; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__myteamTabInactive_22() { return &____myteamTabInactive_22; }
	inline void set__myteamTabInactive_22(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____myteamTabInactive_22 = value;
		Il2CppCodeGenWriteBarrier((&____myteamTabInactive_22), value);
	}

	inline static int32_t get_offset_of__searchTabBtn_23() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____searchTabBtn_23)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__searchTabBtn_23() const { return ____searchTabBtn_23; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__searchTabBtn_23() { return &____searchTabBtn_23; }
	inline void set__searchTabBtn_23(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____searchTabBtn_23 = value;
		Il2CppCodeGenWriteBarrier((&____searchTabBtn_23), value);
	}

	inline static int32_t get_offset_of__searchTabActive_24() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____searchTabActive_24)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__searchTabActive_24() const { return ____searchTabActive_24; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__searchTabActive_24() { return &____searchTabActive_24; }
	inline void set__searchTabActive_24(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____searchTabActive_24 = value;
		Il2CppCodeGenWriteBarrier((&____searchTabActive_24), value);
	}

	inline static int32_t get_offset_of__searchTabInactive_25() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____searchTabInactive_25)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__searchTabInactive_25() const { return ____searchTabInactive_25; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__searchTabInactive_25() { return &____searchTabInactive_25; }
	inline void set__searchTabInactive_25(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____searchTabInactive_25 = value;
		Il2CppCodeGenWriteBarrier((&____searchTabInactive_25), value);
	}

	inline static int32_t get_offset_of__requirementText_26() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____requirementText_26)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__requirementText_26() const { return ____requirementText_26; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__requirementText_26() { return &____requirementText_26; }
	inline void set__requirementText_26(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____requirementText_26 = value;
		Il2CppCodeGenWriteBarrier((&____requirementText_26), value);
	}

	inline static int32_t get_offset_of__uiMain_27() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____uiMain_27)); }
	inline RuntimeObject* get__uiMain_27() const { return ____uiMain_27; }
	inline RuntimeObject** get_address_of__uiMain_27() { return &____uiMain_27; }
	inline void set__uiMain_27(RuntimeObject* value)
	{
		____uiMain_27 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_27), value);
	}

	inline static int32_t get_offset_of__userEvents_28() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____userEvents_28)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_28() const { return ____userEvents_28; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_28() { return &____userEvents_28; }
	inline void set__userEvents_28(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_28 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_28), value);
	}

	inline static int32_t get_offset_of__userVO_29() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____userVO_29)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_29() const { return ____userVO_29; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_29() { return &____userVO_29; }
	inline void set__userVO_29(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_29 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_29), value);
	}

	inline static int32_t get_offset_of__gameSettingsSO_30() { return static_cast<int32_t>(offsetof(TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D, ____gameSettingsSO_30)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_30() const { return ____gameSettingsSO_30; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_30() { return &____gameSettingsSO_30; }
	inline void set__gameSettingsSO_30(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_30 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMVIEWCOMPONENT_TC26F5EF388A1F94F841B0FFD54B1C6204453020D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8500 = { sizeof (GameServerType_tCBECD733E74A8E692D2BBBA2808C030954318020)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8500[2] = 
{
	GameServerType_tCBECD733E74A8E692D2BBBA2808C030954318020::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8501 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8502 = { sizeof (LeaderboardResult_tF0787FB00F09160F7DFA5473557E5B5004C887D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8503 = { sizeof (LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8503[3] = 
{
	LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B::get_offset_of_Position_0(),
	LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B::get_offset_of_Name_1(),
	LeaderboardItem_tBDF8B18DB646A74FD083543E4F0F1C9D72734D2B::get_offset_of_Value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8504 = { sizeof (GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8504[4] = 
{
	GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399::get_offset_of_Name_0(),
	GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399::get_offset_of_Badge_1(),
	GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399::get_offset_of_Description_2(),
	GroupModel_tE75857CD63B052D0127D746D6D58CD8A90655399::get_offset_of_Private_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8505 = { sizeof (GameSparkChapterPatcherTask_tB58333A280C2A0E03D79E5038D0D0D5B6E6D1497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8505[1] = 
{
	GameSparkChapterPatcherTask_tB58333A280C2A0E03D79E5038D0D0D5B6E6D1497::get_offset_of__chapter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8506 = { sizeof (U3CTaskU3Ed__2_t5707BA5537CFD728B20C7BC0EF542494BA061CDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8506[2] = 
{
	U3CTaskU3Ed__2_t5707BA5537CFD728B20C7BC0EF542494BA061CDB::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t5707BA5537CFD728B20C7BC0EF542494BA061CDB::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8507 = { sizeof (GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8507[6] = 
{
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356::get_offset_of__platfrom_7(),
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356::get_offset_of__patchingVO_8(),
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356::get_offset_of_MissingAssets_9(),
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356::get_offset_of__file_10(),
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356::get_offset_of__filePath_11(),
	GameSparkPatcherTask_t5BAF57CC343AF3D35224622137E6F71F96E3F356::get_offset_of__version_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8508 = { sizeof (U3CU3Ec__DisplayClass7_0_t93D61961BFB9FD85994672A961ADF6A89315C6B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8508[2] = 
{
	U3CU3Ec__DisplayClass7_0_t93D61961BFB9FD85994672A961ADF6A89315C6B0::get_offset_of_fileNameGS_0(),
	U3CU3Ec__DisplayClass7_0_t93D61961BFB9FD85994672A961ADF6A89315C6B0::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8509 = { sizeof (U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8509[3] = 
{
	U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__7_t366290A4947CE5D103F6A6125F8664AC64EED18A::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8510 = { sizeof (U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8510[3] = 
{
	U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4::get_offset_of_asset_1(),
	U3CU3Ec__DisplayClass11_0_t3310E910B0C69A0FBECC365551D8BF0E9CA80BC4::get_offset_of_fileNameGS_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8511 = { sizeof (U3CU3Ec__DisplayClass12_0_t539AB4894B46C7A3ED82A559BC7242BCF086B1E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8511[2] = 
{
	U3CU3Ec__DisplayClass12_0_t539AB4894B46C7A3ED82A559BC7242BCF086B1E5::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass12_0_t539AB4894B46C7A3ED82A559BC7242BCF086B1E5::get_offset_of_path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8512 = { sizeof (GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8512[2] = 
{
	GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D::get_offset_of_Version_0(),
	GameSparkTaskResult_t4BD72A1DBD60CBE3D6EE8496698180E59A72D65D::get_offset_of_Hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8513 = { sizeof (Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D), -1, sizeof(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8513[14] = 
{
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D_StaticFields::get_offset_of__sharedInstance_0(),
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D::get_offset_of_U3CgameU3Ek__BackingField_1(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8514 = { sizeof (U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931), -1, sizeof(U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8514[10] = 
{
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_0_2(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_1_3(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_2_4(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_3_5(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_4_6(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_5_7(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_6_8(),
	U3CU3Ec_tF545A53621D4FF47253EEA6E88AA26FA98B68931_StaticFields::get_offset_of_U3CU3E9__24_7_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8515 = { sizeof (ContextsExtensions_t2C3E30EF154BB8994481BB2F2B4249468FC6E123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8516 = { sizeof (DisposeListenerComponent_tDB470FB5BF8A06F03BE869C1E3C361691EF33A6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8516[1] = 
{
	DisposeListenerComponent_tDB470FB5BF8A06F03BE869C1E3C361691EF33A6E::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8517 = { sizeof (GameEventSystems_t761FC34066CD15B909F184B97715937F42D6986C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8518 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8519 = { sizeof (DisposeEventSystem_tF862CD99C56BD5FF05B9AD75ECEDDD0D5862D18C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8519[1] = 
{
	DisposeEventSystem_tF862CD99C56BD5FF05B9AD75ECEDDD0D5862D18C::get_offset_of__listenerBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8520 = { sizeof (Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8521 = { sizeof (GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF), -1, sizeof(GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8521[13] = 
{
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_aggroDisabledComponent_18(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_battleReviveComponent_19(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_blockingSpellComponent_20(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_collectableComponent_21(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_destroyableComponent_22(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_disposeComponent_23(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_effectStartedComponent_24(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_fXSoundComponent_25(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_instantEffectComponent_26(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_musicSoundComponent_27(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_objectiveCompleteComponent_28(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_removeViewOnDisposeComponent_29(),
	GameEntity_t995A5498A539905D7F1F8284D59BD48F077D78FF_StaticFields::get_offset_of_towerHealingComponent_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8522 = { sizeof (GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5), -1, sizeof(GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8522[115] = 
{
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAddGameObjectEffectRender_0(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAggro_1(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAggroDisabled_2(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAggroHeight_3(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAggroRange_4(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAggroStatus_5(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAggroType_6(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAggroWidth_7(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAnimator_8(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAnimatorTrigger_9(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAoe_10(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAoeEffectAfterTime_11(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAttackBoost_12(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherAttackFrequency_13(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherBattleDuration_14(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherBattleExtraDuration_15(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherBattleKickerWithIcon_16(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherBattleRevive_17(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherBattleTime_18(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherBlockingSpell_19(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherButtonTap_20(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherCallouts_21(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherCameraShake_22(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherCart_23(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherCollectable_24(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherCollected_25(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherCompanion_26(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherCreateTrapAfterTime_27(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDamage_28(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDecoy_29(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDefenseId_30(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDefenseSpell_31(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDefenseSpellController_32(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDestroyable_33(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDisposeAfterTime_34(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDispose_35(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDisposeListener_36(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDropCollectable_37(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherDuration_38(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherEffect_39(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherEffectRendrer_40(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherEffectSource_41(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherEffectStarted_42(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherEffectStartTime_43(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherEffectVisual_44(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherFXSound_45(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherGoal_46(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherGroup_47(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherHero_48(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherHeroState_49(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherHPBoost_50(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherHP_51(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherId_52(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherILibrary_53(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherInitPosition_54(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherInstantEffect_55(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherInventory_56(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherInventoryIndex_57(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherInventoryItem_58(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherInventoryItems_59(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherItem_60(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherItemGenerator_61(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherItemsSpawnPoints_62(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherJoystick_63(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherLastAttackTime_64(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherLeftHand_65(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherLevel_66(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherLifetimeEffect_67(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherLoopingAI_68(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherMapItem_69(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherMaxHP_70(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherMusicSound_71(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherObjectiveComplete_72(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherObjectiveCounter_73(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherObjectives_74(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherObjectiveStep_75(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherObjectiveType_76(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherParent_77(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherPosition_78(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherProjectile_79(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherProjectileSource_80(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherProjectileSpell_81(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherProjectileStartPosition_82(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherProjectileTargetEntity_83(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherPulseAI_84(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherRemoveViewOnDispose_85(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherResist_86(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherRotation_87(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherScreenTap_88(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSlowSpeed_89(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSound_90(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSpeedBoost_91(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSpeed_92(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSpell_93(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSpellController_94(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSpellIndex_95(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSpellMeta_96(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSpellStatus_97(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherSphereCollider_98(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherStartTime_99(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTapTapGroundRecognizer_100(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTapTapTowerRecognizer_101(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTower_102(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTowerHealing_103(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTowerHealingDuration_104(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTowerHealingStartTime_105(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTowerHPBar_106(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTowerRender_107(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTowerTap_108(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTowerTop_109(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherTrap_110(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherUIHolder_111(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherView_112(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherVisual_113(),
	GameMatcher_t84E0267ECFC8A0C9550FC34B8CA04FA40D5AB7B5_StaticFields::get_offset_of__matcherWall_114(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8523 = { sizeof (GameAttribute_tEAC4F9478367269006D9122C41F46A607105BE84), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8524 = { sizeof (GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA), -1, sizeof(GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8524[118] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA_StaticFields::get_offset_of_componentNames_116(),
	GameComponentsLookup_t50E480B87F8B2739A0D03BE1DF2404509E7A44AA_StaticFields::get_offset_of_componentTypes_117(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8525 = { sizeof (HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8525[15] = 
{
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__playBtn_4(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__contributeBtn_5(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__levelNumberTxt_6(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__starChest_7(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__uiMain_8(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__starTournamentSystem_9(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__userVO_10(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__kickerManager_11(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__inventorySystem_12(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__diContainer_13(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__container_14(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__soundSystem_15(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__soundSO_16(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__chaptersSO_17(),
	HomeViewComponent_t542EECD1BE167E3BCC518711DDEA72C32D01D818::get_offset_of__levelVariable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8526 = { sizeof (ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8526[6] = 
{
	ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F::get_offset_of__closeBtn_7(),
	ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F::get_offset_of__contributeBtn_8(),
	ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F::get_offset_of__container_9(),
	ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F::get_offset_of__soundSystem_10(),
	ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F::get_offset_of__soundSO_11(),
	ContributePopupNode_t7DFE5B01F50CEB3582A725E489CE5C3251189C3F::get_offset_of__gameSettingsSO_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8527 = { sizeof (SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8527[13] = 
{
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__closeBtn_7(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__fxSoundBtn_8(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__musicSoundBtn_9(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__fxSoundOn_10(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__fxSoundOff_11(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__musicSoundOn_12(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__musicSoundOff_13(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__container_14(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__userVO_15(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__soundSystem_16(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__soundSO_17(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__fx_18(),
	SettingsPopupNode_tD66DF9E2BD66A8D5E5039F02445C385C79EED064::get_offset_of__music_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8528 = { sizeof (IconsUtils_tDB62243081F19841AF984250B871723B52E629A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8529 = { sizeof (GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8529[4] = 
{
	GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF::get_offset_of__gameContainer_4(),
	GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF::get_offset_of__userEvents_5(),
	GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF::get_offset_of__stopwatch_6(),
	GameInitializer_t87F1DF05038F161E7A997344511D38DEF08F6BBF::get_offset_of__globalSO_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8530 = { sizeof (UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8530[3] = 
{
	UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56::get_offset_of__gameContainer_0(),
	UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56::get_offset_of__inventorySystem_1(),
	UserInitializer_tB34747F20CCB084A0CB56CDD4D0F2E70A7904B56::get_offset_of__stopwatch_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8531 = { sizeof (CoinUIComponent_t6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8531[2] = 
{
	CoinUIComponent_t6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3::get_offset_of__coinValue_4(),
	CoinUIComponent_t6EB21D6C485538EB1B55AEAC7077CF2C3FBBEBE3::get_offset_of__inventorySystem_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8532 = { sizeof (HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8532[6] = 
{
	HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90::get_offset_of__heartValue_4(),
	HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90::get_offset_of__heartCountDown_5(),
	HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90::get_offset_of__inventorySystem_6(),
	HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90::get_offset_of__gameSettingsSO_7(),
	HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90::get_offset_of__inventoryVO_8(),
	HeartUIComponent_t1E53AFCB98B2D953F883A55C2FB74C6A6DE2FB90::get_offset_of__fullLocalized_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8533 = { sizeof (BasicCurrency_tFFB51F40C3056DE62E56D9C4FFB0714758FAD359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8534 = { sizeof (CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F), -1, sizeof(CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8534[2] = 
{
	CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F_StaticFields::get_offset_of_OnCoinChanged_0(),
	CoinCurrency_t70769E5BB795869C1DED24C416885D24B3CE856F::get_offset_of__inventoryVO_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8535 = { sizeof (CoinChangeEvent_t6C548FCB73453B99DB2C3676CE6D004563A80B82), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8536 = { sizeof (HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520), -1, sizeof(HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8536[3] = 
{
	HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520::get_offset_of__gameSettingsSO_0(),
	HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520::get_offset_of__inventoryVO_1(),
	HeartCurrency_t05BCFE0B1401C086F694BE623529CBCA8321D520_StaticFields::get_offset_of_OnHeartChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8537 = { sizeof (HeartChangeEvent_tA26E38B4515190BCC8CDA8D960DFFF4F480B4682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8538 = { sizeof (CurrencyModel_tDE590F10FA55F9C8B7ABA73472C2536A14F37550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8538[2] = 
{
	CurrencyModel_tDE590F10FA55F9C8B7ABA73472C2536A14F37550::get_offset_of_Type_0(),
	CurrencyModel_tDE590F10FA55F9C8B7ABA73472C2536A14F37550::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8539 = { sizeof (CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8539[4] = 
{
	CurrencyType_t22F6D5B9845CBB5FA241F40F84F92C0900D51C55::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8540 = { sizeof (SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8), -1, sizeof(SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8540[2] = 
{
	SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8::get_offset_of__inventoryVO_0(),
	SpellCurrency_t9223222B256A6443353853F6C36226D4751FF6F8_StaticFields::get_offset_of_OnSpellChange_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8541 = { sizeof (SpellChangeEvent_tDE9387410E957AFF62D95542A2D2AE619A3A6D9D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8542 = { sizeof (InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8542[2] = 
{
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2::get_offset_of__diContainer_3(),
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2::get_offset_of_Currencies_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8543 = { sizeof (KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8543[2] = 
{
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA::get_offset_of__uiMain_0(),
	KickerManager_t6B713EC4ED206DF81133170EB7545C76EEDEBDCA::get_offset_of__node_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8544 = { sizeof (KickerNode_tBA08241A855C464C10713FAEB8C827AF7B36877E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8544[1] = 
{
	KickerNode_tBA08241A855C464C10713FAEB8C827AF7B36877E::get_offset_of__text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8545 = { sizeof (KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8545[2] = 
{
	KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE::get_offset_of_Duration_0(),
	KickerNodeData_t9707F38C332456DFF9E4FBA0729EFC45866778AE::get_offset_of_LocalizationKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8546 = { sizeof (KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8546[6] = 
{
	KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA::get_offset_of__text_7(),
	KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA::get_offset_of__icon_8(),
	KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA::get_offset_of__itemsSO_9(),
	KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA::get_offset_of__contexts_10(),
	KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA::get_offset_of__container_11(),
	KickerWithIconNode_tC6AD175D504C7A645B06F8C020885470A1B37EEA::get_offset_of__gameSettingsSO_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8547 = { sizeof (KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8547[3] = 
{
	KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E::get_offset_of_UnitId_0(),
	KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E::get_offset_of_UnitPosition_1(),
	KickerWithIconData_t687808E0A968B2ACC743F8678E33BA19AB1EF54E::get_offset_of_UIDestination_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8548 = { sizeof (LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8548[20] = 
{
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_PlayersTab_8(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_PlayersTabActive_9(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_PlayersTabInactive_10(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_TeamsTab_11(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_TeamsTabActive_12(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_TeamsTabInactive_13(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_FriendsTab_14(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_FriendsTabActive_15(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of_FriendsTabInactive_16(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__gameSparks_17(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__userVO_18(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__leaderboardVO_19(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__userEvents_20(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__uiMain_21(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__voSaver_22(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__container_23(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__soundSystem_24(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__soundSO_25(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__lastTimeLeaderboardUpdated_26(),
	LeaderboardViewComponent_tFC4F3A2D563962E22E4EE544E8724F4625E77A60::get_offset_of__activeList_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8549 = { sizeof (LeaderboardDataParser_t87581A0FEF1C1A4B0A48942220FC7FA8B21602FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8550 = { sizeof (U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569), -1, sizeof(U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8550[2] = 
{
	U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1F36BE254EA38A92288ED01B3475412E3639B569_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8551 = { sizeof (LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8551[15] = 
{
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__bgHighlight_4(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__bgNormal_5(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__firstRank_6(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__secondRank_7(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__thirdRank_8(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__rankText_9(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__teamIcon_10(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__playerName_11(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__teamName_12(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__levelScore_13(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__trophyScore_14(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__viewTeamBtn_15(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__uiMain_16(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__userVO_17(),
	LeaderboardPlayerItem_t0DBC7C119FC10FC6140139F36D0DC8945A08114D::get_offset_of__entry_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8552 = { sizeof (LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8552[8] = 
{
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__firstRank_4(),
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__secondRank_5(),
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__thirdRank_6(),
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__rankText_7(),
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__teamIcon_8(),
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__teamName_9(),
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__levelScore_10(),
	LeaderboardTeamItem_t335432750597076BCEC059E5F0A20659CE2552A1::get_offset_of__stageScore_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8553 = { sizeof (FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8553[5] = 
{
	FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE::get_offset_of__list_7(),
	FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE::get_offset_of__loginBtn_8(),
	FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE::get_offset_of__gameSparks_9(),
	FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE::get_offset_of__leaderboardVO_10(),
	FriendLeaderboardNode_tE8BB1DA28030370D85299660C5827B999415C8AE::get_offset_of__voSaver_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8554 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable8554[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8555 = { sizeof (PlayerLeaderboardNode_t639686FA813812A7BBD784C6DEB03606988DD5BA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8556 = { sizeof (TeamLeaderboardNode_tA4967F25251D87815AEE613128701F01D6D5E068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8557 = { sizeof (ViewTeamTooltipNode_tA7F80A6EE2B92BB22816992440073A816176A769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8557[2] = 
{
	ViewTeamTooltipNode_tA7F80A6EE2B92BB22816992440073A816176A769::get_offset_of__btn_7(),
	ViewTeamTooltipNode_tA7F80A6EE2B92BB22816992440073A816176A769::get_offset_of__uiMain_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8558 = { sizeof (GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8558[3] = 
{
	GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F::get_offset_of__gameSparks_7(),
	GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F::get_offset_of__leaderboard_8(),
	GetLeaderboardTask_tB21F2867CBA59C00C45CBC34AEFC21AFDED6595F::get_offset_of__entries_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8559 = { sizeof (U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8559[3] = 
{
	U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__4_t8C151BD8EBC09519433B6B94953858DD97BCBF2D::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8560 = { sizeof (LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8560[5] = 
{
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6::get_offset_of_PlayersWorld_0(),
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6::get_offset_of_PlayersCountry_1(),
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6::get_offset_of_TeamsWorld_2(),
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6::get_offset_of_TeamsCountry_3(),
	LeaderboardsVO_tB97F14B95031799573A5FBA28C02AE297BDE86C6::get_offset_of_Friends_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8561 = { sizeof (LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8561[8] = 
{
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_Rank_0(),
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_PlayerId_1(),
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_PlayerName_2(),
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_TeamName_3(),
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_TeamIcon_4(),
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_TeamId_5(),
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_Trophy_6(),
	LeaderboardEntryVO_tC88EE4E32C6B13EFB59FFB703619EE1115EA9737::get_offset_of_Level_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8562 = { sizeof (LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8562[4] = 
{
	LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16::get_offset_of__joinBtn_4(),
	LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16::get_offset_of__joinText_5(),
	LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16::get_offset_of__gameSettingsSO_6(),
	LivesViewComponent_t06302A373C9D185C357A2BE407DFF82D1D7E3C16::get_offset_of__node_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8563 = { sizeof (MainLoadingNode_tAD03E2D98EBDEFDBB6C2BA4373A016C7A594F5D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8564 = { sizeof (LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8564[1] = 
{
	LoadCurrentChapterConfigTask_t9C541A313EC69D380D57BEB368B1DDE8943562A5::get_offset_of__container_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8565 = { sizeof (U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8565[5] = 
{
	U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D::get_offset_of_U3CchapterModelU3E5__2_3(),
	U3CTaskU3Ed__2_tFBEA4B80789C3F3A2DD3E970156E579693DACD9D::get_offset_of_U3CloadConfTaskU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8566 = { sizeof (LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8566[1] = 
{
	LoadMainConfigsTask_tEFA8D762329C2D5DD9A4310C02729F9239559BCF::get_offset_of__container_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8567 = { sizeof (U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8567[5] = 
{
	U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35::get_offset_of_U3CloadConfTaskU3E5__2_3(),
	U3CTaskU3Ed__2_t8ABC8A72DEF97DCA8E5C2A4BF280E3969E01DC35::get_offset_of_U3CloadCurrentChapterConfigTaskU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8568 = { sizeof (PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8568[1] = 
{
	PatchingLoadingTask_tAD0100057A1341A5BEE5F2F8C5976D322882DFDC::get_offset_of__container_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8569 = { sizeof (U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8569[3] = 
{
	U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_tAF7989670F96DAAF2C2062141D75550BE6EB2710::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8570 = { sizeof (SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8570[1] = 
{
	SoundLoaderTask_t86E568DBD1682DBED4340EF4BDE820F1DB7F3B94::get_offset_of__container_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8571 = { sizeof (U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8571[4] = 
{
	U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__2_t97406B9E700FC87C8C33A6ABB7569928F6574E63::get_offset_of_U3CloadSoundsTaskU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8572 = { sizeof (MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8572[22] = 
{
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__settingsBtn_7(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__teamView_8(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__teamBtn_9(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__leaderboardView_10(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__leaderboardBtn_11(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__homeView_12(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__homeBtn_13(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__lifesView_14(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__lifesBtn_15(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__shopView_16(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__shopBtn_17(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__levelChestComponent_18(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__starsChestComponent_19(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__homwViewComponent_20(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__container_21(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__uiMain_22(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__soundSystem_23(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__soundSO_24(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__globalSO_25(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__kickerManager_26(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__userVO_27(),
	MainUINode_tF5756C53BE30BB39DA5D6F6EE7E4452943D9C9E4::get_offset_of__currentView_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8573 = { sizeof (NotificationsSystem_t112A3E3163906777DF795E66F73BBA012B942BEC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8574 = { sizeof (NotificationsUtils_t5C24829892F954D60BC0B145F54F03700C46D50F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8575 = { sizeof (Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8575[8] = 
{
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__onQuestStarted_0(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__onQuestUpdated_1(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__onQuestEnded_2(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__startTriggers_3(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__endTriggers_4(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__isStarted_5(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__isUpdated_6(),
	Quest_tBD3A07EC45D4C0CF68F5C1EA3790B8208C7E7021::get_offset_of__isEnded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8576 = { sizeof (U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8576[2] = 
{
	U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9::get_offset_of_t_0(),
	U3CU3Ec__DisplayClass8_0_t273D4CBDC29F9DB87ACF1356FE964106CF9E21D9::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8577 = { sizeof (U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8577[2] = 
{
	U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328::get_offset_of_t_0(),
	U3CU3Ec__DisplayClass11_0_t54616E3EFBF3ACD267B52391C2028B56E60D9328::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8578 = { sizeof (U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8578[3] = 
{
	U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802::get_offset_of_t_1(),
	U3CU3Ec__DisplayClass12_0_t7EED697364D448A257B04EB82E68193380EE4802::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8579 = { sizeof (OnQuestUpdatedEvent_t8B85F5C13C8C6D94E08B49B1C3AFA8A6118F8CAB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8580 = { sizeof (OnQuestStartedEvent_t8E404B059FB2011AF0DC8299EBBF6A2DB9657948), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8581 = { sizeof (OnQuestEndedEvent_tC0EC99C69DD18EF07F2A9877323E1CE9E7825D6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8582 = { sizeof (QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8582[2] = 
{
	QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1::get_offset_of__questsVO_0(),
	QuestSystem_tDFB5A64D6154EACAD855C582FF3E40C7A14C83D1::get_offset_of__quests_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8583 = { sizeof (SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8583[2] = 
{
	SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D::get_offset_of__userVO_3(),
	SoundSystem_tAD8B0EBAA6C3E35776FEC9AC64E85EE19E81694D::get_offset_of__soundSystem_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8584 = { sizeof (SoundUtils_t6727D4BB1D79FF716E16DB7DF6045389EF1978ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8585 = { sizeof (TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8585[23] = 
{
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__notConnected_8(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__joinTabs_9(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__joinedTabs_10(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__teamsTabBtn_11(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__teamsTabActive_12(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__teamsTabInactive_13(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__createTabBtn_14(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__createTabActive_15(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__createTabInactive_16(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__chatTabBtn_17(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__chatTabActive_18(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__chatTabInactive_19(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__myteamTabBtn_20(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__myteamTabActive_21(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__myteamTabInactive_22(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__searchTabBtn_23(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__searchTabActive_24(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__searchTabInactive_25(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__requirementText_26(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__uiMain_27(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__userEvents_28(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__userVO_29(),
	TeamViewComponent_tC26F5EF388A1F94F841B0FFD54B1C6204453020D::get_offset_of__gameSettingsSO_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8586 = { sizeof (ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8586[9] = 
{
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__name_4(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__helpBtn_5(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__lifeReceived_6(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__userVO_7(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__userEvents_8(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__gameSparks_9(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__item_10(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__lifeReceivedCount_11(),
	ChatHeartListItem_tBDBC364CD7AE74FAC5A8B29FA33290CBEFF79D29::get_offset_of__index_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8587 = { sizeof (ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8587[7] = 
{
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__name_4(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__yesBtn_5(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__noBtn_6(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__gameSparks_7(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__kickerManager_8(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__data_9(),
	ChatJoinRequestItem_t8DA07962248D92C46A048DD7AAA046D52226CDAC::get_offset_of__index_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8588 = { sizeof (ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8588[6] = 
{
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__btn_4(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__name_5(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__message_6(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__time_7(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__uiMain_8(),
	ChatReceiveMsgListItem_tD9C9B67291E9A1C2E30707832237FA6CC0082670::get_offset_of__item_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8589 = { sizeof (ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8589[3] = 
{
	ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884::get_offset_of__name_4(),
	ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884::get_offset_of__message_5(),
	ChatSendMsgListItem_tC821821D0C7D3E952A9848B38025C10F436A2884::get_offset_of__time_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8590 = { sizeof (IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8590[5] = 
{
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__icon_4(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__selectIcon_5(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__iconLibrary_6(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__userEvents_7(),
	IconsListItem_tFCE5D92F5E1D082DB67513DB5FE32E4D863A40A7::get_offset_of__index_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8591 = { sizeof (JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8591[11] = 
{
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__teamName_4(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__teamIcon_5(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__membersCount_6(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__joinBtn_7(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__joinBtnText_8(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__uiMain_9(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__iconLibrary_10(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__gameSpark_11(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__userEvents_12(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__userVO_13(),
	JoinTeamListItem_t44B45CC0825AB01016E454D9EC234EF33B392A6B::get_offset_of__item_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8592 = { sizeof (MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8592[15] = 
{
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__bgHighlight_4(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__bgNormal_5(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rankText_6(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rank1_7(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rank2_8(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__rank3_9(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__normaleName_10(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__leaderParent_11(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__leaderName_12(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__help_13(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__level_14(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__kick_15(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__userVO_16(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__uiMain_17(),
	MyTeamListItem_tB264E6A70F6FB1A5968128CBF86FF0987023FFBC::get_offset_of__data_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8593 = { sizeof (BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8593[2] = 
{
	BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A::get_offset_of__list_7(),
	BadgesPopupNode_tB7B0E6B874317064FEC0DBDBFF0B63E1CBA25D9A::get_offset_of__globalSO_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8594 = { sizeof (ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8594[8] = 
{
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__chatList_7(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__chatBtn_8(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__heartBtn_9(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__gameSparks_10(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__uiMain_11(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__userVo_12(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__userEvents_13(),
	ChatNode_t7A741E0D933BAFC733CCBF17E9D553DA370552A0::get_offset_of__keyboard_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8595 = { sizeof (ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8595[7] = 
{
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_MessageId_1(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_SenderId_2(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_SenderDisplayName_3(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_Message_4(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_CustomMessageData_5(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_EpochTime_6(),
	ChatListItemData_tB039C181C297B802B7B35327AA170A2106CDD53B::get_offset_of_List_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8596 = { sizeof (CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8596[2] = 
{
	CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD::get_offset_of_Action_0(),
	CustomMessageData_t2BA087DE2A16E4C6D102083A45431AA2E422A6DD::get_offset_of_TargetId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8597 = { sizeof (CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8597[22] = 
{
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__nameField_7(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__browseBtn_8(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__teamIcon_9(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__descriptionField_10(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__typeLeftBtn_11(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__typeRightBtn_12(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__type_13(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__requiredLeftBtn_14(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__requiredRightBtn_15(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__required_16(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__createBtn_17(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__iconLibrary_18(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__uiMain_19(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__gameSpark_20(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__userEvents_21(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__userVo_22(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__kickerManager_23(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__voSaver_24(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__currentType_25(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__currentRequirement_26(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__currentIcon_27(),
	CreateTeamNode_t83E09D1D20B2ADF00639F2549FD9E6935F405689::get_offset_of__badgesNode_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8598 = { sizeof (TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8598[3] = 
{
	TEAM_TYPE_t3C178DBCDEB1B9A42045B6C497BEC33A8557E170::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8599 = { sizeof (EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8599[18] = 
{
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__name_7(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__teamIcon_8(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__descriptionField_9(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__typeLeftBtn_10(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__typeRightBtn_11(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__type_12(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__requiredLeftBtn_13(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__requiredRightBtn_14(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__required_15(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__saveBtn_16(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__iconLibrary_17(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__uiMain_18(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__gameSpark_19(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__userEvents_20(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__userVo_21(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__kickerManager_22(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__currentRequirement_23(),
	EditTeamPopupNode_tCC67D85F6C59BDB715909ED1A0E53D9A660491D5::get_offset_of__currentType_24(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
