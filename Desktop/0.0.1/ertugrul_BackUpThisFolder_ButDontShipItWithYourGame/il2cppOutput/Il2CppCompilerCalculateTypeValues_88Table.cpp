﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.Example.GraphRequest
struct GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_tFFB5515FC97391D04D5034ED7F334357FED1FAE6;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CTAKESCREENSHOTU3ED__4_T32FCC3EB0A36F0973EE1E62E3E835E9133D1C105_H
#define U3CTAKESCREENSHOTU3ED__4_T32FCC3EB0A36F0973EE1E62E3E835E9133D1C105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4
struct  U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.Example.GraphRequest Facebook.Unity.Example.GraphRequest_<TakeScreenshot>d__4::<>4__this
	GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105, ___U3CU3E4__this_2)); }
	inline GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTAKESCREENSHOTU3ED__4_T32FCC3EB0A36F0973EE1E62E3E835E9133D1C105_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#define NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TE4EDC8D5ED2772A911F67696644E6C77FA716DC0_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#define SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CONSOLEBASE_T80F55AC34D95221101149083BAB6EBF9571E72B4_H
#define CONSOLEBASE_T80F55AC34D95221101149083BAB6EBF9571E72B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.ConsoleBase
struct  ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Facebook.Unity.Example.ConsoleBase::status
	String_t* ___status_6;
	// System.String Facebook.Unity.Example.ConsoleBase::lastResponse
	String_t* ___lastResponse_7;
	// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::scrollPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scrollPosition_8;
	// System.Nullable`1<System.Single> Facebook.Unity.Example.ConsoleBase::scaleFactor
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___scaleFactor_9;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___textStyle_10;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::buttonStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___buttonStyle_11;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textInputStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___textInputStyle_12;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::labelStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___labelStyle_13;
	// UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::<LastResponseTexture>k__BackingField
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CLastResponseTextureU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_status_6() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___status_6)); }
	inline String_t* get_status_6() const { return ___status_6; }
	inline String_t** get_address_of_status_6() { return &___status_6; }
	inline void set_status_6(String_t* value)
	{
		___status_6 = value;
		Il2CppCodeGenWriteBarrier((&___status_6), value);
	}

	inline static int32_t get_offset_of_lastResponse_7() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___lastResponse_7)); }
	inline String_t* get_lastResponse_7() const { return ___lastResponse_7; }
	inline String_t** get_address_of_lastResponse_7() { return &___lastResponse_7; }
	inline void set_lastResponse_7(String_t* value)
	{
		___lastResponse_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastResponse_7), value);
	}

	inline static int32_t get_offset_of_scrollPosition_8() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___scrollPosition_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scrollPosition_8() const { return ___scrollPosition_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scrollPosition_8() { return &___scrollPosition_8; }
	inline void set_scrollPosition_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scrollPosition_8 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_9() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___scaleFactor_9)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_scaleFactor_9() const { return ___scaleFactor_9; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_scaleFactor_9() { return &___scaleFactor_9; }
	inline void set_scaleFactor_9(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___scaleFactor_9 = value;
	}

	inline static int32_t get_offset_of_textStyle_10() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___textStyle_10)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_textStyle_10() const { return ___textStyle_10; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_textStyle_10() { return &___textStyle_10; }
	inline void set_textStyle_10(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___textStyle_10 = value;
		Il2CppCodeGenWriteBarrier((&___textStyle_10), value);
	}

	inline static int32_t get_offset_of_buttonStyle_11() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___buttonStyle_11)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_buttonStyle_11() const { return ___buttonStyle_11; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_buttonStyle_11() { return &___buttonStyle_11; }
	inline void set_buttonStyle_11(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___buttonStyle_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStyle_11), value);
	}

	inline static int32_t get_offset_of_textInputStyle_12() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___textInputStyle_12)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_textInputStyle_12() const { return ___textInputStyle_12; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_textInputStyle_12() { return &___textInputStyle_12; }
	inline void set_textInputStyle_12(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___textInputStyle_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInputStyle_12), value);
	}

	inline static int32_t get_offset_of_labelStyle_13() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___labelStyle_13)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_labelStyle_13() const { return ___labelStyle_13; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_labelStyle_13() { return &___labelStyle_13; }
	inline void set_labelStyle_13(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___labelStyle_13 = value;
		Il2CppCodeGenWriteBarrier((&___labelStyle_13), value);
	}

	inline static int32_t get_offset_of_U3CLastResponseTextureU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4, ___U3CLastResponseTextureU3Ek__BackingField_14)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CLastResponseTextureU3Ek__BackingField_14() const { return ___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CLastResponseTextureU3Ek__BackingField_14() { return &___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline void set_U3CLastResponseTextureU3Ek__BackingField_14(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CLastResponseTextureU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResponseTextureU3Ek__BackingField_14), value);
	}
};

struct ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::menuStack
	Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * ___menuStack_5;

public:
	inline static int32_t get_offset_of_menuStack_5() { return static_cast<int32_t>(offsetof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields, ___menuStack_5)); }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * get_menuStack_5() const { return ___menuStack_5; }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 ** get_address_of_menuStack_5() { return &___menuStack_5; }
	inline void set_menuStack_5(Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * value)
	{
		___menuStack_5 = value;
		Il2CppCodeGenWriteBarrier((&___menuStack_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEBASE_T80F55AC34D95221101149083BAB6EBF9571E72B4_H
#ifndef LOGVIEW_T81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_H
#define LOGVIEW_T81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.LogView
struct  LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A  : public ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4
{
public:

public:
};

struct LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields
{
public:
	// System.String Facebook.Unity.Example.LogView::datePatt
	String_t* ___datePatt_15;
	// System.Collections.Generic.IList`1<System.String> Facebook.Unity.Example.LogView::events
	RuntimeObject* ___events_16;

public:
	inline static int32_t get_offset_of_datePatt_15() { return static_cast<int32_t>(offsetof(LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields, ___datePatt_15)); }
	inline String_t* get_datePatt_15() const { return ___datePatt_15; }
	inline String_t** get_address_of_datePatt_15() { return &___datePatt_15; }
	inline void set_datePatt_15(String_t* value)
	{
		___datePatt_15 = value;
		Il2CppCodeGenWriteBarrier((&___datePatt_15), value);
	}

	inline static int32_t get_offset_of_events_16() { return static_cast<int32_t>(offsetof(LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields, ___events_16)); }
	inline RuntimeObject* get_events_16() const { return ___events_16; }
	inline RuntimeObject** get_address_of_events_16() { return &___events_16; }
	inline void set_events_16(RuntimeObject* value)
	{
		___events_16 = value;
		Il2CppCodeGenWriteBarrier((&___events_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGVIEW_T81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_H
#ifndef MENUBASE_TA4B32275F482BF1B58E859A78BF074D13FF3E93A_H
#define MENUBASE_TA4B32275F482BF1B58E859A78BF074D13FF3E93A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MenuBase
struct  MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A  : public ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4
{
public:

public:
};

struct MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Example.MenuBase::shareDialogMode
	int32_t ___shareDialogMode_15;

public:
	inline static int32_t get_offset_of_shareDialogMode_15() { return static_cast<int32_t>(offsetof(MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields, ___shareDialogMode_15)); }
	inline int32_t get_shareDialogMode_15() const { return ___shareDialogMode_15; }
	inline int32_t* get_address_of_shareDialogMode_15() { return &___shareDialogMode_15; }
	inline void set_shareDialogMode_15(int32_t value)
	{
		___shareDialogMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUBASE_TA4B32275F482BF1B58E859A78BF074D13FF3E93A_H
#ifndef ACCESSTOKENMENU_TA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D_H
#define ACCESSTOKENMENU_TA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AccessTokenMenu
struct  AccessTokenMenu_tA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENMENU_TA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D_H
#ifndef APPEVENTS_T20C76E739C9ABC6392F335DCF9902C17C87D6B70_H
#define APPEVENTS_T20C76E739C9ABC6392F335DCF9902C17C87D6B70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppEvents
struct  AppEvents_t20C76E739C9ABC6392F335DCF9902C17C87D6B70  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEVENTS_T20C76E739C9ABC6392F335DCF9902C17C87D6B70_H
#ifndef APPINVITES_T20590921C9CC4B49A844AB27042A146CA6903B0C_H
#define APPINVITES_T20590921C9CC4B49A844AB27042A146CA6903B0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppInvites
struct  AppInvites_t20590921C9CC4B49A844AB27042A146CA6903B0C  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINVITES_T20590921C9CC4B49A844AB27042A146CA6903B0C_H
#ifndef APPLINKS_T0D8BC945EC82E808B1CD4748D540A69D2970913B_H
#define APPLINKS_T0D8BC945EC82E808B1CD4748D540A69D2970913B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppLinks
struct  AppLinks_t0D8BC945EC82E808B1CD4748D540A69D2970913B  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKS_T0D8BC945EC82E808B1CD4748D540A69D2970913B_H
#ifndef APPREQUESTS_T22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3_H
#define APPREQUESTS_T22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppRequests
struct  AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.AppRequests::requestMessage
	String_t* ___requestMessage_16;
	// System.String Facebook.Unity.Example.AppRequests::requestTo
	String_t* ___requestTo_17;
	// System.String Facebook.Unity.Example.AppRequests::requestFilter
	String_t* ___requestFilter_18;
	// System.String Facebook.Unity.Example.AppRequests::requestExcludes
	String_t* ___requestExcludes_19;
	// System.String Facebook.Unity.Example.AppRequests::requestMax
	String_t* ___requestMax_20;
	// System.String Facebook.Unity.Example.AppRequests::requestData
	String_t* ___requestData_21;
	// System.String Facebook.Unity.Example.AppRequests::requestTitle
	String_t* ___requestTitle_22;
	// System.String Facebook.Unity.Example.AppRequests::requestObjectID
	String_t* ___requestObjectID_23;
	// System.Int32 Facebook.Unity.Example.AppRequests::selectedAction
	int32_t ___selectedAction_24;
	// System.String[] Facebook.Unity.Example.AppRequests::actionTypeStrings
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___actionTypeStrings_25;

public:
	inline static int32_t get_offset_of_requestMessage_16() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestMessage_16)); }
	inline String_t* get_requestMessage_16() const { return ___requestMessage_16; }
	inline String_t** get_address_of_requestMessage_16() { return &___requestMessage_16; }
	inline void set_requestMessage_16(String_t* value)
	{
		___requestMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___requestMessage_16), value);
	}

	inline static int32_t get_offset_of_requestTo_17() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestTo_17)); }
	inline String_t* get_requestTo_17() const { return ___requestTo_17; }
	inline String_t** get_address_of_requestTo_17() { return &___requestTo_17; }
	inline void set_requestTo_17(String_t* value)
	{
		___requestTo_17 = value;
		Il2CppCodeGenWriteBarrier((&___requestTo_17), value);
	}

	inline static int32_t get_offset_of_requestFilter_18() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestFilter_18)); }
	inline String_t* get_requestFilter_18() const { return ___requestFilter_18; }
	inline String_t** get_address_of_requestFilter_18() { return &___requestFilter_18; }
	inline void set_requestFilter_18(String_t* value)
	{
		___requestFilter_18 = value;
		Il2CppCodeGenWriteBarrier((&___requestFilter_18), value);
	}

	inline static int32_t get_offset_of_requestExcludes_19() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestExcludes_19)); }
	inline String_t* get_requestExcludes_19() const { return ___requestExcludes_19; }
	inline String_t** get_address_of_requestExcludes_19() { return &___requestExcludes_19; }
	inline void set_requestExcludes_19(String_t* value)
	{
		___requestExcludes_19 = value;
		Il2CppCodeGenWriteBarrier((&___requestExcludes_19), value);
	}

	inline static int32_t get_offset_of_requestMax_20() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestMax_20)); }
	inline String_t* get_requestMax_20() const { return ___requestMax_20; }
	inline String_t** get_address_of_requestMax_20() { return &___requestMax_20; }
	inline void set_requestMax_20(String_t* value)
	{
		___requestMax_20 = value;
		Il2CppCodeGenWriteBarrier((&___requestMax_20), value);
	}

	inline static int32_t get_offset_of_requestData_21() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestData_21)); }
	inline String_t* get_requestData_21() const { return ___requestData_21; }
	inline String_t** get_address_of_requestData_21() { return &___requestData_21; }
	inline void set_requestData_21(String_t* value)
	{
		___requestData_21 = value;
		Il2CppCodeGenWriteBarrier((&___requestData_21), value);
	}

	inline static int32_t get_offset_of_requestTitle_22() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestTitle_22)); }
	inline String_t* get_requestTitle_22() const { return ___requestTitle_22; }
	inline String_t** get_address_of_requestTitle_22() { return &___requestTitle_22; }
	inline void set_requestTitle_22(String_t* value)
	{
		___requestTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___requestTitle_22), value);
	}

	inline static int32_t get_offset_of_requestObjectID_23() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___requestObjectID_23)); }
	inline String_t* get_requestObjectID_23() const { return ___requestObjectID_23; }
	inline String_t** get_address_of_requestObjectID_23() { return &___requestObjectID_23; }
	inline void set_requestObjectID_23(String_t* value)
	{
		___requestObjectID_23 = value;
		Il2CppCodeGenWriteBarrier((&___requestObjectID_23), value);
	}

	inline static int32_t get_offset_of_selectedAction_24() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___selectedAction_24)); }
	inline int32_t get_selectedAction_24() const { return ___selectedAction_24; }
	inline int32_t* get_address_of_selectedAction_24() { return &___selectedAction_24; }
	inline void set_selectedAction_24(int32_t value)
	{
		___selectedAction_24 = value;
	}

	inline static int32_t get_offset_of_actionTypeStrings_25() { return static_cast<int32_t>(offsetof(AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3, ___actionTypeStrings_25)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_actionTypeStrings_25() const { return ___actionTypeStrings_25; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_actionTypeStrings_25() { return &___actionTypeStrings_25; }
	inline void set_actionTypeStrings_25(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___actionTypeStrings_25 = value;
		Il2CppCodeGenWriteBarrier((&___actionTypeStrings_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTS_T22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3_H
#ifndef DIALOGSHARE_T055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0_H
#define DIALOGSHARE_T055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.DialogShare
struct  DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.DialogShare::shareLink
	String_t* ___shareLink_16;
	// System.String Facebook.Unity.Example.DialogShare::shareTitle
	String_t* ___shareTitle_17;
	// System.String Facebook.Unity.Example.DialogShare::shareDescription
	String_t* ___shareDescription_18;
	// System.String Facebook.Unity.Example.DialogShare::shareImage
	String_t* ___shareImage_19;
	// System.String Facebook.Unity.Example.DialogShare::feedTo
	String_t* ___feedTo_20;
	// System.String Facebook.Unity.Example.DialogShare::feedLink
	String_t* ___feedLink_21;
	// System.String Facebook.Unity.Example.DialogShare::feedTitle
	String_t* ___feedTitle_22;
	// System.String Facebook.Unity.Example.DialogShare::feedCaption
	String_t* ___feedCaption_23;
	// System.String Facebook.Unity.Example.DialogShare::feedDescription
	String_t* ___feedDescription_24;
	// System.String Facebook.Unity.Example.DialogShare::feedImage
	String_t* ___feedImage_25;
	// System.String Facebook.Unity.Example.DialogShare::feedMediaSource
	String_t* ___feedMediaSource_26;

public:
	inline static int32_t get_offset_of_shareLink_16() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareLink_16)); }
	inline String_t* get_shareLink_16() const { return ___shareLink_16; }
	inline String_t** get_address_of_shareLink_16() { return &___shareLink_16; }
	inline void set_shareLink_16(String_t* value)
	{
		___shareLink_16 = value;
		Il2CppCodeGenWriteBarrier((&___shareLink_16), value);
	}

	inline static int32_t get_offset_of_shareTitle_17() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareTitle_17)); }
	inline String_t* get_shareTitle_17() const { return ___shareTitle_17; }
	inline String_t** get_address_of_shareTitle_17() { return &___shareTitle_17; }
	inline void set_shareTitle_17(String_t* value)
	{
		___shareTitle_17 = value;
		Il2CppCodeGenWriteBarrier((&___shareTitle_17), value);
	}

	inline static int32_t get_offset_of_shareDescription_18() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareDescription_18)); }
	inline String_t* get_shareDescription_18() const { return ___shareDescription_18; }
	inline String_t** get_address_of_shareDescription_18() { return &___shareDescription_18; }
	inline void set_shareDescription_18(String_t* value)
	{
		___shareDescription_18 = value;
		Il2CppCodeGenWriteBarrier((&___shareDescription_18), value);
	}

	inline static int32_t get_offset_of_shareImage_19() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___shareImage_19)); }
	inline String_t* get_shareImage_19() const { return ___shareImage_19; }
	inline String_t** get_address_of_shareImage_19() { return &___shareImage_19; }
	inline void set_shareImage_19(String_t* value)
	{
		___shareImage_19 = value;
		Il2CppCodeGenWriteBarrier((&___shareImage_19), value);
	}

	inline static int32_t get_offset_of_feedTo_20() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedTo_20)); }
	inline String_t* get_feedTo_20() const { return ___feedTo_20; }
	inline String_t** get_address_of_feedTo_20() { return &___feedTo_20; }
	inline void set_feedTo_20(String_t* value)
	{
		___feedTo_20 = value;
		Il2CppCodeGenWriteBarrier((&___feedTo_20), value);
	}

	inline static int32_t get_offset_of_feedLink_21() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedLink_21)); }
	inline String_t* get_feedLink_21() const { return ___feedLink_21; }
	inline String_t** get_address_of_feedLink_21() { return &___feedLink_21; }
	inline void set_feedLink_21(String_t* value)
	{
		___feedLink_21 = value;
		Il2CppCodeGenWriteBarrier((&___feedLink_21), value);
	}

	inline static int32_t get_offset_of_feedTitle_22() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedTitle_22)); }
	inline String_t* get_feedTitle_22() const { return ___feedTitle_22; }
	inline String_t** get_address_of_feedTitle_22() { return &___feedTitle_22; }
	inline void set_feedTitle_22(String_t* value)
	{
		___feedTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___feedTitle_22), value);
	}

	inline static int32_t get_offset_of_feedCaption_23() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedCaption_23)); }
	inline String_t* get_feedCaption_23() const { return ___feedCaption_23; }
	inline String_t** get_address_of_feedCaption_23() { return &___feedCaption_23; }
	inline void set_feedCaption_23(String_t* value)
	{
		___feedCaption_23 = value;
		Il2CppCodeGenWriteBarrier((&___feedCaption_23), value);
	}

	inline static int32_t get_offset_of_feedDescription_24() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedDescription_24)); }
	inline String_t* get_feedDescription_24() const { return ___feedDescription_24; }
	inline String_t** get_address_of_feedDescription_24() { return &___feedDescription_24; }
	inline void set_feedDescription_24(String_t* value)
	{
		___feedDescription_24 = value;
		Il2CppCodeGenWriteBarrier((&___feedDescription_24), value);
	}

	inline static int32_t get_offset_of_feedImage_25() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedImage_25)); }
	inline String_t* get_feedImage_25() const { return ___feedImage_25; }
	inline String_t** get_address_of_feedImage_25() { return &___feedImage_25; }
	inline void set_feedImage_25(String_t* value)
	{
		___feedImage_25 = value;
		Il2CppCodeGenWriteBarrier((&___feedImage_25), value);
	}

	inline static int32_t get_offset_of_feedMediaSource_26() { return static_cast<int32_t>(offsetof(DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0, ___feedMediaSource_26)); }
	inline String_t* get_feedMediaSource_26() const { return ___feedMediaSource_26; }
	inline String_t** get_address_of_feedMediaSource_26() { return &___feedMediaSource_26; }
	inline void set_feedMediaSource_26(String_t* value)
	{
		___feedMediaSource_26 = value;
		Il2CppCodeGenWriteBarrier((&___feedMediaSource_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGSHARE_T055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0_H
#ifndef GRAPHREQUEST_T81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347_H
#define GRAPHREQUEST_T81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest
struct  GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.GraphRequest::apiQuery
	String_t* ___apiQuery_16;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest::profilePic
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___profilePic_17;

public:
	inline static int32_t get_offset_of_apiQuery_16() { return static_cast<int32_t>(offsetof(GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347, ___apiQuery_16)); }
	inline String_t* get_apiQuery_16() const { return ___apiQuery_16; }
	inline String_t** get_address_of_apiQuery_16() { return &___apiQuery_16; }
	inline void set_apiQuery_16(String_t* value)
	{
		___apiQuery_16 = value;
		Il2CppCodeGenWriteBarrier((&___apiQuery_16), value);
	}

	inline static int32_t get_offset_of_profilePic_17() { return static_cast<int32_t>(offsetof(GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347, ___profilePic_17)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_profilePic_17() const { return ___profilePic_17; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_profilePic_17() { return &___profilePic_17; }
	inline void set_profilePic_17(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___profilePic_17 = value;
		Il2CppCodeGenWriteBarrier((&___profilePic_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHREQUEST_T81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347_H
#ifndef MAINMENU_T7BBE78D87657176F7FD619692A2AA99B7D954B9F_H
#define MAINMENU_T7BBE78D87657176F7FD619692A2AA99B7D954B9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MainMenu
struct  MainMenu_t7BBE78D87657176F7FD619692A2AA99B7D954B9F  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T7BBE78D87657176F7FD619692A2AA99B7D954B9F_H
#ifndef PAY_T8F37A17103EAE0C62C542549D25A1469DD9574FC_H
#define PAY_T8F37A17103EAE0C62C542549D25A1469DD9574FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.Pay
struct  Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC  : public MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A
{
public:
	// System.String Facebook.Unity.Example.Pay::payProduct
	String_t* ___payProduct_16;

public:
	inline static int32_t get_offset_of_payProduct_16() { return static_cast<int32_t>(offsetof(Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC, ___payProduct_16)); }
	inline String_t* get_payProduct_16() const { return ___payProduct_16; }
	inline String_t** get_address_of_payProduct_16() { return &___payProduct_16; }
	inline void set_payProduct_16(String_t* value)
	{
		___payProduct_16 = value;
		Il2CppCodeGenWriteBarrier((&___payProduct_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAY_T8F37A17103EAE0C62C542549D25A1469DD9574FC_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8800 = { sizeof (ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4), -1, sizeof(ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8800[11] = 
{
	0,
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4_StaticFields::get_offset_of_menuStack_5(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_status_6(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_lastResponse_7(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_scrollPosition_8(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_scaleFactor_9(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_textStyle_10(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_buttonStyle_11(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_textInputStyle_12(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_labelStyle_13(),
	ConsoleBase_t80F55AC34D95221101149083BAB6EBF9571E72B4::get_offset_of_U3CLastResponseTextureU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8801 = { sizeof (LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A), -1, sizeof(LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8801[2] = 
{
	LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields::get_offset_of_datePatt_15(),
	LogView_t81FB093AA3DB55ED7D8B97F6BAD8C6BCEFC8DF7A_StaticFields::get_offset_of_events_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8802 = { sizeof (MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A), -1, sizeof(MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8802[1] = 
{
	MenuBase_tA4B32275F482BF1B58E859A78BF074D13FF3E93A_StaticFields::get_offset_of_shareDialogMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8803 = { sizeof (AccessTokenMenu_tA169D1DDBAD90D514BE1C0BA14FA7FAC3AA5542D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8804 = { sizeof (AppEvents_t20C76E739C9ABC6392F335DCF9902C17C87D6B70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8805 = { sizeof (AppInvites_t20590921C9CC4B49A844AB27042A146CA6903B0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8806 = { sizeof (AppLinks_t0D8BC945EC82E808B1CD4748D540A69D2970913B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8807 = { sizeof (AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8807[10] = 
{
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestMessage_16(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestTo_17(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestFilter_18(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestExcludes_19(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestMax_20(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestData_21(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestTitle_22(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_requestObjectID_23(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_selectedAction_24(),
	AppRequests_t22F4C53E63A0E4C23359B3D16D9CDF957A60EEA3::get_offset_of_actionTypeStrings_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8808 = { sizeof (DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8808[11] = 
{
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareLink_16(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareTitle_17(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareDescription_18(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_shareImage_19(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedTo_20(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedLink_21(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedTitle_22(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedCaption_23(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedDescription_24(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedImage_25(),
	DialogShare_t055DE5CF84315A9A01E6ABFEE8F136DFB778EBC0::get_offset_of_feedMediaSource_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8809 = { sizeof (GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8809[2] = 
{
	GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347::get_offset_of_apiQuery_16(),
	GraphRequest_t81F9EA1312B20AB3FDA2B9BE84C03A026BBDC347::get_offset_of_profilePic_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8810 = { sizeof (U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8810[3] = 
{
	U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105::get_offset_of_U3CU3E1__state_0(),
	U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105::get_offset_of_U3CU3E2__current_1(),
	U3CTakeScreenshotU3Ed__4_t32FCC3EB0A36F0973EE1E62E3E835E9133D1C105::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8811 = { sizeof (MainMenu_t7BBE78D87657176F7FD619692A2AA99B7D954B9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8812 = { sizeof (Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8812[1] = 
{
	Pay_t8F37A17103EAE0C62C542549D25A1469DD9574FC::get_offset_of_payProduct_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
